<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CardsExport;
use App\Imports\CardsImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Card;
use App\Models\MainService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }
        
        $cards              = Card::getAll();
        $data['cards']      = $cards;
        $data['total']      = Card::count();
        $data['allocated']  = Card::where('status','=','ALLOCATED')->count();
        $data['active']     = Card::where('status','=','ACTIVE')->count();
        $data['disabled']   = Card::where('status','=','DISABLED')->count();

        $serviceDetails     = MainService::where('service_category','NFC_CARD')->first();

        $data['tax_updation_renew_card_status'] =  $serviceDetails->tax_id != null ? "active" : "not_active"; 
        $data['card_main_service_id'] =  $serviceDetails->id;

        return view('cards.import_export_card')->with($data);
    }

    public function export() 
    {
        return Excel::download(new CardsExport, 'cards.xlsx');
    }
     
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import() 
    {

        $array = Excel::toArray(new CardsImport, request()->file('file'));

        $data     = array();
        $uploaded = 0;
        $total    = 0;

        foreach($array[0] as $key => $arr){

            if(!isset($arr['card_number'])){
                return redirect('/import_export_cards')->with('error', 'File format is wrong');
            }

            $total += 1;

            $dbstatus = Card::where('card_number','=', $arr['card_number'])->first();

            if($dbstatus == null){

                $uploaded += 1; 

                try {
                            
                    $input = [
                        'card_number' => $arr['card_number'],
                        'card_secret_number' => $arr['card_secret_number'],
                        'status' => $arr['status'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
    
                    $card = Card::create($input);
    
                } catch (Exception $ex) {
                    return redirect('/import_export_cards')->with('error', 'Error in Card creation');
                }
            }
        }
        if($uploaded == 0){
            request()->session()->flash('error','No cards uploded, May be all exists.');
        }else{
            request()->session()->flash('success','Cards of ('.$uploaded.'/'.$total.') are imported successfully');
        }
        
        return back();
    }
    public function download() {
        $filename = 'sample_card.xlsx';
        // Get path from storage directory
        $path = storage_path('app/' . $filename);
    
        // Download file with custom headers
        return response()->download($path, $filename, [
            'Content-Type' => 'application/vnd.ms-excel',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }

    public function previewUploadingExcel(Request $request){

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xlsx|max:2048',
        ]);

        if ($validator->fails()) {

            $data = ['errors' => "The file must be a file of type: xlsx."];
            return response()->json($data);
            
        }

        $array = Excel::toArray(new CardsImport, request()->file('file'));

        $data = array();

        foreach($array[0] as $key=>$arr){

            if(!isset($arr['card_number'])){
                $data = ['errors' => "The file format error"];
                return response()->json($data);
            }


            $t = array();
            $t['sno'] = $key+1;
            $t['card_number'] = $arr['card_number'];
            $t['card_secret_number'] = $arr['card_secret_number'];
            $t['status'] = $arr['status'];

            $dbstatus = Card::where('card_number','=', $arr['card_number'])->first();

            if($dbstatus == null){
                $t['dbstatus'] = "NOT_EXISTS";
            }else{
                $t['dbstatus'] = "EXISTS";
            }

            $data[]=$t;
        }

        return response()->json($data);


    }

    public function deactivate(Request $request){
        $res = Card::find($request->id);
        $user = Auth::user();
        
        $res->status        = "DISABLED";
        $res->updated_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Card has been blocked successfully');
        return redirect('/import_export_cards');
    }

    public function activate(Request $request){
        $res = Card::find($request->id);
        $user = Auth::user();
        
        $res->status        = "ACTIVE";
        $res->updated_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Card has been unblocked successfully');
        return redirect('/import_export_cards');
    }
}
