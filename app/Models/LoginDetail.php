<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoginDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id', 'login_time','logout_time','ip_address','browser','created_at','updated_at'
    ];

}
