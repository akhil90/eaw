<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\JobCard;
use App\Models\JobCardDetail;
use App\Models\User;
use App\Models\Customer;
use App\Models\PaymentDetail;
use App\Models\PackagePurchase;
use App\Models\MainService;
use App\Models\CardBuyDetail;
use App\Models\Settings;
use DB;
use PDF;
use Mail;
use Session;

class InvoiceContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id, $type){

        $data          = array();
        $invoice_array = array();
        $cardAmount    = 0.00;
        $cashAmount    = 0.00;

        if($type == "JOB"){

            $job_card = JobCard::find($id);

            $invoice_array['id']            = $id;
            $invoice_array['inv_type']      = "JOB";
            $invoice_array['inv_number']    = $job_card->job_card_number;
            $invoice_array['qb_inv_ref']    = $job_card->inv_ref;
            $invoice_array['qb_inv_status'] = $job_card->inv_status;
            $invoice_array['date']          = date('d-m-Y H:i A',strtotime($job_card->created_at));

            $customer_id = $job_card->customer_id;
            $customer    = Customer::find($customer_id);

            $invoice_array['customer_name']     = $customer->first_name;
            $invoice_array['customer_mobile']   = $customer->mobile;

            $created_user_id = $job_card->created_user_id;
            $created_user    = User::find($created_user_id);

            $invoice_array['created_by']        = $created_user->name;

            $invoice_array['plate_number']      = $job_card->plate_number;

            $invoice_array['total_price']       = $job_card->total_price;
            $invoice_array['total_discount']    = $job_card->total_discount;
            $invoice_array['gross_price']       = $job_card->gross_price;
            $invoice_array['total_tax']         = $job_card->total_tax;
            $invoice_array['net_amount_total']  = $job_card->net_amount_total;
            $invoice_array['payment_type']      = $job_card->payment_type;

            $job_card_details                   = JobCardDetail::getJobcardDetailsBydJob($id);

            $details = array();

            foreach($job_card_details as $job_detail){

                $t = array();

                $t['service_title']     = $job_detail->service_name;
                $t['service_category']  = $job_detail->service_category;
                $t['price']             = $job_detail->price;
                $t['discount']          = $job_detail->discount;
                $t['gross_price']       = $job_detail->gross_price;
                $t['tax']               = $job_detail->tax;
                $t['net_amount']        = $job_detail->net_amount;

                $details[] = $t;
               
            }

            $invoice_array['details'] = $details;

            $payment_details = PaymentDetail::where('concern_id', $job_card->id)->get();

            $invoice_array['payment_type_details']['cardAmount']  =   $cardAmount;
            $invoice_array['payment_type_details']['cashAmount']  =   $cashAmount;

            foreach($payment_details as $payment_detail){
                if($payment_detail->payment_mode == "CASH"){
                    $invoice_array['payment_type_details']['cashAmount']  =  $payment_detail->gross_amount;
                }else if($payment_detail->payment_mode == "CARD"){
                    $invoice_array['payment_type_details']['cardAmount']  =  $payment_detail->gross_amount;
                }
            }

            $pdfName = "Job_Inv_#100".$job_card->id.".pdf";
            $invoice_array['pdf_name'] = $pdfName;

            $invoice_array['arabic_total']          = Settings::getSettingValueByType('arabic_total');
            $invoice_array['arabic_discount']       = Settings::getSettingValueByType('arabic_discount');
            $invoice_array['arabic_GrossAmt']       = Settings::getSettingValueByType('arabic_GrossAmt');
            $invoice_array['arabic_vat']            = Settings::getSettingValueByType('arabic_vat');
            $invoice_array['arabic_NetAmount']      = Settings::getSettingValueByType('arabic_NetAmount');
            $invoice_array['arabic_PaymentMethod']  = Settings::getSettingValueByType('arabic_PaymentMethod');
            $invoice_array['arabic_PaidAmt']        = Settings::getSettingValueByType('arabic_PaidAmt');
            $invoice_array['arabic_CashAmt']        = Settings::getSettingValueByType('arabic_CashAmt');
            $invoice_array['arabic_CardAmt']        = Settings::getSettingValueByType('arabic_CardAmt');
            $invoice_array['arabic_Balance']        = Settings::getSettingValueByType('arabic_Balance');

            $job_card->inv_data     = json_encode($invoice_array);
            $job_card->inv_pdf_name = $pdfName;
            $job_card->save(); 


        } else if($type == "PACKAGE"){

            $packagePurchase = PackagePurchase::find($id);

            $invoice_array['id']            = $id;
            $invoice_array['inv_type']      = "PACKAGE";
            $invoice_array['inv_number']    = $packagePurchase->package_purchase_number;
            $invoice_array['qb_inv_ref']    = $packagePurchase->inv_ref;
            $invoice_array['qb_inv_status'] = $packagePurchase->inv_status;
            $invoice_array['date']          = date('d-m-Y H:i A',strtotime($packagePurchase->created_at));

            $customer_id = $packagePurchase->customer_id;
            $customer    = Customer::find($customer_id);

            $invoice_array['customer_name']     = $customer->first_name;
            $invoice_array['customer_mobile']   = $customer->mobile;

            $created_user_id = $packagePurchase->created_user_id;
            $created_user    = User::find($created_user_id);

            $invoice_array['created_by']        = $created_user->name;

            $invoice_array['plate_number']      = "-";

            $invoice_array['total_price']       = $packagePurchase->price;
            $invoice_array['total_discount']    = $packagePurchase->discount;
            $invoice_array['gross_price']       = $packagePurchase->gross_price;
            $invoice_array['total_tax']         = $packagePurchase->tax;
            $invoice_array['net_amount_total']  = $packagePurchase->net_amount;
            $invoice_array['payment_type']      = $packagePurchase->payment_type;

            $service_details = MainService::find($packagePurchase->main_service_id);

            $details = array();

            $t = array();
            $t['service_title']     = $service_details->service_name;
            $t['service_category']  = $service_details->service_category;
            $t['price']             = $packagePurchase->price;
            $t['discount']          = $packagePurchase->discount;
            $t['gross_price']       = $packagePurchase->gross_price;
            $t['tax']               = $packagePurchase->tax;
            $t['net_amount']        = $packagePurchase->net_amount;

            $details[] = $t;
            $invoice_array['details'] = $details;

            $payment_details = PaymentDetail::where('concern_id', $packagePurchase->id)->get();

            $invoice_array['payment_type_details']['cardAmount']  =   $cardAmount;
            $invoice_array['payment_type_details']['cashAmount']  =   $cashAmount;

            foreach($payment_details as $payment_detail){
                if($payment_detail->payment_mode == "CASH"){
                    $invoice_array['payment_type_details']['cashAmount']  =  $payment_detail->gross_amount;
                }else if($payment_detail->payment_mode == "CARD"){
                    $invoice_array['payment_type_details']['cardAmount']  =  $payment_detail->gross_amount;
                }
            }

            $pdfName = "Pkg_Inv_#100".$packagePurchase->id.".pdf";
            $invoice_array['pdf_name'] = $pdfName;

            $invoice_array['arabic_total']          = Settings::getSettingValueByType('arabic_total');
            $invoice_array['arabic_discount']       = Settings::getSettingValueByType('arabic_discount');
            $invoice_array['arabic_GrossAmt']       = Settings::getSettingValueByType('arabic_GrossAmt');
            $invoice_array['arabic_vat']            = Settings::getSettingValueByType('arabic_vat');
            $invoice_array['arabic_NetAmount']      = Settings::getSettingValueByType('arabic_NetAmount');
            $invoice_array['arabic_PaymentMethod']  = Settings::getSettingValueByType('arabic_PaymentMethod');
            $invoice_array['arabic_PaidAmt']        = Settings::getSettingValueByType('arabic_PaidAmt');
            $invoice_array['arabic_CashAmt']        = Settings::getSettingValueByType('arabic_CashAmt');
            $invoice_array['arabic_CardAmt']        = Settings::getSettingValueByType('arabic_CardAmt');
            $invoice_array['arabic_Balance']        = Settings::getSettingValueByType('arabic_Balance');

            
            $packagePurchase->inv_data     = json_encode($invoice_array);
            $packagePurchase->inv_pdf_name = $pdfName;
            $packagePurchase->save();


        } else if($type == "CARD_BUY"){

            $cardBuyDetail = CardBuyDetail::find($id);

            $invoice_array['id']            = $id;
            $invoice_array['inv_type']      = "CARD_BUY";
            $invoice_array['inv_number']    = $cardBuyDetail->purchase_number;
            $invoice_array['qb_inv_ref']    = $cardBuyDetail->inv_ref;
            $invoice_array['qb_inv_status'] = $cardBuyDetail->inv_status;
            $invoice_array['date']          = date('d-m-Y H:i A',strtotime($cardBuyDetail->created_at));

            $customer_id = $cardBuyDetail->customer_id;
            $customer    = Customer::find($customer_id);

            $invoice_array['customer_name']     = $customer->first_name;
            $invoice_array['customer_mobile']   = $customer->mobile;

            $created_user_id = $cardBuyDetail->created_user_id;
            $created_user    = User::find($created_user_id);

            $invoice_array['created_by']        = $created_user->name;

            $invoice_array['plate_number']      = "-";

            $invoice_array['total_price']       = $cardBuyDetail->price;
            $invoice_array['total_discount']    = $cardBuyDetail->discount;
            $invoice_array['gross_price']       = $cardBuyDetail->gross_price;
            $invoice_array['total_tax']         = $cardBuyDetail->tax;
            $invoice_array['net_amount_total']  = $cardBuyDetail->net_amount;
            $invoice_array['payment_type']      = $cardBuyDetail->payment_type;

            $service_details = MainService::find($cardBuyDetail->main_service_id);

            $details = array();

            $t = array();
            $t['service_title']     = $service_details->service_name;
            $t['service_category']  = $service_details->service_category;
            $t['price']             = $cardBuyDetail->price;
            $t['discount']          = $cardBuyDetail->discount;
            $t['gross_price']       = $cardBuyDetail->gross_price;
            $t['tax']               = $cardBuyDetail->tax;
            $t['net_amount']        = $cardBuyDetail->net_amount;

            $details[] = $t;
            $invoice_array['details'] = $details;

            $payment_details = PaymentDetail::where('concern_id', $cardBuyDetail->id)->get();

            $invoice_array['payment_type_details']['cardAmount']  =   $cardAmount;
            $invoice_array['payment_type_details']['cashAmount']  =   $cashAmount;

            foreach($payment_details as $payment_detail){
                if($payment_detail->payment_mode == "CASH"){
                    $invoice_array['payment_type_details']['cashAmount']  =  $payment_detail->gross_amount;
                }else if($payment_detail->payment_mode == "CARD"){
                    $invoice_array['payment_type_details']['cardAmount']  =  $payment_detail->gross_amount;
                }
            }

            $pdfName = "Crd_Buy_Inv_#100".$cardBuyDetail->id.".pdf";
            $invoice_array['pdf_name'] = $pdfName;

            $invoice_array['arabic_total']          = Settings::getSettingValueByType('arabic_total');
            $invoice_array['arabic_discount']       = Settings::getSettingValueByType('arabic_discount');
            $invoice_array['arabic_GrossAmt']       = Settings::getSettingValueByType('arabic_GrossAmt');
            $invoice_array['arabic_vat']            = Settings::getSettingValueByType('arabic_vat');
            $invoice_array['arabic_NetAmount']      = Settings::getSettingValueByType('arabic_NetAmount');
            $invoice_array['arabic_PaymentMethod']  = Settings::getSettingValueByType('arabic_PaymentMethod');
            $invoice_array['arabic_PaidAmt']        = Settings::getSettingValueByType('arabic_PaidAmt');
            $invoice_array['arabic_CashAmt']        = Settings::getSettingValueByType('arabic_CashAmt');
            $invoice_array['arabic_CardAmt']        = Settings::getSettingValueByType('arabic_CardAmt');
            $invoice_array['arabic_Balance']        = Settings::getSettingValueByType('arabic_Balance');

            $cardBuyDetail->inv_data     = json_encode($invoice_array);
            $cardBuyDetail->inv_pdf_name = $pdfName;
            $cardBuyDetail->save();

        }

        $data['invoice'] = $invoice_array;

        $pdf = PDF::loadView('jobcard.invoice_pdf', $data);

        Storage::put('public/pdf/'.$pdfName, $pdf->output());
        
        return view('jobcard.invoice')->with($data);
    }

    public function printInvoice($id,$type){
        $data = array();
        if($type == "JOB"){
            $job_card  = JobCard::find($id);
            $json_data = $job_card->inv_data;
            $data_ar   = json_decode($json_data,TRUE);

        }else if($type == "PACKAGE"){
            $packagePurchase = PackagePurchase::find($id);
            $json_data       = $packagePurchase->inv_data;
            $data_ar         = json_decode($json_data,TRUE);

        }else if($type == "CARD_BUY"){
            $cardBuyDetail   = CardBuyDetail::find($id);
            $json_data       = $cardBuyDetail->inv_data;
            $data_ar         = json_decode($json_data,TRUE);
        }
        $data['invoice'] = $data_ar;

        return view('jobcard.invoice_print')->with($data);
    }

    public function emailInvoice($id,$type){

        $data = array();
        if($type == "JOB"){
            $job_card    = JobCard::find($id);
            $json_data   = $job_card->inv_data;
            $data_ar     = json_decode($json_data,TRUE);

            $customer_id = $job_card->customer_id;
            $customer    = Customer::find($customer_id);
            $toEMail      = $customer->email;
            $subject     = "Eaw Car Service Invoice";
            $pdfFile     = $job_card->inv_pdf_name;
            
        }else if($type == "PACKAGE"){
            $packagePurchase = PackagePurchase::find($id);
            $json_data       = $packagePurchase->inv_data;
            $data_ar         = json_decode($json_data,TRUE);

            $customer_id = $packagePurchase->customer_id;
            $customer    = Customer::find($customer_id);
            $toEMail     = $customer->email;
            $subject     = "Eaw Car wash Package Purchase Invoice";
            $pdfFile     = $packagePurchase->inv_pdf_name;

        }else if($type == "CARD_BUY"){
            $cardBuyDetail   = CardBuyDetail::find($id);
            $json_data       = $cardBuyDetail->inv_data;
            $data_ar         = json_decode($json_data,TRUE);

            $customer_id = $cardBuyDetail->customer_id;
            $customer    = Customer::find($customer_id);
            $toEMail     = $customer->email;
            $subject     = "Eaw Membership Card Replace Invoice";
            $pdfFile     = $cardBuyDetail->inv_pdf_name;
        }
        $data['invoice'] = $data_ar;

        $files = [
            storage_path('app\public\pdf\\'.$pdfFile)
        ];

        Mail::send('jobcard.invoice_email', $data, function($message)use($toEMail,$subject, $files) {
            $message->to($toEMail)
                    ->subject($subject);
 
            foreach ($files as $file){
                $message->attach($file);
            }
            
        });

        if (Mail::failures()) {
            Session::flash('error', 'The Email not sent to "'.$toEMail.'"');
        }
    
        Session::flash('success', 'The Email sent to "'.$toEMail.'"');


       return view('jobcard.invoice')->with($data);
    }
    
}
