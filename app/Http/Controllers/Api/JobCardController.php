<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\HistoryPoint;
use App\Models\JobCard;
use App\Models\JobCardDetail;
use App\Models\MainService;
use App\Models\RegisteredPlate;
use App\Models\Settings;
use App\Models\Member;
use App\Models\MembershipPackageLog;
use App\Models\Card;
use App\Models\PackagePurchase;
use App\Models\PaymentDetail;
use App\Models\CustomerPackageQueue;
use App\Models\TaxType;
use App\Models\apiLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class JobCardController extends Controller
{
    private $successStatus  =   200;
    private $errorStatus    =   400;
    

    public function index(Request $request){

        $api_key_saved            = Settings::getSettingValueByType('api_key');
        $cashPaymentMethodCode    = Settings::getSettingValueByType('cash_payment_code');
        $cardPaymentMethodCode    = Settings::getSettingValueByType('card_payment_code');

        $cashDepositToAccountCode = Settings::getSettingValueByType('cash_payment_account_code');
        $cardDepositToAccountCode = Settings::getSettingValueByType('card_payment_account_code');
        $discountAccountCode      = Settings::getSettingValueByType('discount_account_code');

        $validator = Validator::make($request->all(), [
            'api_key' => 'required',
            'card_number' => 'required',
        ]);

        if ($validator->fails()) {


            $resp = array('code' => 400, 'status' => 'error', 'message' => $validator->messages()->first(), 'data' => $validator->errors());
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => $validator->messages()->first(), 'data' => $validator->errors()], 400);
        }

        $api_key            = $request->input('api_key');
        $card_secret_number = $request->input('card_number');

        if($api_key_saved  != $api_key){

            $resp = array('code' => 400, 'status' => 'error', 'message' => "api_key is invalid", 'data' => "api_key is invalid");
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => "api_key is invalid", 'data' => "api_key is invalid"], 400);
        }

        $card = Card::where('card_secret_number','=',$card_secret_number)->where('status','=',"ALLOCATED")->first();

        if($card == null){

            $resp = array('code' => 400, 'status' => 'error', 'message' => "Card not exists", 'data' => "Card not exists");
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => "Card not exists", 'data' => "Card not exists"], 400);
        }
       
        $member        = Member::where('card_id', '=', $card->id)->where('status','=','ACTIVE')->first();

        if($member == null){

            $resp = array('code' => 400, 'status' => 'error', 'message' => "Member not exists", 'data' => "Member not exists");
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => "Membership Expired/Inactive", 'data' => "Member not exists"], 400);
        }

        $membership_package_log_id  =  $member->membership_package_log_id;
        $customer_id                =  $member->customer_id;

        $membership_package_logs     = MembershipPackageLog::find($membership_package_log_id);

        $main_service_id            = $membership_package_logs->main_service_id;
        $service_details            = MainService::find($main_service_id);


        $package_validity           = $service_details->package_validity;
        $wash_validity              = $service_details->wash_validity;
        $start_from_type            = $service_details->start_from_type;

        $start_date                 = $member->start_date;
        $end_date                   = $member->end_date;

        $service_completed_member     = $member->service_completed;
        $service_remaining_member     = $member->service_remaining;

        $service_completed     = $membership_package_logs->service_completed;
        $service_remaining     = $membership_package_logs->service_remaining;
        $package_purchase_date = $membership_package_logs->package_purchase_date;
        $package_end_date      = $membership_package_logs->package_end_date;
        $wash_validity_start   = $membership_package_logs->wash_validity_start;
        $wash_validity_end     = $membership_package_logs->wash_validity_end;

        $today = date('Y-m-d H:i:s');

        if($wash_validity_end == null){

            if($package_end_date < $today){

                $member->status = "DISABLED";
                $member->updated_at = date('Y-m-d H:i:s');
                $member->save();
                $membership_package_logs->status = "EXPIRED";
                $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                $membership_package_logs->save();

                $resp = array('code' => 400, 'status' => 'error', 'message' => "Package Validity completed", 'data' => "Package Validity completed");
                $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

                return response()->json(['code' => 400, 'status' => 'error', 'message' => "Package Validity completed", 'data' => "Package Validity completed"], 400);
            }

        }else {

            if($wash_validity_end <  $today){

                $member->status = "DISABLED";
                $member->updated_at = date('Y-m-d H:i:s');
                $member->save();
                $membership_package_logs->status = "EXPIRED";
                $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                $membership_package_logs->save();
                
                $resp = array('code' => 400, 'status' => 'error', 'message' => "Wash Validity completed", 'data' => "Wash Validity completed");
                $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

                return response()->json(['code' => 400, 'status' => 'error', 'message' => "Wash Validity completed", 'data' => "Wash Validity completed"], 400);

            }

        }

        if($service_remaining == 0 || $service_remaining_member == 0){

            $member->status = "DISABLED";
            $member->updated_at = date('Y-m-d H:i:s');
            $member->save();
            $membership_package_logs->status = "EXPIRED";
            $membership_package_logs->updated_at = date('Y-m-d H:i:s');
            $membership_package_logs->save();

            $resp = array('code' => 400, 'status' => 'error', 'message' => "Washes completed", 'data' => "Washes completed");
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => "Washes completed", 'data' => "Washes completed"], 400);
        }

        $new_service_completed_member = $service_completed_member + 1;
        $new_service_remaining_member = $service_remaining_member -1;

        $member->service_completed = $new_service_completed_member;
        $member->service_remaining = $new_service_remaining_member;

        if($start_date == null && $start_from_type=="FIRST_WASH"){

            $member->start_date = date('Y-m-d H:i:s');
            $new_end_date       = date('Y-m-d H:i:s', time() + 86400 * $wash_validity);
            $member->end_date   = $new_end_date;

        }

        $member->updated_at = date('Y-m-d H:i:s');
        $member->save();


        $new_service_completed = $service_completed +1;
        $new_service_remaining = $service_remaining -1;

        $membership_package_logs->service_completed = $new_service_completed;
        $membership_package_logs->service_remaining = $new_service_remaining;
        $membership_package_logs->updated_at = date('Y-m-d H:i:s');
        $membership_package_logs->save();

        if($wash_validity_start == null && $start_from_type=="FIRST_WASH"){

            $membership_package_logs->wash_validity_start = date('Y-m-d H:i:s');
            $new_end_date                                 = date('Y-m-d H:i:s', time() + 86400 * $wash_validity);
            $membership_package_logs->wash_validity_end   = $new_end_date;
            $membership_package_logs->updated_at          = date('Y-m-d H:i:s');
            $membership_package_logs->save();

        }

        if($new_service_remaining == 0){

            $member->status = "DISABLED";
            $member->updated_at = date('Y-m-d H:i:s');
            $member->save();
            $membership_package_logs->status = "EXPIRED";
            $membership_package_logs->updated_at = date('Y-m-d H:i:s');
            $membership_package_logs->save();
        }

        $customer = Customer::find($customer_id);
        
        $total_price      = 0;
        $total_discount   = 0;
        $gross_total      = 0;
        $total_tax        = 0;
        $net_amount_total = 0;

        try {
            $input = [
                'job_card_number' => 'JOB' . time(),
                'job_card_type' => 'MEMBER',
                'total_price' => $total_price,
                'total_discount' => $total_discount,
                'gross_price' => $gross_total,
                'total_tax' => $total_tax,
                'net_amount_total' => $net_amount_total,
                'customer_id' => $customer_id,
                'customer_type' => 'MEMBER',
                'car_type' => $service_details->car_type,
                'plate_number' => "",
                'earned_points' => 0,
                'payment_type' => "SPOT_PAYMENT",
                'txn_number' => "",
                'created_user_id' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $jobCard = JobCard::create($input);
        } catch (Exception $ex) {

            $resp = array('code' => 400, 'status' => 'error', 'message' => "Job card creation failed", 'data' => "Job card creation failed");
            $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

            return response()->json(['code' => 400, 'status' => 'error', 'message' => "Job card creation failed", 'data' => "Job card creation failed"], 400);
        }

        if($customer->qb_cutomer_id == null){
            $responseArray = Customer::createQbCustomer($customer);
            if($responseArray->response_code == 200){

                $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                $customer->updated_at = date('Y-m-d H:i:s');
                $customer->save();
            }
        }               

        $dataInv['Job'] = $jobCard->job_card_number;
        $dataInv['CustomerId'] = $customer->qb_cutomer_id;
        $dataInv['TotalDiscount'] = $total_discount;
        $dataInv['DiscountAccount'] = $discountAccountCode;
        $dataInv['PaymentType'] = "SPOT_PAYMENT";
        $dataInv['TotalAmount'] = $gross_total;
        $dataInv['PaymentDetails'] = [];
        $dataInv['Notes'] = "";

        $paymentInv['PaymentMethod'] = "";
        $paymentInv['PaymentValue'] = "";
        $paymentInv['ReferenceNo'] = "";
        $paymentInv['DepositToAccount'] = "";
        $paymentInv['ChargeRate'] = "";  

        $price       =  0;
        $discount    =  0;
        $tax         =  0;
        $net_amount  =  0;
        $gross_price =  0;

        try {
            $input = [
                'job_card_id' => $jobCard->id,
                'main_service_id' => $service_details->id,
                'price' => $price,
                'discount' => $discount,
                'gross_price' => $gross_price,
                'tax' => $tax,
                'net_amount' => $net_amount,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

        $jobCardDetail = JobCardDetail::create($input);

       } catch (Exception $ex) {

        $resp = array('code' => 400, 'status' => 'error', 'message' => "Job Detail creation failed", 'data' => "Job Detail creation failed");
        $apiLogId  = apiLog::createApiLog(json_encode($request->all()),json_encode($resp), "400", "CREATE_JOB");

        return response()->json(['code' => 400, 'status' => 'error', 'message' => "Job Detail creation failed", 'data' => "Job Detail creation failed"], 400);
       }

       $invoicelineItem['Qty']         = 1;
       $invoicelineItem['Amount']      = $price;
       $invoicelineItem['Discount']    = $discount;

       $taxType = TaxType::find($service_details->tax_id);

       $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
       $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";  
       $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
       $invoicelineItem['Description'] = $service_details->service_category."-".$service_details->service_name;

       $invoiceline[] = $invoicelineItem;  
       $dataInv['Invoiceline'] = $invoiceline;
       
       $payment_type   = "SPOT_PAYMENT";

       if($payment_type == "SPOT_PAYMENT"){

           $cashNetAmount   = $request->input('spotpayCashAmount');
           $cardNetAmount   = $request->input('spotpayCardAmount');

           $cardAmount      = 0;
           $cashAmount      = 0;
           $cashDiscount    = 0;
           $cardDiscount    = 0;
           $cashTax         = 0;
           $cardTax         = 0;
           $cashNet         = 0;
           $cardNet         = 0;

           if($gross_total != 0){

               $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
               $cardAmount = $total_price-$cashAmount; //total_price-cashAmount

               $cashDiscount = number_format(($cashNetAmount/$gross_total) * $total_discount, 2, '.', '');
               $cardDiscount = $total_discount - $cashDiscount; //total_disc-CashDisc

               $cashTax = number_format(($cashNetAmount/$gross_total) * $total_tax, 2, '.', '');
               $cardTax = $total_tax - $cashTax; 

               $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount_total, 2, '.', '');
               $cardNet = $net_amount_total - $cashNet;    

           }    

           if($cardNetAmount != 0){
               $txn_number =  $request->input('txn_number');

               try { 
                   $input = [
                       'concern_id' => $jobCard->id,
                       'type' => 'JOB_CARD',
                       'customer_id' => $customer->id,
                       'customer_type' => $customer->type,
                       'payment_mode' => 'CARD',
                       'txn_number' => $txn_number,
                       'amount' => $cardAmount,
                       'discount' => $cardDiscount,
                       'gross_amount' => $cardNetAmount,
                       'tax' => $cardTax,
                       'net_amount' => $cardNet,
                       'created_at' => date('Y-m-d H:i:s'),
                       'updated_at' => date('Y-m-d H:i:s'),
                   ];
                   $paymentDetail = PaymentDetail::create($input);
               } catch (Exception $ex) {
                   return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
               }

               $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
               $paymentInv['PaymentValue']     = $cardNetAmount;
               $paymentInv['ReferenceNo']      = $txn_number;
               $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
               $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');

               $dataInv['PaymentDetails'][]    = $paymentInv;        
           }
           
           if($cashNetAmount != 0){

               try { 
                   $input = [
                       'concern_id' => $jobCard->id,
                       'type' => 'JOB_CARD',
                       'customer_id' => $customer->id,
                       'customer_type' => $customer->type,
                       'payment_mode' => 'CASH',
                       'txn_number' => null,
                       'amount' => $cashAmount,
                       'discount' => $cashDiscount,
                       'gross_amount' => $cashNetAmount,
                       'tax' => $cashTax,
                       'net_amount' => $cashNet,
                       'created_at' => date('Y-m-d H:i:s'),
                       'updated_at' => date('Y-m-d H:i:s'),
                   ];
                   $paymentDetail = PaymentDetail::create($input);
               } catch (Exception $ex) {
                   return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
               }

               $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
               $paymentInv['PaymentValue']     = $cashNetAmount;
               $paymentInv['ReferenceNo']      = "";
               $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
               $paymentInv['ChargeRate']       = 0;

               $dataInv['PaymentDetails'][]    = $paymentInv;                

           }
       } else{

           $voucher_number =  $request->input('voucher_number');

           try { 
               $input = [
                   'concern_id' => $jobCard->id,
                   'type' => 'JOB_CARD',
                   'customer_id' => $customer->id,
                   'customer_type' => $customer->type,
                   'payment_mode' => $payment_type,
                   'voucher_no' => $voucher_number,
                   'amount' => $total_price,
                   'discount' => $total_discount,
                   'gross_amount' => $gross_total,
                   'tax' => $total_tax,
                   'net_amount' => $net_amount_total,
                   'created_at' => date('Y-m-d H:i:s'),
                   'updated_at' => date('Y-m-d H:i:s'),
               ];
               $paymentDetail = PaymentDetail::create($input);
           } catch (Exception $ex) {
               return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
           }

           $dataInv['Notes']             = $voucher_number;
       }

       if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
           $responseArray = JobCard::invoiceCreation($jobCard, $dataInv);
       }

       $resp      = array('code' => 200, 'status' => 'success', 'message' => "Job Card Created, Job_Id : #".$jobCard->id, 'data' => "Job Card Created.");
       $apiLogId  = apiLog::createApiLog(json_encode($request->all()), json_encode($resp), "200", "CREATE_JOB");

       return response()->json(['code' => 200, 'status' => 'success', 'message' => "Job Card Created, Job_Id : #".$jobCard->id, 'data' => "Job Card Created."], 200);

    }

    public function gateStatus(Request $request){

        $api_key_saved            = Settings::getSettingValueByType('api_key');

        $validator = Validator::make($request->all(), [
            'api_key' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['code' => 400, 'status' => 'error', 'message' => $validator->messages()->first(), 'data' => $validator->errors()], 400);
        }

        $api_key            = $request->input('api_key');

        if($api_key_saved  != $api_key){
            return response()->json(['code' => 400, 'status' => 'error', 'message' => "api_key is invalid", 'data' => "api_key is invalid"], 400);
        }

        $gate_status = Settings::getSettingValueByType('gate_status');

        if($gate_status == "LOCK" || $gate_status == "UNLOCK"){
            Settings::where('type', 'gate_status')->update(['value' => 'ACTIVE']);
        }


        return response()->json(['code' => 200, 'status' => 'success', 'message' => "The gate status is #".$gate_status,  'gate_status'=> $gate_status], 200);

    }



}
