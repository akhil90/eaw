<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;
use DB;

class JobCard extends Model
{
    use HasFactory;

    protected $table = 'job_cards';

    protected $fillable = [
        'job_card_number', 'job_card_type','total_price','total_discount','gross_price','total_tax','net_amount_total','customer_id','customer_type','car_type','plate_number','earned_points','payment_type','txn_number','inv_status','inv_ref','qb_request','qb_response','created_user_id','created_at','updated_at'
    ];


    public static function invoiceCreation($jobcard, $data){

        if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

           $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/invoice/create';
        
            if(Settings::getSettingValueByType('api_url')!=null){
                $url  = Settings::getSettingValueByType('api_url').'invoice/create';
            }
        } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){

            $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/invoice';
        
            if(Settings::getSettingValueByType('api_url')!=null){
                $url  = Settings::getSettingValueByType('api_url').'invoice';
            }
            
        }

        $jobcard->qb_request = json_encode($data);
        $jobcard->save();
        
        try{
            $response  = Http::post($url,$data);
        }
        catch(Exeption $e){
            return $e ;
        }

        $jobcard->qb_response = $response;
        $jobcard->save();

        if($response){

            $responseArray = json_decode($response);

            if($responseArray!=null){

                if($responseArray->response_code == 200){
                    $jobcard->inv_ref = $responseArray->qb_invoice_id;
                    $jobcard->inv_status = 'CREATED';
                    $jobcard->save();
                }else{
                    $jobcard->inv_ref = "";
                    $jobcard->inv_status = 'PENDING';
                    $jobcard->save();
                }

            } else{
                $jobcard->inv_ref = "";
                $jobcard->inv_status = 'PENDING';
                $jobcard->save();
            }
            
        }else{
            $jobcard->inv_ref = "";
            $jobcard->inv_status = 'PENDING';
            $jobcard->save();
        }

        

        return $response ;
    }
}
