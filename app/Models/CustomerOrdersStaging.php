<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerOrdersStaging extends Model
{
    use HasFactory;
    protected $fillable = [
        'wp_order_id', 'wp_cust_id','wp_cust_name','wp_cust_email','wp_cust_phone','wp_package_id','created_at','updated_at'
    ];
}
