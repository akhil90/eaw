<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CustomerOrdersStaging;
use App\Models\Customer;
use App\Models\MainService;
use App\Models\MembershipPackageLog;
use App\Models\Member;
use App\Models\CustomerPackageQueue;
use App\Models\Settings;
use DB;

class ProcessingOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order_processing:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processing pending task from customer_orders_stagings table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(Settings::getSettingValueByType('wp_convert') == 0){
            $staging_datas = CustomerOrdersStaging::where('status','=','PENDING')->get();
            if($staging_datas->isNotEmpty()){
                Settings::where('type', 'wp_convert')->update(['value' => 1]);
                foreach($staging_datas as $staging_data){
                    $customer = Customer::where('email','=',$staging_data->wp_cust_email)->orWhere('mobile','=',$staging_data->wp_cust_phone)->first();
                    if($customer == false){
                        $customer = Customer::create([
                                        'first_name'    => $staging_data->wp_cust_name, 
                                        'email'         => $staging_data->wp_cust_email, 
                                        'mobile'        => $staging_data->wp_cust_phone,
                                        'type'          => 'MEMBER', 
                                        'created_by'    => 1, 
                                        'created_at'    => date('Y-m-d H:i:s')
                                    ]);
                    }
                    $package_details = MainService::where('wp_id','=',$staging_data->wp_package_id)->first();
                    if($package_details != false){
                        $memb_package_log = MembershipPackageLog::where('customer_id','=',$customer->id)->where('status','=','ACTIVE')->first();
                        if($memb_package_log == false){
                            $memb_package_log = MembershipPackageLog::create([
                                'customer_id'           => $customer->id, 
                                'main_service_id'       => $package_details->id, 
                                'service_remaining'     => $package_details->no_of_washes, 
                                'package_purchase_date' => date('Y-m-d H:i:s'), 
                                'package_end_date'      => date('Y-m-d H:i:s', time() + 86400 * $package_details->package_validity), 
                                'created_at'            => date('Y-m-d H:i:s')
                            ]);
                            $member_details = Member::where('customer_id','=',$customer->id)->first();
                            if($member_details == false){
                                $member_details = Member::create([
                                    'customer_id'               => $customer->id,
                                    'membership_package_log_id' => $memb_package_log->id,
                                    'service_completed'         => 0,
                                    'service_remaining'         => $package_details->no_of_washes, 
                                    'status'                    => 'ACTIVE', 
                                    'created_at'                => date('Y-m-d H:i:s')
                                ]);
                            }else{
                                $member_details->membership_package_log_id  = $memb_package_log->id;
                                $member_details->service_completed          = 0;
                                $member_details->service_remaining          = $package_details->no_of_washes;
                                $member_details->status                     = 'ACTIVE';
                                $member_details->updated_at                 = date('Y-m-d H:i:s');
                                $member_details->save();
                            }
                        }else{
                            $cust_pack_queue = CustomerPackageQueue::create([
                                'customer_id'       =>  $customer->id, 
                                'wp_customer_id'    =>  $staging_data->wp_cust_id, 
                                'main_service_id'   =>  $package_details->id, 
                                'wp_package_id'     =>  $staging_data->wp_package_id, 
                                'created_at'        =>  date('Y-m-d H:i:s')
                            ]);
                        }
                    }
                    $staging_data->status       = 'COMPLETED';
                    $staging_data->updated_at   = date('Y-m-d H:i:s');
                    $staging_data->save();
                }
                Settings::where('type', 'wp_convert')->update(['value' => 0]);
                $this->info('Success');
            }else{
                $this->info('No pending task');
            }
        }



        
    }
}
