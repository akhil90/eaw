<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $table = 'cards';

    protected $fillable = [
        'card_number', 'card_secret_number','status'
    ];

    public static function getAll($filter = []){

        $query =  Card::select([
                    'cards.id',
                    'cards.card_number',
                    'cards.card_secret_number',
                    'cards.status', 
                    'cards.customer_id',
                    'cards.updated_by', 
                    'cards.created_at',
                    'cards.updated_at',
                    'customers.first_name',
                    'customers.email',
                    'customers.type',
                    'customers.mobile',
                    'customers.address',
                    'users.name',
                    'users.id as user_id'
                ]);

        $query = $query->leftjoin('customers', 'customers.id', '=', 'cards.customer_id');
        $query = $query->leftjoin('users', 'users.id', '=', 'cards.updated_by');

        if(isset($filter['customer_id'])){
            $query = $query->Where('cards.customer_id',$filter['customer_id']);
        }

        if(isset($filter['updated_by'])){
            $query = $query->Where('cards.updated_by',$filter['updated_by']);
        }

        if(isset($filter['status'])){
            $query = $query->Where('cards.status',$filter['status']);
        }

        $query = $query->orderBy('cards.id', 'DESC');
        $cards = $query->get();

        return $cards;    
    }

}
