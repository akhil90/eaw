<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerOrderStagings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_orders_stagings', function (Blueprint $table) {
            $table->id();
            $table->integer('wp_order_id')->nullable();
            $table->integer('wp_cust_id')->nullable();
            $table->string('wp_cust_name')->nullable();
            $table->string('wp_cust_email')->nullable();
            $table->string('wp_cust_phone')->nullable();
            $table->integer('wp_package_id')->nullable();
            $table->enum('status', ['PENDING', 'COMPLETED'])->default('PENDING');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_order_stagings');
    }
}
