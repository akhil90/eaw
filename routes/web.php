<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\JobCardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);
    Route::post('/home', [App\Http\Controllers\HomeController::class, 'index']);
    
    Route::get('/staff', [App\Http\Controllers\StaffController::class, 'index'])->name('staff');

    Route::get('/jobcard', [App\Http\Controllers\JobCardController::class, 'index'])->name('jobcard');

    Route::get('/jobcard/non_member_by_plate/{plateNumber}', [App\Http\Controllers\JobCardController::class, 'nonMemberSearchByPlateNumber']);
    Route::get('/jobcard/non_member_by_mobile/{mobile}', [App\Http\Controllers\JobCardController::class, 'nonMemberSearchByPhoneNumber']);
    Route::get('/jobcard/member_by_card/{member_id}', [App\Http\Controllers\JobCardController::class, 'memberSearchByCardNumber']);
    Route::get('/jobcard/member_by_plate/{plateNumber}', [App\Http\Controllers\JobCardController::class, 'memberSearchByPlateNumber']);
    Route::get('/jobcard/member_by_mobile/{mobile}', [App\Http\Controllers\JobCardController::class, 'memberSearchByPhoneNumber']);
    Route::get('/jobcard/search_member_card_status/{mobile}', [App\Http\Controllers\JobCardController::class, 'cardEnrolledStatusWithPhoneNumber']);

    Route::get('/jobcard/search_customer/{mobile}', [App\Http\Controllers\JobCardController::class, 'isCustomerExists']);

    Route::post('/jobcard/enroll_card', [App\Http\Controllers\JobCardController::class, 'memberEnrollCard']);

    Route::post('/referedPlateStore',[App\Http\Controllers\JobCardController::class, 'referedPlatenumberSessionSave']);

    Route::post('/jobcard/redirect_choose', [App\Http\Controllers\JobCardController::class, 'redirectToServicesShow']);
    Route::get('/jobcard/choose', [App\Http\Controllers\JobCardController::class, 'chooseServiceShow'])->name('service_choose');

    Route::post('/jobcard/redirect_jobconfirm', [App\Http\Controllers\JobCardController::class, 'redirectToJobCardConfirm']);
    Route::get('/jobcard/confirm', [App\Http\Controllers\JobCardController::class, 'confirmShow'])->name('confirm.show');
    Route::get('/jobcard/confirm_buy', [App\Http\Controllers\JobCardController::class, 'confirmBuy']);

    Route::post('/jobcard/save', [App\Http\Controllers\JobCardController::class, 'saveJobcard']);
    Route::post('/jobcard/save_package', [App\Http\Controllers\JobCardController::class, 'savePackage']);

    Route::post('/jobcard/editcustomer', [App\Http\Controllers\JobCardController::class, 'editCustomer']);

    Route::get('/jobcard/history_points/{plate_number}', [App\Http\Controllers\JobCardController::class, 'getPointHistoryByPlate']);

    Route::get('/main_services',[App\Http\Controllers\MainServicesController::class, 'show']);
    Route::get('/main_services/create',[App\Http\Controllers\MainServicesController::class, 'create']);
    Route::post('/main_services/store',[App\Http\Controllers\MainServicesController::class, 'store']);
    Route::get('/main_services/edit/{id}',[App\Http\Controllers\MainServicesController::class, 'edit']);
    Route::post('/main_services/update/{id}',[App\Http\Controllers\MainServicesController::class, 'update']);
    Route::get('/main_services/block/{id}',[App\Http\Controllers\MainServicesController::class, 'block']);
    Route::get('/main_services/unblock/{id}',[App\Http\Controllers\MainServicesController::class, 'unblock']);
    Route::get('/main_services/refreshtax',[App\Http\Controllers\MainServicesController::class, 'refreshTaxtypesAjax']);

    Route::get('/membership_packages',[App\Http\Controllers\MembershipPackageController::class, 'show']);
    Route::get('/membership_packages/create',[App\Http\Controllers\MembershipPackageController::class, 'create']);
    Route::post('/membership_packages/store',[App\Http\Controllers\MembershipPackageController::class, 'store']);
    Route::get('/membership_packages/edit/{id}',[App\Http\Controllers\MembershipPackageController::class, 'edit']);
    Route::post('/membership_packages/update/{id}',[App\Http\Controllers\MembershipPackageController::class, 'update']);
    Route::get('/membership_packages/block/{id}',[App\Http\Controllers\MembershipPackageController::class, 'block']);
    Route::get('/membership_packages/unblock/{id}',[App\Http\Controllers\MembershipPackageController::class, 'unblock']);

    Route::get('/users',[App\Http\Controllers\UserController::class, 'show']);
    Route::get('/users/create',[App\Http\Controllers\UserController::class, 'create']);
    Route::post('/users/store',[App\Http\Controllers\UserController::class, 'store']);
    Route::get('/users/edit/{id}',[App\Http\Controllers\UserController::class, 'edit']);
    Route::post('/users/update/{id}',[App\Http\Controllers\UserController::class, 'update']);
    Route::get('/users/block/{id}',[App\Http\Controllers\UserController::class, 'block']);
    Route::get('/users/unblock/{id}',[App\Http\Controllers\UserController::class, 'unblock']);

    Route::get('/customers',[App\Http\Controllers\CustomersController::class, 'show']);
    Route::get('/customer/jobs/{cutomer_id}',[App\Http\Controllers\CustomersController::class, 'getJobsByCustomer']);
    Route::get('/customer/packages/{cutomer_id}',[App\Http\Controllers\CustomersController::class, 'getPackageDetails']);

    Route::get('/import_export_cards',[App\Http\Controllers\CardController::class, 'show']);
    Route::get('/import_export_cards/export',[App\Http\Controllers\CardController::class, 'export']);
    Route::get('/import_export_cards/download',[App\Http\Controllers\CardController::class, 'download']);
    Route::post('/import_export_cards/import',[App\Http\Controllers\CardController::class, 'import']);
    Route::post('/preview/import',[App\Http\Controllers\CardController::class, 'previewUploadingExcel']);
    Route::get('/cards/activate/{id}',[App\Http\Controllers\CardController::class, 'activate']);
    Route::get('/cards/deactivate/{id}',[App\Http\Controllers\CardController::class, 'deactivate']);

    Route::get('/profile',[App\Http\Controllers\ProfileController::class, 'show']);
    Route::get('/profile/edit',[App\Http\Controllers\ProfileController::class, 'edit']);
    Route::post('/profile/update',[App\Http\Controllers\ProfileController::class, 'update']);
    Route::get('/change_password',[App\Http\Controllers\ProfileController::class, 'change_password']);
    Route::post('/change_password',[App\Http\Controllers\ProfileController::class, 'update_password']);

    Route::get('/settings',[App\Http\Controllers\SettingsController::class, 'index']);
    Route::post('/updateSettings',[App\Http\Controllers\SettingsController::class, 'updateSettings']);
    Route::post('/settings/refresh',[App\Http\Controllers\SettingsController::class, 'refreshAccountApiAjax']);

    Route::post('/paymentDetailsByMode',[App\Http\Controllers\HomeController::class, 'salesReportByPaymentModeAjax']);
    Route::post('/duplicateCustomerJobs',[App\Http\Controllers\HomeController::class, 'duplicateCustomerJobs']);
    Route::post('/getJobCardDetailsFromJobId',[App\Http\Controllers\HomeController::class, 'getJobCardDetailsFromJobId']);
    Route::get('/repost_invoice/{jobId}',[App\Http\Controllers\HomeController::class, 'rePostInvoice']);

    Route::get('jobcard/invoice/{id}/{type}',[App\Http\Controllers\InvoiceContoller::class, 'index'])->name('invoice');
    Route::get('jobcard/invoice_print/{id}/{type}',[App\Http\Controllers\InvoiceContoller::class, 'printInvoice']);
    Route::get('jobcard/invoice_email/{id}/{type}',[App\Http\Controllers\InvoiceContoller::class, 'emailInvoice']);

    Route::get('/jobcard/change_gate_status/{current_gate_status}', [App\Http\Controllers\JobCardController::class, 'changeGateStatus']);
    Route::get('/jobcard/gate_status', [App\Http\Controllers\JobCardController::class, 'gateStatus']);

    Route::get('/daily_report', [App\Http\Controllers\HomeController::class, 'dailyReport']);
    
    Route::get('jobcard/invoice_print',function(){
        return view('jobcard.invoice_print');
    });
});

