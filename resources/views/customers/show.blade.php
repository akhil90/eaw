@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Customers</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
      <div class="col-lg-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>Customer List</h3>
              <!--<div class="ibox-tools"> <a href="/customers/create" style="margin-right: 25px; color: #fff !important;" type="button" title="Add new service" class="btn btn-round btn-success"><i class="fa fa-plus"></i> customer</a> </div>-->
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Member/Non member</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>  
                    @foreach($customers as $key=>$customer)                
                     <tr>
                     <td width="5%">{{$key+1}}</td>
                      <td width="25%">{{$customer->first_name}} {{$customer->last_name}}</td>
                      <td width="25%">{{$customer->email}}</td> 
                      <td width="20%">{{$customer->mobile}}</td>
                      <td width="20%">{{str_replace('_',' ',$customer->type)}}</td>
                      <td width="5%"><a title="View Job Cards" id="view_jobs" data-customer_id = "{{ $customer->id }}" data-customer_name = "{{ $customer->first_name }}"  class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>   <a title="View Package" id="view_packages" data-customer_id = "{{ $customer->id }}" data-customer_name = "{{ $customer->first_name }}"  class="btn btn-info btn-xs"><i class="fa fa-list"></i></a></td>                    
                    </tr>   
                    @endforeach               
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="JobsModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Jobs of the customer <span id="customer_name"><span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="table-responsive">
                  <table id="dataTables-jobs" class="sum table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                      <th class="lft">SN</th>
                      <th>Wash Date</th>
                      <th class="lft">Job Card Number</th>
                      <th>Job Type</th>
                      <th>Total Price</th>
                      <th>Total Discount</th>
                      <th>Gross Total</th>
                      <th>Car Type</th>
                      <th>Plate Number</th>
                      <th>Earned Points</th>
                      <th>Payment Method</th>
                      <th>Invoice Status</th>
                      <th>Invoice Ref</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="jobs_table_body">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal" id="packageModel">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Packages of the customer <span id="customer_name_p"><span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            <div class="col-md-12 col-sm-12 col-xs-12" style="overflow-y:auto;height:500px">
              <div class="row"><br>
                <h4 class="modal-title">Packages History </h4><br><br>
                <div class="table-responsive">
                  <table id="dataTables-packages_history" class="sum table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                      <th class="lft">SN</th>
                      <th>Package Name</th>
                      <th>Remaining Washes</th>
                      <th>Completed</th>
                      <th>Package Start</th>
                      <th>Package End</th>
                      <th>Wash Start</th>
                      <th>Wash End</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody id="packages_history_body">
                    </tbody>
                  </table>
                </div>
              </div><br>

              <div class="row"><br>
                <h4 class="modal-title">Packages Queue</h4><br><br>
                <div class="table-responsive">
                  <table id="dataTables-packages_queue" class="sum table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                      <th class="lft">SN</th>
                      <th>Package Name</th>
                      <th>No.of Washes</th>
                      <th>Package Purchase</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody id="packages_queue_body">
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
     
@endsection

@section('script')

<script>
$("a#view_jobs").on('click', function() {

  var customer_id = $(this).attr("data-customer_id");
  var customer_name = $(this).attr("data-customer_name");

  $('span#customer_name').html(customer_name);

  $('#dataTables-jobs').DataTable().destroy();

    $.ajax({
       url: '/customer/jobs/'+ customer_id,
       type:'GET',
       dataType:'json',
       success: function(response) {
          var list = '';
          $.each(response.data, function() {
            list += '<tr><td>'+this.SlNo+'</td><td>'+this.date+'</td><td>'+this.job_card_number+'</td><td>'+this.job_card_type+'</td><td>'+this.total_price+'</td><td>'+this.total_discount+'</td><td>'+this.gross_price+'</td><td>'+this.car_type+'</td><td>'+this.plate_number+'</td><td>'+this.earned_points+'</td><td>'+this.payment_type+'</td><td>'+this.inv_status+'</td><td>'+this.inv_ref+'</td><td>'+this.action+'</td></tr>';
          });
          $("#jobs_table_body").html(list); 
          $('#dataTables-jobs').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'jobs'
                },
                {
                    extend: 'pdf',
                    title: 'jobs'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          });

           $('#JobsModal').modal("show");     
         }
      });

});

$("a#view_packages").on('click', function() {
  var customer_id = $(this).attr("data-customer_id");
  var customer_name = $(this).attr("data-customer_name");

  $('span#customer_name_p').html(customer_name);

  $('#dataTables-packages_history').DataTable().destroy();
  $('#dataTables-packages_queue').DataTable().destroy();

    $.ajax({
      url: '/customer/packages/'+ customer_id,
      type:'GET',
      dataType:'json',
      success: function(response) {
          
          var list_pkg_h = '';
          $.each(response.data.history, function() {
            list_pkg_h += '<tr><td>'+this.SlNo+'</td><td>'+this.package_name+'</td><td>'+this.remaining_washes+'</td><td>'+this.completed_washes+'</td><td>'+this.package_start+'</td><td>'+this.package_ends+'</td><td>'+this.wash_start+'</td><td>'+this.wash_ends+'</td><td>'+this.status+'</td></tr>';
          });

          var list_pkg_q = '';
          $.each(response.data.queue, function() {
            list_pkg_q += '<tr><td>'+this.SlNo+'</td><td>'+this.package_name+'</td><td>'+this.no_of_washes+'</td><td>'+this.purchase_date+'</td><td>'+this.status+'</td></tr>';
          });
          
          
          $("#packages_history_body").html(list_pkg_h);
          
          $("#packages_queue_body").html(list_pkg_q); 
        
        
        
          $('#dataTables-packages_history').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'packages_history'
                },
                {
                    extend: 'pdf',
                    title: 'packages_history'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          });


          $('#dataTables-packages_queue').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'packages_queue'
                },
                {
                    extend: 'pdf',
                    title: 'packages_queue'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          });

          $('#packageModel').modal("show");     
        }
      });
});
</script>


@endsection
