<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class apiLog extends Model
{
    use HasFactory;

    public static function createApiLog($request, $response, $status_code, $type){
        
        $apiLog              = new apiLog();
        $apiLog->request     = $request;
        $apiLog->response    = $response;
        $apiLog->status_code = $status_code;
        $apiLog->api_type    = $type;

        $apiLog->save();

        return $apiLog->id;
    }
}
