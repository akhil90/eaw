@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Add Service</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="ibox">
            <div class="ibox-title">
              <h3>Add Details</h3>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content"> <br>
            <form action="/main_services/store" enctype="multipart/form-data" id="form" method="post" class="form-horizontal form-label-left">
              @csrf
              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Name </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="service_name" id="service_name" placeholder="Enter Service Name" value="{{ old('service_name') }}" class="form-control {{ $errors->has('service_name') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('service_name'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('service_name') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Type</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="service_category" id="service_category" class="form-control {{ $errors->has('service_category') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Service Type --</option>
                       <option @if(old('service_category') == "SINGLE") selected @endif value="SINGLE">Single Wash</option>
                       <option @if(old('service_category') == "ADDON") selected @endif value="ADDON">Addon</option>
                       <option @if(old('service_category') == "CAR_DETAILING") selected @endif value="CAR_DETAILING">Car Detailing</option>
                       <option @if(old('service_category') == "FREE") selected @endif value="FREE">Free</option>
                      </select>
                      @if($errors->has('service_category'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('service_category') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Car</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" @if(old('car_type') == "SEDAN") checked @endif  name="car_type" value="SEDAN"> <i></i> SEDAN </label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" @if(old('car_type') == "SUV") checked @endif  name="car_type" value="SUV"> <i></i> SUV </label>
                        </div>
                      </div>
                    </div>
                      @if($errors->has('car_type'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('car_type') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="price" id="price" placeholder="Enter Price" value="{{ old('price') }}" class="form-control {{ $errors->has('price') ? 'is-invalid' : ''}}">
                      @if($errors->has('price'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tax Type</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="tax_id" id="tax_id" class="form-control {{ $errors->has('tax_id') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Tax Type --</option>
                       @foreach($taxTypes as $taxtype)

                        <option @if($default_tax_type == $taxtype->id) selected @endif  value="{{ $taxtype->id }} value="{{ $taxtype->id  }}">{{ $taxtype->taxTitle }}</option>

                       @endforeach
                      </select>
                      @if($errors->has('tax_id'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('vat') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Duration in Minutes</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="duration" id="duration" placeholder="Enter Duration(Minutes)" value="{{ old('duration') }}" class="form-control {{ $errors->has('duration') ? 'is-invalid' : ''}}">
                      @if($errors->has('duration'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('duration') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Discount Threshold</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="discount_threshold" id="discount_threshold" placeholder="Enter Discount Threshold" value="0" class="form-control {{ $errors->has('discount_threshold') ? 'is-invalid' : ''}}">
                      @if($errors->has('discount_threshold'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('discount_threshold') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Features </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                     <textarea class="form-control {{ $errors->has('features') ? 'is-invalid' : ''}}" id="features" name="features" rows="4" cols="50" style="height:90px;" placeholder="Enter Package Features">{{ old('features') }}</textarea>
                     <b>*Note : Features separate with commas</b>
                     @if($errors->has('features'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('features') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>        
                <div class="form-group text-center">
                  <div class="fm-ctr col-md-12 col-sm-12 col-xs-12"> <a href="/main_services" class="btn btn-w-m btn-default" type="button">Cancel</a>
                    <button class="btn btn-w-m btn-info" type="reset">Reset</button>
                    <button type="submit" class="btn btn-w-m btn-primary has-spinner" id="saveUser">Add Service</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')

<script>

$('button#refresh').on('click', function(){
 $('#refresh').html('<i class="fa fa-refresh fa-spin"></i>');
  $.ajax({
    url: '/main_services/refreshtax',
    type:'GET',
    //data: {},
    success: function(response) {
      $('#tax_id').html(response);
      $('#refresh').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});


</script>


@endsection
