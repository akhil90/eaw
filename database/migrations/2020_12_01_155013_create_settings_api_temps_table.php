<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsApiTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_api_temps', function (Blueprint $table) {
            $table->id();
            $table->string('remote_id')->nullable();
            $table->string('name')->nullable();
            $table->string('status')->nullable();
            $table->enum('api_type', ['ACCOUNT_INCOME', 'ACCOUNT_BANK','PAYMENT_METHOD'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_api_temps');
    }
}
