<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->truncate(); //for cleaning earlier data to avoid duplicate entries
        DB::table('users')->insert([
          'name' => 'the admin user',
          'employee_id'=>'EM0001',
          'email' => 'iamadmin@gmail.com',
          'mobile'=>'9605617676',
          'role' => 'ADMIN',
          'password' => Hash::make('admin@123'),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('users')->insert([
          'name' => 'the staff user',
          'employee_id'=>'EM0002',
          'email' => 'iamastaff@gmail.com',
          'mobile'=>'9605617678',
          'role' => 'STAFF',
          'password' => Hash::make('staff@123'),
          'created_at' => date('Y-m-d H:i:s'),
          'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
