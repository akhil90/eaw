@extends('layouts.outer')

@section('content')
<div class="cr-log middle-box loginscreen animated fadeInDown">
  <div>
    <p>Welcome to Express Auto Wash Dashboard</p>
    <h3>Login</h3>
    <form class="m-t" method="POST" action="{{ route('login') }}">
    @csrf
      <div class="form-group">
        <input id="employee_id" type="text" placeholder="Employee id" class="form-control @error('employee_id') is-invalid @enderror" name="employee_id" value="" required autocomplete="employee_id" autofocus>
        @error('employee_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <div class="form-group">
        <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
      </div>
      <!--<button type="submit" class="btn btn78 btn-primary block full-width m-b">Login</button>--> 
      <button type="submit" class="btn btn78 btn-primary block full-width m-b">
        {{ __('Login') }}
      </button> 
      <!-- <a href="{{ route('register') }}" class="btn btn78 btn-info block full-width m-b">Register</a> -->
      <a href="#"><small>Forgot password?</small></a>
    </form>
    <div class="clgo"><img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid"></div>
    <p class="m-t"> <small> ©2020 All Rights Reserved. <a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </small> </p>
  </div>
</div>
@endsection