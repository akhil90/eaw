@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Add User</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="ibox">
            <div class="ibox-title">
              <h3>Add Details</h3>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content"> <br>
            <form action="/users/store" enctype="multipart/form-data" id="form" method="post" class="form-horizontal form-label-left">
              @csrf
              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Name</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="name" id="name" placeholder="Enter Name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('name'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Email</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="email" name="email" id="email" placeholder="Enter Email" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}">
                      @if($errors->has('email'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gender</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="gender" value="MALE"> <i></i> MALE </label>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="gender" value="FEMALE"> <i></i> FEMALE </label>
                        </div>
                      </div>
                    </div>
                      @if($errors->has('gender'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('gender') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Mobile</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="mobile" id="mobile" placeholder="Enter Mobile" value="{{ old('mobile') }}" class="form-control {{ $errors->has('mobile') ? 'is-invalid' : ''}}">
                      @if($errors->has('mobile'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="password" id="password" autocomplete="off" placeholder="Enter Password" value="{{ old('password') }}" class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}">
                      @if($errors->has('password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Role</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="role" id="role" class="form-control {{ $errors->has('role') ? 'is-invalid' : ''}}">
                       <option value="STAFF">Staff</option>
                       <option value="ADMIN">Admin</option>
                      </select>
                      @if($errors->has('role'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" checked name="status" value="ACTIVE"> <i></i> Active </label>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="status" value="DISABLED"> <i></i> Disabled </label>
                        </div>
                      </div>
                    </div>
                     @if($errors->has('status'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>        
                <div class="form-group text-center">
                  <div class="fm-ctr col-md-12 col-sm-12 col-xs-12"> <a href="/users" class="btn btn-w-m btn-default" type="button">Cancel</a>
                    <button class="btn btn-w-m btn-info" type="reset">Reset</button>
                    <button type="submit" class="btn btn-w-m btn-primary has-spinner" id="saveUser">Add User</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
