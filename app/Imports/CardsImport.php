<?php
  
namespace App\Imports;
  
use App\Models\Card;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
  
class CardsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Card([
            'card_number'           => $row['card_number'],
            'card_secret_number'    => $row['card_secret_number'], 
            'status'                => $row['status'],
        ]);
    }
}