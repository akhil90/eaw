<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MainService;
use App\Models\TaxType;
use App\Models\Settings;

class MembershipPackageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        return view('membership_packages.show')->with('membership_packages', MainService::where('service_category', 'PACKAGE')->get());
    }
    public function create(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        $data['taxTypes'] =  TaxType::getTaxTypes();
        $data['default_tax_type'] =  Settings::getSettingValueByType('default_tax_type');
        return view('membership_packages.create')->with($data);
    }
    public function store(Request $request){
        $data = request()->validate([
            'service_name'          => 'required',
            'car_type'              => 'required',
            'price'                 => 'required',
            'tax_id'                => 'required',
            'duration'              => 'required',
            'no_of_washes'          => 'required',
            'discount_threshold'    => 'required',
            'package_validity'      => 'required',
            'wash_validity'         => 'required',
            'start_from_type'       => 'required',
            'features'              => 'required',
        ]);

        $main_service = new MainService;
        $user = Auth::user();

        if($request->input('package_validity') <= 0 || $request->input('wash_validity') <= 0 || $request->input('no_of_washes') <= 0){
            $request->session()->flash('error','Need package_validity, wash_validity, no_of_washes greater than zero');
            return redirect('/membership_packages');
        }

        $taxType = TaxType::find($request->input('tax_id'));
        
        $main_service->service_name       = $request->input('service_name');
        $main_service->service_category   = "PACKAGE";
        $main_service->car_type           = $request->input('car_type');
        $main_service->price              = $request->input('price');
        $main_service->vat                = $taxType->taxPercentage;
        $main_service->tax_id             = $taxType->id;
        $main_service->duration           = $request->input('duration');
        $main_service->no_of_washes       = $request->input('no_of_washes');
        $main_service->discount_threshold = $request->input('discount_threshold');
        $main_service->package_validity   = $request->input('package_validity');
        $main_service->wash_validity      = $request->input('wash_validity');
        $main_service->start_from_type    = $request->input('start_from_type');
        $main_service->features           = $request->input('features');
        $main_service->created_by         = $user->id;
        $main_service->save();
        $request->session()->flash('success','Package has been added successfully');
        return redirect('/membership_packages');
    }
    public function edit($id){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }
        
        $data['taxTypes'] =  TaxType::getTaxTypes();
        $data['default_tax_type'] =  Settings::getSettingValueByType('default_tax_type');
        $data['main_service'] = MainService::find($id);
        return view('membership_packages.update')->with($data);
    }
    public function update(Request $request){
        $data = request()->validate([
            'service_name'          => 'required',
            'car_type'              => 'required',
            'price'                 => 'required',
            'tax_id'                => 'required',
            'duration'              => 'required',
            'no_of_washes'          => 'required',
            'discount_threshold'    => 'required',
            'package_validity'      => 'required',
            'wash_validity'         => 'required',
            'start_from_type'       => 'required',
            'features'              => 'required',
            'status'                => 'required',
        ]);

        if($request->input('package_validity') <= 0 || $request->input('wash_validity') <= 0 || $request->input('no_of_washes') <= 0){
            $request->session()->flash('error','Need package_validity, wash_validity, no_of_washes greater than zero');
            return redirect('/membership_packages');
        }

        $res = MainService::find($request->id);
        $user = Auth::user();

        $taxType = TaxType::find($request->input('tax_id'));
        
        $res->service_name          = $request->input('service_name');
        $res->car_type              = $request->input('car_type');
        $res->price                 = $request->input('price');
        $res->vat                   = $taxType->taxPercentage;
        $res->tax_id                = $taxType->id;
        $res->duration              = $request->input('duration');
        $res->no_of_washes          = $request->input('no_of_washes');
        $res->discount_threshold    = $request->input('discount_threshold');
        $res->package_validity      = $request->input('package_validity');
        $res->wash_validity         = $request->input('wash_validity');
        $res->start_from_type       = $request->input('start_from_type');
        $res->features              = $request->input('features');
        $res->status                = $request->input('status');
        $res->modified_by           = $user->id;
        $res->save();
        $request->session()->flash('success','Package has been updated successfully');
        return redirect('/membership_packages');
    }
    public function block(Request $request){
        $res    = MainService::find($request->id);
        $user   = Auth::user();
        
        $res->status        = "DISABLED";
        $res->modified_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Package has been blocked successfully');
        return redirect('/membership_packages');
    }
    public function unblock(Request $request){
        $res    = MainService::find($request->id);
        $user   = Auth::user();
        
        $res->status        = "ACTIVE";
        $res->modified_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Package has been unblocked successfully');
        return redirect('/membership_packages');
    }
}
