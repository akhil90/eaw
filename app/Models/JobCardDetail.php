<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use DB;

class JobCardDetail extends Model
{
    use HasFactory;

    protected $table = 'job_card_details';

    protected $fillable = [
        'job_card_id', 'main_service_id','price','discount','gross_price','tax','net_amount','created_at','updated_at'
    ];

    public static function getJobcardDetailsBydJob($job_id){
        $query = DB::table('job_card_details');
        $query->select(
        'job_card_details.created_at',
        'job_cards.id as job_card_id',
        'customers.first_name',
        'customers.last_name',
        'customers.mobile',
        'job_cards.plate_number',
        'main_services.service_name',
        'main_services.service_category',
        'job_cards.payment_type',
        'job_card_details.price',
        'job_card_details.discount',
        'job_card_details.gross_price',
        'job_card_details.tax',
        'job_card_details.net_amount',
        'job_cards.inv_status');
        $query->join('job_cards', 'job_cards.id', '=', 'job_card_details.job_card_id');
        $query->join('main_services', 'main_services.id', '=', 'job_card_details.main_service_id');
        $query->join('customers', 'customers.id', '=', 'job_cards.customer_id');

        $query->where('job_card_details.job_card_id', '=', $job_id);

        $query->orderBy("job_card_details.created_at", "desc");
        return $query->get();
    }
}
