<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCardBuyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_buy_details', function (Blueprint $table) {
            $table->id();
            $table->string('purchase_number')->nullable();
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->unsignedBigInteger('main_service_id');
            $table->foreign('main_service_id')->references('id')->on('main_services')->onDelete('cascade');
            $table->decimal('price', 10, 2)->default(0);
            $table->decimal('discount', 10, 2)->default(0);
            $table->decimal('gross_price', 10, 2)->default(0);
            $table->decimal('tax', 10, 2)->default(0);
            $table->decimal('net_amount', 10, 2)->default(0);
            $table->enum('payment_type', ['SPOT_PAYMENT', 'CREDIT', 'ONLINE'])->nullable();
            $table->enum('inv_status', ['PENDING', 'CREATED','FAILED'])->default('PENDING');
            $table->string('inv_ref')->nullable();
            $table->text('qb_request')->nullable();
            $table->text('qb_response')->nullable();
            $table->text('inv_data')->nullable();
            $table->string('inv_pdf_name')->nullable();
            $table->unsignedBigInteger('created_user_id');
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_buy_details');
    }
}
