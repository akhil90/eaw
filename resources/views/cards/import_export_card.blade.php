@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Cards</h2>
      </div>
      <div class="col-lg-9"> <br>    
        @if($tax_updation_renew_card_status == "not_active")
          <div class="alert alert-danger">
            <strong>Warning!</strong> Card renewal tax not updated.Please update the tax <a href="/main_services/edit/{{$card_main_service_id}}" style="color:green;font-weight:500;font-size:24px" title="Update tax" class="update_card_renew_service btn btn-md" id="update_card_renew_service"><i class="fa fa-cog" aria-hidden="true"></i></a>
          </div>
        @endif 
      </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">

         <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="ibox">
         <div class="ibox-content">
         <div class="blk-wrp1">
              <div class="blk-1">
              <div class="blk-lft">TOTAL</div>  <div class="blk-rht">{{$total}}</div>
              </div>
              <div class="blk-1">
              <div class="blk-lft">ALLOCATED</div>  <div class="blk-rht">{{$allocated}}</div>
              </div>
              <div class="blk-1">
              <div class="blk-lft">ACTIVE</div>  <div class="blk-rht">{{$active}}</div>
              </div>
              <div class="blk-1">
              <div class="blk-lft">DISABLED</div>  <div class="blk-rht">{{$disabled}}</div>
              </div>
              </div>
           </div>
          </div>
          </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="ibox">
            <div class="ibox-content"> 
            <h4>Import / Export Card</h4>

            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Warning!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div id="errorAlert"></div> 

       

            <form action="/import_export_cards/import" enctype="multipart/form-data" id="form_import" method="post" class="form-horizontal form-label-left">
              @csrf


              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">                   
                    
                    <div class="col-md-7 col-sm-7 col-xs-12 row">
                    <label class="col-md-3 col-sm-3 control-label" for="file">File</label>
                    <input type="file" name="file" id="file" class="col-md-9 col-sm-9 form-control {{ $errors->has('file') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('file'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('file') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                    <label class="control-label" for="first-name"></label>
                        <a href="/import_export_cards/download" title="download sample file"  id="download"><u>Sample Excel File</u></a>
                        <button type="submit" class="btn btn-primary has-spinner" title="Import the cards" id="import"><i class="fa fa-upload" aria-hidden="true"></i></button>
                        <a href="/import_export_cards/export" class="btn btn-success has-spinner" title="Export the cards" id="export"><i class="fa fa-save" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>  
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox ">
            <div class="ibox-title">
              <h5>Uploaded Card Details</h5>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th>SNo</th>
                      <th>Card Number</th>
                      <th>Card Secret Number</th>
                      <th>Status</th>
                      <th>Allocated Customer</th>
                      <th>Customer Mobile</th>
                      <th>Last Updated User</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @php($i = 1)
                  @foreach($cards as $card)   
                    @if($card->status == "ACTIVE")
                      @php($color = "warning")
                    @elseif($card->status == "ALLOCATED")   
                      @php($color = "primary")
                    @elseif($card->status == "DISABLED")  
                      @php($color = "danger")
                    @else
                      @php($color = "")
                    @endif 
                    <tr>
                      <td>{{$i}}</td>
                      <td>{{$card->card_number}}</td>
                      <td>{{$card->card_secret_number }}</td>
                      <td><span class="badge badge-pill badge-{{$color}}">{{$card->status}}</span></td>
                      <td>{{$card->first_name}}</td>
                      <td>{{$card->mobile}}</td>
                      <td>{{$card->name}}</td>
                      <td width="10%">
                        @if($card->status == "ACTIVE")                        
                        <a href="/cards/deactivate/{{$card->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this Card">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($card->status == "DISABLED")
                        <a href="/cards/activate/{{$card->id}}" class="btn btn-round btn-primary btn-xs" style="margin: 3px;" title="Unblock this Card">
                            <i class="fa fa-unlock" aria-hidden="true"></i>
                        </a>
                        @endif
                      </td> 
                    
                    </tr>
                    @php($i++)
                    @endforeach      
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="excelShow">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Uploading data</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="table-responsive">
                <table id="excelTable" class="table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>SNo</th>
                    <th>Card_Number</th>
                    <th>Card_secret_number</th>
                    <th>status</th>
                    <th>Db_status</th>
                  </tr>
                  </thead>
                  <tbody id="excel_table_body">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="upload_confirm">Upload</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      </div>
      </div>
    </div>
  </div>
@endsection
@section('script')

<script>

$('button#import').on('click', function(e){
  e.preventDefault();

  // Get form 
  var form = $('#form_import')[0];

  // Create an FormData object 
  var data = new FormData(form);

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }    
  });


  $.ajax({
    url: '/preview/import',
    type:'POST',
    enctype: 'multipart/form-data',
    data: data,
    processData: false,
    contentType: false,
    cache: false,
    timeout: 600000,
    success: function(response) {

          if(response.errors){

            $('#errorAlert').html('<div class="alert alert-danger alert-dismissible fade show" id="errorAlert" role="alert"><strong>Error!</strong>'+response.errors+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            return false;

          }

          var list = '';
          $('#excelTable').DataTable().destroy();
          $.each(response, function() {
            let dbstatus = this.dbstatus;
            let color = '';
            if(dbstatus == "EXISTS"){
              color = "danger"
            }else if(dbstatus == "NOT_EXISTS") {
              color = "primary"
            }
            list += '<tr><td>'+this.sno+'</td><td>'+this.card_number+'</td><td>'+this.card_secret_number+'</td><td>'+this.status+'</td><td><span class="badge badge-pill badge-'+color+'">'+this.dbstatus+'</span></td></tr>';
          });

          $("#excel_table_body").html(list); 
          $('#excelTable').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'ExampleFile'
                },
                {
                    extend: 'pdf',
                    title: 'ExampleFile'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          }); 
          
          $('#excelShow').modal('show');

    }
  });                          
           
});

$("button#upload_confirm").on('click', function(e){
  e.preventDefault();

  $("#form_import").submit();
});


</script>


@endsection
