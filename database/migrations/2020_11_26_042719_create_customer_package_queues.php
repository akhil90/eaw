<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerPackageQueues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_package_queues', function (Blueprint $table) {
            $table->id();
            $table->integer('customer_id')->nullable();
            $table->string('wp_customer_id')->nullable();
            $table->integer('main_service_id')->nullable();
            $table->integer('wp_package_id')->nullable();
            $table->enum('status', ['PENDING', 'ACTIVATED', 'EXPIRED'])->default('PENDING');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_package_queues');
    }
}
