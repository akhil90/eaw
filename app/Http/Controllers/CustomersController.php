<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Customer;
use App\Models\JobCard;
use App\Models\CustomerPackageQueue;
use App\Models\MembershipPackageLog;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }
        
        return view('customers.show')->with('customers', Customer::all());
    }

    
    public function getJobsByCustomer($customer_id){

        $jobcards   = JobCard::where('customer_id', $customer_id)->get();
        $temp      = array();

        foreach($jobcards as $key => $jobcard){

            $t = array();

            $t['SlNo'] = $key+1;
            $t['date'] = date('d/m/Y h:i A',strtotime($jobcard->created_at));
            $t['job_card_number'] = $jobcard->job_card_number;
            $t['job_card_type'] = $jobcard->job_card_type;
            $t['total_price'] = $jobcard->total_price;
            $t['total_discount'] = $jobcard->total_discount;
            $t['gross_price'] = $jobcard->gross_price;
            $t['customer_type'] = $jobcard->customer_type;
            $t['car_type'] = $jobcard->car_type;
            $t['plate_number'] = $jobcard->plate_number;
            $t['earned_points'] = $jobcard->earned_points;
            $t['payment_type'] = $jobcard->payment_type;
            $t['txn_number'] = $jobcard->txn_number;
            $t['inv_status'] = $jobcard->inv_status;
            $t['inv_ref'] = $jobcard->inv_ref;

            if($jobcard->inv_data){
                $t['action'] = '<a href="/jobcard/invoice/'. $jobcard->id .'/JOB" target="new" title="Print Invoice"><i class="fa fa-print" aria-hidden="true"></i></a>';
            }else{
                $t['action'] = "";
            }
           
            
            $temp[]= $t;
        }

        return response()->json(['data' => $temp]);
    }

    public function getPackageDetails($customer_id){
        
        $data      = array();
        $histories = MembershipPackageLog::getPackageHistory($customer_id);

        foreach($histories as $key=>$history){

            $t =array();
            $t['SlNo'] = $key +1;
            $t['package_name'] = $history->service_name;
            $t['remaining_washes'] = $history->service_remaining;
            $t['completed_washes'] = $history->service_completed;
            $t['package_start'] = date('d/m/Y h:i A',strtotime($history->package_purchase_date));
            $t['package_ends'] = date('d/m/Y h:i A',strtotime($history->package_end_date));
            $t['wash_start'] = $history->wash_validity_start ? date('d/m/Y h:i A',strtotime($history->wash_validity_start)) : "NILL";
            $t['wash_ends'] = $history->wash_validity_end ? date('d/m/Y h:i A',strtotime($history->wash_validity_end)) : "NILL";
            $t['status'] = $history->status;

            $data['history'][] = $t;

        }

        $queues = CustomerPackageQueue::getPackageQueue($customer_id);

        foreach($queues as $key=>$queue){

            $t =array();
            $t['SlNo'] = $key +1;
            $t['package_name'] = $queue->service_name;
            $t['no_of_washes'] = $queue->no_of_washes;
            $t['purchase_date'] = date('d/m/Y h:i A',strtotime($queue->created_at));
            $t['status'] = $queue->status;

            $data['queue'][] = $t;

        }

        return response()->json(['data' => $data]);


    }
}
