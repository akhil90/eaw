<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Home extends Model
{
    use HasFactory;

    public static function carwash_details($search){
        $query = DB::table('job_card_details');
        $query->select('job_card_details.created_at','job_cards.id as job_card_id','customers.first_name','customers.last_name','customers.mobile','job_cards.plate_number','main_services.service_name','main_services.service_category','job_cards.payment_type','job_card_details.price','job_card_details.discount','job_card_details.gross_price','job_cards.inv_status');
        $query->join('job_cards', 'job_cards.id', '=', 'job_card_details.job_card_id');
        $query->join('main_services', 'main_services.id', '=', 'job_card_details.main_service_id');
        $query->join('customers', 'customers.id', '=', 'job_cards.customer_id');
        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_card_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_card_details.created_at', '>=', $search['weekly_start_date'])->where('job_card_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_card_details.created_at', '=', $search['month']);
            $query->whereYear('job_card_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_card_details.created_at', '>=', $search['custom_start_date'])->where('job_card_details.created_at', '<=', $search['custome_end_date']);
        }
        $query->orderBy("job_card_details.created_at", "desc");
        return $query->get();
    }
    public static function sales_report($search){
        $query = DB::table('payment_details');
        $query->select('payment_mode',DB::raw('SUM(amount) AS total_price'),DB::raw('SUM(discount) AS total_discount'),DB::raw('SUM(gross_amount) AS gross_price'),DB::raw('SUM(tax) AS tax_total'),DB::raw('SUM(net_amount) AS net_amount_total'));
        if($search['filter_type'] == "Daily"){
            $query->whereDate('created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('created_at', '>=', $search['weekly_start_date'])->where('created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('created_at', '=', $search['month']);
            $query->whereYear('created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('created_at', '>=', $search['custom_start_date'])->where('created_at', '<=', $search['custome_end_date']);
        }
        $query->groupBy("payment_mode");
        return $query->get();
    }
    public static function service_report($search,$type){

        $query = DB::table('job_card_details');
        $query->select('main_services.service_name','main_services.price',DB::raw('COUNT(job_card_details.main_service_id) AS quantity'),DB::raw('SUM(job_card_details.price) AS total_price'),DB::raw('SUM(job_card_details.discount) AS total_discount'),DB::raw('SUM(job_card_details.gross_price) AS gross_price'),DB::raw('SUM(job_card_details.tax) AS tax_total'),DB::raw('SUM(job_card_details.net_amount) AS net_amount_total'));
        $query->join('main_services', 'main_services.id', '=', 'job_card_details.main_service_id');
        if($type == "PACKAGE"){
            // $query->where(function ($q) {
            //     $q->where('main_services.service_category', 'SINGLE')->orWhere('main_services.service_category', 'PACKAGE');
            // });
            $query->where('main_services.service_category', '=', 'PACKAGE');
        }
        if($type == "SINGLE"){
            $query->where('main_services.service_category', '=', 'SINGLE');
        }
        if($type == "ADDON"){
            $query->where('main_services.service_category', '=', 'ADDON');
        }
        if($type == "FREE"){
            $query->where('main_services.service_category', '=', 'FREE');
        }
        if($type == "CAR_DETAILING"){
            $query->where('main_services.service_category', '=', 'CAR_DETAILING');
        }
        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_card_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_card_details.created_at', '>=', $search['weekly_start_date'])->where('job_card_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_card_details.created_at', '=', $search['month']);
            $query->whereYear('job_card_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_card_details.created_at', '>=', $search['custom_start_date'])->where('job_card_details.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('main_services.status', '=', 'ACTIVE');

        $query->groupBy('main_services.service_name','main_services.price');
        return $query->get();
    }
    public static function package_purchase_report($search){

        $query = DB::table('package_purchases');
        $query->select('main_services.service_name','main_services.price',DB::raw('COUNT(package_purchases.main_service_id) AS quantity'),DB::raw('SUM(package_purchases.price) AS total_price'),DB::raw('SUM(package_purchases.discount) AS total_discount'),DB::raw('SUM(package_purchases.gross_price) AS gross_price'),DB::raw('SUM(package_purchases.tax) AS tax_total'),DB::raw('SUM(package_purchases.net_amount) AS net_amount_total'));
        $query->join('main_services', 'main_services.id', '=', 'package_purchases.main_service_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('package_purchases.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('package_purchases.created_at', '>=', $search['weekly_start_date'])->where('package_purchases.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('package_purchases.created_at', '=', $search['month']);
            $query->whereYear('package_purchases.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('package_purchases.created_at', '>=', $search['custom_start_date'])->where('package_purchases.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('main_services.status', '=', 'ACTIVE');

        $query->groupBy('main_services.service_name','main_services.price');
        return $query->get();
    }
    public static function card_renew_report($search){

        $query = DB::table('card_buy_details');
        $query->select('main_services.service_name','main_services.price',DB::raw('COUNT(card_buy_details.main_service_id) AS quantity'),DB::raw('SUM(card_buy_details.price) AS total_price'),DB::raw('SUM(card_buy_details.discount) AS total_discount'),DB::raw('SUM(card_buy_details.gross_price) AS gross_price'),DB::raw('SUM(card_buy_details.tax) AS tax_total'),DB::raw('SUM(card_buy_details.net_amount) AS net_amount_total'));
        $query->join('main_services', 'main_services.id', '=', 'card_buy_details.main_service_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('card_buy_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('card_buy_details.created_at', '>=', $search['weekly_start_date'])->where('card_buy_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('card_buy_details.created_at', '=', $search['month']);
            $query->whereYear('card_buy_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('card_buy_details.created_at', '>=', $search['custom_start_date'])->where('card_buy_details.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('main_services.status', '=', 'ACTIVE');

        $query->groupBy('main_services.service_name','main_services.price');
        return $query->get();
    }
    public static function graph_values_for_sale($sql){
        return DB::select(DB::raw("SELECT `created_at`,`customer_type`,`gross_amount` FROM `payment_details` ".$sql." ORDER BY `created_at`"));
    }

    public static function graph_values_for_count($sql){
        return DB::select(DB::raw("SELECT `created_at`,`customer_type`,`gross_price` FROM `job_cards` ".$sql." ORDER BY `created_at`"));
    }

    public static function car_count_groupby($search,$type){

        $query = DB::table('job_cards');
        $query->select('main_services.id','job_cards.car_type',DB::raw('COUNT(job_cards.id) AS count'));
        $query->join('job_card_details', 'job_card_details.job_card_id', '=', 'job_cards.id');
        $query->join('main_services', 'main_services.id', '=', 'job_card_details.main_service_id');

        if($type == "NON_MEMBER"){
            $query->where('job_cards.customer_type', '=', 'NON_MEMBER');
        }
        if($type == "MEMBER"){
            $query->where('job_cards.customer_type', '=', 'MEMBER');
        }

        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_cards.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_cards.created_at', '>=', $search['weekly_start_date'])->where('job_cards.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_cards.created_at', '=', $search['month']);
            $query->whereYear('job_cards.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_cards.created_at', '>=', $search['custom_start_date'])->where('job_cards.created_at', '<=', $search['custome_end_date']);
        }
        $query->where('main_services.status', '=', 'ACTIVE');
        $query->groupBy('main_services.id','job_cards.car_type');
        return $query->get();

    }

    public static function total_car_count_groupby($search,$type){

        $query = DB::table('job_cards');
        $query->select(DB::raw('COUNT(job_cards.id) AS count'));

        if($type == "SEDAN"){
            $query->where('job_cards.car_type', '=', 'SEDAN');
        }
        if($type == "SUV"){
            $query->where('job_cards.car_type', '=', 'SUV');
        }

        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_cards.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_cards.created_at', '>=', $search['weekly_start_date'])->where('job_cards.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_cards.created_at', '=', $search['month']);
            $query->whereYear('job_cards.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_cards.created_at', '>=', $search['custom_start_date'])->where('job_cards.created_at', '<=', $search['custome_end_date']);
        }
        $data = $query->get();

        return $data[0]->count;

    }

    public static function dupliacteCustomers($search){

        $query = DB::table('job_cards');
        $query->select('customers.id','customers.first_name','customers.email','customers.mobile','customers.type',DB::raw('COUNT(job_cards.customer_id) AS CustomerCount'));
        $query->leftjoin('customers', 'customers.id', '=', 'job_cards.customer_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_cards.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_cards.created_at', '>=', $search['weekly_start_date'])->where('job_cards.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_cards.created_at', '=', $search['month']);
            $query->whereYear('job_cards.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_cards.created_at', '>=', $search['custom_start_date'])->where('job_cards.created_at', '<=', $search['custome_end_date']);
        }

        $query->groupBy('job_cards.customer_id');
        $query->having(DB::raw('COUNT(job_cards.customer_id)'), '>', 1);
        return $query->get();

    }

    public static function dupliacteCustomersJobs($search,$cutomer_id){

        $query = DB::table('job_cards');

        $query->select(
            'customers.id',
            'customers.first_name',
            'job_cards.job_card_number',
            'job_cards.job_card_type',
            'customers.type as customer_type',
            'job_cards.total_price',
            'job_cards.total_discount',
            'job_cards.gross_price',
            'job_cards.total_tax',
            'job_cards.net_amount_total',
            'job_cards.earned_points',
            'job_cards.payment_type',
            'job_cards.car_type',
            'job_cards.inv_status',
            'job_cards.inv_ref',
            'job_cards.created_at'
         );

        $query->leftjoin('customers', 'customers.id', '=', 'job_cards.customer_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('job_cards.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('job_cards.created_at', '>=', $search['weekly_start_date'])->where('job_cards.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('job_cards.created_at', '=', $search['month']);
            $query->whereYear('job_cards.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('job_cards.created_at', '>=', $search['custom_start_date'])->where('job_cards.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('job_cards.customer_id','=',$cutomer_id);

        return $query->get();

    }


    public static function payment_type_sales_report_job_cards($search, $mode){

        $query = DB::table('payment_details');

        $query->select(
            'payment_details.payment_mode',
            'payment_details.concern_id',
            'payment_details.type',
            'payment_details.voucher_no',
            'payment_details.txn_number',
            'payment_details.amount',
            'payment_details.discount',
            'payment_details.gross_amount',
            'payment_details.tax',
            'payment_details.net_amount',
            'payment_details.created_at',
            'job_cards.job_card_number',
            'job_cards.job_card_type',
            'job_cards.customer_type',
            'job_cards.inv_status',
            'job_cards.inv_ref',
            'customers.first_name',
            'customers.email',
            'customers.mobile'
        );
        $query->leftjoin('job_cards', 'job_cards.id', '=', 'payment_details.concern_id');
        $query->leftjoin('customers', 'customers.id', '=', 'job_cards.customer_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('payment_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('payment_details.created_at', '>=', $search['weekly_start_date'])->where('payment_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('payment_details.created_at', '=', $search['month']);
            $query->whereYear('payment_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('payment_details.created_at', '>=', $search['custom_start_date'])->where('payment_details.created_at', '<=', $search['custome_end_date']);
        }


        $query->where('payment_details.payment_mode','=',$mode);
        $query->where('payment_details.type','=','JOB_CARD');
        return $query->get();
    }


    public static function payment_type_sales_report_package_purchase($search, $mode){

        $query = DB::table('payment_details');

        $query->select(
            'payment_details.payment_mode',
            'payment_details.concern_id',
            'payment_details.type',
            'payment_details.voucher_no',
            'payment_details.txn_number',
            'payment_details.amount',
            'payment_details.discount',
            'payment_details.gross_amount',
            'payment_details.tax',
            'payment_details.net_amount',
            'payment_details.created_at',
            'package_purchases.package_purchase_number',
            'package_purchases.inv_status',
            'package_purchases.inv_ref',
            'customers.first_name',
            'customers.email',
            'customers.mobile'
        );
        $query->leftjoin('package_purchases', 'package_purchases.id', '=', 'payment_details.concern_id');
        $query->leftjoin('customers', 'customers.id', '=', 'package_purchases.customer_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('payment_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('payment_details.created_at', '>=', $search['weekly_start_date'])->where('payment_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('payment_details.created_at', '=', $search['month']);
            $query->whereYear('payment_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('payment_details.created_at', '>=', $search['custom_start_date'])->where('payment_details.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('payment_details.payment_mode','=',$mode);
        $query->where('payment_details.type','=','PACKAGE');
        return $query->get();
    }

    public static function payment_type_sales_report_card_renew($search, $mode){

        $query = DB::table('payment_details');

        $query->select(
            'payment_details.payment_mode',
            'payment_details.concern_id',
            'payment_details.type',
            'payment_details.voucher_no',
            'payment_details.txn_number',
            'payment_details.amount',
            'payment_details.discount',
            'payment_details.gross_amount',
            'payment_details.tax',
            'payment_details.net_amount',
            'payment_details.created_at',
            'card_buy_details.purchase_number',
            'card_buy_details.inv_status',
            'card_buy_details.inv_ref',
            'customers.first_name',
            'customers.email',
            'customers.mobile'
        );
        $query->leftjoin('card_buy_details', 'card_buy_details.id', '=', 'payment_details.concern_id');
        $query->leftjoin('customers', 'customers.id', '=', 'card_buy_details.customer_id');

        if($search['filter_type'] == "Daily"){
            $query->whereDate('payment_details.created_at', $search['daily_date']);
        }
        if($search['filter_type'] == "Weekly"){
            $query->where('payment_details.created_at', '>=', $search['weekly_start_date'])->where('payment_details.created_at', '<=', $search['weekly_end_date']);
        }
        if($search['filter_type'] == "Monthly"){
            $query->whereMonth('payment_details.created_at', '=', $search['month']);
            $query->whereYear('payment_details.created_at', '=', $search['year']);
        }
        if($search['filter_type'] == "Custom"){
            $query->where('payment_details.created_at', '>=', $search['custom_start_date'])->where('payment_details.created_at', '<=', $search['custome_end_date']);
        }

        $query->where('payment_details.payment_mode','=',$mode);
        $query->where('payment_details.type','=','CARD');
        return $query->get();
    }

    public static function jobDetailsFromJobId($job_card_id){

        $query = DB::table('job_card_details');

        $query->select(
            'job_card_details.price',
            'job_card_details.discount',
            'job_card_details.gross_price',
            'job_card_details.tax',
            'job_card_details.net_amount',
            'job_card_details.created_at',
            'main_services.service_name'
        );
        $query->leftjoin('main_services', 'main_services.id', '=', 'job_card_details.main_service_id');

        $query->where('job_card_details.job_card_id','=', $job_card_id);
        return $query->get();

    }
}
