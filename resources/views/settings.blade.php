@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Update Settings</h2>
      </div>
      <div class="col-lg-9">  <br>
        @if($tax_updation_renew_card_status == "not_active")
          <div class="alert alert-danger">
            <strong>Warning!</strong> Card renewal tax not updated.Please update the tax <a href="/main_services/edit/{{$card_main_service_id}}" style="color:green;font-weight:500;font-size:24px" title="Update tax" class="update_card_renew_service btn btn-md" id="update_card_renew_service"><i class="fa fa-cog" aria-hidden="true"></i></a>
          </div>
        @endif  
      </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>Update Settings</h3>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content"> <br>
            <form action="/updateSettings" enctype="multipart/form-data" id="form" method="post" class="form-horizontal form-label-left">
              @csrf
              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Loyality Points </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="point" id="point" value="{{$point}}" placeholder="Enter Loyality point" value="{{ old('point') }}" class="form-control {{ $errors->has('point') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('point'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('point') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Cash Payment Code</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <select name="cash_payment_code" id="cash_payment_code" class="form-control {{ $errors->has('cash_payment_code') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Cash Payment Code --</option>
                       @if($bankAccounts)
                          @foreach($paymentMethods as $paymentMethod)

                            <option @if($cash_payment_code == $paymentMethod->remote_id) selected @endif value="{{ $paymentMethod->remote_id  }}">{{ $paymentMethod->name }}-{{ $paymentMethod->remote_id }}</option>

                          @endforeach
                        @endif
                      </select>
                      @if($errors->has('cash_payment_code'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('cash_payment_code') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh_cash_payment" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Card Payment Code</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                      <select name="card_payment_code" id="card_payment_code" class="form-control {{ $errors->has('card_payment_code') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Card Payment Code --</option>
                        @if($bankAccounts)
                          @foreach($paymentMethods as $paymentMethod)

                            <option @if($card_payment_code == $paymentMethod->remote_id) selected @endif value="{{ $paymentMethod->remote_id  }}">{{ $paymentMethod->name }}-{{ $paymentMethod->remote_id }}</option>

                          @endforeach
                        @endif
                      </select>
                      @if($errors->has('card_payment_code'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('card_payment_code') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh_card_payment" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>
                
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Cash Account Code </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="cash_payment_account_code" id="cash_payment_account_code" class="form-control {{ $errors->has('cash_payment_account_code') ? 'is-invalid' : ''}}">
                       <option value="">-- Cash Account Code --</option>
                     @if($bankAccounts)
                       @foreach($bankAccounts as $bankAccount)

                        <option @if($cash_payment_account_code == $bankAccount->remote_id) selected @endif value="{{ $bankAccount->remote_id  }}">{{ $bankAccount->name }}-{{ $bankAccount->remote_id }}</option>

                       @endforeach
                     @endif
                      </select>
                      @if($errors->has('cash_payment_account_code'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('cash_payment_account_code') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh_cash_account" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Card Account Code </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="card_payment_account_code" id="card_payment_account_code" class="form-control {{ $errors->has('cash_payment_account_code') ? 'is-invalid' : ''}}">
                       <option value="">-- Card Account Code --</option>
                    @if($bankAccounts)
                       @foreach($bankAccounts as $bankAccount)

                        <option @if($card_payment_account_code == $bankAccount->remote_id) selected @endif value="{{ $bankAccount->remote_id  }}">{{ $bankAccount->name }}-{{ $bankAccount->remote_id }}</option>

                       @endforeach
                    @endif
                      </select>
                      @if($errors->has('card_payment_account_code'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('card_payment_account_code') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh_card_account" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> Discount Account Code </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="discount_account_code" id="discount_account_code" class="form-control {{ $errors->has('discount_account_code') ? 'is-invalid' : ''}}">
                       <option value="">-- Discount Account Code --</option>
                  @if($incomeAccounts)
                       @foreach($incomeAccounts as $incomeAccount)

                        <option @if($discount_account_code == $incomeAccount->remote_id) selected @endif value="{{ $incomeAccount->remote_id  }}">{{ $incomeAccount->name }}-{{ $incomeAccount->remote_id }}</option>

                       @endforeach
                  @endif
                      </select>
                      @if($errors->has('discount_account_code'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('discount_account_code') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh_discount_account" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Default Tax Type</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="tax_id" id="tax_id" class="form-control {{ $errors->has('tax_id') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Tax Type --</option>
                       @foreach($taxTypes as $taxtype)

                        <option  @if($default_tax_type == $taxtype->id ) selected @endif  value="{{ $taxtype->id }}">{{ $taxtype->taxTitle }}</option>

                       @endforeach
                      </select>
                      @if($errors->has('vat'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tax_id') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Zero Invoice Creation</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="zero_invoice_creation" @if($zero_invoice_creation=="yes") checked @endif value="yes"> <i></i> YES </label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="zero_invoice_creation" @if($zero_invoice_creation == "no") checked @endif value="no"> <i></i> NO </label>
                        </div>
                      </div>
                    </div>
                      @if($errors->has('zero_invoice_creation'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('zero_invoice_creation') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Bank Charge Rate </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="bank_charge_rate" id="bank_charge_rate" value="{{$bank_charge_rate}}" placeholder="Enter Bank Charge Rate" value="{{ old('bank_charge_rate') }}" class="form-control {{ $errors->has('bank_charge_rate') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('bank_charge_rate'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('bank_charge_rate') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Gate Status</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <div class="row">
                        <div class="col-md-3 col-sm-3  col-xs-3">
                          <div class="i-checks">
                            <label><input type="radio" name="gate_status" @if($gate_status == "LOCK") checked @endif value="LOCK"> <i></i>  LOCK</label>
                          </div>
                        </div>
                        <div class="col-md-3  col-sm-3  col-xs-3">
                          <div class="i-checks">
                            <label><input type="radio" name="gate_status" @if($gate_status == "ACTIVE") checked @endif value="ACTIVE"> <i></i>  ACTIVE</label>
                          </div>
                        </div>
                        <div class="col-md-3  col-sm-3  col-xs-3">
                          <div class="i-checks">
                            <label><input type="radio" name="gate_status" @if($gate_status == "DISABLED") checked @endif value="DISABLED"> <i></i>  DISABLED</label>
                          </div>
                        </div>
                      </div>
                      @if($errors->has('gate_status'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('gate_status') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>


                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Total) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_total" id="arabic_total" value="{{$arabic_total}}" placeholder="Enter Arabic (Total)" value="{{ old('arabic_total') }}" class="form-control {{ $errors->has('arabic_total') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_total'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_total') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Discount) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_discount" id="arabic_discount" value="{{$arabic_discount}}" placeholder="Enter Arabic (Discount)" value="{{ old('arabic_discount') }}" class="form-control {{ $errors->has('arabic_discount') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_discount'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_discount') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Goss Amt) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_GrossAmt" id="arabic_GrossAmt" value="{{$arabic_GrossAmt}}" placeholder="Enter Arabic (Goss Amt)" value="{{ old('arabic_GrossAmt') }}" class="form-control {{ $errors->has('arabic_GrossAmt') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_GrossAmt'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_GrossAmt') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (VAT) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_vat" id="arabic_vat" value="{{$arabic_vat}}" placeholder="Enter Arabic (VAT)" value="{{ old('arabic_vat') }}" class="form-control {{ $errors->has('arabic_vat') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_vat'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_vat') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Net Amount) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_NetAmount" id="arabic_NetAmount" value="{{$arabic_NetAmount}}" placeholder="Enter Arabic (Net Amount)" value="{{ old('arabic_NetAmount') }}" class="form-control {{ $errors->has('arabic_NetAmount') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_NetAmount'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_NetAmount') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Payment Method) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_PaymentMethod" id="arabic_PaymentMethod" value="{{$arabic_PaymentMethod}}" placeholder="Enter Arabic (Payment Method)" value="{{ old('arabic_PaymentMethod') }}" class="form-control {{ $errors->has('arabic_PaymentMethod') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_PaymentMethod'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_PaymentMethod') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                 <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Balance) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_Balance" id="arabic_Balance" value="{{$arabic_Balance}}" placeholder="Enter Arabic (Card Amt)" value="{{ old('arabic_Balance') }}" class="form-control {{ $errors->has('arabic_Balance') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_Balance'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_Balance') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Paid Amount) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_PaidAmt" id="arabic_PaidAmt" value="{{$arabic_PaidAmt}}" placeholder="Enter Arabic (Paid Amount)" value="{{ old('arabic_PaidAmt') }}" class="form-control {{ $errors->has('arabic_PaidAmt') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_PaidAmt'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_PaidAmt') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Cash Amt) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_CashAmt" id="arabic_CashAmt" value="{{$arabic_CashAmt}}" placeholder="Enter Arabic (Cash Amt)" value="{{ old('arabic_CashAmt') }}" class="form-control {{ $errors->has('arabic_CashAmt') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_CashAmt'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_CashAmt') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Arabic (Card Amt) </label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <input type="text" name="arabic_CardAmt" id="arabic_CardAmt" value="{{$arabic_CardAmt}}" placeholder="Enter Arabic (Card Amt)" value="{{ old('arabic_CardAmt') }}" class="form-control {{ $errors->has('arabic_CardAmt') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('arabic_CardAmt'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('arabic_CardAmt') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                
                
                
                      
                <div class="form-group text-center">
                  <div class="fm-ctr col-md-12 col-sm-12 col-xs-12"> <a href="/settings" class="btn btn-w-m btn-default" type="button">Cancel</a>
                    <button class="btn btn-w-m btn-info" type="reset">Reset</button>
                    <button type="submit" class="btn btn-w-m btn-primary has-spinner" id="saveUser">Update Settings</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('script')

<script>

$('button#refresh').on('click', function(){
 $('#refresh').html('<i class="fa fa-refresh fa-spin"></i>');
  $.ajax({
    url: '/main_services/refreshtax',
    type:'GET',
    //data: {},
    success: function(response) {
      $('#tax_id').html(response);
      $('#refresh').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});

$('button#refresh_cash_account').on('click', function(){
 $('#refresh_cash_account').html('<i class="fa fa-refresh fa-spin"></i>');
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/settings/refresh',
    type:'POST',
    data: {type:"Bank"},
    success: function(response) {
      $('#cash_payment_account_code').html(response);
      $('#refresh_cash_account').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});

$('button#refresh_card_account').on('click', function(){
 $('#refresh_card_account').html('<i class="fa fa-refresh fa-spin"></i>');
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/settings/refresh',
    type:'POST',
    data: {type:"Bank"},
    success: function(response) {
      $('#card_payment_account_code').html(response);
      $('#refresh_card_account').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});

$('button#refresh_discount_account').on('click', function(){
 $('#refresh_discount_account').html('<i class="fa fa-refresh fa-spin"></i>');
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/settings/refresh',
    type:'POST',
    data: {type:"Income"},
    success: function(response) {
      $('#discount_account_code').html(response);
      $('#refresh_discount_account').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});

$('button#refresh_cash_payment').on('click', function(){
 $('#refresh_cash_payment').html('<i class="fa fa-refresh fa-spin"></i>');
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/settings/refresh',
    type:'POST',
    data: {type:"payment_method"},
    success: function(response) {
      $('#cash_payment_code').html(response);
      $('#refresh_cash_payment').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});

$('button#refresh_card_payment').on('click', function(){
 $('#refresh_card_payment').html('<i class="fa fa-refresh fa-spin"></i>');
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/settings/refresh',
    type:'POST',
    data: {type:"payment_method"},
    success: function(response) {
      $('#card_payment_code').html(response);
      $('#refresh_card_payment').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});


</script>


@endsection
