<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MainServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('main_services')->truncate(); //for cleaning earlier data to avoid duplicate entries
        DB::table('main_services')->insert([
            'service_name' => 'FREE SEDAN WASH',
            'service_category'=>'FREE',
            'car_type'=>'SEDAN',
            'price'=>'0',
            'duration'=>'5',
            'features'=>'Free wash available for the point redumption, It is single free service for sedan',
            'discount_threshold'=>'0',
            'no_of_washes'=>'1',
            'loyality_points'=>'0',
            'vat'=>'5',
            'tax_id'=>'4',
            'created_by'=>'1',
            'modified_by'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('main_services')->insert([
            'service_name' => 'FREE SUV WASH',
            'service_category'=>'FREE',
            'car_type'=>'SUV',
            'price'=>'0',
            'duration'=>'5',
            'features'=>'Free wash available for the point redumption, It is single free service for suv',
            'discount_threshold'=>'0',
            'no_of_washes'=>'1',
            'loyality_points'=>'0',
            'vat'=>'5',
            'tax_id'=>'4',
            'created_by'=>'1',
            'modified_by'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('main_services')->insert([
            'service_name' => 'NON MEMBER SEDAN SINGLE WASH',
            'service_category'=>'SINGLE',
            'car_type'=>'SEDAN',
            'price'=>'30',
            'duration'=>'5',
            'features'=>'Single wash service for non member service, It is using for Sedan type model',
            'discount_threshold'=>'0',
            'no_of_washes'=>'1',
            'loyality_points'=>'9',
            'vat'=>'5',
            'tax_id'=>'4',
            'created_by'=>'1',
            'modified_by'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('main_services')->insert([
            'service_name' => 'NON MEMBER SUV SINGLE WASH',
            'service_category'=>'SINGLE',
            'car_type'=>'SUV',
            'price'=>'35',
            'duration'=>'5',
            'features'=>'Single wash service for non member service, It is using for Sedan type model',
            'discount_threshold'=>'0',
            'no_of_washes'=>'1',
            'loyality_points'=>'9',
            'vat'=>'5',
            'tax_id'=>'4',
            'created_by'=>'1',
            'modified_by'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('main_services')->insert([
            'service_name' => 'NFC-CARD',
            'service_category'=>'NFC_CARD',
            'car_type'=>'SUV',
            'price'=>'100',
            'duration'=>'0',
            'features'=>'NFC card',
            'discount_threshold'=>'0',
            'no_of_washes'=>'0',
            'loyality_points'=>'0',
            'vat'=>'5',
            'tax_id'=>'4',
            'created_by'=>'1',
            'modified_by'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
