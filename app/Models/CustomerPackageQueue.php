<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPackageQueue extends Model
{
    use HasFactory;

    protected $table = 'customer_package_queues';

    protected $fillable = [
        'customer_id', 'wp_customer_id','main_service_id','wp_package_id','status','created_at','updated_at'
    ];


    public static function ActivateQueuePackages($customer_id){

        $active_status = 'DISABLED';

        $customerPackageQueue = CustomerPackageQueue::where('customer_id', '=',  $customer_id)->where('status', '=',  'PENDING')->first();

        if($customerPackageQueue != null){

            $main_service_id    = $customerPackageQueue->main_service_id;
            $service_details    = MainService::find($main_service_id);

            if($service_details){

                $member        = Member::where('customer_id', '=', $customer_id)->first();

                if($member != null){

                    $membership_package_log_id  = $member->membership_package_log_id;
                    $membership_package_logs    = MembershipPackageLog::find($membership_package_log_id);
                    $membership_package_logs->status = "EXPIRED";
                    $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                    $membership_package_logs->save();

                    $package_purchase_date = date('Y-m-d H:i:s');
                    $package_end_date      = date('Y-m-d H:i:s', time() + 86400 * ($service_details->package_validity));

                    try {
                        $input = [
                            'customer_id' => $customer_id,
                            'main_service_id' => $service_details->id,
                            'service_completed' => 0,
                            'service_remaining' => $service_details->no_of_washes,
                            'package_purchase_date' => $package_purchase_date,
                            'package_end_date' => $package_end_date,
                            'wash_validity_start' => null,
                            'wash_validity_end' => null,
                            'status' => 'ACTIVE',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $membershipPackageLog = MembershipPackageLog::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Package Buying, Package log section.');
                    }

                    $member->service_remaining = $service_details->no_of_washes;
                    $member->membership_package_log_id = $membershipPackageLog->id;
                    $member->status = 'ACTIVE';
                    $member->updated_at = date('Y-m-d H:i:s');
                    $member->save();

                    $active_status = "ACTIVE";

                    $customerPackageQueue->status = 'ACTIVATED';
                    $customerPackageQueue->save();

                    return $active_status;

                } else{
                    return $active_status;
                }

            } else{
                return $active_status;
            }

        }

    }


    public static function getPackageQueue($customer_id){

        $query =  CustomerPackageQueue::select([
            'customer_package_queues.id',
            'customer_package_queues.customer_id',
            'customer_package_queues.wp_customer_id',
            'customer_package_queues.main_service_id',
            'customer_package_queues.wp_package_id',
            'customer_package_queues.status',
            'customer_package_queues.created_at',
            'main_services.service_name',
            'main_services.no_of_washes'
        ]);

        $query = $query->leftjoin('customers', 'customers.id', '=', 'customer_package_queues.customer_id');
        $query = $query->leftjoin('main_services', 'main_services.id', '=', 'customer_package_queues.main_service_id');

        $query = $query->Where('customer_package_queues.customer_id',$customer_id);

        $query = $query->orderBy('customer_package_queues.id', 'DESC');
        $history = $query->get();

        return $history;    
    }


}
