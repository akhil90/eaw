<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Redirect;
use App\Models\Customer;
use App\Models\HistoryPoint;
use App\Models\JobCard;
use App\Models\JobCardDetail;
use App\Models\MainService;
use App\Models\RegisteredPlate;
use App\Models\Settings;
use App\Models\Member;
use App\Models\MembershipPackageLog;
use App\Models\Card;
use App\Models\PackagePurchase;
use App\Models\PaymentDetail;
use App\Models\CustomerPackageQueue;
use App\Models\TaxType;
use App\Models\CardBuyDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Session;

class JobCardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role != 'STAFF'){
            return redirect('/home');
        }


        $plates = RegisteredPlate::getAll();
        $memebers = Member::getAll();
        $temp = array();
        $tempPlate = array();
        $tempMem = array();
        $tempCard = array();
        $tempCustomers =array();


        foreach ($plates as $plate) {

            $temp['plate_number'][] = $plate->plate_number;
            $temp['car_color'][] = $plate->car_color;
            $temp['car_make'][] = $plate->car_make;
            $temp['car_model'][] = $plate->car_model;
            $temp['first_name'][] = $plate->first_name;
            $temp['mobile'][] = $plate->mobile;
            $temp['email'][] = $plate->email;
        }

        foreach ($plates as $plate) {

            $r = array();

            $r['plate_number'] = $plate->plate_number;
            $r['car_color'] = $plate->car_color;
            $r['car_make'] = $plate->car_make;
            $r['car_model'] = $plate->car_model;
            $r['first_name'] = $plate->first_name;
            $r['mobile'] = $plate->mobile;
            $r['email'] = $plate->email;
            $r['remaining_points'] = $plate->remaining_points;
            $r['remaining_free_washes'] = floor(($plate->remaining_points) / Settings::getSettingValueByType('point'));

            $tempPlate[] = $r;
        }

        
        foreach ($memebers as $member) {

            $r = array();

            $r['member_id'] = $member->card_number;
            $r['first_name'] = $member->first_name;
            $r['email'] = $member->email;
            $r['mobile'] = $member->mobile;
            $r['package_name'] = $member->service_name;
            $r['completed_washes'] = $member->service_completed;
            $r['remaining_washes'] = $member->service_remaining;
            $r['validity_end'] = date('F j, Y',strtotime($member->package_end_date));
            $r['main_service_id'] = $member->main_service_id;
            
            $tempMem[] = $r;
        }

        $cards = Card::where('status','=','ACTIVE')->get();

        foreach ($cards as $card) {

            $r = array();

            $r['card_number'] = $card->card_number;
            $r['card_id'] = $card->id;
            $r['status'] = $card->status;
            
            
            $tempCard[] = $r;
        }

        $customers = Customer::all();

        foreach ($customers as $customer) {

            $r = array();

            $r['first_name'] = $customer->first_name;
            $r['mobile'] = $customer->mobile;
            $r['email'] = $customer->email;
            $r['type'] = $customer->type;
            
            
            $tempCustomers[] = $r;
        }



        $data = ['data' => $temp, 'customPlateData' => $tempPlate, 'point_needed_for_freewash'=> Settings::getSettingValueByType('point'),'memberDetails'=>$tempMem,'cards'=>$tempCard, 'customerDetails'=>$tempCustomers];

        // echo "<pre>";
        // print_r($tempMem);
        // echo "</pre>";
        // exit();

        return view('jobcard.index')->with($data);
    }

    public function redirectToServicesShow(Request $request)
    {
        Session::forget(['non_member_data', 'submit_type', 'non_memeber_choose_service', 'redeem_plate_number','member_data','memeber_choose_service']);

        if($request->input('howtoknow') != "referedby"){
            Session::forget(['refer_plate_id']);
        }

        if ($request->input('submit_type') == "non_member_common") {

            $validator = Validator::make($request->all(), [

                'plate_number' => 'required',
                //'car_color' => 'required',
                //'car_make' => 'required',
                //'car_model' => 'required',
                'mobile' => 'required',
                //'email' => 'required|email',
            ]);

            $attributeNames = [

                'plate_number' => 'Plate_number',
                //'car_color' => 'car_color',
                //'car_make' => 'car_make',
                //'car_model' => 'car_model',
                'mobile' => 'mobile',
                //'email' => 'email',
            ];

            $validator->setAttributeNames($attributeNames);

            if ($validator->fails()) {

                $data = ['errors' => $validator->errors()];
                return redirect()->back()->withInput()->with($data);
            }

            $non_member_data = $request->all();

            $request->session()->put('non_member_data', $non_member_data);
            $request->session()->put('submit_type', 'non_member_common');

        } else if ($request->input('submit_type') == "redeem_points") {

            $redeem_plate = $request->input('redeem_plate_number');
            $request->session()->put('submit_type', 'redeem_points');
            $request->session()->put('redeem_plate_number', $redeem_plate);

        } else if ($request->input('submit_type') == "member_common"){

            $validator = Validator::make($request->all(), [

                'member_id' => 'required',
                'member_name' => 'required',
                'member_mobile' => 'required',
                //'member_email' => 'required|email',
            ]);

            $attributeNames = [

                'member_id' => 'member id',
                'member_name' => 'member name',
                'member_mobile' => 'member mobile',
                //'member_email' => 'member email',
            ];

            $validator->setAttributeNames($attributeNames);

            if ($validator->fails()) {

                $data = ['errors' => $validator->errors()];
                return redirect()->back()->withInput()->with($data);
            }

            $member_data = $request->all();

            $request->session()->put('member_data', $member_data);
            $request->session()->put('submit_type', 'member_common');

        } else if($request->input('submit_type') == "buy_package"){

            // print_r($request->all());
            // exit;

            // $validator = Validator::make($request->all(), [
            //     'buyer_mobile' => 'required',
            //     'buyer_email' => 'required',
            //     'buyer_name' => 'required',
            // ]);

            // $attributeNames = [

            //     'buyer_mobile' => 'Buyer Mobile',
            //     'buyer_email' => 'Buyer Email',
            //     'buyer_name' => 'Buyer Name',

            // ];

            // $validator->setAttributeNames($attributeNames);

            // if ($validator->fails()) {

            //     $data = ['errors' => $validator->errors()];
            //     return redirect()->back()->withInput()->with($data);
            // }

            $buy_package_data = $request->all();

            $request->session()->put('buy_package_data', $buy_package_data);
            $request->session()->put('submit_type', 'buy_package');

        }

        return redirect('/jobcard/choose');
    }

    public function chooseServiceShow(Request $request)
    {

        $data = $request->session()->all();

        if ($request->session()->has('submit_type')) {

            if ($data['submit_type'] == "non_member_common") {

                $single_services = MainService::where('service_category', 'SINGLE')->where('status', 'ACTIVE')->where('car_type', $data['non_member_data']['car_type'])->get();
                $addons = MainService::where('service_category', 'ADDON')->where('status', 'ACTIVE')->where('car_type', $data['non_member_data']['car_type'])->get();
                $car_detailing = MainService::where('service_category', 'CAR_DETAILING')->where('status', 'ACTIVE')->where('car_type', $data['non_member_data']['car_type'])->get();
                $data = ['single_services' => $single_services, 'addons' => $addons, 'car_detailing'=> $car_detailing, 'submit_type' => $data['submit_type']];

            } else if ($data['submit_type'] == "redeem_points") {

                $single_services = MainService::where('service_category', 'FREE')->where('status', 'ACTIVE')->get();
                $addons = MainService::where('service_category', 'ADDON')->where('status', 'ACTIVE')->get();
                $car_detailing = MainService::where('service_category', 'CAR_DETAILING')->where('status', 'ACTIVE')->get();
                $data = ['single_services' => $single_services, 'addons' => $addons, 'car_detailing'=> $car_detailing, 'submit_type' => $data['submit_type']];

            } else if ($data['submit_type'] == "member_common") {

                $single_services = MainService::find([$data['member_data']['main_service_id']]);
                $addons = MainService::where('service_category', 'ADDON')->where('status', 'ACTIVE')->get();
                $car_detailing = MainService::where('service_category', 'CAR_DETAILING')->where('status', 'ACTIVE')->get();
                $data = ['single_services' => $single_services, 'addons' => $addons, 'car_detailing'=> $car_detailing, 'submit_type' => $data['submit_type']];

            } else if ($data['submit_type'] == "buy_package") {

                $packages = MainService::where('service_category', 'PACKAGE')->where('status', 'ACTIVE')->get();
                $data = ['single_services' => $packages,'addons' => null, 'car_detailing'=> null, 'submit_type' => $data['submit_type']];

            }
        }

        return view('jobcard.choose')->with($data);

    }

    public function redirectToJobCardConfirm(Request $request)
    {
        $data = $request->session()->all();

        $allrequest = $request->all();

        // echo "<pre>";
        // print_r($allrequest);
        // echo "</pre>";
        // exit;


        if(!$request->exists('addon_id')){

            $allrequest['addon_id'] = 0;

        }

        if(!$request->exists('car_detail_id')){

            $allrequest['car_detail_id'] = 0;

        }

        if(isset($data['submit_type'])){

            if ($data['submit_type'] == "non_member_common") {

                $request->session()->put('non_memeber_choose_service', $allrequest);
    
            } else if ($data['submit_type'] == "redeem_points") {
    
                $request->session()->put('non_memeber_choose_service', $allrequest);
    
            } else if ($data['submit_type'] == "member_common") {
    
                $request->session()->put('memeber_choose_service', $allrequest);
    
            } else if ($data['submit_type'] == "buy_package") {
    
                $request->session()->put('choose_package', $request->all());
                return redirect('/jobcard/confirm_buy');
    
            } 

        } else if($request->input('submit_type') == "card_enroll"){
            $card_buy_data = $request->all();
            $request->session()->put('card_buy_data', $card_buy_data);
            $request->session()->put('submit_type', 'card_enroll');
            return redirect('/jobcard/confirm_buy');
        }
        return redirect('/jobcard/confirm');

    }

    public function confirmBuy(Request $request)
    {
        $data = $request->session()->all();
  
        if ($request->session()->has('submit_type')) {

            if ($data['submit_type'] == "buy_package") {

                $buy_package_data = $data['buy_package_data'];

                $customer_id      = $buy_package_data['customer_id'];
                $customer         = Customer::find($customer_id);

                $choose_package = $data['choose_package'];
                $data_service_id = $choose_package['service_id'];

                $mainServices = new MainService();
                $service_details = $mainServices->find($data_service_id);

                $data_pass = ['customer_data' => $customer, 'buy_package_data'=>$buy_package_data, 'service_details' => $service_details,'submit_type' => $data['submit_type']];

                return view('jobcard.buy_package_confirmation')->with($data_pass);

            } else if($data['submit_type'] == "card_enroll"){

                $card_buy_data =  $data['card_buy_data'];

                $mobile =  $card_buy_data['card_mobile'];
                $email  =  $card_buy_data['card_email'];

                if($mobile){
                    $searchTerm = $mobile;
                } else if($email){
                    $searchTerm = $email;
                }
                
                $customer = Customer::query()->where('mobile', 'LIKE', "%{$searchTerm}%")->orWhere('email', 'LIKE', "%{$searchTerm}%")->first();

                $service_details = MainService::where('service_category','NFC_CARD')->first();

                $data_pass = ['customer_data' => $customer, 'card_buy_data'=>$card_buy_data, 'service_details' => $service_details,'submit_type' => $data['submit_type']];

                return view('jobcard.buy_card_confirmation')->with($data_pass);
                

            }
        }
    }

    public function confirmShow(Request $request)
    {

        $data = $request->session()->all();
        $refer_plate = null;
        $addon_details = null;
        $car_detailings = null;
        $data_service_id = null; 
        $service_details = null;

        if ($request->session()->has('submit_type')) {

            if ($data['submit_type'] == "non_member_common") {

                $non_member_data = $data['non_member_data'];
                $non_memeber_choose_service = $data['non_memeber_choose_service'];

                if(isset($non_memeber_choose_service['service_id']))
                     $data_service_id = $non_memeber_choose_service['service_id'];
                

                $mainServices = new MainService();

                if($data_service_id){
                    $service_details = $mainServices->find($data_service_id);
                }
                    
                $addon_ids = $non_memeber_choose_service['addon_id'];
                if($addon_ids){
                    $addon_details = $mainServices->find($addon_ids);
                }

                $car_detail_ids = $non_memeber_choose_service['car_detail_id'];
                if($car_detail_ids){
                    $car_detailings = $mainServices->find($car_detail_ids);
                }
                   

                if ($request->session()->has('refer_plate_id')) {

                    $refer_plate = RegisteredPlate::getAll(array('plate_number' => $data['refer_plate_id']));

                }

                $data_pass = ['non_member_data' => $non_member_data, 'service_details' => $service_details, 'addon_details' => $addon_details, 'car_detailings' => $car_detailings, 'refer_plate_details' => $refer_plate, 'submit_type' => $data['submit_type']];

            } else if ($data['submit_type'] == "redeem_points") {

                if ($request->session()->has('redeem_plate_number')) {

                    $redeem_plate_details = RegisteredPlate::getAll(array('plate_number' => $data['redeem_plate_number']));
                    $non_memeber_choose_service = $data['non_memeber_choose_service'];
                    $data_service_id = $non_memeber_choose_service['service_id'];

                    $mainServices = new MainService();
                    $service_details = $mainServices->find($data_service_id);
                    $addon_ids = $non_memeber_choose_service['addon_id'];
                    if($addon_ids){
                        $addon_details = $mainServices->find($addon_ids);
                    }
                    $car_detail_ids = $non_memeber_choose_service['car_detail_id'];
                    if($car_detail_ids){
                        $car_detailings = $mainServices->find($car_detail_ids);
                    }

                    $data_pass = ['redeem_plate_details' => $redeem_plate_details, 'service_details' => $service_details, 'addon_details' => $addon_details, 'car_detailings' => $car_detailings, 'refer_plate_details' => $refer_plate, 'submit_type' => $data['submit_type']];

                }

            } else if($data['submit_type'] == "member_common"){

                $member_data = $data['member_data'];
                $memeber_choose_service = $data['memeber_choose_service'];
                $data_service_id = $memeber_choose_service['service_id'];

                $mainServices = new MainService();
                $service_details = $mainServices->find($data_service_id);
                $addon_ids = $memeber_choose_service['addon_id'];
                if($addon_ids){
                    $addon_details = $mainServices->find($addon_ids);
                }

                $car_detail_ids = $memeber_choose_service['car_detail_id'];
                if($car_detail_ids){
                    $car_detailings = $mainServices->find($car_detail_ids);
                }

                $data_pass = ['member_data' => $member_data, 'service_details' => $service_details, 'addon_details' => $addon_details, 'car_detailings' => $car_detailings, 'refer_plate_details' => $refer_plate, 'submit_type' => $data['submit_type']];

            }

        }

        return view('jobcard.jobcard_confirmation')->with($data_pass);

    }

    public function referedPlatenumberSessionSave(Request $request)
    {

        $request->session()->put('refer_plate_id', $request->input('refered_plate_number'));

        return response()->json([
            'status' => 'success',
            'refer_id' => $request->input('refered_plate_number'),
            'message' => 'Successfully granted the refered points',
        ]);

    }

    public function savePackage(Request $request)
    {

        $data       = $request->session()->all();
        $input_data = $request->all();

        $user       = Auth::user();

        $cashPaymentMethodCode    = Settings::getSettingValueByType('cash_payment_code');
        $cardPaymentMethodCode    = Settings::getSettingValueByType('card_payment_code');

        $cashDepositToAccountCode = Settings::getSettingValueByType('cash_payment_account_code');
        $cardDepositToAccountCode = Settings::getSettingValueByType('card_payment_account_code');
        $discountAccountCode      = Settings::getSettingValueByType('discount_account_code');


        $buy_package_data = $data['buy_package_data'];

        $customer_id      = $buy_package_data['customer_id'];

        if (!$customer_id) {

            try {
                $input = [
                    'first_name' => $buy_package_data['buyer_name'],
                    'last_name' => null,
                    'email' => $buy_package_data['buyer_email'],
                    'mobile' => $buy_package_data['buyer_mobile'],
                    'address' => null,
                    'type' => 'MEMBER',
                    'how_to_know_type' => null,
                    'how_to_know_desc' => null,
                    'created_by' => $user->id,
                    'modified_by' => $user->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $customer = Customer::create($input);
            } catch (Exception $ex) {
                return redirect('/jobcard')->with('error', 'Error in Job Card creation, Customer section.');
            }

            $responseArray = Customer::createQbCustomer($customer);

            if($responseArray->response_code == 200){

                $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                $customer->updated_at = date('Y-m-d H:i:s');
                $customer->save();
            }

            $customer_id = $customer->id;
        }
        
        MembershipPackageLog::where('customer_id', $customer_id)
          ->where('status', 'ACTIVE')
          ->update(['status' => 'EXPIRED']);

        // Member::where('customer_id', $customer_id)
        //   ->where('status', 'ACTIVE')
        //   ->update(['status' => 'DISABLED']);

        $plate_number    = $buy_package_data['plate_number_hidden'];

        $card_number     = $buy_package_data['card_number'];
        $card            = Card::where('card_number', '=', $card_number)->first();

        $choose_package  = $data['choose_package'];
        $data_service_id = $choose_package['service_id'];

        $mainServices    = new MainService();
        $service_details = $mainServices->find($data_service_id);

        $package_purchase_date = date('Y-m-d H:i:s');
        $package_end_date      = date('Y-m-d H:i:s', time() + 86400 * ($service_details->package_validity));

        try {
            $input = [
                'customer_id' => $customer_id,
                'main_service_id' => $data_service_id,
                'service_completed' => 0,
                'service_remaining' => $service_details->no_of_washes,
                'package_purchase_date' => $package_purchase_date,
                'package_end_date' => $package_end_date,
                'wash_validity_start' => null,
                'wash_validity_end' => null,
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $membershipPackageLog = MembershipPackageLog::create($input);
        } catch (Exception $ex) {
            return redirect('/jobcard')->with('error', 'Error in Package Buying, Package log section.');
        }

        $member       = Member::query()->where('customer_id', '=', $customer_id)->where('card_id', '=', $card->id)->first();
        if($member != null){

            $member->service_remaining = $service_details->no_of_washes;
            $member->membership_package_log_id = $membershipPackageLog->id;
            $member->status = 'ACTIVE';
            $member->updated_at = date('Y-m-d H:i:s');
            $member->save();


        } else {
            try { 
                $input = [
                    'card_number' => $card_number,
                    'customer_id' => $customer_id,
                    'membership_package_log_id' => $membershipPackageLog->id,
                    'service_completed' => 0,
                    'service_remaining' => $service_details->no_of_washes,
                    'card_id' => $card->id,
                    'start_date' => null,
                    'end_date' => null,
                    'status' => 'ACTIVE',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $member = Member::create($input);
            } catch (Exception $ex) {
                return redirect('/jobcard')->with('error', 'Error in Member Creation, Memeber Insertion section.');
            }
        }

        if($card){

            $card->status = "ALLOCATED";
            $card->customer_id = $member->customer_id;
            $card->updated_by = $user->id;
            $card->updated_at = date('Y-m-d H:i:s');
            $card->save();
        }

        $customer = Customer::find($customer_id);

        if($customer){

            $customer->type = "MEMBER";
            $customer->modified_by  = $user->id;
            $customer->updated_at = date('Y-m-d H:i:s');
            $customer->save();

            if($customer->qb_cutomer_id == null){
                $responseArray = Customer::createQbCustomer($customer);
                if($responseArray->response_code == 200){
                    $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                    $customer->updated_at = date('Y-m-d H:i:s');
                    $customer->save();
                }
            }    

        }

        $price                   = $input_data['price'][$data_service_id];
        $discount                = $input_data['discount_amount'][$data_service_id];
        $discount_reduced_amount = $input_data['discount_reduced_amount'][$data_service_id];
        $tax                     = $input_data['tax'][$data_service_id];
        $net_amount              = $input_data['tax_reduced_total'][$data_service_id];

        $total_price    = $input_data['total_price'];
        $discount_total = $input_data['discount_total'];
        $gross_total    = $input_data['discount_reduced_total'];                   

        $payment_type   = $input_data['payment_mode'];

        try { 
            $input = [
                'customer_id' => $customer_id,
                'package_purchase_number' => 'P'.time(),
                'main_service_id' => $data_service_id,
                'price' => $total_price,
                'discount' => $discount_total,
                'gross_price' => $gross_total,
                'tax' => $tax,
                'net_amount' => $net_amount,
                'payment_type' => $payment_type,
                'created_user_id' =>  $user->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $packagePurchase = PackagePurchase::create($input);
        } catch (Exception $ex) {
            return redirect('/jobcard')->with('error', 'Error in Package Purchase, Purchase Insertion section.');
        }

        $dataInv['Job'] = $packagePurchase->package_purchase_number;
        $dataInv['CustomerId'] = $customer->qb_cutomer_id;
        $dataInv['TotalDiscount'] = $discount_total;
        $dataInv['DiscountAccount'] = $discountAccountCode;
        $dataInv['PaymentType'] = $input_data['payment_mode'];
        $dataInv['TotalAmount'] = $gross_total;
        $dataInv['PaymentDetails'] = [];
        $dataInv['Notes'] = "";

        $paymentInv['PaymentMethod'] = "";
        $paymentInv['PaymentValue'] = "";
        $paymentInv['ReferenceNo'] = "";
        $paymentInv['DepositToAccount'] = "";
        $paymentInv['ChargeRate'] = 0; 

        $invoicelineItem['Qty']         = 1;
        $invoicelineItem['Amount']      = $total_price;
        $invoicelineItem['Discount']    = $discount_total;

        $taxType = TaxType::find($service_details->tax_id);

        $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
        $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";
        $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
        $invoicelineItem['Description'] = $service_details->service_category."-".$service_details->service_name;

        $invoiceline[] = $invoicelineItem;
        $dataInv['Invoiceline'] = $invoiceline;                            

        if($payment_type == "SPOT_PAYMENT"){

            $cashNetAmount = $request->input('spotpayCashAmount');
            $cardNetAmount = $request->input('spotpayCardAmount');

            $cardAmount      = 0;
            $cashAmount      = 0;
            $cashDiscount    = 0;
            $cardDiscount    = 0;
            $cashTax         = 0;
            $cardTax         = 0;
            $cashNet         = 0;
            $cardNet         = 0;

            if($gross_total != 0){
                $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
                $cardAmount = $total_price-$cashAmount; //total_price-cashAmount

                $cashDiscount = number_format(($cashNetAmount/$gross_total) * $discount_total, 2, '.', '');
                $cardDiscount = $discount_total - $cashDiscount; //total_disc-CashDisc

                $cashTax = number_format(($cashNetAmount/$gross_total) * $tax, 2, '.', '');
                $cardTax = $tax - $cashTax; 

                $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount, 2, '.', '');
                $cardNet = $net_amount - $cashNet;
            }
                    
        
            if($cardNetAmount != 0){
                $txn_number =  $request->input('txn_number');

                try { 
                    $input = [
                        'concern_id' => $packagePurchase->id,
                        'type' => 'PACKAGE',
                        'customer_id' => $customer->id,
                        'customer_type' => $customer->type,
                        'payment_mode' => 'CARD',
                        'txn_number' => $txn_number,
                        'amount' => $cardAmount,
                        'discount' => $cardDiscount,
                        'gross_amount' => $cardNetAmount,
                        'tax' => $cardTax,
                        'net_amount' => $cardNet,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $paymentDetail = PaymentDetail::create($input);
                } catch (Exception $ex) {
                    return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                }

                $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
                $paymentInv['PaymentValue']     = $cardNetAmount;
                $paymentInv['ReferenceNo']      = $txn_number;
                $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
                $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');

                $dataInv['PaymentDetails'][]    = $paymentInv;                  
            }
            
            if($cashNetAmount != 0){

                try { 
                    $input = [
                        'concern_id' => $packagePurchase->id,
                        'type' => 'PACKAGE',
                        'customer_id' => $customer->id,
                        'customer_type' => $customer->type,
                        'payment_mode' => 'CASH',
                        'txn_number' => null,
                        'amount' => $cashAmount,
                        'discount' => $cashDiscount,
                        'gross_amount' => $cashNetAmount,
                        'tax' => $cashTax,
                        'net_amount' => $cashNet,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $paymentDetail = PaymentDetail::create($input);
                } catch (Exception $ex) {
                    return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                }

                $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
                $paymentInv['PaymentValue']     = $cashNetAmount;
                $paymentInv['ReferenceNo']      = "";
                $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
                $paymentInv['ChargeRate']       = 0;

                $dataInv['PaymentDetails'][]    = $paymentInv;

            }
        } else{

            $voucher_number =  $request->input('voucher_number');

            try { 
                $input = [
                    'concern_id' => $packagePurchase->id,
                    'type' => 'PACKAGE',
                    'customer_id' => $customer->id,
                    'customer_type' => $customer->type,
                    'payment_mode' => $payment_type,
                    'voucher_no' => $voucher_number,
                    'amount' => $total_price,
                    'discount' => $discount_total,
                    'gross_amount' => $gross_total,
                    'tax' => $tax,
                    'net_amount' => $net_amount,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $paymentDetail = PaymentDetail::create($input);
            } catch (Exception $ex) {
                return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
            }

            $dataInv['Notes']             = $voucher_number;
        
        }

        if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){

            $responseArray = PackagePurchase::invoiceCreation($packagePurchase, $dataInv);

        }

        
        
        $request->session()->forget(['buy_package_data', 'submit_type', 'choose_package']);

        if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
            return redirect()->route('invoice', ['id' => $packagePurchase->id,'type'=>'PACKAGE']);
        }
        return redirect('/jobcard')->with('success', 'Successfully Buy the package and activated  #' . $service_details->service_name);
    }

    public function saveJobcard(Request $request)
    {

        $data            = $request->session()->all();
        $refer_plate     = null;
        $addon_details   = null;
        $car_detailings  = null;
        $email           = null;
        $mobile          = null;
        $data_service_id = null;
        $service_details = null;
        $user            = Auth::user();
        $dataInv         = array();
        $paymentInv      = array();
        $invoiceline     = array();

        $cashPaymentMethodCode    = Settings::getSettingValueByType('cash_payment_code');
        $cardPaymentMethodCode    = Settings::getSettingValueByType('card_payment_code');

        $cashDepositToAccountCode = Settings::getSettingValueByType('cash_payment_account_code');
        $cardDepositToAccountCode = Settings::getSettingValueByType('card_payment_account_code');
        $discountAccountCode      = Settings::getSettingValueByType('discount_account_code');

        if ($request->session()->has('submit_type')) {

            if ($data['submit_type'] == "non_member_common") {

                $non_member_data = $data['non_member_data'];
                $non_memeber_choose_service = $data['non_memeber_choose_service'];

                if(isset($non_memeber_choose_service['service_id']))
                    $data_service_id = $non_memeber_choose_service['service_id'];

                $mainServices = new MainService();
                if($data_service_id){
                    $service_details = $mainServices->find($data_service_id);
                }   

                $addon_ids = $non_memeber_choose_service['addon_id'];
                if($addon_ids){
                    $addon_details = $mainServices->find($addon_ids);
                }

                $car_detail_ids = $non_memeber_choose_service['car_detail_id'];
                if($car_detail_ids){
                    $car_detailings = $mainServices->find($car_detail_ids);
                }

                $plate_number = $non_member_data['plate_number'];
                $email = $non_member_data['email'];
                $mobile = $non_member_data['mobile'];

                $registeredPlate = RegisteredPlate::where('plate_number', '=', $plate_number)->first();

                if ($registeredPlate === null) {

                    
                    $customer = Customer::query()->where('mobile', '=', $mobile)->first();
                    if ($customer === null) {

                        try {
                            $input = [
                                'first_name' => $non_member_data['name'],
                                'last_name' => null,
                                'email' => $non_member_data['email'],
                                'mobile' => $non_member_data['mobile'],
                                'address' => null,
                                'type' => 'NON_MEMBER',
                                'how_to_know_type' => $non_member_data['howtoknow'],
                                'how_to_know_desc' => $non_member_data['howtoknowdesc'],
                                'created_by' => $user->id,
                                'modified_by' => $user->id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $customer = Customer::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Job Card creation, Customer section.');
                        }

                        $responseArray = Customer::createQbCustomer($customer);

                        if($responseArray->response_code == 200){

                            $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                            $customer->updated_at = date('Y-m-d H:i:s');
                            $customer->save();
                        }
 

                    }

                    $customer = Customer::query()->where('mobile', '=', $mobile)->first();

                    try {
                        $input = [
                            'customer_id' => $customer->id,
                            'plate_number' => $non_member_data['plate_number'],
                            'total_points_earned' => 0,
                            'remaining_points' => 0,
                            'car_color' => $non_member_data['car_color'],
                            'car_make' => $non_member_data['car_make'],
                            'car_model' => $non_member_data['car_model'],
                            'car_type' => $non_member_data['car_type'],
                            'status' => 'ACTIVE',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $registeredPlate = RegisteredPlate::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, Plate Registration section.');
                    }

                }

                $registeredPlate->car_color = $non_member_data['car_color'];
                $registeredPlate->car_make = $non_member_data['car_make'];
                $registeredPlate->car_model = $non_member_data['car_model'];
                $registeredPlate->car_type = $non_member_data['car_type'];

                $registeredPlate->save();

                $customer = Customer::query()->where('mobile', '=', $mobile)->first();

                if($customer->qb_cutomer_id == null){
                    $responseArray = Customer::createQbCustomer($customer);
                    if($responseArray->response_code == 200){
                        $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                        $customer->updated_at = date('Y-m-d H:i:s');
                        $customer->save();
                    }
                }

                $total_price      = 0;
                $total_discount   = 0;
                $gross_total      = 0;
                $total_tax        = 0;
                $net_amount_total = 0;

                $priceArray     = $request->input('price');
                $discountArray  = $request->input('discount_amount');
                $taxArray       = $request->input('tax');
                $netAmountArray = $request->input('tax_reduced_total');

                if( $request->has('price') ){
                    $total_price = array_sum($priceArray);
                }
                 
                if( $request->has('discount_amount') ){
                    $total_discount = array_sum($discountArray);
                }

                if( $request->has('tax') ){
                    $total_tax = array_sum($taxArray);
                }

                if( $request->has('tax_reduced_total') ){
                    $net_amount_total = array_sum($netAmountArray);
                }
                
                $gross_total  = $total_price - $total_discount;

                $total_jobs_details_array = array();

                if($addon_details){
                    $total_jobs_details_array = $addon_ids;
                }

                if($car_detailings){
                    $total_jobs_details_array = array_merge($total_jobs_details_array, $car_detail_ids);
                }
                array_push($total_jobs_details_array, $data_service_id);

                 if( $request->has('txn_number') ){
                    $txn_number = $request->input('txn_number');
                 } else{
                    $txn_number = null;
                 }

                try {
                    $input = [
                        'job_card_number' => 'JOB' . time(),
                        'job_card_type' => 'SINGLE',
                        'total_price' => $total_price,
                        'total_discount' => $total_discount,
                        'gross_price' => $gross_total,
                        'total_tax' => $total_tax,
                        'net_amount_total' => $net_amount_total,
                        'customer_id' => $customer->id,
                        'customer_type' => 'NON_MEMBER',
                        'car_type' => $non_member_data['car_type'],
                        'plate_number' => $non_member_data['plate_number'],
                        'earned_points' => 1,
                        'payment_type' => $request->input('payment_mode'),
                        'txn_number' => $txn_number,
                        'created_user_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $jobCard = JobCard::create($input);
                } catch (Exception $ex) {
                    return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCard section.');
                }

                $dataInv['Job'] = $jobCard->job_card_number;
                $dataInv['CustomerId'] = $customer->qb_cutomer_id;
                $dataInv['TotalDiscount'] = $total_discount;
                $dataInv['DiscountAccount'] = $discountAccountCode;
                $dataInv['PaymentType'] = $request->input('payment_mode');
                $dataInv['TotalAmount'] = $gross_total;
                $dataInv['PaymentDetails'] = [];
                $dataInv['Notes'] = "";

                $paymentInv['PaymentMethod'] = "";
                $paymentInv['PaymentValue'] = "";
                $paymentInv['ReferenceNo'] = "";
                $paymentInv['DepositToAccount'] = "";
                $paymentInv['ChargeRate'] = "";

                $all_service_details = $mainServices->find($total_jobs_details_array);

                foreach ($all_service_details as $key => $each_servie) {

                    $invoicelineItem = array();

                    $price       =  $priceArray[$each_servie->id];
                    $discount    =  $discountArray[$each_servie->id];
                    $tax         =  $taxArray[$each_servie->id];
                    $net_amount  =  $netAmountArray[$each_servie->id];
                    $gross_price =  $price - $discount;

                    try {
                        $input = [
                            'job_card_id' => $jobCard->id,
                            'main_service_id' => $each_servie->id,
                            'price' => $price,
                            'discount' => $discount,
                            'gross_price' => $gross_price,
                            'tax' => $tax,
                            'net_amount' => $net_amount,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $jobCardDetail = JobCardDetail::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCardDetail section.');
                    }

                    $invoicelineItem['Qty']         = 1;
                    $invoicelineItem['Amount']      = $price;
                    $invoicelineItem['Discount']    = $discount;

                    $taxType = TaxType::find($each_servie->tax_id);

                    $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
                    $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";
                    $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
                    $invoicelineItem['Description'] = $each_servie->service_category."-".$each_servie->service_name;

                    $invoiceline[] = $invoicelineItem;
                }

                $dataInv['Invoiceline'] = $invoiceline;
                $payment_type           = $request->input('payment_mode');

                if($payment_type == "SPOT_PAYMENT"){

                    $cashNetAmount = $request->input('spotpayCashAmount');
                    $cardNetAmount = $request->input('spotpayCardAmount');

                    $cardAmount      = 0;
                    $cashAmount      = 0;
                    $cashDiscount    = 0;
                    $cardDiscount    = 0;
                    $cashTax         = 0;
                    $cardTax         = 0;
                    $cashNet         = 0;
                    $cardNet         = 0;

                    if($gross_total != 0){
                        $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
                        $cardAmount = $total_price-$cashAmount; //total_price-cashAmount

                        $cashDiscount = number_format(($cashNetAmount/$gross_total) * $total_discount, 2, '.', '');
                        $cardDiscount = $total_discount - $cashDiscount; //total_disc-CashDisc

                        $cashTax = number_format(($cashNetAmount/$gross_total) * $total_tax, 2, '.', '');
                        $cardTax = $total_tax - $cashTax; 

                        $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount_total, 2, '.', '');
                        $cardNet = $net_amount_total - $cashNet;
                    }

                    if($cardNetAmount != 0){
                        $txn_number =  $request->input('txn_number');
        
                        try { 
                            $input = [
                                'concern_id' => $jobCard->id,
                                'type' => 'JOB_CARD',
                                'customer_id' => $customer->id,
                                'customer_type' => 'NON_MEMBER',
                                'payment_mode' => 'CARD',
                                'txn_number' => $txn_number,
                                'amount' => $cardAmount,
                                'discount' => $cardDiscount,
                                'gross_amount' => $cardNetAmount,
                                'tax' => $cardTax,
                                'net_amount' => $cardNet,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $paymentDetail = PaymentDetail::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                        }

                        $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
                        $paymentInv['PaymentValue']     = $cardNetAmount;
                        $paymentInv['ReferenceNo']      = $txn_number;
                        $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
                        $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');

                        $dataInv['PaymentDetails'][]    = $paymentInv;
                    }
                    
                    if($cashNetAmount != 0){
        
                        try { 
                            $input = [
                                'concern_id' => $jobCard->id,
                                'type' => 'JOB_CARD',
                                'customer_id' => $customer->id,
                                'customer_type' => 'NON_MEMBER',
                                'payment_mode' => 'CASH',
                                'txn_number' => null,
                                'amount' => $cashAmount,
                                'discount' => $cashDiscount,
                                'gross_amount' => $cashNetAmount,
                                'tax' => $cashTax,
                                'net_amount' => $cashNet,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $paymentDetail = PaymentDetail::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                        }

                        $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
                        $paymentInv['PaymentValue']     = $cashNetAmount;
                        $paymentInv['ReferenceNo']      = "";
                        $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
                        $paymentInv['ChargeRate']       = 0;

                        $dataInv['PaymentDetails'][]    = $paymentInv;
        
                    }
                } else{
        
                    $voucher_number =  $request->input('voucher_number');
        
                    try { 
                        $input = [
                            'concern_id' => $jobCard->id,
                            'type' => 'JOB_CARD',
                            'customer_id' => $customer->id,
                            'customer_type' => 'NON_MEMBER',
                            'payment_mode' => $payment_type,
                            'voucher_no' => $voucher_number,
                            'amount' => $total_price,
                            'discount' => $total_discount,
                            'gross_amount' => $gross_total,
                            'tax' => $total_tax,
                            'net_amount' => $net_amount_total,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $paymentDetail = PaymentDetail::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                    }

                    $dataInv['Notes']             = $voucher_number;
        
                }

                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                    $responseArray = JobCard::invoiceCreation($jobCard, $dataInv);
                }


                if ($request->session()->has('refer_plate_id')) {

                    $refer_plate = RegisteredPlate::where('plate_number', '=', $data['refer_plate_id'])->first();

                    $total_points_earned = $refer_plate->total_points_earned;
                    $remaining_points = $refer_plate->remaining_points;

                    $total_points_earned = $total_points_earned + 9;
                    $remaining_points = $remaining_points + 9;

                    $refer_plate->total_points_earned = $total_points_earned;
                    $refer_plate->remaining_points = $remaining_points;
                    $refer_plate->updated_at = date('Y-m-d H:i:s');
                    $refer_plate->save();

                    $registeredPlate = RegisteredPlate::where('plate_number', '=', $plate_number)->first();

                    $refer_plateId = $refer_plate->id;
                    $registeredPlateId = $registeredPlate->id;

                    try {
                        $input = [
                            'type' => 'CREDIT',
                            'registered_plate_id' => $refer_plate->id,
                            'job_card_id' => $jobCard->id,
                            'recieved_points' => 9,
                            'remaining_points' => $remaining_points,
                            'recieve_type' => 'REFER',
                            'refered_by' => $registeredPlateId,
                            'created_user_id' => $user->id,
                            'narration' => "Credited for a wash for refer the plate -" . $registeredPlate->plate_number,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $historyPoint = HistoryPoint::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, HistoryPoint section.');
                    }

                    $remaining_points = $registeredPlate->remaining_points;

                    try {
                        $input1 = [
                            'type' => 'CREDIT',
                            'registered_plate_id' => $registeredPlate->id,
                            'job_card_id' => $jobCard->id,
                            'recieved_points' => 1,
                            'remaining_points' => $remaining_points,
                            'recieve_type' => 'SINGLE',
                            'refered_by' => $refer_plateId,
                            'created_user_id' => $user->id,
                            'narration' => "Credited for a wash, It is referedby -" . $refer_plate->plate_number,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $historyPoint1 = HistoryPoint::create($input1);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, HistoryPoint section.');
                    }

                    $registeredPlate->referer_id = $refer_plateId;
                    $registeredPlate->save();

                } else {

                    $registeredPlate = RegisteredPlate::where('plate_number', '=', $plate_number)->first();

                    $total_points_earned = $registeredPlate->total_points_earned;
                    $remaining_points = $registeredPlate->remaining_points;

                    $total_points_earned = $total_points_earned + 1;
                    $remaining_points = $remaining_points + 1;

                    $registeredPlate->total_points_earned = $total_points_earned;
                    $registeredPlate->remaining_points = $remaining_points;
                    $registeredPlate->updated_at = date('Y-m-d H:i:s');
                    $registeredPlate->save();

                    try {
                        $input = [
                            'type' => 'CREDIT',
                            'registered_plate_id' => $registeredPlate->id,
                            'job_card_id' => $jobCard->id,
                            'recieved_points' => 1,
                            'remaining_points' => $remaining_points,
                            'recieve_type' => 'SINGLE',
                            'refered_by ' => null,
                            'created_user_id' => $user->id,
                            'narration' => "Credited for a wash",
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $historyPoint = HistoryPoint::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, HistoryPoint section.');
                    }

                }

                $request->session()->forget(['refer_plate_id', 'non_member_data', 'submit_type', 'non_memeber_choose_service']);

                // For open barrier
                Settings::where('type', 'gate_status')->update(['value' => 'UNLOCK']);

                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                    return redirect()->route('invoice', ['id' => $jobCard->id,'type'=>'JOB']);
                }
                return redirect('/jobcard')->with('success', 'Successfully Submitted your JOB  #' . $jobCard->job_card_number);

            } else if ($data['submit_type'] == "redeem_points") {

                if ($request->session()->has('redeem_plate_number')) {

                    $redeem_plate_details = RegisteredPlate::where('plate_number', '=', $data['redeem_plate_number'])->first();

                    $non_memeber_choose_service = $data['non_memeber_choose_service'];
                    $data_service_id = $non_memeber_choose_service['service_id'];

                    $mainServices = new MainService();
                    $service_details = $mainServices->find($data_service_id);
                    $addon_ids = $non_memeber_choose_service['addon_id'];
                    if($addon_ids){
                        $addon_details = $mainServices->find($addon_ids);
                    }
                    $car_detail_ids = $non_memeber_choose_service['car_detail_id'];
                    if($car_detail_ids){
                        $car_detailings = $mainServices->find($car_detail_ids);
                    }

                    $total_price      = 0;
                    $total_discount   = 0;
                    $gross_total      = 0;
                    $total_tax        = 0;
                    $net_amount_total = 0;

                    $priceArray     = $request->input('price');
                    $discountArray  = $request->input('discount_amount');
                    $taxArray       = $request->input('tax');
                    $netAmountArray = $request->input('tax_reduced_total');

                    if( $request->has('price') ){
                        $total_price = array_sum($priceArray);
                    }
                     
                    if( $request->has('discount_amount') ){
                        $total_discount = array_sum($discountArray);
                    }
    
                    if( $request->has('tax') ){
                        $total_tax = array_sum($taxArray);
                    }
    
                    if( $request->has('tax_reduced_total') ){
                        $net_amount_total = array_sum($netAmountArray);
                    }
                    
                    $gross_total  = $total_price - $total_discount;
    
                    $total_jobs_details_array = array();
    
                    if($addon_details){
                        $total_jobs_details_array = $addon_ids;
                    }
                    if($car_detailings){
                        $total_jobs_details_array = array_merge($total_jobs_details_array, $car_detail_ids);
                    }
                    array_push($total_jobs_details_array, $data_service_id);

                    if( $request->has('txn_number')  ){
                        $txn_number = $request->input('txn_number');
                     } else{
                        $txn_number = null;
                     }

                    try {
                        $input = [
                            'job_card_number' => 'JOB' . time(),
                            'job_card_type' => 'FREE',
                            'total_price' => $total_price,
                            'total_discount' => $total_discount,
                            'gross_price' => $gross_total,
                            'total_tax' => $total_tax,
                            'net_amount_total' => $net_amount_total,
                            'customer_id' => $redeem_plate_details->customer_id,
                            'customer_type' => 'NON_MEMBER',
                            'car_type' => $redeem_plate_details->car_type,
                            'plate_number' => $redeem_plate_details->plate_number,
                            'earned_points' => 0,
                            'payment_type' => $request->input('payment_mode'),
                            'txn_number' => $txn_number,
                            'created_user_id' => $user->id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $jobCard = JobCard::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCard section.');
                    }

                    
                    $customer = Customer::find($redeem_plate_details->customer_id);
                    if($customer->qb_cutomer_id == null){
                        $responseArray = Customer::createQbCustomer($customer);
                        if($responseArray->response_code == 200){

                            $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                            $customer->updated_at = date('Y-m-d H:i:s');
                            $customer->save();
                        }
                    }

                    $dataInv['Job'] = $jobCard->job_card_number;
                    $dataInv['CustomerId'] = $customer->qb_cutomer_id;
                    $dataInv['TotalDiscount'] = $total_discount;
                    $dataInv['DiscountAccount'] = $discountAccountCode;
                    $dataInv['PaymentType'] = $request->input('payment_mode');
                    $dataInv['TotalAmount'] = $gross_total;
                    $dataInv['PaymentDetails'] = [];
                    $dataInv['Notes'] = "";

                    $paymentInv['PaymentMethod'] = "";
                    $paymentInv['PaymentValue'] = "";
                    $paymentInv['ReferenceNo'] = "";
                    $paymentInv['DepositToAccount'] = "";
                    $paymentInv['ChargeRate'] = "";

                    $all_service_details = $mainServices->find($total_jobs_details_array);

                    foreach ($all_service_details as $key => $each_servie) {

                        $price       =  $priceArray[$each_servie->id];
                        $discount    =  $discountArray[$each_servie->id];
                        $tax         =  $taxArray[$each_servie->id];
                        $net_amount  =  $netAmountArray[$each_servie->id];
                        $gross_price =  $price - $discount;

                        try {
                            
                            $input = [
                                'job_card_id' => $jobCard->id,
                                'main_service_id' => $each_servie->id,
                                'price' => $price,
                                'discount' => $discount,
                                'gross_price' => $gross_price,
                                'tax' => $tax,
                                'net_amount' => $net_amount,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];

                            $jobCardDetail = JobCardDetail::create($input);

                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCardDetail section.');
                        }

                        $invoicelineItem['Qty']         = 1;
                        $invoicelineItem['Amount']      = $price;
                        $invoicelineItem['Discount']    = $discount;

                        $taxType = TaxType::find($each_servie->tax_id);

                        $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
                        $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";
                        $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
                        $invoicelineItem['Description'] = $each_servie->service_category."-".$each_servie->service_name;

                        $invoiceline[] = $invoicelineItem;
                    }

                    $dataInv['Invoiceline'] = $invoiceline;
                    $payment_type   = $request->input('payment_mode');

                    if($payment_type == "SPOT_PAYMENT"){

                        $cashNetAmount = $request->input('spotpayCashAmount');
                        $cardNetAmount = $request->input('spotpayCardAmount');

                        $cardAmount      = 0;
                        $cashAmount      = 0;
                        $cashDiscount    = 0;
                        $cardDiscount    = 0;
                        $cashTax         = 0;
                        $cardTax         = 0;
                        $cashNet         = 0;
                        $cardNet         = 0;
    
                        if($gross_total != 0){

                            $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
                            $cardAmount = $total_price-$cashAmount; //total_price-cashAmount
        
                            $cashDiscount = number_format(($cashNetAmount/$gross_total) * $total_discount, 2, '.', '');
                            $cardDiscount = $total_discount - $cashDiscount; //total_disc-CashDisc
        
                            $cashTax = number_format(($cashNetAmount/$gross_total) * $total_tax, 2, '.', '');
                            $cardTax = $total_tax - $cashTax; 
        
                            $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount_total, 2, '.', '');
                            $cardNet = $net_amount_total - $cashNet;
                        }
        
                        if($cardNetAmount != 0){
                            $txn_number =  $request->input('txn_number');
            
                            try { 
                                $input = [
                                    'concern_id' => $jobCard->id,
                                    'type' => 'JOB_CARD',
                                    'customer_id' => $redeem_plate_details->customer_id,
                                    'customer_type' => 'NON_MEMBER',
                                    'payment_mode' => 'CARD',
                                    'txn_number' => $txn_number,
                                    'amount' => $cardAmount,
                                    'discount' => $cardDiscount,
                                    'gross_amount' => $cardNetAmount,
                                    'tax' => $cardTax,
                                    'net_amount' => $cardNet,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ];
                                $paymentDetail = PaymentDetail::create($input);
                            } catch (Exception $ex) {
                                return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                            }

                            $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
                            $paymentInv['PaymentValue']     = $cardNetAmount;
                            $paymentInv['ReferenceNo']      = $txn_number;
                            $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
                            $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');
    
                            $dataInv['PaymentDetails'][]    = $paymentInv;
                        }
                        
                        if($cashNetAmount != 0){
            
                            try { 
                                $input = [
                                    'concern_id' => $jobCard->id,
                                    'type' => 'JOB_CARD',
                                    'customer_id' => $redeem_plate_details->customer_id,
                                    'customer_type' => 'NON_MEMBER',
                                    'payment_mode' => 'CASH',
                                    'txn_number' => null,
                                    'amount' => $cashAmount,
                                    'discount' => $cashDiscount,
                                    'gross_amount' => $cashNetAmount,
                                    'tax' => $cashTax,
                                    'net_amount' => $cashNet,
                                    'created_at' => date('Y-m-d H:i:s'),
                                    'updated_at' => date('Y-m-d H:i:s'),
                                ];
                                $paymentDetail = PaymentDetail::create($input);
                            } catch (Exception $ex) {
                                return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                            }

                            $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
                            $paymentInv['PaymentValue']     = $cashNetAmount;
                            $paymentInv['ReferenceNo']      = "";
                            $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
                            $paymentInv['ChargeRate']       = 0;
    
                            $dataInv['PaymentDetails'][]    = $paymentInv;
            
                        }
                    } else{
            
                        $voucher_number =  $request->input('voucher_number');
            
                        try { 
                            $input = [
                                'concern_id' => $jobCard->id,
                                'type' => 'JOB_CARD',
                                'customer_id' => $redeem_plate_details->customer_id,
                                'customer_type' => 'NON_MEMBER',
                                'payment_mode' => $payment_type,
                                'voucher_no' => $voucher_number,
                                'amount' => $total_price,
                                'discount' => $total_discount,
                                'gross_amount' => $gross_total,
                                'tax' => $total_tax,
                                'net_amount' => $net_amount_total,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $paymentDetail = PaymentDetail::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                        }
            
                        $dataInv['Notes']             = $voucher_number;
                    }
    
                    if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                        $responseArray = JobCard::invoiceCreation($jobCard, $dataInv);
                    }

                    $total_points_earned = $redeem_plate_details->total_points_earned;
                    $remaining_points = $redeem_plate_details->remaining_points;
                    $total_points_earned = $total_points_earned - 9;
                    $remaining_points = $remaining_points - 9;

                    $redeem_plate_details->total_points_earned = $total_points_earned;
                    $redeem_plate_details->remaining_points = $remaining_points;
                    $redeem_plate_details->updated_at = date('Y-m-d H:i:s');
                    $redeem_plate_details->save();

                    try {
                        $input = [
                            'type' => 'DEBIT',
                            'registered_plate_id' => $redeem_plate_details->id,
                            'job_card_id' => $jobCard->id,
                            'recieved_points' => 9,
                            'remaining_points' => $remaining_points,
                            'recieve_type' => 'SINGLE',
                            'refered_by ' => null,
                            'created_user_id' => $user->id,
                            'narration' => "Credited for a wash by redeem",
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $historyPoint = HistoryPoint::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, HistoryPoint section.');
                    }

                }
                $request->session()->forget(['refer_plate_id', 'non_member_data', 'submit_type', 'non_memeber_choose_service', 'redeem_plate_number']);
                
                // For open barrier
                Settings::where('type', 'gate_status')->update(['value' => 'UNLOCK']);

                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                    return redirect()->route('invoice', ['id' => $jobCard->id,'type'=>'JOB']);
                }
                return redirect('/jobcard')->with('success', 'Successfully Submitted your JOB  #' . $jobCard->job_card_number);

            } else if ($data['submit_type'] == "member_common") {

                $member_data = $data['member_data'];
                $memeber_choose_service = $data['memeber_choose_service'];
                $data_service_id = $memeber_choose_service['service_id'];

                $mainServices = new MainService();
                $service_details = $mainServices->find($data_service_id);
                $addon_ids = $memeber_choose_service['addon_id'];
                if($addon_ids){
                    $addon_details = $mainServices->find($addon_ids);
                }

                $car_detail_ids = $memeber_choose_service['car_detail_id'];
                if($car_detail_ids){
                    $car_detailings =  $mainServices->find($car_detail_ids);
                }


                $email        = $member_data['member_email'];
                $mobile       = $member_data['member_mobile'];
                $plate_number = $member_data['member_plate_number'];
                $member_card  = $member_data['member_id'];

                $member        = Member::where('card_number', '=', $member_card)->first();

                $registeredPlate = RegisteredPlate::where('plate_number', '=', $plate_number)->first();

                if ($registeredPlate === null) {

                    try {
                        $input = [
                            'customer_id' => $member->customer_id,
                            'plate_number' => $plate_number,
                            'total_points_earned' => 0,
                            'remaining_points' => 0,
                            'car_color' => null,
                            'car_make' => null,
                            'car_model' => null,
                            'car_type' => null,
                            'status' => 'ACTIVE',
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $registeredPlate = RegisteredPlate::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, Plate Registration section.');
                    }

                }

                $package_validity = $service_details->package_validity;
                $wash_validity    = $service_details->wash_validity;
                $start_from_type  = $service_details->start_from_type;

                $start_date = $member->start_date;
                $end_date   = $member->end_date;

                $service_completed_member     = $member->service_completed;
                $service_remaining_member     = $member->service_remaining;

                $membership_package_log_id  = $member->membership_package_log_id;


                $membershipPackageLog   = new MembershipPackageLog();
                $membership_package_logs = $membershipPackageLog->find($membership_package_log_id);

                $service_completed     = $membership_package_logs->service_completed;
                $service_remaining     = $membership_package_logs->service_remaining;
                $package_purchase_date = $membership_package_logs->package_purchase_date;
                $package_end_date      = $membership_package_logs->package_end_date;
                $wash_validity_start   = $membership_package_logs->wash_validity_start;
                $wash_validity_end     = $membership_package_logs->wash_validity_end;

                $today = date('Y-m-d H:i:s');

                if($wash_validity_end == null){

                    if($package_end_date < $today){

                        $member->status = "DISABLED";
                        $member->updated_at = date('Y-m-d H:i:s');
                        $member->save();
                        $membership_package_logs->status = "EXPIRED";
                        $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                        $membership_package_logs->save();

                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, Services for this card completed.');
                    }

                }else {

                    if($wash_validity_end <  $today){

                        $member->status = "DISABLED";
                        $member->updated_at = date('Y-m-d H:i:s');
                        $member->save();
                        $membership_package_logs->status = "EXPIRED";
                        $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                        $membership_package_logs->save();

                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, Services for this card completed.');

                    }

                }

                if($service_remaining == 0 || $service_remaining_member == 0){

                    $member->status = "DISABLED";
                    $member->updated_at = date('Y-m-d H:i:s');
                    $member->save();
                    $membership_package_logs->status = "EXPIRED";
                    $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                    $membership_package_logs->save();

                    return redirect('/jobcard')->with('error', 'Error in Job Card creation, Services for this card completed.');
                }

                $new_service_completed_member = $service_completed_member + 1;
                $new_service_remaining_member = $service_remaining_member -1;

                $member->service_completed = $new_service_completed_member;
                $member->service_remaining = $new_service_remaining_member;

                if($start_date == null && $start_from_type=="FIRST_WASH"){

                    $member->start_date = date('Y-m-d H:i:s');
                    $new_end_date       = date('Y-m-d H:i:s', time() + 86400 * $wash_validity);
                    $member->end_date   = $new_end_date;

                }

                $member->updated_at = date('Y-m-d H:i:s');
                $member->save();

    
                $new_service_completed = $service_completed +1;
                $new_service_remaining = $service_remaining -1;

                $membership_package_logs->service_completed = $new_service_completed;
                $membership_package_logs->service_remaining = $new_service_remaining;
                $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                $membership_package_logs->save();

                if($wash_validity_start == null && $start_from_type=="FIRST_WASH"){

                    $membership_package_logs->wash_validity_start = date('Y-m-d H:i:s');
                    $new_end_date                                 = date('Y-m-d H:i:s', time() + 86400 * $wash_validity);
                    $membership_package_logs->wash_validity_end   = $new_end_date;
                    $membership_package_logs->updated_at          = date('Y-m-d H:i:s');
                    $membership_package_logs->save();

                }

                if($new_service_remaining == 0){

                    $member->status = "DISABLED";
                    $member->updated_at = date('Y-m-d H:i:s');
                    $member->save();
                    $membership_package_logs->status = "EXPIRED";
                    $membership_package_logs->updated_at = date('Y-m-d H:i:s');
                    $membership_package_logs->save();
                }

                $customer = Customer::query()->where('mobile', '=', $mobile)->first();

                $total_price      = 0;
                $total_discount   = 0;
                $gross_total      = 0;
                $total_tax        = 0;
                $net_amount_total = 0;

                $priceArray     = $request->input('price');
                $discountArray  = $request->input('discount_amount');
                $taxArray       = $request->input('tax');
                $netAmountArray = $request->input('tax_reduced_total');

                if( $request->has('price') ){
                    $total_price = array_sum($priceArray);
                }
                 
                if( $request->has('discount_amount') ){
                    $total_discount = array_sum($discountArray);
                }

                if( $request->has('tax') ){
                    $total_tax = array_sum($taxArray);
                }

                if( $request->has('tax_reduced_total') ){
                    $net_amount_total = array_sum($netAmountArray);
                }

                $gross_total  = $total_price - $total_discount;

                $total_jobs_details_array = array();

                if($addon_details){
                    $total_jobs_details_array = $addon_ids;
                }    

                if($car_detailings){
                    $total_jobs_details_array = array_merge($total_jobs_details_array, $car_detail_ids);
                }

                array_push($total_jobs_details_array, $data_service_id);

                if( $request->has('txn_number') ){
                    $txn_number = $request->input('txn_number');
                 } else{
                    $txn_number = null;
                 }

                try {
                    $input = [
                        'job_card_number' => 'JOB' . time(),
                        'job_card_type' => 'MEMBER',
                        'total_price' => $total_price,
                        'total_discount' => $total_discount,
                        'gross_price' => $gross_total,
                        'total_tax' => $total_tax,
                        'net_amount_total' => $net_amount_total,
                        'customer_id' => $customer->id,
                        'customer_type' => 'MEMBER',
                        'car_type' => $service_details->car_type,
                        'plate_number' => $plate_number,
                        'earned_points' => 0,
                        'payment_type' => $request->input('payment_mode'),
                        'txn_number' => $txn_number,
                        'created_user_id' => $user->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                    $jobCard = JobCard::create($input);
                } catch (Exception $ex) {
                    return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCard section.');
                }

                if($customer->qb_cutomer_id == null){
                    $responseArray = Customer::createQbCustomer($customer);
                    if($responseArray->response_code == 200){

                        $customer->qb_cutomer_id = $responseArray->qb_customer_id;
                        $customer->updated_at = date('Y-m-d H:i:s');
                        $customer->save();
                    }
                }               

                $dataInv['Job'] = $jobCard->job_card_number;
                $dataInv['CustomerId'] = $customer->qb_cutomer_id;
                $dataInv['TotalDiscount'] = $total_discount;
                $dataInv['DiscountAccount'] = $discountAccountCode;
                $dataInv['PaymentType'] = $request->input('payment_mode');
                $dataInv['TotalAmount'] = $gross_total;
                $dataInv['PaymentDetails'] = [];
                $dataInv['Notes'] = "";

                $paymentInv['PaymentMethod'] = "";
                $paymentInv['PaymentValue'] = "";
                $paymentInv['ReferenceNo'] = "";
                $paymentInv['DepositToAccount'] = "";
                $paymentInv['ChargeRate'] = "";                

                $all_service_details = $mainServices->find($total_jobs_details_array);

                foreach ($all_service_details as $key => $each_servie) {

                    $price       =  $priceArray[$each_servie->id];
                    $discount    =  $discountArray[$each_servie->id];
                    $tax         =  $taxArray[$each_servie->id];
                    $net_amount  =  $netAmountArray[$each_servie->id];
                    $gross_price =  $price - $discount;

                    if($each_servie->service_category == 'PACKAGE'){
                        $price       =  0;
                        $discount    =  0;
                        $tax         =  0;
                        $net_amount  =  0;
                        $gross_price =  0;
                    }

                    try {
                        $input = [
                            'job_card_id' => $jobCard->id,
                            'main_service_id' => $each_servie->id,
                            'price' => $price,
                            'discount' => $discount,
                            'gross_price' => $gross_price,
                            'tax' => $tax,
                            'net_amount' => $net_amount,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];

                    $jobCardDetail = JobCardDetail::create($input);

                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Job Card creation, JobCardDetail section.');
                    }

                    $invoicelineItem['Qty']         = 1;
                    $invoicelineItem['Amount']      = $price;
                    $invoicelineItem['Discount']    = $discount;

                    $taxType = TaxType::find($each_servie->tax_id);

                    $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
                    $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";  
                    $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
                    $invoicelineItem['Description'] = $each_servie->service_category."-".$each_servie->service_name;

                    $invoiceline[] = $invoicelineItem;                
                }

                $dataInv['Invoiceline'] = $invoiceline;
                $payment_type   = $request->input('payment_mode');

                if($payment_type == "SPOT_PAYMENT"){

                    $cashNetAmount   = $request->input('spotpayCashAmount');
                    $cardNetAmount   = $request->input('spotpayCardAmount');

                    $cardAmount      = 0;
                    $cashAmount      = 0;
                    $cashDiscount    = 0;
                    $cardDiscount    = 0;
                    $cashTax         = 0;
                    $cardTax         = 0;
                    $cashNet         = 0;
                    $cardNet         = 0;

                    if($gross_total != 0){

                        $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
                        $cardAmount = $total_price-$cashAmount; //total_price-cashAmount

                        $cashDiscount = number_format(($cashNetAmount/$gross_total) * $total_discount, 2, '.', '');
                        $cardDiscount = $total_discount - $cashDiscount; //total_disc-CashDisc

                        $cashTax = number_format(($cashNetAmount/$gross_total) * $total_tax, 2, '.', '');
                        $cardTax = $total_tax - $cashTax; 

                        $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount_total, 2, '.', '');
                        $cardNet = $net_amount_total - $cashNet;    

                    }    

                    if($cardNetAmount != 0){
                        $txn_number =  $request->input('txn_number');
        
                        try { 
                            $input = [
                                'concern_id' => $jobCard->id,
                                'type' => 'JOB_CARD',
                                'customer_id' => $customer->id,
                                'customer_type' => 'MEMBER',
                                'payment_mode' => 'CARD',
                                'txn_number' => $txn_number,
                                'amount' => $cardAmount,
                                'discount' => $cardDiscount,
                                'gross_amount' => $cardNetAmount,
                                'tax' => $cardTax,
                                'net_amount' => $cardNet,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $paymentDetail = PaymentDetail::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                        }

                        $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
                        $paymentInv['PaymentValue']     = $cardNetAmount;
                        $paymentInv['ReferenceNo']      = $txn_number;
                        $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
                        $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');

                        $dataInv['PaymentDetails'][]    = $paymentInv;        
                    }
                    
                    if($cashNetAmount != 0){
        
                        try { 
                            $input = [
                                'concern_id' => $jobCard->id,
                                'type' => 'JOB_CARD',
                                'customer_id' => $customer->id,
                                'customer_type' => 'MEMBER',
                                'payment_mode' => 'CASH',
                                'txn_number' => null,
                                'amount' => $cashAmount,
                                'discount' => $cashDiscount,
                                'gross_amount' => $cashNetAmount,
                                'tax' => $cashTax,
                                'net_amount' => $cashNet,
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ];
                            $paymentDetail = PaymentDetail::create($input);
                        } catch (Exception $ex) {
                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                        }

                        $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
                        $paymentInv['PaymentValue']     = $cashNetAmount;
                        $paymentInv['ReferenceNo']      = "";
                        $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
                        $paymentInv['ChargeRate']       = 0;

                        $dataInv['PaymentDetails'][]    = $paymentInv;                
        
                    }
                } else{
        
                    $voucher_number =  $request->input('voucher_number');
        
                    try { 
                        $input = [
                            'concern_id' => $jobCard->id,
                            'type' => 'JOB_CARD',
                            'customer_id' => $customer->id,
                            'customer_type' => 'MEMBER',
                            'payment_mode' => $payment_type,
                            'voucher_no' => $voucher_number,
                            'amount' => $total_price,
                            'discount' => $total_discount,
                            'gross_amount' => $gross_total,
                            'tax' => $total_tax,
                            'net_amount' => $net_amount_total,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s'),
                        ];
                        $paymentDetail = PaymentDetail::create($input);
                    } catch (Exception $ex) {
                        return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                    }
        
                    $dataInv['Notes']             = $voucher_number;
                }

                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                    $responseArray = JobCard::invoiceCreation($jobCard, $dataInv);
                }

                $request->session()->forget(['refer_plate_id', 'member_data', 'submit_type', 'memeber_choose_service', 'redeem_plate_number']);
                
                // For open barrier
                Settings::where('type', 'gate_status')->update(['value' => 'UNLOCK']);

                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                    return redirect()->route('invoice', ['id' => $jobCard->id,'type'=>'JOB']);
                }
                return redirect('/jobcard')->with('success', 'Successfully Submitted your JOB  #' . $jobCard->job_card_number);
                
            }
        }

    }


    public function getPointHistoryByPlate($plate_number){

        $histories = HistoryPoint::getPlatePointHistory($plate_number);
        $temp      = array();

        foreach($histories as $key => $history){

            $t = array();

            $t['SlNo'] = $key+1;
            $t['date'] = date('d/m/Y h:i A',strtotime($history->created_at));
            $t['plate_number'] = $history->plate_number;
            $t['mobile'] = $history->mobile;
            $t['type'] = $history->type;
            $t['recieved_points'] = $history->recieved_points;
            $t['remaining_points'] = $history->remaining_points;
            $t['recieve_type'] = $history->recieve_type;
            $t['narration'] = $history->narration;
            
            $temp[]= $t;
        }

        return response()->json(['data' => $temp]);
    }

    public function nonMemebrSearch($search)
    {

        $plates = RegisteredPlate::getAll(array('plate_number' => $search));

        $temp = array();

        foreach ($plates as $plate) {

            $temp['plate_number'][] = $plate->plate_number;
            $temp['car_color'][] = $plate->car_color;
            $temp['car_make'][] = $plate->car_make;
            $temp['car_model'][] = $plate->car_model;
            $temp['first_name'][] = $plate->first_name;
            $temp['mobile'][] = $plate->mobile;
            $temp['email'][] = $plate->email;

        }

        $data = ['data' => $temp];

        return response()->json(['data' => $data]);
    }

    public function nonMemberSearchByPlateNumber($plateNumber)
    {

        $plates = RegisteredPlate::getAll(array('plate_number' => $plateNumber));

        $temp = array();

        foreach ($plates as $plate) {

            $temp['plate_number'] = $plate->plate_number;
            $temp['car_color'] = $plate->car_color;
            $temp['car_make'] = $plate->car_make;
            $temp['car_model'] = $plate->car_model;
            $temp['car_type'] = $plate->car_type;
            $temp['first_name'] = $plate->first_name;
            $temp['mobile'] = $plate->mobile;
            $temp['email'] = $plate->email;
            $temp['remaining_points'] = $plate->remaining_points;
            $temp['customer_id'] = $plate->customer_id;
        }

        return response()->json($temp);
    }

    public function nonMemberSearchByPhoneNumber($mobile)
    {

        $customer = Customer::where('mobile', '=', $mobile)->first();
      
        $temp = array();

        $temp['first_name'] = $customer->first_name;
        $temp['mobile'] = $customer->mobile;
        $temp['email'] = $customer->email;
        $temp['customer_id'] = $customer->id;

        $plates = RegisteredPlate::getAll(array('customer_id' => $customer->id));

        $temp_plates = array(); 

        foreach ($plates as $plate) {
            
            $t = array();

            $t['plate_number'] = $plate->plate_number;
            $t['car_color'] = $plate->car_color;
            $t['car_make'] = $plate->car_make;
            $t['car_model'] = $plate->car_model;
            $t['car_type'] = $plate->car_type;
            $t['first_name'] = $plate->first_name;
            $t['mobile'] = $plate->mobile;
            $t['email'] = $plate->email;
            $t['remaining_points'] = $plate->remaining_points;
            $t['customer_id'] = $plate->customer_id;

            $temp_plates[] = $t;
        }

        $temp['plate_list'] = $temp_plates;

        return response()->json($temp);
    }

    public function memberSearchByCardNumber($member_id)
    {

        $members = Member::getAll(array('card_number' => $member_id));

        $temp = array();
        $temp_plate_list = array(); 
        $today = date('Y-m-d H:i:s');

        foreach ($members as $member) {

            $temp['member_id'] = $member->card_number;
            $temp['first_name'] = $member->first_name;
            $temp['email'] = $member->email;
            $temp['mobile'] = $member->mobile;
            $temp['package_name'] = $member->service_name;
            $temp['completed_washes'] = $member->service_completed;
            $temp['remaining_washes'] = $member->service_remaining;
            $temp['main_service_id'] = $member->main_service_id;
            $temp['customer_id'] = $member->customer_id;
            $temp['plate_number'] = "";
            $temp['active_status'] = 'ACTIVE';
            $temp['plate_list'] = $temp_plate_list;

            $plates = RegisteredPlate::getAll(array('customer_id' => $member->customer_id));

            if($plates){

                foreach ($plates as $plate) {

                    $t_plate = array();

                    $t_plate['plate_number'] = $plate->plate_number;
                    $t_plate['car_color'] = $plate->car_color;
                    $t_plate['car_make'] = $plate->car_make;
                    $t_plate['car_model'] = $plate->car_model;
                    $t_plate['car_type'] = $plate->car_type;
                    $t_plate['first_name'] = $plate->first_name;
                    $t_plate['mobile'] = $plate->mobile;
                    $t_plate['email'] = $plate->email;
                    $t_plate['remaining_points'] = $plate->remaining_points;

                    $temp_plate_list[] = $t_plate;
                }

                $temp['plate_list'] = $temp_plate_list;
            }

            if($member->service_remaining == 0){

                $active_status = 'DISABLED';

                $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
               
                $temp['active_status'] = $active_status;

                if($active_status == 'ACTIVE'){
                    $membe_new = Member::find($member->id);
                    $temp['completed_washes'] = $membe_new->service_completed;
                    $temp['remaining_washes'] = $membe_new->service_remaining;
                }
            }

            if($member->wash_validity_end == null){

                $temp['validity_end'] = date('F j, Y',strtotime($member->package_end_date));

                if($member->package_end_date < $today){

                    $active_status = 'DISABLED';

                    $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                
                    $temp['active_status'] = $active_status;
                    if($active_status == 'ACTIVE'){
                        $membe_new = Member::find($member->id);
                        $temp['completed_washes'] = $membe_new->service_completed;
                        $temp['remaining_washes'] = $membe_new->service_remaining;
                    }
                }

            } else {

                $temp['validity_end'] = date('F j, Y',strtotime($member->wash_validity_end));

                if($member->wash_validity_end <  $today){

                    $active_status = 'DISABLED';

                    $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                   
                    $temp['active_status'] = $active_status;
                    if($active_status == 'ACTIVE'){
                        $membe_new = Member::find($member->id);
                        $temp['completed_washes'] = $membe_new->service_completed;
                        $temp['remaining_washes'] = $membe_new->service_remaining;
                    }

                }

            }

        }

        return response()->json($temp);
    }

    public function memberSearchByPhoneNumber($mobile)
    {

        $members = Member::getAll(array('mobile' => $mobile));

        $temp = array();
        $temp_plate_list = array(); 
        $today = date('Y-m-d H:i:s');

        foreach ($members as $member) {

            $temp['member_id'] = $member->card_number;
            $temp['first_name'] = $member->first_name;
            $temp['email'] = $member->email;
            $temp['mobile'] = $member->mobile;
            $temp['package_name'] = $member->service_name;
            $temp['completed_washes'] = $member->service_completed;
            $temp['remaining_washes'] = $member->service_remaining;
            $temp['main_service_id'] = $member->main_service_id;
            $temp['customer_id'] = $member->customer_id;
            $temp['plate_number'] = "";
            $temp['active_status'] = 'ACTIVE';
            $temp['plate_list'] = $temp_plate_list;

            $plates = RegisteredPlate::getAll(array('customer_id' => $member->customer_id));

            if($plates){

                foreach ($plates as $plate) {

                    $t_plate = array();

                    $t_plate['plate_number'] = $plate->plate_number;
                    $t_plate['car_color'] = $plate->car_color;
                    $t_plate['car_make'] = $plate->car_make;
                    $t_plate['car_model'] = $plate->car_model;
                    $t_plate['car_type'] = $plate->car_type;
                    $t_plate['first_name'] = $plate->first_name;
                    $t_plate['mobile'] = $plate->mobile;
                    $t_plate['email'] = $plate->email;
                    $t_plate['remaining_points'] = $plate->remaining_points;

                    $temp_plate_list[] = $t_plate;
                }

                $temp['plate_list'] = $temp_plate_list;
            }

            if($member->service_remaining == 0){

                $active_status = 'DISABLED';

                $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
               
                $temp['active_status'] = $active_status;
                
                if($active_status == 'ACTIVE'){
                    $membe_new = Member::find($member->id);
                    $temp['completed_washes'] = $membe_new->service_completed;
                    $temp['remaining_washes'] = $membe_new->service_remaining;
                }
            }

            if($member->wash_validity_end == null){

                $temp['validity_end'] = date('F j, Y',strtotime($member->package_end_date));

                if($member->package_end_date < $today){

                    $active_status = 'DISABLED';

                    $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                
                    $temp['active_status'] = $active_status;

                    if($active_status == 'ACTIVE'){
                        $membe_new = Member::find($member->id);
                        $temp['completed_washes'] = $membe_new->service_completed;
                        $temp['remaining_washes'] = $membe_new->service_remaining;
                    }
                }

            } else {

                $temp['validity_end'] = date('F j, Y',strtotime($member->wash_validity_end));

                if($member->wash_validity_end <  $today){

                    $active_status = 'DISABLED';

                    $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                   
                    $temp['active_status'] = $active_status;

                    if($active_status == 'ACTIVE'){
                        $membe_new = Member::find($member->id);
                        $temp['completed_washes'] = $membe_new->service_completed;
                        $temp['remaining_washes'] = $membe_new->service_remaining;
                    }

                }

            }

        }

        return response()->json($temp);
    }

    public function memberSearchByPlateNumber($plateNumber){

        $registeredPlate = RegisteredPlate::where('plate_number', '=', $plateNumber)->first();
        $customer_id = $registeredPlate->customer_id;

        $members = Member::getAll(array('customer_id' => $customer_id));

        if(!$members->isEmpty()){
            
            $temp = array();
            $today = date('Y-m-d H:i:s');

            foreach ($members as $member) {

                $temp['member_id'] = $member->card_number;
                $temp['first_name'] = $member->first_name;
                $temp['email'] = $member->email;
                $temp['mobile'] = $member->mobile;
                $temp['package_name'] = $member->service_name;
                $temp['completed_washes'] = $member->service_completed;
                $temp['remaining_washes'] = $member->service_remaining;
                $temp['main_service_id'] = $member->main_service_id;
                $temp['customer_id'] = $member->customer_id;
                $temp['plate_number'] = $plateNumber;
                $temp['active_status'] = 'ACTIVE';

                if($member->service_remaining == 0){
                    $active_status = 'DISABLED';

                    $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                
                    $temp['active_status'] = $active_status;

                    if($active_status == 'ACTIVE'){
                        $membe_new = Member::find($member->id);
                        $temp['completed_washes'] = $membe_new->service_completed;
                        $temp['remaining_washes'] = $membe_new->service_remaining;
                    }
                }

                if($member->wash_validity_end == null){

                    $temp['validity_end'] = date('F j, Y',strtotime($member->package_end_date));

                    if($member->package_end_date < $today){

                        $active_status = 'DISABLED';

                        $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                    
                        $temp['active_status'] = $active_status;

                        if($active_status == 'ACTIVE'){
                            $membe_new = Member::find($member->id);
                            $temp['completed_washes'] = $membe_new->service_completed;
                            $temp['remaining_washes'] = $membe_new->service_remaining;
                        }
                    }

                } else {

                    $temp['validity_end'] = date('F j, Y',strtotime($member->wash_validity_end));

                    if($member->wash_validity_end <  $today){

                        $active_status = 'DISABLED';

                        $active_status = CustomerPackageQueue::ActivateQueuePackages($member->customer_id);
                    
                        $temp['active_status'] = $active_status;

                        if($active_status == 'ACTIVE'){
                            $membe_new = Member::find($member->id);
                            $temp['completed_washes'] = $membe_new->service_completed;
                            $temp['remaining_washes'] = $membe_new->service_remaining;
                        }

                    }

                }
            
            }

        } else{

            $customer = Customer::find($customer_id);

            $temp['member_id'] = "";
            $temp['first_name'] = $customer->first_name;
            $temp['email'] = $customer->email;
            $temp['mobile'] = $customer->mobile;
            $temp['package_name'] = "";
            $temp['completed_washes'] = "";
            $temp['remaining_washes'] = "";
            $temp['validity_end'] = "";
            $temp['main_service_id'] = "";
            $temp['customer_id'] = $customer->id;
            $temp['plate_number'] = $plateNumber;
            $temp['active_status'] = 'DISABLED';

        }


        return response()->json($temp);

    }

    public function memberEnrollCard(Request $request)
    {
       
        $data       = $request->session()->all();
        $input_data = $request->all();

        $user       = Auth::user();

        $cashPaymentMethodCode    = Settings::getSettingValueByType('cash_payment_code');
        $cardPaymentMethodCode    = Settings::getSettingValueByType('card_payment_code');

        $cashDepositToAccountCode = Settings::getSettingValueByType('cash_payment_account_code');
        $cardDepositToAccountCode = Settings::getSettingValueByType('card_payment_account_code');
        $discountAccountCode      = Settings::getSettingValueByType('discount_account_code');


        $mobile       = isset($input_data['card_mobile']) ? $input_data['card_mobile'] : "";
        $email        = isset($input_data['card_email']) ? $input_data['card_email'] : "";
        $card_number  = isset($input_data['card_number']) ? $input_data['card_number'] : "";
        $renew_status = $input_data['renew_card'];
         
        if(isset($data['card_buy_data'])){

            $card_buy_data = $data['card_buy_data'];

            $mobile        = $card_buy_data['card_mobile'];
            $email         = $card_buy_data['card_email'];
            $card_number   = $card_buy_data['card_number'];
        }

        if($renew_status =="no"){

            $searchTerm =null;

            if($mobile){
            $searchTerm = $mobile;
            } else if($email){
            $searchTerm = $email;
            }

            if($searchTerm != null){

                $customer = Customer::query()->where('mobile', 'LIKE', "%{$searchTerm}%")->orWhere('email', 'LIKE', "%{$searchTerm}%")->first();

                if($customer != null){

                    $member = Member::where('customer_id', '=', $customer->id)->first();

                    if($member!=null){

                        if($member->card_id == null || $member->status == 'DISABLED'){

                            $card   = Card::where('card_number', '=', $card_number)->first();
                            if($card!=null){

                                $member->card_number = $card->card_number;
                                $member->card_id = $card->id;
                                $member->status = 'ACTIVE';
                                $member->save();

                                $card->status ="ALLOCATED";
                                $card->customer_id = $member->customer_id;
                                $card->updated_by = $user->id;
                                $card->save();

                            } else{
                                return redirect('/jobcard')->with('error', 'Selected Card not Exists or it is Disabled by admin');
                            }

                        }else{
                            return redirect('/jobcard')->with('error', 'Member have a Card with No - '.$member->card_number);
                        }

                    } else{
                        return redirect('/jobcard')->with('error', 'Error in Enroll Card, Member not Exists.');
                    }


                } else {
                    return redirect('/jobcard')->with('error', 'Error in Enroll Card, Customer not Exists.');
                }

            } else {
                return redirect('/jobcard')->with('error', 'Error in Enroll Card, Search not Exists.');
            }
                
            return redirect('/jobcard')->with('success', 'Card Successfully Enrolled to Cutomer with Name:'.$customer->first_name.' mail:'.$customer->email.' Mobile: '.$customer->mobile);

        } else if($renew_status =="yes"){

            $searchTerm =null;

            if($mobile){
            $searchTerm = $mobile;
            } else if($email){
            $searchTerm = $email;
            }

            if($searchTerm != null){

                $customer = Customer::query()->where('mobile', 'LIKE', "%{$searchTerm}%")->orWhere('email', 'LIKE', "%{$searchTerm}%")->first();

                if($customer != null){

                    $member = Member::where('customer_id', '=', $customer->id)->first();

                    if($member!=null){

                        if($member->card_id!=null){

                            $card = Card::find($member->card_id);
                            $card->status ="DISABLED";
                            $card->customer_id = $member->customer_id;
                            $card->updated_by = $user->id;
                            $card->save(); 

                            $card_new   = Card::where('card_number', '=', $card_number)->first();

                            if($card_new!=null){

                                $member->card_number = $card_new->card_number;
                                $member->card_id = $card_new->id;
                                $member->save();

                                $card_new->status ="ALLOCATED";
                                $card_new->customer_id = $member->customer_id;
                                $card_new->updated_by = $user->id;
                                $card_new->save();

                                $service_details = MainService::where('service_category','NFC_CARD')->first();
                                $data_service_id = $service_details->id;
                                $customer_id     = $customer->id;

                                $price                   = $input_data['price'][$data_service_id];
                                $discount                = $input_data['discount_amount'][$data_service_id];
                                $discount_reduced_amount = $input_data['discount_reduced_amount'][$data_service_id];
                                $tax                     = $input_data['tax'][$data_service_id];
                                $net_amount              = $input_data['tax_reduced_total'][$data_service_id];

                                $total_price    = $input_data['total_price'];
                                $discount_total = $input_data['discount_total'];
                                $gross_total    = $input_data['discount_reduced_total'];                   

                                $payment_type   = $input_data['payment_mode'];

                                try { 
                                    $input = [
                                        'customer_id' => $customer_id,
                                        'purchase_number' => 'C'.time(),
                                        'main_service_id' => $data_service_id,
                                        'price' => $total_price,
                                        'discount' => $discount_total,
                                        'gross_price' => $gross_total,
                                        'tax' => $tax,
                                        'net_amount' => $net_amount,
                                        'payment_type' => $payment_type,
                                        'created_user_id' =>  $user->id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s'),
                                    ];
                                    $cardBuy = CardBuyDetail::create($input);
                                } catch (Exception $ex) {
                                    return redirect('/jobcard')->with('error', 'Error in Card Purchase, Purchase Insertion section.');
                                }

                                $dataInv['Job'] = $cardBuy->purchase_number;
                                $dataInv['CustomerId'] = $customer->qb_cutomer_id;
                                $dataInv['TotalDiscount'] = $discount_total;
                                $dataInv['DiscountAccount'] = $discountAccountCode;
                                $dataInv['PaymentType'] = $input_data['payment_mode'];
                                $dataInv['TotalAmount'] = $gross_total;
                                $dataInv['PaymentDetails'] = [];
                                $dataInv['Notes'] = "";

                                $paymentInv['PaymentMethod'] = "";
                                $paymentInv['PaymentValue'] = "";
                                $paymentInv['ReferenceNo'] = "";
                                $paymentInv['DepositToAccount'] = "";
                                $paymentInv['ChargeRate'] = 0; 

                                $invoicelineItem['Qty']         = 1;
                                $invoicelineItem['Amount']      = $total_price;
                                $invoicelineItem['Discount']    = $discount_total;

                                $taxType = TaxType::find($service_details->tax_id);

                                $invoicelineItem['TaxId']       = ($taxType != null) ? $taxType->taxId : "";
                                $invoicelineItem['TaxRateId']   = ($taxType != null) ? $taxType->taxRateId : "";
                                $invoicelineItem['TaxRate']     = ($taxType != null) ? $taxType->taxValue : "";
                                $invoicelineItem['Description'] = $service_details->service_category."-".$service_details->service_name;

                                $invoiceline[] = $invoicelineItem;
                                $dataInv['Invoiceline'] = $invoiceline;                            

                                if($payment_type == "SPOT_PAYMENT"){

                                    $cashNetAmount = $request->input('spotpayCashAmount');
                                    $cardNetAmount = $request->input('spotpayCardAmount');

                                    $cardAmount      = 0;
                                    $cashAmount      = 0;
                                    $cashDiscount    = 0;
                                    $cardDiscount    = 0;
                                    $cashTax         = 0;
                                    $cardTax         = 0;
                                    $cashNet         = 0;
                                    $cardNet         = 0;
                
                                    if($gross_total != 0){
                                        $cashAmount = number_format(($cashNetAmount/$gross_total) * $total_price, 2, '.', '');
                                        $cardAmount = $total_price-$cashAmount; //total_price-cashAmount

                                        $cashDiscount = number_format(($cashNetAmount/$gross_total) * $discount_total, 2, '.', '');
                                        $cardDiscount = $discount_total - $cashDiscount; //total_disc-CashDisc

                                        $cashTax = number_format(($cashNetAmount/$gross_total) * $tax, 2, '.', '');
                                        $cardTax = $tax - $cashTax; 

                                        $cashNet = number_format(($cashNetAmount/$gross_total) * $net_amount, 2, '.', '');
                                        $cardNet = $net_amount - $cashNet;
                                    }
                                            
                                
                                    if($cardNetAmount != 0){
                                        $txn_number =  $request->input('txn_number');

                                        try { 
                                            $input = [
                                                'concern_id' => $cardBuy->id,
                                                'type' => 'CARD',
                                                'customer_id' => $customer->id,
                                                'customer_type' => $customer->type,
                                                'payment_mode' => 'CARD',
                                                'txn_number' => $txn_number,
                                                'amount' => $cardAmount,
                                                'discount' => $cardDiscount,
                                                'gross_amount' => $cardNetAmount,
                                                'tax' => $cardTax,
                                                'net_amount' => $cardNet,
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'updated_at' => date('Y-m-d H:i:s'),
                                            ];
                                            $paymentDetail = PaymentDetail::create($input);
                                        } catch (Exception $ex) {
                                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                                        }

                                        $paymentInv['PaymentMethod']    = $cardPaymentMethodCode;
                                        $paymentInv['PaymentValue']     = $cardNetAmount;
                                        $paymentInv['ReferenceNo']      = $txn_number;
                                        $paymentInv['DepositToAccount'] = $cardDepositToAccountCode;
                                        $paymentInv['ChargeRate']       = Settings::getSettingValueByType('bank_charge_rate');

                                        $dataInv['PaymentDetails'][]    = $paymentInv;                  
                                    }
                                    
                                    if($cashNetAmount != 0){

                                        try { 
                                            $input = [
                                                'concern_id' => $cardBuy->id,
                                                'type' => 'CARD',
                                                'customer_id' => $customer->id,
                                                'customer_type' => $customer->type,
                                                'payment_mode' => 'CASH',
                                                'txn_number' => null,
                                                'amount' => $cashAmount,
                                                'discount' => $cashDiscount,
                                                'gross_amount' => $cashNetAmount,
                                                'tax' => $cashTax,
                                                'net_amount' => $cashNet,
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'updated_at' => date('Y-m-d H:i:s'),
                                            ];
                                            $paymentDetail = PaymentDetail::create($input);
                                        } catch (Exception $ex) {
                                            return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                                        }

                                        $paymentInv['PaymentMethod']    = $cashPaymentMethodCode;
                                        $paymentInv['PaymentValue']     = $cashNetAmount;
                                        $paymentInv['ReferenceNo']      = "";
                                        $paymentInv['DepositToAccount'] = $cashDepositToAccountCode;
                                        $paymentInv['ChargeRate']       = 0;

                                        $dataInv['PaymentDetails'][]    = $paymentInv;

                                    }
                                 } else{

                                    $voucher_number =  $request->input('voucher_number');

                                    try { 
                                        $input = [
                                            'concern_id' => $cardBuy->id,
                                            'type' => 'PACKAGE',
                                            'customer_id' => $customer->id,
                                            'customer_type' => $customer->type,
                                            'payment_mode' => $payment_type,
                                            'voucher_no' => $voucher_number,
                                            'amount' => $total_price,
                                            'discount' => $discount_total,
                                            'gross_amount' => $gross_total,
                                            'tax' => $tax,
                                            'net_amount' => $net_amount,
                                            'created_at' => date('Y-m-d H:i:s'),
                                            'updated_at' => date('Y-m-d H:i:s'),
                                        ];
                                        $paymentDetail = PaymentDetail::create($input);
                                    } catch (Exception $ex) {
                                        return redirect('/jobcard')->with('error', 'Error in Payment Insertion section.');
                                    }

                                    $dataInv['Notes']             = $voucher_number;
                                
                                }

                                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){

                                    $responseArray = CardBuyDetail::invoiceCreation($cardBuy, $dataInv);

                                }

                                $request->session()->forget(['buy_package_data', 'submit_type', 'choose_package']);

                                if(($gross_total == 0 && Settings::getSettingValueByType('zero_invoice_creation') == 'yes') || $gross_total > 0){
                                    return redirect()->route('invoice', ['id' => $cardBuy->id,'type'=>'CARD_BUY']);
                                }

                            } else{
                                return redirect('/jobcard')->with('error', 'Selected Card not Exists or it is Disabled by admin');
                            }
                            
                            
                        } else{
                            return redirect('/jobcard')->with('error', 'Error in Renew Card, Member have no cards.');
                        }

                    } else{
                        return redirect('/jobcard')->with('error', 'Error in Renew Card, Member not Exists.');
                    }
                } else{
                    return redirect('/jobcard')->with('error', 'Error in Renew Card, Customer not Exists.');
                }
            }

            return redirect('/jobcard')->with('success', 'Card Successfully renewed to Cutomer with Name:'.$customer->first_name.' mail:'.$customer->email.' Mobile: '.$customer->mobile);

        }

        

    }


    public function editCustomer(Request $request){

        $customer_id = $request->input('id');
        $field       = $request->input('field');
        $value       = $request->input('value');

        if($customer_id != ""){

            $customer = Customer::find($customer_id);

            $customer->$field = $value;
            $customer->save();

        }

        return response()->json(array('status'=>"Success",'value' => $value));

    }

    public function cardEnrolledStatusWithPhoneNumber($mobile){

        $card_number = null;
        $card_status = "NOT_EXISTS";
        $member_name = "";

        $customer = Customer::query()->where('mobile', '=', $mobile)->first();

            if($customer != null){

                $member = Member::where('customer_id', '=', $customer->id)->first();

                if($member!=null){

                    if($member->card_id != null){

 
                        $card_number = $member->card_number;
                        $card_status = "EXISTS";
                        $member_name = $customer->first_name;


                    }
                }
            }


        return response()->json(array('status'=>"Success",'card_number' => $card_number,'card_status'=> $card_status,'member_name'=> $member_name));

    }

    public function isCustomerExists($mobile){

        $customer_exits = "NOT_EXISTS";
        $customer = Customer::query()->where('mobile', '=', $mobile)->first();
        if($customer != null){
            $customer_exits = "EXISTS";
        }

        return response()->json(array('status'=>"Success",'mobile' => $mobile,'customer_exits'=> $customer_exits));

    }

    public function gateStatus(){

        $gate_status = Settings::getSettingValueByType('gate_status');

        return response()->json(array('status'=>"Success",'gate_status' => $gate_status));

    }

    public function changeGateStatus($current_gate_status){
        if($current_gate_status == "ACTIVE"){

            Settings::where('type', 'gate_status')->update(['value' => 'DISABLED']);

        }else if($current_gate_status == "DISABLED"){

            Settings::where('type', 'gate_status')->update(['value' => 'ACTIVE']);

        }
        $gate_status = Settings::getSettingValueByType('gate_status');

        return response()->json(array('status'=>"Success",'gate_status' => $gate_status));
    }

    
}
