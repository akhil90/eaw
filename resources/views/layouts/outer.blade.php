  <!DOCTYPE html>
  <html style="height:100%;">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Express Auto Wash</title>
  <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
  <link href="{{ asset('assets/css/eaw-style.css') }}" rel="stylesheet">
  </head>
  <body class="gray-bg logbg" style="height:100%;">
      @yield('content')
  </body>

  

  <!-- Mainly scripts --> 
  <script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script> 
  <script src="{{ asset('assets/js/popper.min.js') }}"></script> 
  <script src="{{ asset('assets/js/bootstrap.js') }}"></script> 
  <!-- <script src="{{ asset('assets/js/plugins/typehead/bootstrap3-typeahead.min.js') }}"></script> -->
  <script src="{{ asset('assets/js/handlebars.js') }}"></script>
  <script src="{{ asset('assets/js/jquery.xdomainrequest.min.js') }}"></script>
  <script src="{{ asset('assets/js/typeahead.bundle.js') }}"></script>
  <script src="{{ asset('assets/js/typeahead.main.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script>
  <script src="{{ asset('assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
  <script type="text/javascript">

$('button.barrier_status').on('click', function(e) {
      e.preventDefault();
      let status = $(this).data('gate_status');
      $('#confirm').modal({
          backdrop: 'static',
          keyboard: false
      })
      .on('click', '#change', function(e) {
        $.ajax({
          url: '/jobcard/change_gate_status/'+status,
          type:'GET',
          dataType:'json',
          //data: {},
          success: function(response) {
            if(response.gate_status == 'ACTIVE'){
              $('#gate_active_buttton').show()
              $('#gate_disable_buttton').hide()
            }else{
              $('#gate_active_buttton').hide()
              $('#gate_disable_buttton').show()
            }
          }
        });
      });
      $("#cancel").on('click',function(e){
       e.preventDefault();
       $('#confirm').modal.model('hide');
      });
  });

  function gate_status(){

    $.ajax({
          url: '/jobcard/gate_status',
          type:'GET',
          dataType:'json',
          //data: {},
          success: function(response) {
            if(response.gate_status == 'ACTIVE'){
              $('#gate_active_buttton').show()
              $('#gate_disable_buttton').hide()
            }else{
              $('#gate_active_buttton').hide()
              $('#gate_disable_buttton').show()
            }
          }
        });

  }

    function formatDate(d) {
        minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
        hours   = d.getHours();
        hours   = (hours % 12) || 12;
        hours   = hours.toString().length == 1 ? '0'+hours : hours;
        seconds = d.getSeconds().toString().length == 1 ? '0'+d.getSeconds() : d.getSeconds();
        ampm    = d.getHours() >= 12 ? 'PM' : 'AM';

        months  = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
        days    = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        
        return days[d.getDay()]+' '+months[d.getMonth()]+' '+d.getDate()+' '+d.getFullYear()+' '+ hours + ':' + minutes + ':' + seconds + ' ' + ampm;
    }


    function display_c(){
      var refresh=1000; // Refresh rate in milli seconds
      mytime=setTimeout('display_ct()',refresh)
    }

    function display_ct() {
      var x = new Date();
      var e = formatDate(x);
      document.getElementById('ct').innerHTML = e;
      display_c();
    }

    $(document).ready(function () {
      
    gate_status();
      display_ct();

      $('input[name="intervaltype"]').click(function () {
          $(this).tab('show');
          $(this).removeClass('active');
      });


    });
  </script>
  

   @yield('script')

  </html>