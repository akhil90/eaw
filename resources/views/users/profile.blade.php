@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Profile</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row m-b-lg m-t-lg">
                <div class="col-md-6">
                    <div class="profile-image">
                        <img src="{{ asset('assets/images/avatars/no-image-icon.png') }}" class="rounded-circle circle-border m-b-md" alt="profile">
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">{{$user->name}}</h2>
                                <h4>{{$user->role}}</h4>
                                <p>{{$user->designation}}</p>
                                <span>EID: {{$user->employee_id}}</span>
                                <a href="/profile/edit" class="vertical-timeline-icon navy-bg">
                                <i class="fa fa-edit"></i>
                            </a>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="ibox">
                        <div class="ibox-content">
                            <p>
                                Email : {{$user->email}}</br>
                                Mob No : {{$user->mobile}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
