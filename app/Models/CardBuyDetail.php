<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class CardBuyDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'purchase_number','customer_id', 'main_service_id','price','discount','gross_price','tax','net_amount','payment_type','created_user_id','created_at','updated_at'
    ];

    public static function invoiceCreation($cardPurchase, $data){

        $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/invoice';

        if(Settings::getSettingValueByType('api_url')!=null){
            $url  = Settings::getSettingValueByType('api_url').'invoice';
        }

        $cardPurchase->qb_request = json_encode($data);
        $cardPurchase->save();

        try{
            $response  = Http::post($url,$data);
        }
        catch(Exeption $e){
            return $e ;
        }

        $cardPurchase->qb_response = $response;
        $cardPurchase->save();

        if($response){

            $responseArray = json_decode($response);

            if($responseArray!=null){

                if($responseArray->response_code == 200){
                    $cardPurchase->inv_ref = $responseArray->qb_invoice_id;
                    $cardPurchase->inv_status = 'CREATED';
                    $cardPurchase->save();
                }else{
                    $cardPurchase->inv_ref = "";
                    $cardPurchase->inv_status = 'PENDING';
                    $cardPurchase->save();
                }

            } else{
                $cardPurchase->inv_ref = "";
                $cardPurchase->inv_status = 'PENDING';
                $cardPurchase->save();
            }
            
        }else{
            $cardPurchase->inv_ref = "";
            $cardPurchase->inv_status = 'PENDING';
            $cardPurchase->save();
        }

        return $response ;
    }
}
