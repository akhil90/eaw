<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MembershipPackageLog extends Model
{
    use HasFactory;

    protected $table = 'membership_package_logs';
    protected $fillable = [
        'customer_id', 'main_service_id','service_completed','service_remaining','package_purchase_date','package_end_date','wash_validity_start','wash_validity_end','status','created_at','updated_at'
    ];


    public static function getPackageHistory($customer_id){

        $query =  MembershipPackageLog::select([
            'membership_package_logs.id',
            'membership_package_logs.customer_id',
            'membership_package_logs.status',
            'membership_package_logs.main_service_id',
            'membership_package_logs.service_remaining',
            'membership_package_logs.service_completed',
            'membership_package_logs.package_purchase_date',
            'membership_package_logs.package_end_date',
            'membership_package_logs.wash_validity_start',
            'membership_package_logs.wash_validity_end',
            'membership_package_logs.service_remaining',
            'main_services.service_name'
        ]);

        $query = $query->leftjoin('customers', 'customers.id', '=', 'membership_package_logs.customer_id');
        $query = $query->leftjoin('main_services', 'main_services.id', '=', 'membership_package_logs.main_service_id');

        $query = $query->Where('membership_package_logs.customer_id',$customer_id);

        $query = $query->orderBy('membership_package_logs.id', 'DESC');
        $history = $query->get();

        return $history;    
    }

}
