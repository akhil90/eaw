<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function show(){
        return view('users.profile')->with('user', Auth::user());
    }
    public function edit(){
        return view('users.update_profile')->with('user', Auth::user());
    }
    public function update(Request $request){
        $user = Auth::user();
        $data = request()->validate([
            'name'      => 'required',
            'email'     => 'required|unique:users,email,'.$user->id,
            'gender'    => 'required',
            'mobile'    => 'required'
        ]);
        
        $user->name              = $request->input('name');
        $user->email             = $request->input('email');
        $user->gender            = $request->input('gender');
        $user->mobile            = $request->input('mobile');
        $user->designation       = $request->input('designation');
        $user->department        = $request->input('department');
        $user->save();
        $request->session()->flash('success','Profile has been updated successfully');
        return redirect('/profile');
    }
    public function change_password(){
        return view('users.change_password');
    }
    public function update_password(Request $request){
        $user = Auth::user();
        $request->validate([
            'current_password'      => 'required',
            'new_password'          => 'required',
            'confirm_password'      => 'same:new_password'
        ]);
        if(Hash::check($request->input('current_password'), $user->password)){
            $user->password = Hash::make($request->new_password);
            $user->save();
            $request->session()->flash('success','New password updated successfully.');
            return redirect('/change_password');
        }else{
            $request->session()->flash('error','The current password you entered is incorrect.');
            return redirect('/change_password');
        }
    }
}
