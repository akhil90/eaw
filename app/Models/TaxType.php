<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class TaxType extends Model
{
    use HasFactory;


    public static function getTaxTypes($fetchStatus = 0){

        if($fetchStatus == 1){

            if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

                $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/taxes';

                if(Settings::getSettingValueByType('api_url')!=null){
                    $url  = Settings::getSettingValueByType('api_url').'taxes';
                }

             } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){
     
                 $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/taxes';
 
                if(Settings::getSettingValueByType('api_url') != null){
                    $url  = Settings::getSettingValueByType('api_url').'taxes';
                }
                 
             }

            $response            = Http::get($url);

            $responseArray       = json_decode($response);

            if($responseArray){
                foreach($responseArray as $arr){

                    $taxId         = $arr->taxId;
                    $taxRateId     = $arr->taxRateId;
                    $taxTitle      = $arr->taxTitle;
                    $taxPercentage = $arr->taxPercentage;     
                    $taxValue      = $arr->taxValue;
                    $status        = $arr->status;
        
                    $taxType       = TaxType::Where('taxId', $taxId)->first();
        
                    if($taxType == null){
        
                        $taxType = new TaxType();
        
                        $taxType->taxId = $taxId;
                        $taxType->taxRateId = $taxRateId;
                        $taxType->taxTitle = $taxTitle;
                        $taxType->taxPercentage = $taxPercentage;
                        $taxType->taxValue = $taxValue;
                        $taxType->status = $status;
                        $taxType->save();
                    } else {
        
                        $taxType->taxId = $taxId;
                        $taxType->taxRateId = $taxRateId;
                        $taxType->taxTitle = $taxTitle;
                        $taxType->taxPercentage = $taxPercentage;
                        $taxType->taxValue = $taxValue;
                        $taxType->status = $status;
                        $taxType->save();
                    }
                }
            }

        }

        return TaxType::all();
    }

}
