<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Settings extends Model
{
    use HasFactory;

    protected $table = 'settings';

    public static function getSettingValueByType($type){

        $settings = Settings::where('type', $type)->first();

        if($settings != null){

            return $settings->value;

        }
    }

    public static function getAccounts($accountType, $fetchStatus = 0){

        if($fetchStatus == 1){

            if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

                $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/account/'.$accountType;

                if(Settings::getSettingValueByType('api_url') != null){
                    $url  = Settings::getSettingValueByType('api_url').'account/'.$accountType;
                }

             } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){
     
                 $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/';
 
                if(Settings::getSettingValueByType('api_url') != null){
                    $url  = Settings::getSettingValueByType('api_url').'account?AccountType='.$accountType;
                }
                 
             }
            

            $response            = Http::get($url);
    
            $responseArray       = json_decode($response);
    
            if($responseArray){

                foreach($responseArray->accounts  as $arr){

                    $settingsApiTemp = SettingsApiTemp::where('remote_id','=',$arr->accountId)->where(function($q){
                        $q->where('api_type', 'ACCOUNT_INCOME')->orWhere('api_type', 'ACCOUNT_BANK');
                    })->first();

                    if($settingsApiTemp == null){

                        $settingsApiTemp = new SettingsApiTemp();
                        $settingsApiTemp->remote_id   = $arr->accountId;
                        $settingsApiTemp->name        = $arr->accountName;
                        $settingsApiTemp->status      = $arr->status;

                        if($accountType == "Bank"){
                            $settingsApiTemp->api_type    = "ACCOUNT_BANK";
                        }else if($accountType == "Income"){
                            $settingsApiTemp->api_type    = "ACCOUNT_INCOME";
                        }

                        $settingsApiTemp->created_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->updated_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->save();

                    }else{

                        $settingsApiTemp->remote_id   = $arr->accountId;
                        $settingsApiTemp->name        = $arr->accountName;
                        $settingsApiTemp->status      = $arr->status;

                        if($accountType == "Bank"){
                            $settingsApiTemp->api_type    = "ACCOUNT_BANK";
                        }else if($accountType == "Income"){
                            $settingsApiTemp->api_type    = "ACCOUNT_INCOME";
                        }

                        $settingsApiTemp->updated_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->save();

                    }

                }
                
            }else{
                return 0;
            }

        }

        if($accountType == "Bank"){
            $dataArray = SettingsApiTemp::where('api_type','=','ACCOUNT_BANK')->get();
        }else if($accountType == "Income"){
            $dataArray = SettingsApiTemp::where('api_type','=','ACCOUNT_INCOME')->get();
        }
            

        return $dataArray;

       
      
    }


    public static function getPaymentMethods($fetchStatus = 0){

        if($fetchStatus == 1){

            if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

                $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/paymentmethod';

                if(Settings::getSettingValueByType('api_url')!=null){
                    $url  = Settings::getSettingValueByType('api_url').'paymentmethod';
                }

             } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){
     
                 $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/paymentmethod';
 
                if(Settings::getSettingValueByType('api_url') != null){
                    $url  = Settings::getSettingValueByType('api_url').'paymentmethod';
                }
                 
             }
            

            $response            = Http::get($url);
    
            $responseArray       = json_decode($response);
    
            if($responseArray){

                foreach($responseArray->paymentMethods  as $arr){

                    $settingsApiTemp = SettingsApiTemp::where('remote_id','=',$arr->methodId)->where('api_type','=','PAYMENT_METHOD')->first();

                    if($settingsApiTemp == null){

                        $settingsApiTemp = new SettingsApiTemp();
                        $settingsApiTemp->remote_id   = $arr->methodId;
                        $settingsApiTemp->name        = $arr->methodName;
                        $settingsApiTemp->status      = 1;
                        $settingsApiTemp->api_type    = "PAYMENT_METHOD";
                        $settingsApiTemp->created_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->updated_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->save();

                    }else{

                        $settingsApiTemp->remote_id   = $arr->methodId;
                        $settingsApiTemp->name        = $arr->methodName;
                        $settingsApiTemp->status      = 1;
                        $settingsApiTemp->api_type    = "PAYMENT_METHOD";
                        $settingsApiTemp->updated_at  = date('Y-m-d H:i:s');
                        $settingsApiTemp->save();

                    }

                }
                
            }else{
                return 0;
            }

        }

        $dataArray = SettingsApiTemp::where('api_type','=','PAYMENT_METHOD')->get();
            
        return $dataArray;

       
      
    }

}
