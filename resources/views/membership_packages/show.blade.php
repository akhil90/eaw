@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Membership Packages</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
      <div class="col-lg-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>Package List</h3>
              <div class="ibox-tools"> <a href="/membership_packages/create" style="margin-right: 25px; color: #fff !important;" type="button" title="Add new package" class="btn btn-round btn-success"><i class="fa fa-plus"></i> Package</a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Package Name</th>
                      <th>Car Type</th>
                      <th>Price</th>
                      <th>No.of Washes</th>
                      <th>Features</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>  
                    @foreach($membership_packages as $key=>$membership_package) 
                      @if($membership_package->status == "PENDING")
                        @php($color = "warning")
                      @elseif($membership_package->status == "ACTIVE")   
                        @php($color = "primary")
                      @elseif($membership_package->status == "DISABLED")  
                        @php($color = "danger")
                      @else
                        @php($color = "")
                      @endif               
                     <tr>
                      <td width="5%"> {{ $key+1 }}</td>
                      <td width="15%">{{$membership_package->service_name}}</td>
                      <td width="10%">{{$membership_package->car_type}}</td>
                      <td width="10%">{{$membership_package->price}}</td>
                      <td width="10%">{{$membership_package->no_of_washes}}</td>
                      <td width="30%">
                        @if ($membership_package->features != "")
                            <ul class="features">
                                @foreach(explode(',', $membership_package->features) as $feature) 
                                    <li>{{$feature}}</li> 
                                @endforeach
                            </ul>
                        @endif
                      </td>
                      <td width="10%"><span class="badge badge-pill badge-{{$color}}">{{$membership_package->status}}</span></td>
                      <td width="8%">
                        <a title="Edit this package" href="/membership_packages/edit/{{$membership_package->id}}" class="btn btn-round btn-info btn-xs" style="margin: 3px;">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        @if($membership_package->status == "ACTIVE")                        
                        <a href="/membership_packages/block/{{$membership_package->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this package">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($membership_package->status == "DISABLED")
                        <a href="/membership_packages/unblock/{{$membership_package->id}}" class="btn btn-round btn-primary btn-xs" style="margin: 3px;" title="Unblock this package">
                            <i class="fa fa-unlock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($membership_package->status == "PENDING")
                        <a href="/membership_packages/block/{{$membership_package->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this service">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                      </td>                     
                    </tr>   
                    @endforeach               
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
