<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'first_name', 'last_name','email','mobile','address','type','how_to_know_type','how_to_know_desc','created_by','modified_by','created_at','updated_at'
    ];


    public static function createQbCustomer($customer){

        if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

            $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/customer';

            if(Settings::getSettingValueByType('api_url')!=null){
                $url  = Settings::getSettingValueByType('api_url').'customer';
            }

         } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){
 
             $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/customer';

             if(Settings::getSettingValueByType('api_url')!=null){
                 $url  = Settings::getSettingValueByType('api_url').'customer';
             }
             
         }

         if(Settings::getSettingValueByType('qb_cutomer_name_with_mobile') == "yes"){

            $displayName = $customer->first_name;
            
            if($customer->mobile)
                $displayName = $customer->first_name." (". $customer->mobile .")";

         } else if(Settings::getSettingValueByType('qb_cutomer_name_with_mobile') == "no"){

            $displayName = $customer->first_name;
         }

        $data = array(
            "CustomerId" => $customer->id,
            "DisplayName" => $displayName,
            "GivenName"=> "",
            "FamilyName"=> "",
            "Email"=> $customer->email,
            "Mobile"=> $customer->mobile,
            "billingAddress"=> array(
                "Line1"=> "",
                "Line2"=> "",
                "City"=> "",
                "CountrySubDivisionCode"=> "",
                "Country"=> "",
                "PostalCode"=> "",
                "Note"=> ""
            )
        ); 

        $customer->qb_request = json_encode($data);
        $customer->save();

        $response      = Http::post($url,$data);

        $customer->qb_response = $response;
        $customer->save();

        $responseArray = json_decode($response);

        return $responseArray ;
    }


}
