@extends('layouts.outer')

@section('content')
<style>
.highlight { background-color: #b3ffb3; }
</style>
<div class="register-panel animated fadeInDown">
    <div style="width: 100%; display: inline-block;">
      <div class="log">
      <a style="font-size: 12px;font-weight: 600;color:grey;border:solid 1px;border-radius:5px;padding:4px " href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Log out </a></div>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
       </form>
      <div class="cl-wrp" style="float:right; margin-top: 2px; width: 350px; padding-left: 75px;">
      
        <div class="cl-wrp-pl"></div>
        
        <span id='ct' style="font-size: 12px;font-weight: 600;color:grey"></span>
        <button type="button" id="gate_disable_buttton"  data-gate_status="DISABLED" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-danger barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier Off | Please click here for <span style="background-color:white;padding:5px;color:green;"> Activate </span></marquee></button>
        <button type="button" id="gate_active_buttton" data-gate_status="ACTIVE" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-success barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier On | Please click here for <span style="background-color:white;padding:5px;color:red"> Disable </span></marquee></button>
      </div>
    </div>
    <div class="cr4">
      <div class="tab-content">
        <p>Welcome to Express Auto Wash</p>
        <h3>Give Information to Entrance</h3>
          @if(session()->has('success'))
          <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>{{ session()->get('success') }}</div>
          @elseif(session()->has('error'))
          <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>{{ session()->get('error') }}</div>
          @endif


          @if ($errors->any())
          <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
         @endif

         <div id="submit-error" style="display:none;" class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button></div>
         <div id="submit-success"  style="display:none;" class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert">&times;</button></div>

        <div class="hcap">Please select the below option</div>
        <div class="nav nav-tabs" role="tablist">
          <div style="margin-right:15px;">
            <input id="optEnrollCard" name="intervaltype" type="radio" data-target="#entrol_card">
            <label for="optEnrollCard">Enroll Card</label>
          </div>
          <div style="margin-right:15px;">
            <input id="optMember"  name="intervaltype" type="radio" data-target="#member">
            <label for="optMember">Member</label>
          </div>
          <div>
            <input id="optNonMember" checked name="intervaltype" type="radio" data-target="#non-member">
            <label for="optNonMember">Non Member</label>
          </div>
        </div>

        <div id="entrol_card" class="tab-pane">

          <div class="info-section" id="info-renew-card">
              <div class="info-innr">
                <div class="info-sec-left">
                  <h4>This Member ( <span id="mem_name_renew_card"></span> ) have a Card, <br>
                    Card No - <span id="card_number_info"></span></h4>
                  <p>Renew card with below form</p>
                </div>
              </div>
            </div>

        <form class="m-t" role="form" id="enroll_card" method="post" action="/jobcard/enroll_card">
          @csrf
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Card Number <span class="required">*</span></label>
                  <input type="hidden" value="no" id="renew_card" class="renew_card" name="renew_card">
                  <input type="hidden" value="card_enroll" id="submit_type" class="submit_type" name="submit_type">

                  <div id="result-template-enroll-card">
                    <input type="text" name="card_number" id="card_number" class="card_number form-control" placeholder="Card Number" required>
                  </div>

                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Phone Number</label>
                  <div id="result-template-enroll-mobile">
                    <input type="text" autocomplete="off" name="card_mobile" id="card_mobile" class="card_mobile form-control" placeholder="Phone Number">
                  </div>
                </div>OR
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>email</label>
                  <div id="result-template-enroll-email">
                    <input type="email" name="card_email" id="card_email" class="card_email form-control" placeholder="Email">
                  </div>
                </div>
              </div>
              <div class="bt-sec-wrp"><button type="submit" id="enrolCard" class="btn btn78 btn-primary block m-b">Enter</button></div>
            </div>

        </form>
      </div>

        <div id="member" class="tab-pane">

          <div class="info-section" id="info-package-details">
            <div class="info-innr">
              <div class="info-sec-left">
                <h4><span id="package_name"> Package Name Not Found</span></h4>
                <p>Expiry on <span id="expiry">Data Not Found</span>.</p>
              </div>
              <div class="info-sec-right"> <span class="bts wash-info" id="wash_remain">0</span>
                <p>Carwash left</p>
              </div>
            </div>
          </div>

          <div class="info-section" id="info-buy-membership">
            <div class="info-innr lp">
              <div class="info-sec-left-n">
                <h4>Please buy a Membership</h4>
                <form method="post" action="/jobcard/redirect_choose">                
                    @csrf
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                      <label>Card Number <span class="required">*</span></label>
                      <input type="hidden" value="" id="customer_id" name="customer_id">
                      <input type="hidden" name="submit_type" value="buy_package"/>
                      <input type="hidden" name="plate_number_hidden" id="plate_number_hidden_buy" value=""/>
                      <input type="hidden" name="buyer_name" id="buyer_name" value=""/>
                      <input type="hidden" name="buyer_email" id="buyer_email" value=""/>
                      <input type="hidden" name="buyer_mobile" id="buyer_mobile" value=""/>
                      <div id="result-template-enroll-card" style="width:100%; display:inline-block;">
                         <ul>
                            <li style="width:100%; margin-bottom:15px;"> <input type="text" name="card_number" id="card_number_by" class="card_number form-control" placeholder="Please Choose a Card Number" required></li>
                            <li><button type="submit" id="buyMembership" class="btn btn78 btn-primary block m-b"><i class="fa fa-shopping-cart" aria-hidden="true"></i>  Buy</button></li>
                         </ul>
                      </div>
                    </div>                
                </form>
              </div>
            </div>
          </div>

          <div class="info-section" id="info-renew-membership">
            <div class="info-innr lp">
              <div class="info-sec-left-n">
                <h4>Please renew your Membership</h4>
                <form method="post" action="/jobcard/redirect_choose">                
                    @csrf
                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                      <label>Card Number <span class="required">*</span></label>
                      <input type="hidden" value="" id="renew_customer_id" name="customer_id">
                      <input type="hidden" name="submit_type" value="buy_package"/>
                      <input type="hidden" name="plate_number_hidden" id="plate_number_hidden_renew" value=""/>
                      <input type="hidden" name="buyer_name" id="buyer_name" value=""/>
                      <input type="hidden" name="buyer_email" id="buyer_email" value=""/>
                      <input type="hidden" name="buyer_mobile" id="buyer_mobile" value=""/>
                      <div id="result-template-enroll-card" style="width:100%; display:inline-block;">
                         <ul>
                            <li style="width:100%; margin-bottom:15px;"> <input type="text" name="card_number" id="card_number_renew" class="card_number form-control" placeholder="Please Choose a Card Number" readonly></li>
                            <li><button type="submit" id="renewMembership" class="btn btn78 btn-primary block m-b"><i class="fa fa-shopping-cart" aria-hidden="true"></i>  Renew</button></li>
                         </ul>
                      </div>
                    </div>                
                </form>
              </div>
            </div>
          </div>



          <form class="m-t" role="form" method="post" action="/jobcard/redirect_choose">

            @csrf
            <input type="hidden" name="main_service_id" id="main_service_id" value="">
            <input type="hidden" name="submit_type" value="member_common"/>

            <div class="col-md-12 col-sm-12 col-xs-12" >
              <div class="row" style="width: 100%;">
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Membership ID <span class="required">*</span></label>

                  <div id="result-template-member-id">
                    <input type="text" name="member_id" id="member_id" placeholder="Search with Membership ID Number..." class="member_id_class form-control" required/>
                  </div>
                  
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Plate Number <span class="required">*</span></label>

                  <div id="result-template-member-plate">
                    <input type="text" autocomplete="off" name="member_plate_number" id="member_plate_number" placeholder="Search with Plate Number..." class="member_plate_number form-control" required/>
                  </div>
                  
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Name<span class="required">*</span></label>
                  <input type="text" autocomplete="off" name="member_name" id="member_name" placeholder="Choose Name..." class="form-control" required/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Phone Number<span class="required">*</span></label>
                  <div id="result-template-member-mobile">
                    <input type="text" autocomplete="off" name="member_mobile" id="member_mobile" placeholder="Choose Phone Number..." class="member_mobile form-control" required/>
                  </div>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>E-mail ID</label>
                  <input type="text" autocomplete="off" name="member_email" id="member_email" placeholder="Choose E-mail ID..." class="form-control"/>
                </div>
                <div class="bt-sec-wrp"><button type="submit" id="redirectToChooseFromMember" class="btn btn78 btn-primary block m-b">Enter</button></div>
              </div>
            </div>
          </form>
        </div>
        <div id="non-member" class="tab-pane active">

          <div class="info-section" id="info-loyality-points" style="display:none;">
            <div class="info-innr lp">
              <div class="info-sec-left">
                <h4>Loyalty Points</h4>
                <p>Refer a car wash or wash your car, you earned 1 point, if you earn 9 loyality points , get 1 car wash free.</p>
                <div class="bts-wrp"> 
                  <a title="History of earned loyality points" href="#" id="historyPointLink" class="btn btn-round btn-info btn-xs" data-toggle="modal" data-target="#lhModal"><i class="fa fa-history" aria-hidden="true"></i></a> 
                  <a title="Redeem your Points" id="redeen_btn" class="btn btn-round btn-primary btn-xs" data-toggle="modal" data-target="#rdm-confirmModal"> <i class="fa fa-star-o" aria-hidden="true"></i></a>
                </div>
              </div>
              <div class="info-sec-right"> <span class="bts earned" id="points_earned"></span>
                <p id="freewash_msg"></p>
              </div>
            </div>
          </div>

          <form class="m-t" role="form" method="post" action="/jobcard/redirect_choose">
            
          {{csrf_field()}}

            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Plate Number <span class="required">*</span></label>
                  <input type="hidden" name="submit_type" value="non_member_common"/>
                  <input type="hidden" name="fromdb" id="fromdb" value="0"/>
                  <input type="hidden" name="edit_customer_id" id="edit_customer_id" value=""/>
                  
                  <div id="result-template">
                    <!-- <input type="text" class="typeahead" placeholder="Best picture winners" /> -->
                    <input type="text" autocomplete="off" placeholder="Choose Plate Number..." name="plate_number" id="plate_number" class="plate_number form-control" required/>
                  </div>
    
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Car Color<span class="required">*</span></label>
                  <input type="text" placeholder="Choose Car Color..." name="car_color" id="car_color" class="car_color readPermission form-control" required/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Car Make<span class="required">*</span></label>
                  <input type="text" placeholder="Choose Car Make..." name="car_make" id="car_make" class="car_make readPermission form-control" required/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Car Model</label>
                  <input type="text" placeholder="Choose Car Model..." name="car_model" id="car_model" class="car_model readPermission form-control"/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Name<span class="required">*</span></label>
                  <input type="text" autocomplete="off" placeholder="Choose Name..." name="name" id="name" class="name readPermission doubleTapEdit form-control" required/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Phone Number<span class="required">*</span></label>
                  <div id="result-template-non-member-mobile">
                    <input type="text" autocomplete="off" placeholder="Choose Phone Number..." name="mobile" id="mobile" class="mobile readPermission doubleTapEdit form-control" required/>
                  </div>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>E-mail ID</label>
                  <input type="text" autocomplete="off" placeholder="Choose E-mail ID..." name="email" id="email" class="email readPermission doubleTapEdit form-control"/>
                </div>
                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                  <label>Car Type? <span class="required">*</span></label>
                  <ul>
                    <li>
                      <input type="radio" value="SEDAN" id="sedan" name="car_type" required>
                      Sedan</li>
                    <li>
                      <input type="radio" value="SUV" id="suv" name="car_type" required>
                      SUV</li>
                  </ul>
                </div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12" id='howtoknow'>
                  <label>How do you know about Express Auto Wash? <span class="required">*</span></label>
                  <ul>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="walkin">
                      Walk-In</li>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow"  value = "referedby" id="referedby" />
                      <label for="test6">Referral by Friend/ Member</label>
                    </li>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="employee" >
                      Employee</li>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="newspaper">
                      Newspaper Ad</li>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="facebook">
                      Facebook</li>
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="twitter">
                      Twitter</li>
                  </ul>
                  <ul class="cr2">
                    <li>
                      <input type="radio" name="howtoknow" class="howtoknow" value="other">
                      Other (Please specify)
                      <input type="text" name="howtoknowdesc" class="form-control" placeholder="Please specify">
                    </li>
                  </ul>
                </div>
                <div class="bt-sec-wrp"><button type="submit" id="redirectToChoose" class="btn btn78 btn-primary block m-b">Enter</button></div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="clgo1"> <img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid">
        <p class="m-t"> <small> ©2020 All Rights Reserved. <a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </small> </p>
      </div>
    </div>
  </div>


<!-- model -->
  <div class="modal" id="ReferralModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Referral by Friend/ Member</h4>
          <button type="button" class="close" id="ReferralModalClose" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label>Plate Number<span class="required">*</span></label>
                  <div id="result-template-referPlate">
                    <input type="text" autocomplete="off" name='refered_plate_number' id='refered_plate_number' placeholder="Choose Plate Number..." class="refered_plate_number form-control" />
                  </div>
                </div>
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label>Name</label>
                <input type="text" autocomplete="off" name='refered_name' id='refered_name' placeholder="Choose Name..." class="refered_name form-control" />
              </div>
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label>Phone Number</label>
                <div id="result-template-refer-mobile">
                  <input type="text" autocomplete="off" name='refered_mobile' id='refered_mobile' placeholder="Choose Phone Number..." class="refered_mobile form-control" />
                </div>    
              </div>
              <div class="form-group col-md-6 col-sm-6 col-xs-12">
                <label>Car Type?<span class="required">*</span></label>
                <ul>
                  <li>
                    <input type="radio" value="SEDAN" id="optionsRadios1" name="refered_car_type">
                    Sedan</li>
                  <li>
                    <input type="radio" value="SUV" id="optionsRadios2" name="refered_car_type">
                    SUV</li>
                </ul>
              </div>
              <div class="rf-sec-in" id="refer_detail_table"> 
                <h5>Vehicle Details</h5>
                <div class="table-responsive eaw">
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>SN</th>
                      <th>Plate Number</th>
                      <th>Car Color</th>
                      <th>Car Make</th>
                      <th>Car Model</th>
                      <th>Car Type</th>
                      <th>Name</th>
                      <th>Phone Number</th>
                    </tr>
                    </thead>
                    <tbody id="refer_detail_table_body" style="background-color:white;">
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button class="btn btn-w-m btn-info" id="referFriendSubmit" disabled type="button">Submit Details</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="lhModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">History of Earned Loyalty Points</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="table-responsive">
                <table id="dataTables-history-points" class="sum table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <th class="lft">No</th>
                    <th>Date</th>
                    <th class="lft">Plate Number</th>
                    <th>Mobile Number</th>
                    <th>Type</th>
                    <th>Received Points</th>
                    <th>Remaining points</th>
                    <th>Receive Type</th>
                    <th>Narration</th>
                  </tr>
                  </thead>
                  <tbody id="history_table_body">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="rdm-confirmModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Redeem Your points</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row">Do you want to Redeem points for Carwash?</div>
          </div>
        </div>
        <!-- Modal footer -->
        <div class="modal-footer">
        <form class="m-t" role="form" method="post" action="/jobcard/redirect_choose">
          @csrf
          <input type="hidden" name="submit_type" value="redeem_points"/>
          <input type="hidden" name="redeem_plate_number" value=""/>
          <button type="button" class="btn btn-w-m btn-danger" data-dismiss="modal">No</button>
          <button type="submit" class="btn btn-w-m btn-info">Yes</button>
        </form>
        </div>
      </div>
    </div>
  </div>

  <div id="confirm" class="modal">
    <div class="modal-dialog">

      <div class="modal-content">

      <div class="modal-body">
        <h3><b>Are you sure to change the barrier status ?</b></h3>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="change">Change</button>
        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
      </div>

      </div>
    </div>
  </div>

  <!-- end model -->

  @endsection

  @section('script')

  <script>

  $(document).ready(function () {
//defaut-pageload--area----------------------

    $('#info-loyality-points').hide();
    $('#info-package-details').hide();
    $('#refer_detail_table').hide();
    $('#howtoknow').hide();
    $(".howtoknow").prop('required',false);
    $('.howtoknow').prop('checked', false);
    $('#submit-error').hide();
    $('#submit-success').hide();
    $('#info-buy-membership').hide();
    $('#info-renew-membership').hide();
    $('#info-renew-card').hide();


//non-memeber------------------------------------

    var customPlateData =  <?= json_encode($customPlateData); ?>;

    var cutomizedPlates = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(customPlateData, function(d) {
        return {
          value: d.plate_number,
          suggest: d
        }
      })
    });

    cutomizedPlates.initialize();

    $("#result-template .plate_number").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "plate_number",
      display: "value",
      source: cutomizedPlates.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='ProfileCard-stats'><ul><li>" + data.suggest.remaining_points +" Loyalty Points</li><li>" + data.suggest.remaining_free_washes +" Free Carwash Left</li></ul></div><div class='clr " + data.suggest.car_color +"'></div></div>"
          }
        }
      }).on('typeahead:selected', function(ev,res) {
        
        $.ajax({
          url: '/jobcard/non_member_by_plate/'+res.value,
          type:'GET',
          dataType:'json',
          //data: {},
          success: function(response) {
            $("input[name='redeem_plate_number']").val("");
            $('#freewash_msg').text("");
            $("#redeen_btn").hide();
            $("#points_earned").css("background-color", "orange");
            if($.trim(response)){
              
              $('#howtoknow').hide();
              $(".howtoknow").prop('required',false);

              $('.readPermission').attr('readonly', true);

              $("input[name='car_color']").val(response.car_color);

              if(response.car_color == null){

                $("input[name='car_color']").attr('readonly', false);

              }

              $("input[name='car_make']").val(response.car_make);

              if(response.car_make == null){

                $("input[name='car_make']").attr('readonly', false);

              }

              $("input[name='car_model']").val(response.car_model);

              if(response.car_model == null){

                $("input[name='car_model']").attr('readonly', false);

              }


              $("input[name='mobile']").val(response.mobile);
              $("input[name='name']").val(response.first_name);
              $("input[name='email']").val(response.email);
              $("input[name='fromdb']").val(1);
              $("input[name='edit_customer_id']").val(response.customer_id);
              

              $("input[name='car_type'][value='" + response.car_type + "']").prop('checked', true);

              if(response.remaining_points > 0){
                $('#info-loyality-points').show();
                $('#points_earned').text(response.remaining_points);
                $("input[name='redeem_plate_number']").val(response.plate_number);
                if(response.remaining_points >= <?= $point_needed_for_freewash ?>){
                  $("#points_earned").css("background-color", "green");
                  $('#freewash_msg').text("Eligible for free wash");
                  $("#redeen_btn").show();
                }
              }
            } else{
              $('.readPermission').val('');
              $("input[name='fromdb']").val(0);
              $('#howtoknow').show();
              $(".howtoknow").prop('required',true);
            }
            
          }
        });
      }); 


      $('input#plate_number').on('keyup', function(){
        
            $("input[name='fromdb']").val(0);
            $('.readPermission').attr('readonly', false);
            $('#info-loyality-points').hide();
           // $('.readPermission').val('');
            $('#howtoknow').show();
            $(".howtoknow").prop('required',true);
          
      });

      $("#refered_plate_number").keyup(function(){
        $('#refer_detail_table_body').html("");
        $('#refer_detail_table').hide();
        $("#referFriendSubmit").prop('disabled', true);
        $("#refered_mobile").val("");
        $("#refered_name").val("");
        $("input[name='refered_car_type']").prop('checked', false);
      });
      $("#refered_mobile").keyup(function(){
        $('#refer_detail_table_body').html("");
        $('#refer_detail_table').hide();
        $("#referFriendSubmit").prop('disabled', true);
        $("#refered_plate_number").val("");
        $("#refered_name").val("");
        $("input[name='refered_car_type']").prop('checked', false);
      });

      $("#result-template-referPlate .refered_plate_number").typeahead(
          {
            minLength: 1,
            highlight: true,
            offset:false,
            hint:true
          }, 
          {
          name: "plate_number",
          display: "value",
          source: cutomizedPlates.ttAdapter(),
            templates: {
              notFound: [
                "<div class='EmptyMessage'>",
                "Your search turned up 0 results. This most likely means the backend is down, yikes!",
                "</div>"
              ].join("\n"),
              suggestion: function(data) {
                return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='ProfileCard-stats'><ul><li>"+ data.suggest.remaining_points +" Loyalty Points</li><li>"+ data.suggest.remaining_free_washes +" Free Carwash Left</li></ul></div><div class='clr " + data.suggest.car_color +"'></div></div>";
              }
            }
          }).on('typeahead:selected', function(ev,res) {

              $.ajax({
                url: '/jobcard/non_member_by_plate/'+ res.value,
                type:'GET',
                dataType:'json',
                success: function(response) {

                  $("input[name='refered_mobile']").val(response.mobile);
                  $("input[name='refered_name']").val(response.first_name);

                  $("input[name='refered_car_type'][value='" + response.car_type + "']").prop('checked', true);

                  let list = '<tr class="refer_tr" style="cursor:pointer;">'+
                                '<td>1</td>'+
                                '<td class="refer_plate_number">'+response.plate_number+'</td>'+
                                '<td>'+response.car_color+'</td>'+
                                '<td>'+response.car_make+'</td>'+
                                '<td>'+response.car_model+'</td>'+
                                '<td class="refer_car_type">'+response.car_type+'</td>'+
                                '<td class="refer_first_name">'+response.first_name+'</td>'+
                                '<td class="refer_mobile">'+response.mobile+'</td>'+
                              '</tr>';
                              
                  $('#refer_detail_table_body').html(list);

                  $('#refer_detail_table').show();
                  
                }
              });
          }); 


    $('input#referedby[type="radio"]').on('change', function(e){
      if(e.target.checked){
        $('#ReferralModal').modal({
            backdrop: 'static',
            keyboard: false
        });
      }
    });

    $("#ReferralModalClose").on("click", function () {
      $('.howtoknow').prop('checked', false);
    });

  
   $('button#referFriendSubmit').on('click', function(){

      var refered_plate_number = $("input[name='refered_plate_number']").val();
      if(refered_plate_number == ""){
        $('#submit-error').show();
        $('#submit-error').text('Refered Plate number is missing');
        return false;
      }

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: '/referedPlateStore',
        type:'POST',
        dataType:'json',
        data: {refered_plate_number:refered_plate_number},
        success: function(response) {
          if(response.status == "success"){
            $('#submit-error').hide();
            $('#submit-success').show();
            $('#submit-success').text(response.message);
          }else{
            $('#submit-success').hide();
            $('#submit-error').show();
            $('#submit-error').text(response.message);
          }
          $('#ReferralModal').modal("hide");
        }
      });
   });

  $("a#historyPointLink").on('click',function(){
    var plate_number = $("input#plate_number").val();
    $('#dataTables-history-points').DataTable().destroy();
    $.ajax({
       url: '/jobcard/history_points/'+ plate_number,
       type:'GET',
       dataType:'json',
       success: function(response) {
          var list = '';
          $.each(response.data, function() {
            list += '<tr><td>'+this.SlNo+'</td><td>'+this.date+'</td><td>'+this.plate_number+'</td><td>'+this.mobile+'</td><td>'+this.type+'</td><td>'+this.recieved_points+'</td><td>'+this.remaining_points+'</td><td>'+this.recieve_type+'</td><td>'+this.narration+'</td></tr>';
          });
          $("#history_table_body").html(list); 
          $('#dataTables-history-points').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'ExampleFile'
                },
                {
                    extend: 'pdf',
                    title: 'ExampleFile'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          });     
         }
      });          
  });

  

  

  var customCustomerData =  <?= json_encode($customerDetails); ?>;

    var cutomizedCustomers = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(customCustomerData, function(d) {
        return {
          value: d.mobile,
          suggest: d
        }
      })
    });

    cutomizedCustomers.initialize();


    $("#result-template-refer-mobile .refered_mobile").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "mobile",
      display: "value",
      source: cutomizedCustomers.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.mobile +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.email +"</div></div></div>"
          }
        }
      }).on('typeahead:selected', function(ev,res) {

        $.ajax({
          url: '/jobcard/non_member_by_mobile/'+res.value,
          type:'GET',
          dataType:'json',
          //data: {},
          success: function(response) {
              let list = '';
              $("input[name='refered_name']").val(response.first_name);
              $.each(response.plate_list, function( index, value ) {
                list += '<tr class="refer_tr" style="cursor:pointer;">'+
                          '<td>'+(index+1)+'</td>'+
                          '<td class="refer_plate_number">'+value.plate_number+'</td>'+
                          '<td>'+value.car_color+'</td>'+
                          '<td>'+value.car_make+'</td>'+
                          '<td>'+value.car_model+'</td>'+
                          '<td class="refer_car_type">'+value.car_type+'</td>'+
                          '<td class="refer_first_name">'+value.first_name+'</td>'+
                          '<td class="refer_mobile">'+value.mobile+'</td>'+
                        '</tr>';
                  
              });
              $('#refer_detail_table_body').html(list);

              $('#refer_detail_table').show();
          }
        });
      }); 

  $('html').on('click', '.refer_tr', function(){
    $("#referFriendSubmit").prop('disabled', false);
    $('.refer_tr').removeClass("highlight");
    $(this).addClass("highlight");
    let refer_plate_number = $(this).closest('tr').find('.refer_plate_number').text();
    let refer_first_name = $(this).closest('tr').find('.refer_first_name').text();
    let refer_mobile = $(this).closest('tr').find('.refer_mobile').text();
    let refer_car_type = $(this).closest('tr').find('.refer_car_type').text();
    $("#refered_plate_number").val(refer_plate_number);
    $("#refered_name").val(refer_first_name);
    $("#refered_mobile").val(refer_mobile);
    $("input[name='refered_car_type'][value='" + refer_car_type + "']").prop('checked', true);
  });

  $("#result-template-non-member-mobile .mobile").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "mobile",
      display: "value",
      source: cutomizedCustomers.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.mobile +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.email +"</div></div></div>"
          }
        }
      }).on('typeahead:selected', function(ev,res) {

        $.ajax({
          url: '/jobcard/non_member_by_mobile/'+res.value,
          type:'GET',
          dataType:'json',
          //data: {},
          success: function(response) {

            if($.trim(response)){

              $('#howtoknow').hide();
              $(".howtoknow").prop('required',false);

              $('.readPermission').attr('readonly', false);

              $("input[name='plate_number']").val("");
              $("input[name='car_color']").val("");
              $("input[name='car_make']").val("");
              $("input[name='car_model']").val("");
              $("input[name='mobile']").val(response.mobile);
              $("input[name='name']").val(response.first_name);
              $("input[name='email']").val(response.email);
              $("input[name='fromdb']").val(1);
              $("input[name='edit_customer_id']").val(response.customer_id);              

              $("#result-template .plate_number").typeahead("destroy");
              
              var customPlateData =  response.plate_list;

              var cutomizedPlates = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: $.map(customPlateData, function(d) {
                  return {
                    value: d.plate_number,
                    suggest: d
                  }
                })
              });

              cutomizedPlates.initialize();

              $("#result-template .plate_number").typeahead({
                minLength: 1,
                highlight: true,
                offset:false,
                hint:true
                }, {
                name: "plate_number",
                display: "value",
                source: cutomizedPlates.ttAdapter(),
                  templates: {
                    notFound: [
                      "<div class='EmptyMessage'>",
                      "Your search turned up 0 results. This most likely means the backend is down, yikes!",
                      "</div>"
                    ].join("\n"),
                    suggestion: function(data) {
                      return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='ProfileCard-stats'><ul><li>" + data.suggest.remaining_points +" Loyalty Points</li><li>" + data.suggest.remaining_free_washes +" Free Carwash Left</li></ul></div><div class='clr " + data.suggest.car_color +"'></div></div>"
                    }
                  }
                }).on('typeahead:selected', function(ev,res) {

                  $.ajax({
                    url: '/jobcard/non_member_by_plate/'+res.value,
                    type:'GET',
                    dataType:'json',
                    //data: {},
                    success: function(response) {

                      if($.trim(response)){

                        $('#howtoknow').hide();
                        $(".howtoknow").prop('required',false);

                        $('.readPermission').attr('readonly', true);

                        $("input[name='car_color']").val(response.car_color);
                        $("input[name='car_make']").val(response.car_make);
                        $("input[name='car_model']").val(response.car_model);
                        $("input[name='mobile']").val(response.mobile);
                        $("input[name='name']").val(response.first_name);
                        $("input[name='email']").val(response.email);
                        $("input[name='fromdb']").val(1);
                        $("input[name='edit_customer_id']").val(response.customer_id);
                        

                        $("input[name='car_type'][value='" + response.car_type + "']").prop('checked', true);

                        if(response.remaining_points >= <?= $point_needed_for_freewash ?>){
                          $('#info-loyality-points').show();
                          $("input[name='redeem_plate_number']").val(response.plate_number);
                        } else {
                          $('#info-loyality-points').hide();
                          $("input[name='redeem_plate_number']").val("");
                        }

                      } else{
                        $('#info-loyality-points').hide();
                        $('.readPermission').val('');
                        $("input[name='fromdb']").val(0);
                        $('#howtoknow').show();
                        $(".howtoknow").prop('required',true);
                      }
                      
                    }
                  });
                }); 
            } 
            
          }
        });
      }); 

//memeber ---------------------

  var customMemberData =  <?= json_encode($memberDetails); ?>;

    var cutomizedMembers = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(customMemberData, function(d) {
        return {
          value: d.member_id,
          suggest: d
        }
      })
    });

    cutomizedMembers.initialize();

    $("#result-template-member-id .member_id_class").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "member_id",
      display: "value",
      source: cutomizedMembers.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.member_id +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.email +"</li><li>"+ data.suggest.package_name +"</li><li>" + data.suggest.remaining_washes +" Carwash Left</li></ul></div></div>"
          }
        }
      }).on('typeahead:selected', function(ev,res) {

        $.ajax({
          url: '/jobcard/member_by_card/'+res.value,
          type:'GET',
          dataType:'json',
          success: function(response) {

            if($.trim(response)){

              $("input[name='member_id']").val(response.member_id);
              $("input[name='member_name']").val(response.first_name);
              $("input[name='member_mobile']").val(response.mobile);
              $("input[name='member_email']").val(response.email);
              $("input[name='main_service_id']").val(response.main_service_id);
              $("input[name='member_plate_number']").val('');
              $('#buyer_mobile').val(response.mobile);
              $('#buyer_email').val(response.email);
              $('#buyer_name').val(response.first_name);
              
              
              if(response.active_status == "ACTIVE"){

                $('#info-buy-membership').hide();
                $('#info-renew-membership').hide();
                $("span#package_name").text(response.package_name);
                $("span#expiry").text(response.validity_end);
                $("span#wash_remain").text(response.remaining_washes);
                $('#info-package-details').show();


              } else {

                  $('#info-package-details').hide();
                  $('#info-buy-membership').hide();
                  $('#card_number_renew').val(response.member_id);
                  $('#renew_customer_id').val(response.customer_id);
                  $('#plate_number_hidden').val(response.plate_number);
                  $('#info-renew-membership').show();
              }

              $("#result-template-member-plate .member_plate_number").typeahead("destroy");
              
              var customPlateData =  response.plate_list;

              var cutomizedPlates = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: $.map(customPlateData, function(d) {
                  return {
                    value: d.plate_number,
                    suggest: d
                  }
                })
              });
           
              cutomizedPlates.initialize(true);
              
              $("#result-template-member-plate .member_plate_number").typeahead({
                    minLength: 1,
                    highlight: true,
                    offset:false,
                    hint:true
                    }, {
                    name: "plate_number",
                    display: "value",
                    source: cutomizedPlates.ttAdapter(),
                      templates: {
                        notFound: [
                          "<div class='EmptyMessage'>",
                          "Your search turned up 0 results. This most likely means the backend is down, yikes!",
                          "</div>"
                        ].join("\n"),
                        suggestion: function(data) {
                          return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='clr " + data.suggest.car_color +"'></div></div>";
                        }
                      }
                  });

            } else{

              $('#info-package-details').hide();
              $("input[name='member_id']").val('');
              $("input[name='member_name']").val('');
              $("input[name='member_email']").val('');
              $("input[name='member_mobile']").val('');
              $("input[name='member_plate_number']").val('');
            }
            
          }
        });
      }); 


  $("#result-template-member-mobile .member_mobile").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "mobile",
      display: "value",
      source: cutomizedCustomers.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.mobile +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div> | <div class='ProfileCard-screenName'>"+ data.suggest.email +"</div></div></div>"
          }
        }
    }).on('typeahead:selected', function(ev,res) {

      $.ajax({
        url: '/jobcard/member_by_mobile/'+res.value,
        type:'GET',
        dataType:'json',
        success: function(response) {

          if($.trim(response)){

            $("input[name='member_id']").val(response.member_id);
            $("input[name='member_name']").val(response.first_name);
            $("input[name='member_mobile']").val(response.mobile);
            $("input[name='member_email']").val(response.email);
            $("input[name='main_service_id']").val(response.main_service_id);
            $("input[name='member_plate_number']").val('');
            $('#buyer_mobile').val(response.mobile);
            $('#buyer_email').val(response.email);
            $('#buyer_name').val(response.first_name);
            
            
            if(response.active_status == "ACTIVE"){

              $('#info-buy-membership').hide();
              $('#info-renew-membership').hide();
              $("span#package_name").text(response.package_name);
              $("span#expiry").text(response.validity_end);
              $("span#wash_remain").text(response.remaining_washes);
              $('#info-package-details').show();


            } else {

                $('#info-package-details').hide();
                $('#info-buy-membership').hide();
                $('#card_number_renew').val(response.member_id);
                $('#renew_customer_id').val(response.customer_id);
                $('#plate_number_hidden').val(response.plate_number);
                $('#info-renew-membership').show();
            }

            $("#result-template-member-plate .member_plate_number").typeahead("destroy");
            
            var customPlateData =  response.plate_list;

            var cutomizedPlates = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              local: $.map(customPlateData, function(d) {
                return {
                  value: d.plate_number,
                  suggest: d
                }
              })
            });
        
            cutomizedPlates.initialize(true);
            
            $("#result-template-member-plate .member_plate_number").typeahead({
                  minLength: 1,
                  highlight: true,
                  offset:false,
                  hint:true
                  }, {
                  name: "plate_number",
                  display: "value",
                  source: cutomizedPlates.ttAdapter(),
                    templates: {
                      notFound: [
                        "<div class='EmptyMessage'>",
                        "Your search turned up 0 results. This most likely means the backend is down, yikes!",
                        "</div>"
                      ].join("\n"),
                      suggestion: function(data) {
                        return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='clr " + data.suggest.car_color +"'></div></div>";
                      }
                    }
                });

          } else{
            
            $('#info-package-details').hide();
            $("input[name='member_id']").val('');
            $("input[name='member_name']").val('');
            $("input[name='member_email']").val('');
            $("input[name='member_mobile']").val('');
            $("input[name='member_plate_number']").val('');

          }
          
        }
      });
    }); 

    $('.member_mobile').on('change',function(){
      $.ajax({
        url: '/jobcard/search_customer/'+$(this).val(),
        type:'GET',
        dataType:'json',
        success: function(response) {
          if($.trim(response)){
            if(response.customer_exits == 'NOT_EXISTS'){
              $('#info-package-details').hide();
              $("input[name='member_id']").val('');
              $("input[name='member_name']").val('');
              $("input[name='member_email']").val('');
              $("input[name='member_plate_number']").val('');
              $('#info-buy-membership').show();
            }
          }
        }
      });
    });

    $('#member_mobile').on('keyup',function(){
      $('#buyer_mobile').val($(this).val());
    });
    $('#member_email').on('keyup',function(){
      $('#buyer_email').val($(this).val());
    });
    $('#member_name').on('keyup',function(){
      $('#buyer_name').val($(this).val());
    });
    
  //enroll card---------------------------------

  $("#result-template-enroll-mobile .card_mobile").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "mobile",
      display: "value",
      source: cutomizedCustomers.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.mobile +"</div><div class='ProfileCard-screenName'>"+ data.suggest.email +"</div></div></div>"
          }
        }
    }).on('typeahead:selected', function(ev,res) {
      $.ajax({
        url: '/jobcard/search_member_card_status/'+res.value,
        type:'GET',
        dataType:'json',
        success: function(response) {

          if(response.card_status == 'EXISTS'){

            $('#info-renew-card').show();
            $('input#renew_card').val('yes');
            $('button#enrolCard').html('RENEW CARD');
            $('#card_number_info').html(response.card_number);
            $('#mem_name_renew_card').html(response.member_name);
            $("#enroll_card").attr('action', '/jobcard/redirect_jobconfirm');
            

          }else{
            $('#info-renew-card').hide();
            $('input#renew_card').val('no');
            $('button#enrolCard').html('ENTER');
            $('#card_number_info').html("");
            $('#mem_name_renew_card').html("");
          }

          
        }
      });
    }); 

  var customcardsData =  <?= json_encode($cards); ?>;

    var cutomizedCards = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(customcardsData, function(d) {
        return {
          value: d.card_number,
          suggest: d
        }
      })
    });

    cutomizedCards.initialize();

  $("#result-template-enroll-card .card_number").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "card_number",
      display: "value",
      source: cutomizedCards.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.card_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.status +"</div></div></div>"
          }
        }
      });


   var cutomizedCustomerEmails = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace("value"),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      local: $.map(customCustomerData, function(d) {
        return {
          value: d.email,
          suggest: d
        }
      })
    });

  cutomizedCustomerEmails.initialize();

  $("#result-template-enroll-email .card_email").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "email",
      display: "value",
      source: cutomizedCustomerEmails.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.email +"</div><div class='ProfileCard-screenName'>"+ data.suggest.mobile +"</div></div></div>"
          }
        }
  });

  $("#result-template-member-plate .member_plate_number").typeahead({
      minLength: 1,
      highlight: true,
      offset:false,
      hint:true
      }, {
      name: "plate_number",
      display: "value",
      source: cutomizedPlates.ttAdapter(),
        templates: {
          notFound: [
            "<div class='EmptyMessage'>",
            "Your search turned up 0 results. This most likely means the backend is down, yikes!",
            "</div>"
          ].join("\n"),
          suggestion: function(data) {
            return "<div class='ProfileCard u-cf'><img class='ProfileCard-avatar' src='{{ asset('assets/img/car.jpg') }}'><div class='ProfileCard-details'><div class='ProfileCard-realName'>"+ data.suggest.plate_number +"</div><div class='ProfileCard-screenName'>"+ data.suggest.first_name +"</div><div class='ProfileCard-description'><ul><li>"+ data.suggest.car_make +"</li><li>"+ data.suggest.car_model +"</li><li>"+ data.suggest.car_color +"</li></ul></div></div><div class='clr " + data.suggest.car_color +"'></div></div>";
          }
        }
    }).on('typeahead:selected', function(ev,res) {

      $.ajax({
        url: '/jobcard/member_by_plate/'+res.value,
        type:'GET',
        dataType:'json',
        success: function(response) {

          if($.trim(response)){

            $("input[name='member_id']").val(response.member_id);
            $("input[name='member_name']").val(response.first_name);
            $("input[name='member_mobile']").val(response.mobile);
            $("input[name='member_email']").val(response.email);
            $("input[name='main_service_id']").val(response.main_service_id);


            if(response.member_id != ""){

              $('#info-buy-membership').hide();
              $('#info-renew-membership').hide();
              $("span#package_name").text(response.package_name);
              $("span#expiry").text(response.validity_end);
              $("span#wash_remain").text(response.remaining_washes);
              $('#info-package-details').show();
            } else {

              $('#info-renew-membership').hide();
              $('#info-package-details').hide();
              $('#info-buy-membership').show();
              $('#customer_id').val(response.customer_id);
              $('#plate_number_hidden').val(response.plate_number);
            }



          } else{

            $('#info-package-details').hide();
            $("input[name='member_id']").val('');
            $("input[name='member_name']").val('');
            $("input[name='member_email']").val('');
            $("input[name='member_mobile']").val('');
            
          }
          
        }
      });
  });

  //double tap edit section

  $( "input#name" ).dblclick(function() {
    $(this).attr('readonly', false);
  }); 

  $( "input#name" ).focusout(function() {

    var id     = $( "input#edit_customer_id" ).val();
    var value  = $( "input#name" ).val();
    var field  = "first_name";

    if($('input#fromdb').val() == "1"){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({

        url: '/jobcard/editcustomer',
        data:{ id:id, field:field, value:value},
        type:'POST',
        dataType:'json',
        success: function(response) {
          $( "input#name" ).val(response.value);
          $( "input#name" ).attr('readonly', true);
        }

      });

    }
  });

  $( "input#mobile" ).dblclick(function() {
    $(this).attr('readonly', false);
  }); 

  $( "input#mobile" ).focusout(function() {

    var id     = $( "input#edit_customer_id" ).val();
    var value  = $( "input#mobile" ).val();
    var field  = "mobile";

    if($('input#fromdb').val() == "1"){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({

        url: '/jobcard/editcustomer',
        data:{ id:id, field:field, value:value},
        type:'POST',
        dataType:'json',
        success: function(response) {
          $( "input#mobile" ).val(response.value);
          $( "input#mobile" ).attr('readonly', true);
        }

      });

    }
  });
  $( ".howtoknow" ).click(function() {
    $("#submit-error").hide();
    $("#submit-success").hide();
  }); 
  $( "input#email" ).dblclick(function() {
    $(this).attr('readonly', false);
  }); 

  $( "input#email" ).focusout(function() {

    var id     = $( "input#edit_customer_id" ).val();
    var value  = $( "input#email" ).val();
    var field  = "email";

    if($('input#fromdb').val() == "1"){

      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({

        url: '/jobcard/editcustomer',
        data:{ id:id, field:field, value:value},
        type:'POST',
        dataType:'json',
        success: function(response) {
          $( "input#email" ).val(response.value);
          $( "input#email" ).attr('readonly', true);
        }

      });

    }
  });


   


});


  
</script>



  @endsection

