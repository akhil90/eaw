<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class PackagePurchase extends Model
{
    use HasFactory;

    protected $table = 'package_purchases';
    protected $fillable = [
        'package_purchase_number','customer_id', 'main_service_id','price','discount','gross_price','tax','net_amount','payment_type','created_user_id','created_at','updated_at'
    ];

    public static function invoiceCreation($packagePurchase, $data){

        if(Settings::getSettingValueByType('qb_url_format') == "laravel"){

            $url  = 'http://64.4.160.171/eaw_invoice_manager/public/index.php/api/invoice/create';

            if(Settings::getSettingValueByType('api_url')!=null){
                $url  = Settings::getSettingValueByType('api_url').'invoice/create';
            }

         } else if(Settings::getSettingValueByType('qb_url_format') == "docker"){
 
            $url  = 'http://transituae.net:5005/QBO/carwash/api/v1/invoice';

            if(Settings::getSettingValueByType('api_url') != null){
                $url  = Settings::getSettingValueByType('api_url').'invoice';
            }
             
         }

        $packagePurchase->qb_request = json_encode($data);
        $packagePurchase->save();

        try{
            $response  = Http::post($url,$data);
        }
        catch(Exeption $e){
            return $e ;
        }
        
        $packagePurchase->qb_response = $response;
        $packagePurchase->save();

        if($response){

            $responseArray = json_decode($response);

            if($responseArray!=null){

                if($responseArray->response_code == 200){
                    $packagePurchase->inv_ref = $responseArray->qb_invoice_id;
                    $packagePurchase->inv_status = 'CREATED';
                    $packagePurchase->save();
                }else{
                    $packagePurchase->inv_ref = "";
                    $packagePurchase->inv_status = 'PENDING';
                    $packagePurchase->save();
                }

            } else{
                $packagePurchase->inv_ref = "";
                $packagePurchase->inv_status = 'PENDING';
                $packagePurchase->save();
            }
            
        }else{
            $packagePurchase->inv_ref = "";
            $packagePurchase->inv_status = 'PENDING';
            $packagePurchase->save();
        }

        return $response ;
    }
}
