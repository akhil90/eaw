<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->truncate(); //for cleaning earlier data to avoid duplicate entries
        DB::table('settings')->insert([
            'type' => 'point',
            'value'=>'9',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'cash_payment_code',
            'value'=>'1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'card_payment_code',
            'value'=>'5',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'cash_payment_account_code',
            'value'=>'133',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'card_payment_account_code',
            'value'=>'134',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'discount_account_code',
            'value'=>'0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'default_tax_type',
            'value'=>'0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'wp_sync',
            'value'=>'0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'wp_convert',
            'value'=>'0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'api_url',
            'value'=>'http://transituae.net:5005/QBO/carwash/api/v1/',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'zero_invoice_creation',
            'value'=>'yes',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'bank_charge_rate',
            'value'=>'0',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'api_key',
            'value'=>'YXBpa2V5Zm9yam9iY2FyZDE2MDc1OTM4Mzc2ODQyNTg=',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'gate_status',
            'value'=>'LOCK',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_total',
            'value'=>'المجموع',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_discount',
            'value'=>'الخصم',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_GrossAmt',
            'value'=>'المبلغ الإجمالي',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_vat',
            'value'=>'ضريبة القيمة المضافة',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_NetAmount',
            'value'=>'صافي المبلغ',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_PaymentMethod',
            'value'=>'طريقة الدفع',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_PaidAmt',
            'value'=>'المبلغ المدفوع',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_CashAmt',
            'value'=>'المبلغ النقدي',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'arabic_CardAmt',
            'value'=>'مبلغ البطاقة',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'qb_cutomer_name_with_mobile',
            'value'=>'yes', // or no
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('settings')->insert([
            'type' => 'qb_url_format',
            'value'=>'laravel', // or docker
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
