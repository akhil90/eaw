<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_services', function (Blueprint $table) {
            $table->id();
            $table->integer('wp_id')->nullable();
            $table->string('service_name');
            $table->enum('service_category', ['SINGLE', 'ADDON','PACKAGE','FREE','CAR_DETAILING','NFC_CARD']);
            $table->enum('car_type', ['SEDAN', 'SUV'])->nullable();
            $table->decimal('price', 10, 2)->default(0);
            $table->string('duration')->default(0);
            $table->text('features')->nullable();
            $table->string('discount_threshold')->default(0);
            $table->integer('no_of_washes')->default(0);
            $table->string('package_validity')->default(0);
            $table->string('wash_validity')->default(0);
            $table->enum('start_from_type', ['FIRST_WASH', 'PURCHASE_DATE'])->nullable();
            $table->integer('loyality_points')->default(0);
            $table->decimal('vat', 10, 2);
            $table->integer('tax_id')->nullable();
            $table->enum('status', ['ACTIVE', 'DISABLED','PENDING'])->default('ACTIVE');
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('modified_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('modified_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_services');
    }
}
