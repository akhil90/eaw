<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccessPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // if (Auth::check()) {

        //     $role = Auth::user()->role; 

        //     switch ($role) {

        //     case 'ADMIN':

        //         return redirect()->route('home');
        //         break;

        //     case 'STAFF':

        //         return redirect()->route('staff');
        //         break; 
        
        //     default:

        //         return redirect()->route('home');
        //         break;

        //     }

        // } else {

        //     return redirect()->route('login');
        // }
        

        return $next($request);
    }
}
