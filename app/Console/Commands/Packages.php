<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\MainService;
use DB;

class Packages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'package:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching active packages from EAW database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $packages = DB::connection('mysql2')->select("SELECT `ID`,`post_title`,`post_type` FROM `wp_posts` WHERE (`post_type`='product' OR `post_type`='service') AND `post_status`='publish'");
        if($packages != false){
            foreach ($packages as $package) {
                if(MainService::where('wp_id','=',$package->ID)->count() == 0){
                    if($package->post_type == "product"){
                        $service_category = "PACKAGE";
                    }
                    if($package->post_type == "service"){
                        $service_category = "ADDON";
                    }
                    $data = [
                        "wp_id"             => $package->ID,
                        "service_name"      => $package->post_title,
                        "service_category"  => $service_category,
                        "status"            => "PENDING",
                        "created_by"        => 1
                    ];
                    MainService::create($data);
                }
            }
            $this->info('Success');
        }else{
            $this->info('No new packages');
        }
    }
}
