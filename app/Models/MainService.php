<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MainService extends Model
{
    use HasFactory;

    //protected $table = 'main_services';
    protected $fillable = [
        'wp_id','service_name', 'service_category','status','created_by'
    ];
}
