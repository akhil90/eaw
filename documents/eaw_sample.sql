-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2020 at 12:41 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eaw_sample`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_secret_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('ACTIVE','DISABLED','ALLOCATED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `card_number`, `card_secret_number`, `status`, `created_at`, `updated_at`) VALUES
(1, 'card001', '020504', 'ALLOCATED', '2020-10-22 12:06:01', '2020-10-23 04:47:50'),
(2, 'card002', '020505', 'ALLOCATED', '2020-10-22 12:06:01', '2020-10-22 12:06:01'),
(3, 'card003', '020508', 'ALLOCATED', '2020-10-22 12:06:01', '2020-10-23 02:34:52'),
(4, 'card004', '0205010', 'ACTIVE', '2020-10-22 12:06:01', '2020-10-23 04:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('MEMEBER','NON_MEMBER') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NON_MEMBER',
  `how_to_know_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `how_to_know_desc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email`, `mobile`, `address`, `type`, `how_to_know_type`, `how_to_know_desc`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'jyothis', 'simon', 'jyothis@sample.com', '9605617676', 'NIL', 'MEMEBER', NULL, NULL, 2, 2, '2020-10-17 07:03:31', '2020-10-17 07:03:31'),
(2, 'Akhil', 'P', 'Akhil@sample.com', '7907911958', 'NIL', 'MEMEBER', NULL, NULL, 2, 2, '2020-10-17 07:06:31', '2020-10-17 07:06:31'),
(3, 'Athul', 'P', 'Athul@sample.com', '9447452250', 'NIL', 'MEMEBER', NULL, NULL, 2, 2, '2020-10-17 07:07:31', '2020-10-17 07:07:31'),
(4, 'Dipil', 'Raj', 'dipil@sample.com', '9497495180', 'NIL', 'NON_MEMBER', NULL, NULL, 2, 2, '2020-10-17 07:07:45', '2020-10-17 07:07:45'),
(6, 'Manu', NULL, 'manu@123.com', '9605617676', NULL, 'NON_MEMBER', 'referedby', NULL, 2, 2, '2020-10-22 01:26:36', '2020-10-22 01:26:36'),
(7, 'mathew', NULL, 'mathew@123', '9497495180', NULL, 'NON_MEMBER', 'referedby', NULL, 2, 2, '2020-10-22 01:47:07', '2020-10-22 01:47:07'),
(8, 'new_user', NULL, 'new@123', '9447452250', NULL, 'NON_MEMBER', 'referedby', NULL, 2, 2, '2020-10-22 01:54:44', '2020-10-22 01:54:44'),
(9, 'Dipil', NULL, 'jyothis@gmail.ocm', '9497495180', NULL, 'NON_MEMBER', 'referedby', NULL, 2, 2, '2020-10-22 02:27:37', '2020-10-22 02:27:37'),
(10, 'Jm', NULL, 'jm@123.com', '9605617676', NULL, 'NON_MEMBER', 'referedby', NULL, 2, 2, '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(11, 'Athul', NULL, 'ath@gmail.com', '9497495180', NULL, 'NON_MEMBER', 'other', 'From friend', 2, 2, '2020-10-23 04:10:34', '2020-10-23 04:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `history_points`
--

CREATE TABLE `history_points` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('CREDIT','DEBIT') COLLATE utf8mb4_unicode_ci NOT NULL,
  `registered_plate_id` bigint(20) UNSIGNED NOT NULL,
  `job_card_id` bigint(20) UNSIGNED NOT NULL,
  `recieved_points` int(11) NOT NULL,
  `remaining_points` int(11) NOT NULL,
  `recieve_type` enum('SINGLE','REFER') COLLATE utf8mb4_unicode_ci NOT NULL,
  `refered_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `narration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_points`
--

INSERT INTO `history_points` (`id`, `type`, `registered_plate_id`, `job_card_id`, `recieved_points`, `remaining_points`, `recieve_type`, `refered_by`, `created_user_id`, `narration`, `created_at`, `updated_at`) VALUES
(1, 'CREDIT', 54, 1, 9, 27, 'REFER', NULL, 2, 'Credited for a wash for refer the plate -NO 0001 P 6071', '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(2, 'CREDIT', 68, 1, 1, 1, 'SINGLE', NULL, 2, 'Credited for a wash, It is referedby -KL 0001 P 6068', '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(3, 'CREDIT', 64, 2, 9, 10, 'REFER', NULL, 2, 'Credited for a wash for refer the plate -NP 0001 P 6071', '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(4, 'CREDIT', 69, 2, 1, 1, 'SINGLE', NULL, 2, 'Credited for a wash, It is referedby -ML 002 K 1245', '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(6, 'CREDIT', 64, 4, 9, 28, 'REFER', NULL, 2, 'Credited for a wash for refer the plate -KP 0001 P 6069', '2020-10-22 03:42:49', '2020-10-22 03:42:49'),
(7, 'CREDIT', 70, 4, 1, 1, 'SINGLE', NULL, 2, 'Credited for a wash, It is referedby -ML 002 K 1245', '2020-10-22 03:42:49', '2020-10-22 03:42:49'),
(8, 'CREDIT', 64, 5, 9, 37, 'REFER', 71, 2, 'Credited for a wash for refer the plate -KJ 0001 P 6071', '2020-10-22 03:47:28', '2020-10-22 03:47:28'),
(9, 'CREDIT', 71, 5, 1, 1, 'SINGLE', 64, 2, 'Credited for a wash, It is referedby -ML 002 K 1245', '2020-10-22 03:47:28', '2020-10-22 03:47:28'),
(10, 'CREDIT', 64, 6, 1, 38, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-22 05:51:44', '2020-10-22 05:51:44'),
(11, 'CREDIT', 54, 7, 1, 28, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-22 05:54:09', '2020-10-22 05:54:09'),
(12, 'DEBIT', 55, 8, 9, 0, 'SINGLE', NULL, 2, 'Credited for a wash by redeem', '2020-10-22 06:19:18', '2020-10-22 06:19:18'),
(13, 'CREDIT', 70, 9, 9, 10, 'REFER', 72, 2, 'Credited for a wash for refer the plate -KS 0001 P 6069', '2020-10-22 06:26:57', '2020-10-22 06:26:57'),
(14, 'CREDIT', 72, 9, 1, 1, 'SINGLE', 70, 2, 'Credited for a wash, It is referedby -KP 0001 P 6069', '2020-10-22 06:26:57', '2020-10-22 06:26:57'),
(15, 'DEBIT', 57, 13, 9, 2, 'SINGLE', NULL, 2, 'Credited for a wash by redeem', '2020-10-23 03:24:02', '2020-10-23 03:24:02'),
(16, 'CREDIT', 57, 14, 1, 3, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-23 04:01:35', '2020-10-23 04:01:35'),
(17, 'CREDIT', 8, 15, 1, 5, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-23 04:03:00', '2020-10-23 04:03:00'),
(18, 'CREDIT', 72, 16, 1, 2, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-23 04:05:41', '2020-10-23 04:05:41'),
(19, 'DEBIT', 70, 17, 9, 1, 'SINGLE', NULL, 2, 'Credited for a wash by redeem', '2020-10-23 04:06:02', '2020-10-23 04:06:02'),
(20, 'CREDIT', 71, 18, 9, 10, 'REFER', 73, 2, 'Credited for a wash for refer the plate -CC 002 K 1245', '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(21, 'CREDIT', 73, 18, 1, 1, 'SINGLE', 71, 2, 'Credited for a wash, It is referedby -KJ 0001 P 6071', '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(22, 'CREDIT', 74, 19, 1, 2, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-23 04:08:08', '2020-10-23 04:08:08'),
(23, 'CREDIT', 75, 20, 1, 2, 'SINGLE', NULL, 2, 'Credited for a wash', '2020-10-23 04:10:34', '2020-10-23 04:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `job_cards`
--

CREATE TABLE `job_cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_card_type` enum('MEMBER','SINGLE','FREE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `total_discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `gross_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `plate_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `earned_points` int(11) NOT NULL DEFAULT 0,
  `payment_type` enum('CARD','CASH','OTHER') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txn_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inv_status` enum('PENDING','CREATED','FAILED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PENDING',
  `inv_ref` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_cards`
--

INSERT INTO `job_cards` (`id`, `job_card_number`, `job_card_type`, `total_price`, `total_discount`, `gross_price`, `customer_id`, `plate_number`, `earned_points`, `payment_type`, `txn_number`, `inv_status`, `inv_ref`, `created_user_id`, `created_at`, `updated_at`) VALUES
(1, 'JOB1603357451', 'SINGLE', '50.00', '0.00', '50.00', 10, 'NO 0001 P 6071', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(2, 'JOB1603357679', 'SINGLE', '50.00', '0.00', '50.00', 9, 'NP 0001 P 6071', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(3, 'JOB1603357855', 'SINGLE', '50.00', '0.00', '50.00', 9, 'KP 0001 P 6069', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 03:40:55', '2020-10-22 03:40:55'),
(4, 'JOB1603357969', 'SINGLE', '50.00', '0.00', '50.00', 9, 'KP 0001 P 6069', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 03:42:49', '2020-10-22 03:42:49'),
(5, 'JOB1603358248', 'SINGLE', '50.00', '0.00', '50.00', 3, 'KJ 0001 P 6071', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 03:47:28', '2020-10-22 03:47:28'),
(6, 'JOB1603365704', 'SINGLE', '50.00', '0.00', '50.00', 9, 'ML 002 K 1245', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 05:51:44', '2020-10-22 05:51:44'),
(7, 'JOB1603365849', 'SINGLE', '55.00', '0.00', '55.00', 2, 'KL 0001 P 6068', 1, 'CARD', NULL, 'PENDING', NULL, 2, '2020-10-22 05:54:09', '2020-10-22 05:54:09'),
(8, 'JOB1603367358', 'FREE', '40.00', '0.00', '40.00', 3, 'KL 0001 P 6069', 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 06:19:18', '2020-10-22 06:19:18'),
(9, 'JOB1603367817', 'SINGLE', '50.00', '0.00', '50.00', 9, 'KS 0001 P 6069', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-22 06:26:57', '2020-10-22 06:26:57'),
(10, 'JOB1603432325', 'MEMBER', '20.00', '0.00', '20.00', 2, NULL, 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 00:22:05', '2020-10-23 00:22:05'),
(11, 'JOB1603432501', 'MEMBER', '20.00', '0.00', '20.00', 1, 'pl 001 258 125', 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 00:25:01', '2020-10-23 00:25:01'),
(12, 'JOB1603435920', 'MEMBER', '20.00', '0.00', '20.00', 1, 'KL 123 4578', 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 01:22:00', '2020-10-23 01:22:00'),
(13, 'JOB1603443242', 'FREE', '20.00', '0.00', '20.00', 1, 'KL 0001 P 6071', 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 03:24:02', '2020-10-23 03:24:02'),
(14, 'JOB1603445495', 'SINGLE', '50.00', '0.00', '50.00', 1, 'KL 0001 P 6071', 1, 'CARD', NULL, 'PENDING', NULL, 2, '2020-10-23 04:01:35', '2020-10-23 04:01:35'),
(15, 'JOB1603445579', 'SINGLE', '50.00', '0.00', '50.00', 4, 'GO 0051 P 8091', 1, 'CARD', NULL, 'PENDING', NULL, 2, '2020-10-23 04:02:59', '2020-10-23 04:02:59'),
(16, 'JOB1603445741', 'SINGLE', '50.00', '0.00', '50.00', 9, 'KS 0001 P 6069', 1, 'CARD', '1231554', 'PENDING', NULL, 2, '2020-10-23 04:05:41', '2020-10-23 04:05:41'),
(17, 'JOB1603445762', 'FREE', '20.00', '0.00', '20.00', 9, 'KP 0001 P 6069', 0, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 04:06:02', '2020-10-23 04:06:02'),
(18, 'JOB1603445836', 'SINGLE', '50.00', '0.00', '50.00', 4, 'CC 002 K 1245', 1, 'CARD', 'dfgh', 'PENDING', NULL, 2, '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(19, 'JOB1603445888', 'SINGLE', '50.00', '0.00', '50.00', 9, 'KC 0001 P 6069', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 04:08:08', '2020-10-23 04:08:08'),
(20, 'JOB1603446034', 'SINGLE', '50.00', '0.00', '50.00', 11, 'CD 002 K 1245', 1, 'CASH', NULL, 'PENDING', NULL, 2, '2020-10-23 04:10:34', '2020-10-23 04:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `job_card_details`
--

CREATE TABLE `job_card_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `job_card_id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` bigint(20) UNSIGNED NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `discount` decimal(10,2) NOT NULL DEFAULT 0.00,
  `gross_price` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_card_details`
--

INSERT INTO `job_card_details` (`id`, `job_card_id`, `main_service_id`, `price`, `discount`, `gross_price`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '30.00', '0.00', '30.00', '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(2, 1, 3, '20.00', '0.00', '20.00', '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(3, 2, 1, '30.00', '0.00', '30.00', '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(4, 2, 3, '20.00', '0.00', '20.00', '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(5, 3, 1, '30.00', '0.00', '30.00', '2020-10-22 03:40:55', '2020-10-22 03:40:55'),
(6, 3, 3, '20.00', '0.00', '20.00', '2020-10-22 03:40:55', '2020-10-22 03:40:55'),
(7, 4, 1, '30.00', '0.00', '30.00', '2020-10-22 03:42:49', '2020-10-22 03:42:49'),
(8, 4, 3, '20.00', '0.00', '20.00', '2020-10-22 03:42:49', '2020-10-22 03:42:49'),
(9, 5, 1, '30.00', '0.00', '30.00', '2020-10-22 03:47:28', '2020-10-22 03:47:28'),
(10, 5, 3, '20.00', '0.00', '20.00', '2020-10-22 03:47:28', '2020-10-22 03:47:28'),
(11, 6, 1, '30.00', '0.00', '30.00', '2020-10-22 05:51:44', '2020-10-22 05:51:44'),
(12, 6, 3, '20.00', '0.00', '20.00', '2020-10-22 05:51:44', '2020-10-22 05:51:44'),
(13, 7, 2, '35.00', '0.00', '35.00', '2020-10-22 05:54:09', '2020-10-22 05:54:09'),
(14, 7, 4, '20.00', '0.00', '20.00', '2020-10-22 05:54:09', '2020-10-22 05:54:09'),
(15, 8, 3, '20.00', '0.00', '20.00', '2020-10-22 06:19:18', '2020-10-22 06:19:18'),
(16, 8, 4, '20.00', '0.00', '20.00', '2020-10-22 06:19:18', '2020-10-22 06:19:18'),
(17, 8, 5, '0.00', '0.00', '0.00', '2020-10-22 06:19:18', '2020-10-22 06:19:18'),
(18, 9, 1, '30.00', '0.00', '30.00', '2020-10-22 06:26:57', '2020-10-22 06:26:57'),
(19, 9, 3, '20.00', '0.00', '20.00', '2020-10-22 06:26:57', '2020-10-22 06:26:57'),
(20, 10, 3, '20.00', '0.00', '20.00', '2020-10-23 00:22:05', '2020-10-23 00:22:05'),
(21, 10, 6, '0.00', '0.00', '0.00', '2020-10-23 00:22:05', '2020-10-23 00:22:05'),
(22, 11, 3, '20.00', '0.00', '20.00', '2020-10-23 00:25:01', '2020-10-23 00:25:01'),
(23, 11, 6, '0.00', '0.00', '0.00', '2020-10-23 00:25:01', '2020-10-23 00:25:01'),
(24, 12, 3, '20.00', '0.00', '20.00', '2020-10-23 01:22:00', '2020-10-23 01:22:00'),
(25, 12, 6, '0.00', '0.00', '0.00', '2020-10-23 01:22:00', '2020-10-23 01:22:00'),
(26, 13, 3, '20.00', '0.00', '20.00', '2020-10-23 03:24:02', '2020-10-23 03:24:02'),
(27, 13, 5, '0.00', '0.00', '0.00', '2020-10-23 03:24:02', '2020-10-23 03:24:02'),
(28, 14, 1, '30.00', '0.00', '30.00', '2020-10-23 04:01:35', '2020-10-23 04:01:35'),
(29, 14, 3, '20.00', '0.00', '20.00', '2020-10-23 04:01:35', '2020-10-23 04:01:35'),
(30, 15, 1, '30.00', '0.00', '30.00', '2020-10-23 04:03:00', '2020-10-23 04:03:00'),
(31, 15, 3, '20.00', '0.00', '20.00', '2020-10-23 04:03:00', '2020-10-23 04:03:00'),
(32, 16, 1, '30.00', '0.00', '30.00', '2020-10-23 04:05:41', '2020-10-23 04:05:41'),
(33, 16, 3, '20.00', '0.00', '20.00', '2020-10-23 04:05:41', '2020-10-23 04:05:41'),
(34, 17, 4, '20.00', '0.00', '20.00', '2020-10-23 04:06:02', '2020-10-23 04:06:02'),
(35, 17, 5, '0.00', '0.00', '0.00', '2020-10-23 04:06:02', '2020-10-23 04:06:02'),
(36, 18, 1, '30.00', '0.00', '30.00', '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(37, 18, 3, '20.00', '0.00', '20.00', '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(38, 19, 1, '30.00', '0.00', '30.00', '2020-10-23 04:08:08', '2020-10-23 04:08:08'),
(39, 19, 3, '20.00', '0.00', '20.00', '2020-10-23 04:08:08', '2020-10-23 04:08:08'),
(40, 20, 1, '30.00', '0.00', '30.00', '2020-10-23 04:10:34', '2020-10-23 04:10:34'),
(41, 20, 3, '20.00', '0.00', '20.00', '2020-10-23 04:10:34', '2020-10-23 04:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `main_services`
--

CREATE TABLE `main_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_category` enum('SINGLE','ADDON','PACKAGE','FREE') COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_type` enum('SEDAN','SUV','BOTH') COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `duration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `features` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_threshhold` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `no_of_washes` int(11) NOT NULL DEFAULT 1,
  `package_validity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `wash_validity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `start_from_type` enum('FIRST_WASH','PURCHASE_DATE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'FIRST_WASH',
  `loyality_points` int(11) NOT NULL DEFAULT 0,
  `vat` decimal(10,2) NOT NULL DEFAULT 0.00,
  `status` enum('ACTIVE','DISABLED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `modified_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_services`
--

INSERT INTO `main_services` (`id`, `service_name`, `service_category`, `car_type`, `price`, `duration`, `features`, `discount_threshhold`, `no_of_washes`, `package_validity`, `wash_validity`, `start_from_type`, `loyality_points`, `vat`, `status`, `created_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'EXPRESS GOLD', 'SINGLE', 'SEDAN', '30.00', '5', '[\"High Pressure Rinse\", \"Pre-Soak, Wheel Boss (Wheel Clean)\", \"Undercarriage Friction/Touch Free\", \"Tri-Colour Foam, Lustra Wax\", \"Rain Shield, Gloss Boss (Tyre Shine)\", \"Super Dryer\", \"Hand Dry Finish\"]', '10', 1, '0', '0', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:27:57', '2020-10-20 03:27:57'),
(2, 'EXPRESS GOLD', 'SINGLE', 'SUV', '35.00', '5', '[\"High Pressure Rinse\", \"Pre-Soak, Wheel Boss (Wheel Clean)\", \"Undercarriage Friction/Touch Free\", \"Tri-Colour Foam, Lustra Wax\", \"Rain Shield, Gloss Boss (Tyre Shine)\", \"Super Dryer\", \"Hand Dry Finish\"]', '10', 1, '0', '0', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:27:57', '2020-10-20 03:27:57'),
(3, 'Interior Detaining', 'ADDON', 'BOTH', '20.00', '20', '[\"Cover Clean Vacuum Inside Fuming Perfume\",\"30 days Validity\",\"Single Plate Number\"]', '10', 1, '0', '0', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:35:06', '2020-10-20 03:35:06'),
(4, 'Tyre altration', 'ADDON', 'BOTH', '20.00', '20', '[\"Cover Clean Vacuum Inside Fuming Perfume\",\"Single Plate Number\"]', '10', 1, '0', '0', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:35:06', '2020-10-20 03:35:06'),
(5, 'FREE SERVICE', 'FREE', 'BOTH', '0.00', '5', '[\"High Pressure Rinse\", \"Pre-Soak, Wheel Boss (Wheel Clean)\", \"Undercarriage Friction/Touch Free\", \"Tri-Colour Foam, Lustra Wax\", \"Rain Shield, Gloss Boss (Tyre Shine)\", \"Super Dryer\", \"Hand Dry Finish\"]', '10', 1, '0', '0', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:27:57', '2020-10-20 03:27:57'),
(6, 'SILVER PACKAGE', 'PACKAGE', 'BOTH', '0.00', '5', '[\"High Pressure Rinse\", \"Pre-Soak, Wheel Boss (Wheel Clean)\", \"Undercarriage Friction/Touch Free\", \"Tri-Colour Foam, Lustra Wax\", \"Rain Shield, Gloss Boss (Tyre Shine)\", \"Super Dryer\", \"Hand Dry Finish\"]', '10', 10, '90', '60', 'FIRST_WASH', 0, '5.00', 'ACTIVE', 1, 1, '2020-10-20 03:27:57', '2020-10-20 03:27:57');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `membership_package_log_id` bigint(20) UNSIGNED NOT NULL,
  `service_completed` int(11) NOT NULL DEFAULT 0,
  `service_remaining` int(11) NOT NULL DEFAULT 0,
  `card_id` bigint(20) UNSIGNED DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` enum('ACTIVE','DISABLED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `card_number`, `customer_id`, `membership_package_log_id`, `service_completed`, `service_remaining`, `card_id`, `start_date`, `end_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'card001', 1, 1, 2, 8, 1, '2020-10-23 05:55:01', '2020-12-22 05:55:01', 'ACTIVE', NULL, '2020-10-23 04:47:50'),
(2, 'card002', 2, 2, 0, 10, 2, '2020-10-23 05:49:41', '2020-12-22 05:49:41', 'ACTIVE', NULL, '2020-10-23 00:22:05'),
(4, 'card003', 3, 2, 0, 10, 3, NULL, NULL, 'ACTIVE', NULL, '2020-10-23 02:34:52');

-- --------------------------------------------------------

--
-- Table structure for table `membership_package_logs`
--

CREATE TABLE `membership_package_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `main_service_id` bigint(20) UNSIGNED NOT NULL,
  `service_completed` int(11) NOT NULL DEFAULT 0,
  `service_remaining` int(11) NOT NULL DEFAULT 0,
  `package_purchase_date` datetime DEFAULT NULL,
  `package_end_date` datetime DEFAULT NULL,
  `wash_validity_start` datetime DEFAULT NULL,
  `wash_validity_end` datetime DEFAULT NULL,
  `status` enum('ACTIVE','EXPIRED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `membership_package_logs`
--

INSERT INTO `membership_package_logs` (`id`, `customer_id`, `main_service_id`, `service_completed`, `service_remaining`, `package_purchase_date`, `package_end_date`, `wash_validity_start`, `wash_validity_end`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 2, 8, '2020-10-22 17:42:44', '2020-12-22 17:42:44', '2020-10-23 05:55:01', '2020-12-22 05:55:01', 'ACTIVE', NULL, '2020-10-23 01:22:00'),
(2, 2, 6, 0, 10, '2020-10-22 17:42:44', '2020-12-22 17:42:44', '2020-10-23 05:52:05', '2020-12-22 05:52:05', 'ACTIVE', NULL, '2020-10-23 00:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_29_112022_create_main_services_table', 1),
(5, '2020_10_14_062138_create_customers_table', 1),
(6, '2020_10_14_063146_create_registered_plates_table', 1),
(7, '2020_10_14_065155_create_cards_table', 1),
(8, '2020_10_14_071301_create_job_cards_table', 1),
(9, '2020_10_14_072953_create_job_card_details_table', 1),
(10, '2020_10_15_034833_create_history_points_table', 1),
(11, '2020_10_15_095010_create_membership_package_logs', 1),
(12, '2020_10_16_070227_create_members_table', 1),
(13, '2020_10_22_094034_create_settings_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registered_plates`
--

CREATE TABLE `registered_plates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `plate_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_points_earned` int(11) NOT NULL DEFAULT 0,
  `remaining_points` int(11) NOT NULL DEFAULT 0,
  `car_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_make` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `car_type` enum('SEDAN','SUV') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer_id` bigint(11) UNSIGNED DEFAULT NULL,
  `status` enum('ACTIVE','DISABLED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registered_plates`
--

INSERT INTO `registered_plates` (`id`, `customer_id`, `plate_number`, `total_points_earned`, `remaining_points`, `car_color`, `car_make`, `car_model`, `car_type`, `referer_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'KL 0001 P 6022', 2, 2, 'red', 'maruti', '2016', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 01:15:15'),
(2, 2, 'TN 0001 P 6000', 2, 2, 'black', 'tata', '2015', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 3, 'AN 0001 P 8090', 3, 3, 'blue', 'hundai', '2017', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, 'GO 0051 P 8090', 4, 4, 'white', 'toyota', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 'TN 0001 P 6001', 2, 2, 'black', 'tata', '2015', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 3, 'AN 0001 P 8091', 21, 21, 'blue', 'hundai', '2017', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 03:31:07'),
(8, 4, 'GO 0051 P 8091', 5, 5, 'white', 'toyota', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-23 04:03:00'),
(9, 1, 'KL 0001 P 6023', 0, 0, 'red', 'maruti', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 'KL 0001 P 6024', 0, 0, 'black', 'tata', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 3, 'KL 0001 P 6025', 0, 0, 'blue', 'hundai', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 4, 'KL 0001 P 6026', 0, 0, 'white', 'toyota', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 'KL 0001 P 6027', 0, 0, 'red', 'maruti', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 'KL 0001 P 6028', 0, 0, 'black', 'tata', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 3, 'KL 0001 P 6029', 0, 0, 'blue', 'hundai', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 4, 'KL 0001 P 6030', 0, 0, 'white', 'toyota', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 1, 'KL 0001 P 6031', 0, 0, 'red', 'maruti', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 2, 'KL 0001 P 6032', 0, 0, 'black', 'tata', '2020', 'SUV', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 3, 'KL 0001 P 6033', 0, 0, 'blue', 'hundai', '2020', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 4, 'KL 0001 P 6034', 0, 0, 'blue', 'hundai', '2021', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 1, 'KL 0001 P 6035', 0, 0, 'blue', 'hundai', '2022', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 2, 'KL 0001 P 6036', 0, 0, 'blue', 'hundai', '2023', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 3, 'KL 0001 P 6037', 0, 0, 'blue', 'hundai', '2024', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 4, 'KL 0001 P 6038', 0, 0, 'blue', 'hundai', '2025', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 1, 'KL 0001 P 6039', 0, 0, 'blue', 'hundai', '2026', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 2, 'KL 0001 P 6040', 0, 0, 'blue', 'hundai', '2027', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 3, 'KL 0001 P 6041', 0, 0, 'blue', 'hundai', '2028', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 4, 'KL 0001 P 6042', 0, 0, 'blue', 'hundai', '2029', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 1, 'KL 0001 P 6043', 0, 0, 'blue', 'hundai', '2030', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 2, 'KL 0001 P 6044', 0, 0, 'blue', 'hundai', '2031', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 3, 'KL 0001 P 6045', 0, 0, 'blue', 'hundai', '2032', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 4, 'KL 0001 P 6046', 0, 0, 'blue', 'hundai', '2033', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 1, 'KL 0001 P 6047', 0, 0, 'blue', 'hundai', '2034', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 2, 'KL 0001 P 6048', 0, 0, 'blue', 'hundai', '2035', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 3, 'KL 0001 P 6049', 0, 0, 'blue', 'hundai', '2036', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 4, 'KL 0001 P 6050', 0, 0, 'blue', 'hundai', '2037', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 1, 'KL 0001 P 6051', 0, 0, 'blue', 'hundai', '2038', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 2, 'KL 0001 P 6052', 0, 0, 'blue', 'hundai', '2039', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 3, 'KL 0001 P 6053', 0, 0, 'blue', 'hundai', '2040', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 4, 'KL 0001 P 6054', 0, 0, 'blue', 'hundai', '2041', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 1, 'KL 0001 P 6055', 0, 0, 'blue', 'hundai', '2042', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 2, 'KL 0001 P 6056', 0, 0, 'blue', 'hundai', '2043', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 3, 'KL 0001 P 6057', 0, 0, 'blue', 'hundai', '2044', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 4, 'KL 0001 P 6058', 0, 0, 'blue', 'hundai', '2045', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 1, 'KL 0001 P 6059', 0, 0, 'blue', 'hundai', '2046', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 2, 'KL 0001 P 6060', 0, 0, 'blue', 'hundai', '2047', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 3, 'KL 0001 P 6061', 0, 0, 'blue', 'hundai', '2048', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 4, 'KL 0001 P 6062', 0, 0, 'blue', 'hundai', '2049', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 1, 'KL 0001 P 6063', 0, 0, 'blue', 'hundai', '2050', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 2, 'KL 0001 P 6064', 0, 0, 'blue', 'hundai', '2051', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 3, 'KL 0001 P 6065', 0, 0, 'blue', 'hundai', '2052', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 4, 'KL 0001 P 6066', 0, 0, 'blue', 'hundai', '2053', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 1, 'KL 0001 P 6067', 18, 18, 'blue', 'hundai', '2054', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 02:27:37'),
(54, 2, 'KL 0001 P 6068', 28, 28, 'blue', 'hundai', '2055', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 05:54:09'),
(55, 3, 'KL 0001 P 6069', 0, 0, 'blue', 'hundai', '2056', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 06:19:18'),
(56, 4, 'KL 0001 P 6070', 1, 1, 'blue', 'hundai', '2057', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-22 01:00:47'),
(57, 1, 'KL 0001 P 6071', 3, 3, 'blue', 'hundai', '2058', 'SEDAN', NULL, 'ACTIVE', '0000-00-00 00:00:00', '2020-10-23 04:01:35'),
(59, 6, 'JN 0001 P 6001', 1, 1, 'red', 'maruti', '2030', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 01:26:36', '2020-10-22 01:26:36'),
(60, 7, 'PN 0001 P 6071', 1, 1, 'blue', 'hundai', '2017', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 01:47:07', '2020-10-22 01:47:07'),
(61, 8, 'MN 002 K 1245', 1, 1, 'yellow', 'Wolswagon', '2010', 'SUV', NULL, 'ACTIVE', '2020-10-22 01:54:45', '2020-10-22 01:54:45'),
(62, 1, 'MK 002 K 1245', 1, 1, 'yellow', 'Wolswagon', '2017', 'SUV', NULL, 'ACTIVE', '2020-10-22 02:01:25', '2020-10-22 02:01:25'),
(63, 3, 'JK 0001 P 6001', 1, 1, 'green', 'Wolswagon', '2058', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 02:15:14', '2020-10-22 02:15:14'),
(64, 9, 'ML 002 K 1245', 38, 38, 'red', 'Wolswagon', '2017', 'SUV', NULL, 'ACTIVE', '2020-10-22 02:27:37', '2020-10-22 05:51:44'),
(65, 9, 'MNC 0001 P 6068', 1, 1, 'yellow', 'maruti', '2017', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:20:10', '2020-10-22 03:20:10'),
(66, 9, 'At 0001 P 8091', 1, 1, 'yellow', 'hundai', '2058', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:25:05', '2020-10-22 03:25:05'),
(67, 9, 'MC 002 K 1245', 1, 1, 'blue', 'maruti', '2058', 'SUV', NULL, 'ACTIVE', '2020-10-22 03:31:07', '2020-10-22 03:31:07'),
(68, 10, 'NO 0001 P 6071', 1, 1, 'yellow', 'Wolswagon', '2058', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:34:11', '2020-10-22 03:34:11'),
(69, 9, 'NP 0001 P 6071', 1, 1, 'red', 'hundai', '2058', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:37:59', '2020-10-22 03:37:59'),
(70, 9, 'KP 0001 P 6069', 1, 1, 'yellow', 'hundai', '2017', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:40:55', '2020-10-23 04:06:02'),
(71, 3, 'KJ 0001 P 6071', 10, 10, 'green', 'hundai', '2017', 'SEDAN', NULL, 'ACTIVE', '2020-10-22 03:47:28', '2020-10-23 04:07:16'),
(72, 9, 'KS 0001 P 6069', 2, 2, 'green', 'Wolswagon', '2057', 'SEDAN', 70, 'ACTIVE', '2020-10-22 06:26:57', '2020-10-23 04:05:41'),
(73, 4, 'CC 002 K 1245', 1, 1, 'blue', 'Wolswagon', '2017', 'SUV', 71, 'ACTIVE', '2020-10-23 04:07:16', '2020-10-23 04:07:16'),
(74, 9, 'KC 0001 P 6069', 2, 2, 'green', 'hundai', '2058', 'SEDAN', NULL, 'ACTIVE', '2020-10-23 04:08:08', '2020-10-23 04:08:08'),
(75, 11, 'CD 002 K 1245', 2, 2, 'blue', 'Wolswagon', '2017', 'SUV', NULL, 'ACTIVE', '2020-10-23 04:10:34', '2020-10-23 04:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'point', 9, '2020-10-22 09:46:30', '2020-10-22 09:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('MALE','FEMALE') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `department` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_file_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('ADMIN','STAFF') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'STAFF',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `employee_id`, `name`, `email`, `mobile`, `gender`, `designation`, `department`, `password`, `image_file_name`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'EM0001', 'the admin user', 'iamadmin@gmail.com', '9605617676', NULL, NULL, NULL, '$2y$10$cwiBN9gpw4Tjm.pxQdKQs.WCr9G8QbRqEJK.X/LGiNrTgsBFI.pLu', NULL, 'ADMIN', NULL, '2020-10-15 05:33:00', '2020-10-15 05:33:00'),
(2, 'EM0002', 'the staff user', 'iamastaff@gmail.com', '9605617678', NULL, NULL, NULL, '$2y$10$2eTwqAk4.66w1vmOeu8pyu6FKdoNf3MQvZc.lFOzzxCc3i3009rH.', NULL, 'STAFF', NULL, '2020-10-15 05:33:00', '2020-10-15 05:33:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`),
  ADD KEY `customers_created_by_foreign` (`created_by`),
  ADD KEY `customers_modified_by_foreign` (`modified_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `history_points`
--
ALTER TABLE `history_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `history_points_registered_plate_id_foreign` (`registered_plate_id`),
  ADD KEY `history_points_job_card_id_foreign` (`job_card_id`),
  ADD KEY `history_points_created_user_id_foreign` (`created_user_id`);

--
-- Indexes for table `job_cards`
--
ALTER TABLE `job_cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_cards_customer_id_foreign` (`customer_id`),
  ADD KEY `job_cards_created_user_id_foreign` (`created_user_id`);

--
-- Indexes for table `job_card_details`
--
ALTER TABLE `job_card_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_card_details_job_card_id_foreign` (`job_card_id`),
  ADD KEY `job_card_details_main_service_id_foreign` (`main_service_id`);

--
-- Indexes for table `main_services`
--
ALTER TABLE `main_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_services_created_by_foreign` (`created_by`),
  ADD KEY `main_services_modified_by_foreign` (`modified_by`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `members_customer_id_foreign` (`customer_id`),
  ADD KEY `members_membership_package_log_id_foreign` (`membership_package_log_id`),
  ADD KEY `members_card_id_foreign` (`card_id`);

--
-- Indexes for table `membership_package_logs`
--
ALTER TABLE `membership_package_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_package_logs_customer_id_foreign` (`customer_id`),
  ADD KEY `membership_package_logs_main_service_id_foreign` (`main_service_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registered_plates`
--
ALTER TABLE `registered_plates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate_number` (`plate_number`),
  ADD KEY `registered_plates_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_employee_id_unique` (`employee_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `history_points`
--
ALTER TABLE `history_points`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `job_cards`
--
ALTER TABLE `job_cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `job_card_details`
--
ALTER TABLE `job_card_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `main_services`
--
ALTER TABLE `main_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `membership_package_logs`
--
ALTER TABLE `membership_package_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `registered_plates`
--
ALTER TABLE `registered_plates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `customers_modified_by_foreign` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `history_points`
--
ALTER TABLE `history_points`
  ADD CONSTRAINT `history_points_created_user_id_foreign` FOREIGN KEY (`created_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_points_job_card_id_foreign` FOREIGN KEY (`job_card_id`) REFERENCES `job_cards` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_points_registered_plate_id_foreign` FOREIGN KEY (`registered_plate_id`) REFERENCES `registered_plates` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_cards`
--
ALTER TABLE `job_cards`
  ADD CONSTRAINT `job_cards_created_user_id_foreign` FOREIGN KEY (`created_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_cards_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `job_card_details`
--
ALTER TABLE `job_card_details`
  ADD CONSTRAINT `job_card_details_job_card_id_foreign` FOREIGN KEY (`job_card_id`) REFERENCES `job_cards` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_card_details_main_service_id_foreign` FOREIGN KEY (`main_service_id`) REFERENCES `main_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `main_services`
--
ALTER TABLE `main_services`
  ADD CONSTRAINT `main_services_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `main_services_modified_by_foreign` FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_card_id_foreign` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `members_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `members_membership_package_log_id_foreign` FOREIGN KEY (`membership_package_log_id`) REFERENCES `membership_package_logs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `membership_package_logs`
--
ALTER TABLE `membership_package_logs`
  ADD CONSTRAINT `membership_package_logs_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `membership_package_logs_main_service_id_foreign` FOREIGN KEY (`main_service_id`) REFERENCES `main_services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `registered_plates`
--
ALTER TABLE `registered_plates`
  ADD CONSTRAINT `registered_plates_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
