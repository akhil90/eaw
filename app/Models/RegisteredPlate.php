<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class RegisteredPlate extends Model
{
    use HasFactory;

    protected $table = 'registered_plates';

    protected $fillable = [
        'customer_id', 'plate_number','total_points_earned','remaining_points','car_color','car_make','car_model','car_type','status','created_at','updated_at'
    ];


    public static function getAll($filter = []){

        $query =  RegisteredPlate::select([
                    'registered_plates.id',
                    'registered_plates.customer_id',
                    'registered_plates.plate_number',
                    'registered_plates.total_points_earned', 
                    'registered_plates.remaining_points',
                    'registered_plates.car_color',
                    'registered_plates.car_make', 
                    'registered_plates.car_model',
                    'registered_plates.car_type',
                    'registered_plates.status',
                    'registered_plates.created_at',
                    'registered_plates.updated_at',
                    'customers.first_name',
                    'customers.email',
                    'customers.type',
                    'customers.mobile',
                    'customers.address',
                    'customers.created_by',
                    'customers.modified_by',
                ]);

        $query = $query->leftjoin('customers', 'customers.id', '=', 'registered_plates.customer_id');
        
      

        if(isset($filter['plate_number'])){
            $query = $query->Where('registered_plates.plate_number',$filter['plate_number']);
        }

        if(isset($filter['customer_id'])){
            $query = $query->Where('registered_plates.customer_id',$filter['customer_id']);
        }

        if(isset($filter['car_color'])){
                $query = $query->Where('registered_plates.car_color',$filter['car_color']);
        }

        if(isset($filter['car_make'])){
            $query = $query->Where('registered_plates.car_make',$filter['car_make']);
        }

        if(isset($filter['car_model'])){
            $query = $query->Where('registered_plates.car_model',$filter['car_model']);
        }

        if(isset($filter['first_name'])){
            $query = $query->Where('customers.first_name',$filter['first_name']);
        }

        if(isset($filter['mobile'])){
            $query = $query->Where('customers.mobile',$filter['mobile']);
        }

        if(isset($filter['email'])){
            $query = $query->Where('customers.email',$filter['email']);
        }

        $query = $query->orderBy('registered_plates.id', 'DESC');
        $plates = $query->get();

        return $plates;    
    }




}
