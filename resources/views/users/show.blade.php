@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Users</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
      <div class="col-lg-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>User List</h3>
              <div class="ibox-tools"> <a href="/users/create" style="margin-right: 25px; color: #fff !important;" type="button" title="Add new service" class="btn btn-round btn-success"><i class="fa fa-plus"></i> User</a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th>User Id</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Mobile</th>
                      <th>Role</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>  
                    @foreach($users as $user)                
                     <tr>
                      <td width="15%">{{$user->employee_id}}</td>
                      <td width="10%">{{$user->name}}</td> 
                      <td width="10%">{{$user->email}}</td>
                      <td width="10%">{{$user->mobile}}</td>
                      <td width="10%">{{$user->role}}</td>
                      <td width="10%">{{$user->status}}</td>
                      <td width="8%">
                        <a title="Edit this service" href="/users/edit/{{$user->id}}" class="btn btn-round btn-info btn-xs" style="margin: 3px;">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        @if($user->status == "ACTIVE")                        
                        <a href="/users/block/{{$user->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this service">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($user->status == "DISABLED")
                        <a href="/users/unblock/{{$user->id}}" class="btn btn-round btn-primary btn-xs" style="margin: 3px;" title="Unblock this service">
                            <i class="fa fa-unlock" aria-hidden="true"></i>
                        </a>
                        @endif
                      </td>                     
                    </tr>   
                    @endforeach               
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
