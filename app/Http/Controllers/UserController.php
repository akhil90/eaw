<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        $user_id = Auth::user()->id;
        return view('users.show')->with('users', User::where('id', '!=' , $user_id)->get());
    }
    public function create(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }
        
        return view('users.create');
    }
    public function store(Request $request){
        $data = request()->validate([
            'name'          => 'required',
            'email'         => 'required|unique:users',
            'gender'        => 'required',
            'mobile'        => 'required',
            'password'      => 'required',
            'role'          => 'required',
            'status'        => 'required'
        ]);

        $user       = new User;
        $last_id    = User::latest('id')->first()->id;

        $user->employee_id      = "EM000".($last_id+1);
        $user->name             = $request->input('name');
        $user->email            = $request->input('email');
        $user->gender           = $request->input('gender');
        $user->mobile           = $request->input('mobile');
        $user->password         = Hash::make($request->input('password'));
        $user->role             = $request->input('role');
        $user->status           = $request->input('status');
        $user->save();
        $request->session()->flash('success','User has been added successfully');
        return redirect('/users');
    }
    public function edit(User $User, $id){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        return view('users.update')->with('user', User::find($id));;
    }
    public function update(Request $request){
        $data = request()->validate([
            'name'      => 'required',
            'email'     => 'required|unique:users,email,'.$request->id,
            'gender'    => 'required',
            'mobile'    => 'required',
            'role'      => 'required',
            'status'    => 'required'
        ]);

        $res = User::find($request->id);
        
        $res->name      = $request->input('name');
        $res->email     = $request->input('email');
        $res->gender    = $request->input('gender');
        $res->mobile    = $request->input('mobile');
        $res->role      = $request->input('role');
        $res->status    = $request->input('status');
        $res->save();
        $request->session()->flash('success','User has been updated successfully');
        return redirect('/users');
    }
    public function block(Request $request){
        $res = User::find($request->id);
        
        $res->status = "DISABLED";
        $res->save();
        $request->session()->flash('success','User has been blocked successfully');
        return redirect('/users');
    }
    public function unblock(Request $request){
        $res = User::find($request->id);
        
        $res->status = "ACTIVE";
        $res->save();
        $request->session()->flash('success','User has been unblocked successfully');
        return redirect('/users');
    }
}
