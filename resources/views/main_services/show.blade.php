@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Services</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
      <div class="col-lg-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>Service List</h3>
              <div class="ibox-tools"> <a href="/main_services/create" style="margin-right: 25px; color: #fff !important;" type="button" title="Add new service" class="btn btn-round btn-success"><i class="fa fa-plus"></i> Service</a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                  <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Service Name</th>
                      <th>Service Type</th>
                      <th>Car Type</th>
                      <th>Price</th>
                      <th>Vat</th>
                      <th>Features</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>  
                    @foreach($main_services as $key => $main_service)
                      @if($main_service->status == "PENDING")
                        @php($color = "warning")
                      @elseif($main_service->status == "ACTIVE")   
                        @php($color = "primary")
                      @elseif($main_service->status == "DISABLED")  
                        @php($color = "danger")
                      @else
                        @php($color = "")
                      @endif
                     <tr>
                      <td width="5%"> {{ $key+1 }}</td>
                      <td width="15%">{{$main_service->service_name}}</td>
                      <td width="10%">{{str_replace("_"," ",$main_service->service_category)}}</td> 
                      <td width="10%">{{$main_service->car_type}}</td>
                      <td width="10%">{{$main_service->price}}</td>
                      <td width="5%">{{$main_service->vat}}</td>
                      <td width="25%">
                        @if($main_service->features != "")
                            <ul class="features">
                                @foreach(explode(',', $main_service->features) as $feature) 
                                    <li>{{$feature}}</li> 
                                @endforeach
                            </ul>
                        @endif
                      </td>
                      <td width="10%"><span class="badge badge-pill badge-{{$color}}">{{$main_service->status}}</span></td>
                      <td width="10%">
                        <a title="Edit this service" href="/main_services/edit/{{$main_service->id}}" class="btn btn-round btn-info btn-xs" style="margin: 3px;">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </a>
                        @if($main_service->status == "ACTIVE")                        
                        <a href="/main_services/block/{{$main_service->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this service">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($main_service->status == "DISABLED")
                        <a href="/main_services/unblock/{{$main_service->id}}" class="btn btn-round btn-primary btn-xs" style="margin: 3px;" title="Unblock this service">
                            <i class="fa fa-unlock" aria-hidden="true"></i>
                        </a>
                        @endif
                        @if($main_service->status == "PENDING")
                        <a href="/main_services/block/{{$main_service->id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Block this service">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </a>
                        @endif
                      </td>                     
                    </tr>   
                    @endforeach               
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
