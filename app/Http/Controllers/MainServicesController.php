<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\MainService;
use App\Models\TaxType;
use App\Models\Settings;

class MainServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        return view('main_services.show')->with('main_services', MainService::whereIn('service_category', ['SINGLE','ADDON','CAR_DETAILING'])->get());
    }

    public function create(){

        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        $data['taxTypes']         =  TaxType::getTaxTypes();
        $data['default_tax_type'] =  Settings::getSettingValueByType('default_tax_type');
        return view('main_services.create')->with($data);
    }
    
    public function store(Request $request){
        $data = request()->validate([
            'service_name'          => 'required',
            'service_category'      => 'required',
            'car_type'              => 'required',
            'price'                 => 'required',
            'tax_id'                => 'required',
            'duration'              => 'required',
            'discount_threshold'    => 'required',
            'features'              => 'required',
        ]);

        $main_service = new MainService;
        $user = Auth::user();
        
        $no_of_washes = 0;
        if($request->input('service_category') == "SINGLE"){
            $no_of_washes = 1;
        }

        $taxType = TaxType::find($request->input('tax_id'));

        $main_service->service_name         = $request->input('service_name');
        $main_service->service_category     = $request->input('service_category');
        $main_service->car_type             = $request->input('car_type');
        $main_service->price                = $request->input('price');
        $main_service->vat                  = $taxType->taxPercentage;
        $main_service->tax_id               = $taxType->id;
        $main_service->duration             = $request->input('duration');
        $main_service->no_of_washes         = $no_of_washes;
        $main_service->discount_threshold   = $request->input('discount_threshold');
        $main_service->features             = $request->input('features');
        $main_service->created_by           = $user->id;
        $main_service->save();
        $request->session()->flash('success','Service has been added successfully');
        return redirect('/main_services');
    }
    public function edit($id){
        
        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        $data['taxTypes']         =  TaxType::getTaxTypes();
        $data['main_service']     =  MainService::find($id);
        $data['default_tax_type'] =  Settings::getSettingValueByType('default_tax_type');
        return view('main_services.update')->with($data);
    }
    public function update(Request $request){
        $data = request()->validate([
            'service_name'          => 'required',
            'service_category'      => 'required',
            'car_type'              => 'required',
            'price'                 => 'required',
            'tax_id'                => 'required',
            'duration'              => 'required',
            'discount_threshold'    => 'required',
            'features'              => 'required',
            'status'                => 'required',
        ]);

        $res = MainService::find($request->id);
        $user = Auth::user();

        $taxType = TaxType::find($request->input('tax_id'));
        
        $res->service_name          = $request->input('service_name');
        $res->service_category      = $request->input('service_category');
        $res->car_type              = $request->input('car_type');
        $res->price                 = $request->input('price');
        $res->vat                   = $taxType->taxPercentage;
        $res->tax_id                = $taxType->id;
        $res->duration              = $request->input('duration');
        $res->discount_threshold    = $request->input('discount_threshold');
        $res->features              = $request->input('features');
        $res->status                = $request->input('status');
        $res->modified_by           = $user->id;
        $res->save();
        $request->session()->flash('success','Service has been updated successfully');
        return redirect('/main_services');
    }
    public function block(Request $request){
        $res = MainService::find($request->id);
        $user = Auth::user();
        
        $res->status        = "DISABLED";
        $res->modified_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Service has been blocked successfully');
        return redirect('/main_services');
    }
    public function unblock(Request $request){
        $res = MainService::find($request->id);
        $user = Auth::user();
        
        $res->status        = "ACTIVE";
        $res->modified_by   =  $user->id;
        $res->save();
        $request->session()->flash('success','Service has been unblocked successfully');
        return redirect('/main_services');
    }

    public function refreshTaxtypesAjax(){

        $taxTypes =  TaxType::getTaxTypes(1);

        $select = ""; 

        $select .= '<option value="">-- Select Tax Type --</option>';

        foreach($taxTypes as $taxType){

            $select .= '<option value="'. $taxType->id .'">'. $taxType->taxTitle .'</option>';

        }

        return $select;

    }

}
