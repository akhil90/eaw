<?php

function utf8_strrev($str){
    preg_match_all('/./us', $str, $ar);
    return join('', array_reverse($ar[0]));
 }


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 
<title>Receipt</title>

<style>
    * { font-family: DejaVu Sans, sans-serif; }
  @page { margin: 5px; }
#invoice-POS{
	font-size: 12px;
	font-family: Arial, Helvetica, sans-serif;
  	padding:2mm;
  	margin: 0 auto;
  	width: 100mm;
  	background: #FFF;
}

/* 
h1{
  font-size: 1.5em;
  color: #222;
} */

h2 {font-size: 20px;
	line-height: 18px;}

h3{
  font-size: 16px;
  font-weight: bold;
  line-height: 20px;
}
h4{
  font-size: 14px;
  font-weight: bold;
  line-height: 16px;
}
h5{
  font-size: 12px;
  font-weight: bold;
  line-height: 12px;
  margin: 10px;
}
h6{
  font-size: 9px;
  font-weight: bold;
  line-height: 10px;
  margin: 10px;
}

p{
  	font-size: 12px;
	  color: #000;
  	line-height:12px;
	  margin: 9px;
}

#address {
	font-size:14px;
	line-height:24px;
	padding-top:10px;
	padding-bottom:10px;
}

#mid-line {
	font-size:12px;
	line-height:24px;
	padding: 20px;
} 

/* #bot{ min-height: 50px;} */

.clientlogo {
	float: left;
	height: 60px;
	width: 60px;
	background-size: 60px 60px;
  	border-radius: 50px;
}

.title p{text-align: right;} 

table {
  	width: 100%;
  	border-collapse: collapse;
}

.tabletitle {
  	font-size:10px;
  	line-height:10px;
  	background: #fff;
  	border-top:dotted 1px solid #000;
}

.table-total {
  	font-size:16px;
  	background: #fff;
  	border-top:dotted 1px solid #333;
}

.service{border-bottom: 1px solid #EEE;}
/* .item{width: 24mm;}
.itemtext{font-size: .5em;} */

#legalcopy {
  	margin: 30px;

}

.ticket {text-align:left;}

/* border */
hr.new2 {
  	border-top: 2px dashed gray;
}
hr.new1 {
  	border-top: 1px solid gray;
}
</style>

</head>
<body>

    <div id="invoice-POS">
    
    <center id="top">
        <div class="logo"><img  alt="EXPRESS CAR WASH" src='{{ $message->embed(public_path("assets/images/eawuae_logo.png")) }}' /></div>
    </center><!--End InvoiceTop-->
    
    <center>     
        <div>
            <h3>
            Mushrif Mall, Airport Road</br>
            Abu Dhabi 2 554482</br>
            </h3>
        </div>
                
        <hr class="new2"> 
                
        <div class="info">
            <h2>TAX INVOICE</h2>
        </div>        
        <hr class="new2">  
        @if($invoice['inv_type'] == "JOB")  
            <h4> Service Invoice </h4>   
        @elseif($invoice['inv_type'] == "PACKAGE")
            <h4> Package Purchase Invoice </h4>   
        @elseif($invoice['inv_type'] == "CARD_BUY")
            <h4> Member Card Replace Invoice </h4>
        @endif   
    </center>
    <table border="0" cellpadding="0" cellspacing="0">
    <tr class="table-total">
                <td width="20%" class=""><h5><b>Inv-No:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['inv_number'] }}</b></h5></td>
            </tr>                       
            
            <tr class="table-total">
                <td width="20%" class=""><h5><b>Date:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['date'] }}</b></h5></td>
            </tr>
            
            <tr class="table-total">
                <td width="20%" class=""><h5><b>Reception:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['created_by'] }}</b></h5></td>
            </tr>
            
            <tr class="table-total">
                <td width="20%" class=""><h5><b>Customer:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['customer_name'] }}</b></h5></td>
            </tr>
            
            <tr class="table-total">
                <td width="20%" class=""><h5><b>Mobile No:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['customer_mobile'] }}</b></h5></td>
            </tr>
            
            <tr class="table-total">
                <td width="20%" class=""><h5><b>Plate No:</b></h5></td>
                <td width="80%" class=""><h5><b>{{ $invoice['plate_number'] }}</b></h5></td>
            </tr>

            <tr class="table-total">
                <td width="20%" class=""><h5><b>TRN:</b></h5></td>
                <td width="80%" class=""><h5><b>-</b></h5></td>
            </tr>
        
    </table><br><br>                       
    <div id="table-total" align="justify">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr class="table-total" style="border-bottom:solid 1px; border-top:solid 1px; padding-bottom:10px">
                <th valign="top" ><h6>#</h6></th>
                <th valign="top" class=""><h6>Qty</h6></th>
                <th valign="top" class=""><h6>Rate</h6></th>
                <th valign="top" class=""><h6>Amount<br>(AED)</h6></th>
                <th valign="top" class=""><h6>Discount</h6></th>
                <th valign="top" class=""><h6>Taxable Value<br>(AED)</h6></th>
                <th valign="top" class=""><h6>VAT Rate <br>(AED)</h6></th>
                <th valign="top" class=""><h6>VAT Amount<br>(AED)</h6></th>
            </tr>   
                <tr>
                    <td colspan="8" valign="top" ></td>
                </tr>
                @foreach($invoice['details'] as $key => $detail)
                    <tr border="1">
                        <td valign="top" ><h6>{{ $key+1 }}</h6></td>
                        <td valign="top" ><h6>1</h6></td>
                        <td valign="top" ><h6>{{ $detail['price'] }}</h6></td>
                        <td valign="top" ><h6>{{ $detail['gross_price'] }}</h6></td>
                        <td valign="top" ><h6>{{ $detail['discount'] }}</h6></td>
                        <td valign="top" ><h6>{{ $detail['net_amount'] }}</h6></td>
                        <td valign="top" ><h6>5.00%</h6></td>
                        <td valign="top" ><h6>{{ $detail['tax'] }}</h6></td>
                    </tr>   
                    <tr> 
                        <td colspan ="1" valign="top" style="border-bottom:solid 1px; padding-bottom:10px"></td>
                        <td colspan="7" valign="top" style="border-bottom:solid 1px; padding-bottom:10px"><b>{{ $detail['service_title'] }}, {{ ucwords(str_replace('_', ' ', $detail['service_category'])) }}</b></td>
                    </tr>
                @endforeach
        </table>
    </div><!--End Table-->
    <table border="0" cellpadding="0" cellspacing="0">                 

        <tr class="table-total">
            <td valign="top" class=""><h5><b>Total:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['total_price'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_total'] }}</b></h5></td>
        </tr>  


        <tr class="table-total">
            <td valign="top" class=""><h5><b>Discount:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['total_discount'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_discount'] }}</b></h5></td>
        </tr>  

        <tr class="table-total">
            <td valign="top" class=""><h5><b>Goss Amt:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['gross_price'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_GrossAmt'] }}</b></h5></td>
        </tr>  

        <tr class="table-total">
            <td valign="top" class=""><h5><b>5% VAT:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['total_tax'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>5% {{ $invoice['arabic_vat'] }}</b></h5></td>
        </tr>  

        <tr class="table-total">
            <td valign="top" class=""><h5><b>Net Amount:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['net_amount_total'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_NetAmount'] }}</b></h5></td>
        </tr>  
        <tr class="table-total">
            <td valign="top" class=""><h5><b>Payment Method:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ ucwords(str_replace('_', ' ', $invoice['payment_type'])) }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_PaymentMethod'] }}</b></h5></td>
        </tr>  
    @if($invoice['payment_type'] == "CREDIT")
        <tr class="table-total">
            <td valign="top" class=""><h5><b>Paid Amt:</b></h5></td>
            <td valign="top" class=""><h5><b>0.00</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_PaidAmt'] }}</b></h5></td>
        </tr> 
    @else
        <tr class="table-total">
            <td valign="top" class=""><h5><b>Paid Amt:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['gross_price'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_PaidAmt'] }}</b></h5></td>
        </tr> 
    @endif

    @if($invoice['payment_type'] == "CREDIT")
        <tr class="table-total">
            <td valign="top" class=""><h5><b>Balance:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['gross_price'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_Balance'] }}</b></h5></td>
        </tr>  
    @endif
    @if($invoice['payment_type'] == "SPOT_PAYMENT")
        <tr class="table-total">
            <td valign="top" class=""><h5><b>Cash Amt:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['payment_type_details']['cashAmount'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_CashAmt'] }}</b></h5></td>
        </tr>  

        <tr class="table-total">
            <td valign="top" class=""><h5><b>Card Amt:</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['payment_type_details']['cardAmount'] }}</b></h5></td>
            <td valign="top" class=""><h5><b>{{ $invoice['arabic_CardAmt'] }}</b></h5></td>
        </tr>  
    @endif

    </table><br>
    <center>
        <div id="legalcopy">
            <p class="legal">
                <strong>Thank you for visiting us!</strong>
            </p>
        </div>
    </center>

    </div><!--End InvoiceBot-->
    </div>
    <!--End Invoice-->
    <script>
        window.onload = function () {
            window.print();
            setTimeout(function(){window.close();}, 1);
        }

    </script>

</body>
</html>