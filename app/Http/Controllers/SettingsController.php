<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\TaxType;
use App\Models\MainService;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        $data['point']                     = Settings::getSettingValueByType('point');
        $data['cash_payment_code']         = Settings::getSettingValueByType('cash_payment_code');
        $data['card_payment_code']         = Settings::getSettingValueByType('card_payment_code');
        $data['cash_payment_account_code'] = Settings::getSettingValueByType('cash_payment_account_code');
        $data['card_payment_account_code'] = Settings::getSettingValueByType('card_payment_account_code');
        $data['discount_account_code']  = Settings::getSettingValueByType('discount_account_code');
        $data['zero_invoice_creation']  = Settings::getSettingValueByType('zero_invoice_creation');
        $data['bank_charge_rate']       = Settings::getSettingValueByType('bank_charge_rate');
        $data['bankAccounts']           = Settings::getAccounts('Bank',0);
        $data['incomeAccounts']         = Settings::getAccounts('Income',0);
        $data['default_tax_type']       =  Settings::getSettingValueByType('default_tax_type');
        $data['taxTypes']               =  TaxType::getTaxTypes();
        $data['paymentMethods']         = Settings::getPaymentMethods();
        $data['gate_status']            = Settings::getSettingValueByType('gate_status');;

        $data['arabic_total']          = Settings::getSettingValueByType('arabic_total');
        $data['arabic_discount']       = Settings::getSettingValueByType('arabic_discount');
        $data['arabic_GrossAmt']       = Settings::getSettingValueByType('arabic_GrossAmt');
        $data['arabic_vat']            = Settings::getSettingValueByType('arabic_vat');
        $data['arabic_NetAmount']      = Settings::getSettingValueByType('arabic_NetAmount');
        $data['arabic_PaymentMethod']  = Settings::getSettingValueByType('arabic_PaymentMethod');
        $data['arabic_PaidAmt']        = Settings::getSettingValueByType('arabic_PaidAmt');
        $data['arabic_CashAmt']        = Settings::getSettingValueByType('arabic_CashAmt');
        $data['arabic_CardAmt']        = Settings::getSettingValueByType('arabic_CardAmt');
        $data['arabic_Balance']        = Settings::getSettingValueByType('arabic_Balance');

        $serviceDetails     = MainService::where('service_category','NFC_CARD')->first();

        $data['tax_updation_renew_card_status'] =  $serviceDetails->tax_id != null ? "active" : "not_active";
        $data['card_main_service_id'] =  $serviceDetails->id;

        return view('settings')->with($data);
    }

    public function updateSettings(Request $request){

        $data = request()->validate([
            'point'                         => 'required',
            'cash_payment_code'             => 'required',
            'card_payment_code'             => 'required',
            'cash_payment_account_code'     => 'required',
            'card_payment_account_code'     => 'required',
            'discount_account_code'         => 'required',
            'tax_id'                        => 'required'
        ]);

        Settings::where('type', 'point')->update(['value' => $request->input('point')]);
        Settings::where('type', 'cash_payment_code')->update(['value' => $request->input('cash_payment_code')]);
        Settings::where('type', 'card_payment_code')->update(['value' => $request->input('card_payment_code')]);
        Settings::where('type', 'cash_payment_account_code')->update(['value' => $request->input('cash_payment_account_code')]);
        Settings::where('type', 'card_payment_account_code')->update(['value' => $request->input('card_payment_account_code')]);
        Settings::where('type', 'discount_account_code')->update(['value' => $request->input('discount_account_code')]);
        Settings::where('type', 'default_tax_type')->update(['value' => $request->input('tax_id')]);
        Settings::where('type', 'zero_invoice_creation')->update(['value' => $request->input('zero_invoice_creation')]);
        Settings::where('type', 'bank_charge_rate')->update(['value' => $request->input('bank_charge_rate')]);
        Settings::where('type', 'gate_status')->update(['value' => $request->input('gate_status')]);

        Settings::where('type', 'arabic_total')->update(['value' => $request->input('arabic_total')]);
        Settings::where('type', 'arabic_discount')->update(['value' => $request->input('arabic_discount')]);
        Settings::where('type', 'arabic_GrossAmt')->update(['value' => $request->input('arabic_GrossAmt')]);
        Settings::where('type', 'arabic_vat')->update(['value' => $request->input('arabic_vat')]);
        Settings::where('type', 'arabic_NetAmount')->update(['value' => $request->input('arabic_NetAmount')]);
        Settings::where('type', 'arabic_PaymentMethod')->update(['value' => $request->input('arabic_PaymentMethod')]);
        Settings::where('type', 'arabic_Balance')->update(['value' => $request->input('arabic_Balance')]);
        Settings::where('type', 'arabic_PaidAmt')->update(['value' => $request->input('arabic_PaidAmt')]);
        Settings::where('type', 'arabic_CashAmt')->update(['value' => $request->input('arabic_CashAmt')]);
        Settings::where('type', 'arabic_CardAmt')->update(['value' => $request->input('arabic_CardAmt')]);

        $request->session()->flash('success','Settings has been updated successfully');
        return redirect('/settings');
    }


    public function refreshAccountApiAjax(Request $request){

        $select = ""; 

        if($request->type == "Bank"){
            $accounts      = Settings::getAccounts('Bank',1);
            $select .= '<option value="">-- Select Cash/Card Account Code --</option>';
        }else if($request->type == "Income"){
            $accounts      = Settings::getAccounts('Income',1);
            $select .= '<option value="">-- Discount Account Code --</option>';
        }else if($request->type == "payment_method"){
            $accounts      = Settings::getPaymentMethods(1);
            $select .= '<option value="">-- Select Cash/Card Payment Method Code --</option>';
        }

        foreach($accounts as $account){

            $select .= '<option value="'. $account->remote_id .'">'. $account->name .'-'.$account->remote_id.'</option>';

        }

        return $select;

    }
}
