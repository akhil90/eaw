@extends('layouts.inner')

@section('content')
<style>

td.details-control {
    background: url('{{ asset('assets/img/details_open.png') }}') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('{{ asset('assets/img/details_close.png') }}') no-repeat center center;
}

</style>
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-2">
        <h2>Analytical Dashboard</h2>
      </div>
      <div class="col-lg-10"></div>
    </div>
    <div class="fltr wrapper wrapper-content">
      <div class="row animated fadeInDown">
        <div class="col-lg-12 fltr-wrp">
          <div class="ibox accordion" id="accordionExample">
            <div class="ibox-title" id="headingOne" style="min-height: 60px;">
              <h5 style=" float: left; margin-right: 7px;">Filter</h5>
              <div class="fltr-sec" id="fold"> <i class="fa fa-filter"></i>
                <button id="fold_p" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Hide Filter</button>
              </div>
            </div>
            <div class="ibox-content collapse show" id="collapseOne"  aria-labelledby="headingOne" data-parent="#accordionExample">
            <form method="post" id="search-form" action="/home">
            @csrf
              <div class="row">
                <div class="col-lg-12 fltr">
                  <div class="tab">
                      <ul class="fltr-lst">
                   <li><input class="tablinks" onclick="openCity(event, 'sc1')" type="radio" id="<?php if ($param['filter_type'] == "Daily") {echo "defaultOpen";} else {echo "";}?>" name="filter_type" value="Daily"><label for="other">Daily</label></li>
                   <li><input class="tablinks" onclick="openCity(event, 'sc2')" type="radio" id="<?php if ($param['filter_type'] == "Weekly") {echo "defaultOpen";} else {echo "";}?>" name="filter_type" value="Weekly"><label for="other">Weekly</label></li>
                   <li><input class="tablinks" onclick="openCity(event, 'sc3')" type="radio" id="<?php if ($param['filter_type'] == "Monthly") {echo "defaultOpen";} else {echo "";}?>" name="filter_type" value="Monthly"><label for="other">Monthly</label></li>
                   <li><input class="tablinks" onclick="openCity(event, 'sc4')" type="radio" id="<?php if ($param['filter_type'] == "Custom") {echo "defaultOpen";} else {echo "";}?>" name="filter_type" value="Custom"><label for="other">Custom</label></li>
                  </ul>
                  </div>
                  <div id="sc1" class="tabcontent">
                    <ul>
                      <li>
                        <input type="text" class="crc1 form-control" name="daily_date" id="datetimepicker" value="{{ $param['daily_date'] }}" placeholder="Select Date">
                      </li>
                      <li>
                        <button class="btn btn-w-m btn-info search-btn">Search</button>
                      </li>
                    </ul>
                  </div>
                  <div id="sc2" class="tabcontent">
                    <div id="DateDemo" style="margin-top:-15px;">
                      <ul>
                        <li>
                          <input type="text" class="form-control" name="weekly_date" id="weeklyDatePicker" autocomplete="off" value="{{ $param['weekly_date'] }}" placeholder="Select Week">
                        </li>
                        <li>
                          <button class="btn btn-w-m btn-info search-btn">Search</button>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div id="sc3" class="tabcontent">
                    <ul>
                      <li>
                        <input type="text" class="crc1 form-control" name="monthly_date" id="MonthDatePicker"  value="{{ $param['monthly_date'] }}" placeholder="Select Month">
                      </li>
                      <li>
                        <button class="btn btn-w-m btn-info search-btn">Search</button>
                      </li>
                    </ul>
                  </div>
                  <div id="sc4" class="tabcontent">
                    <ul>
                      <li>
                        <input type="text" class="crc1 form-control" name="custom_start_date" value="{{ $param['custom_start_date'] }}" id="datetimepicker3" placeholder="From Date">
                      </li>
                      <li>
                        <input type="text" class="crc1 form-control" name="custome_end_date" value="{{ $param['custome_end_date'] }}" id="datetimepicker4" placeholder="To Date">
                      </li>
                      <li>
                        <button class="btn btn-w-m btn-info search-btn">Search</button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-4">
          <div class="blk b1">
            <h4>Total Carwash</h4>
            <div class="wr">
              <table class="sum table table-striped table-bordered table-hover" >
              </table>
              <table class="sum table table-striped table-bordered table-hover">
                <tr>
                  <td class="lft" ><b>Non Member</b></td>
                  <th class="rc">Sedan</th>
                  <th class="rc">SUV</th>
                  <th class="rc">Total</th>
                </tr>
                @php($grand_total_sedan = 0)
                @php($grand_total_suv = 0)
                @php($grand_total_car_count = 0)

                @php($grand_total_sedan_non = 0)
                @php($grand_total_suv_non = 0)
                @php($grand_total_car_count_non = 0)

                @php($grand_total_sedan_mem = 0)
                @php($grand_total_suv_mem = 0)
                @php($grand_total_car_count_mem = 0)

                @foreach($non_member_service_car_count as $non_car_count)

                <tr>
                  <td class="lft" >{{ $non_car_count['service_name'] }}</td>
                  <td>{{ $non_car_count['SEDAN'] }}</td>
                  <td>{{ $non_car_count['SUV'] }}</td>
                  <td>{{ $non_car_count['total'] }}</td>
                </tr>

                @php($grand_total_sedan_non += $non_car_count['SEDAN'])
                @php($grand_total_suv_non += $non_car_count['SUV'])
                @php($grand_total_car_count_non += $non_car_count['total'])

                @php($grand_total_sedan  += $non_car_count['SEDAN'])
                @php($grand_total_suv += $non_car_count['SUV'])
                @php($grand_total_car_count += $non_car_count['total'])

                @endforeach

                <tr>
                  <td class="lft" >Total</td>
                  <td>{{ $grand_total_sedan_non }}</td>
                  <td>{{ $grand_total_suv_non }}</td>
                  <td>{{ $grand_total_car_count_non }}</td>
                </tr>

              </table>
              <table class="sum table table-striped table-bordered table-hover">
                <tr>
                  <td class="lft" ><b>Member</b></td>
                  <th class="rc">Sedan</th>
                  <th class="rc">SUV</th>
                  <th class="rc">Total</th>
                </tr>
                @foreach($member_service_car_count as $mem_car_count)
                <tr>
                  <td class="lft">{{ $mem_car_count['service_name'] }}</td>
                  <td>{{ $mem_car_count['SEDAN'] }}</td>
                  <td>{{ $mem_car_count['SUV'] }}</td>
                  <td>{{ $mem_car_count['total'] }}</td>
                </tr>
                @php($grand_total_sedan_mem  += $mem_car_count['SEDAN'])
                @php($grand_total_suv_mem += $mem_car_count['SUV'])
                @php($grand_total_car_count_mem += $mem_car_count['total'])

                @php($grand_total_sedan  += $mem_car_count['SEDAN'])
                @php($grand_total_suv += $mem_car_count['SUV'])
                @php($grand_total_car_count += $mem_car_count['total'])

                @endforeach

                <tr>
                  <td class="lft">Total</td>
                  <td>{{ $grand_total_sedan_mem }}</td>
                  <td>{{ $grand_total_suv_mem }}</td>
                  <td>{{ $grand_total_car_count_mem }}</td>
                </tr>
              </table>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <td class="lft" ><b>Total Carcount</b></td>
                  <td width="11%">{{ $sedan_car_count }}</td>
                  <td width="11%">{{ $suv_car_count }}</td>
                  <td width="11%">{{ $total_car_count }}</td>
                </tr>
              </table>
              <!-- <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <td class="lft sc"><b>Total Express Gold</b></td>
                  <td class="rc">13</td>
                </tr>
                <tr>
                  <td class="lft sc"><b>Total Platinum</b></td>
                  <td class="rc">13</td>
                </tr>
              </table> -->
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <td class="lft sc"><b>Repeated Customers </b><span style="margin-left:10px;"><a  id="view_repeated_customers">View Details</a></span></td>
                  <td class="rc">{{count($duplicate_customers)}}</td>
                </tr>
                <!-- <tr>
                  <td class="lft sc"><b>SPC</b></td>
                  <td class="rc">4</td>
                </tr> -->
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-8">
          <div class="blk">
            <h4>Sales Report</h4>
            <div class="wr">
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc" width="18%">Payment Mode</th>
                  <th class="rc"></th>
                  <th class="rc"></th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($grand_total = 0)
                @php($grand_discount = 0)
                @php($grand_gross = 0)
                @php($net_gross = 0)
                @php($vat = 5)
                @php($salesVatTax = 0)
                @php($vatTotal = 0)
                @php($grandGrossWithTax = 0)


                @foreach($sales_reports as $sales_report)

                <tr>
                  <td class="lft" ><a title="{{$sales_report->payment_mode}} Payment Details" data-payment_mode="{{$sales_report->payment_mode}}"  id="payment_mode">{{$sales_report->payment_mode}}</a></td>
                  <td></td>
                  <td></td>
                  <td>{{ number_format($sales_report->total_price,2)}}</td>
                  <td>{{ number_format($sales_report->total_discount,2) }}</td>
                  <td>{{ number_format($sales_report->net_amount_total,2) }}</td>
                  <td>{{ number_format($sales_report->tax_total,2) }}</td>
                  <td>{{ number_format($sales_report->gross_price,2) }}</td>
                </tr>
                  @php($grand_total += $sales_report->total_price)
                  @php($grand_discount += $sales_report->total_discount)
                  @php($grand_gross += $sales_report->gross_price)
                  @php($vatTotal += $sales_report->tax_total)
                  @php($net_gross += $sales_report->net_amount_total)

                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th></th>
                  <th></th>
                  <th>{{number_format($grand_total,2)}}</th>
                  <th>{{number_format($grand_discount,2)}}</th>
                  <th>{{number_format($net_gross,2)}}</th>
                  <td>{{number_format($vatTotal,2)}}</td>
                  <th>NET AMNT: {{number_format($grand_gross,2)}}</th>
                </tr>
              </table>
              <h4>Single Service</h4>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc" width="18%">Name</th>
                  <th class="rc">Qty</th>
                  <th class="rc">Price</th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($single_service_qty_total = 0)
                @php($single_service_price_total = 0)
                @php($single_service_sale_total = 0)
                @php($single_service_discount_total = 0)
                @php($single_service_gross_total = 0)
                @php($single_service_vat_total = 0)
                @php($single_service_net_total = 0)
                @php($vat = 5)
                @php($single_service_vat = 0)
                @php($grossSingleService = 0)

                @foreach($single_service_reports as $single_service_report)

                <tr>
                  <td class="lft">{{$single_service_report->service_name}}</td>
                  <td>{{$single_service_report->quantity}}</td>
                  <td>{{$single_service_report->price}}</td>
                  <td>{{$single_service_report->total_price}}</td>
                  <td>{{$single_service_report->total_discount}}</td>
                  <td>{{ $single_service_report->net_amount_total }}</td>
                  <td>{{$single_service_report->tax_total}}</td>
                  <td>{{$single_service_report->gross_price}}</td>
                </tr>
                @php($single_service_qty_total += $single_service_report->quantity)
                @php($single_service_price_total += $single_service_report->price)
                @php($single_service_sale_total += $single_service_report->total_price)
                @php($single_service_discount_total += $single_service_report->total_discount)
                @php($single_service_gross_total += $single_service_report->gross_price)
                @php($single_service_vat_total += $single_service_report->tax_total)
                @php($single_service_net_total += $single_service_report->net_amount_total)
                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th>{{$single_service_qty_total}}</th>
                  <th>{{number_format($single_service_price_total,2)}}</th>
                  <th>{{number_format($single_service_sale_total,2)}}</th>
                  <th>{{number_format($single_service_discount_total,2)}}</th>
                  <th>{{number_format($single_service_net_total,2)}}</th>
                  <td>{{number_format($single_service_vat_total,2)}}</td>
                  <th>NET AMNT: {{number_format($single_service_gross_total,2)}}</th>
                </tr>
              </table>
              <h4>Package Purchase</h4>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc" width="18%">Name</th>
                  <th class="rc">Qty</th>
                  <th class="rc">Price</th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($package_qty_total = 0)
                @php($package_price_total = 0)
                @php($package_sale_total = 0)
                @php($package_discount_total = 0)
                @php($package_net_total = 0)
                @php($package_gross_total = 0)
                @php($vat = 5)
                @php($package_vat_total = 0)
                @php($package_vat = 0)

                @foreach($package_reports as $package_report)

                <tr>
                  <td class="lft">{{$package_report->service_name}}</td>
                  <td>{{ $package_report->quantity }}</td>
                  <td>{{ $package_report->price }}</td>
                  <td>{{ $package_report->total_price }}</td>
                  <td>{{ $package_report->total_discount }}</td>
                  <td>{{ $package_report->net_amount_total }}</td>
                  <td>{{ $package_report->tax_total }}</td>
                  <td>{{ $package_report->gross_price }}</td>
                </tr>
                @php($package_qty_total += $package_report->quantity)
                @php($package_price_total += $package_report->price)
                @php($package_sale_total += $package_report->total_price)
                @php($package_discount_total += $package_report->total_discount)
                @php($package_gross_total += $package_report->gross_price)
                @php($package_vat_total += $package_report->tax_total)
                @php($package_net_total += $package_report->net_amount_total)
                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th>{{$package_qty_total}}</th>
                  <th>{{number_format($package_price_total,2)}}</th>
                  <th>{{number_format($package_sale_total,2)}}</th>
                  <th>{{number_format($package_discount_total,2)}}</th>
                  <th>{{number_format($package_net_total,2)}}</th>
                  <td>{{number_format($package_vat_total,2)}}</td>
                  <th>NET AMNT: {{number_format($package_gross_total,2)}}</th>
                </tr>
              </table>
              <h4>Add-ons</h4>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc">Name</th>
                  <th class="rc">Qty</th>
                  <th class="rc">Price</th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($addon_qty_total = 0)
                @php($addon_price_total = 0)
                @php($addon_sale_total = 0)
                @php($addon_discount_total = 0)
                @php($addon_gross_total = 0)
                @php($addon_net_total = 0)
                @php($vat = 5)
                @php($addon_vat_total = 0)
                @php($addon_vat = 0)

                @foreach($addon_reports as $addon_report)

                  @php($addon_vat = $addon_report->gross_price * ($vat/100))
                  @php($netAddonPrice = $addon_report->gross_price - $addon_vat)

                <tr>
                  <td class="lft">{{$addon_report->service_name}}</td>
                  <td>{{$addon_report->quantity}}</td>
                  <td>{{$addon_report->price}}</td>
                  <td>{{$addon_report->total_price}}</td>
                  <td>{{$addon_report->total_discount}}</td>
                  <td>{{ $addon_report->net_amount_total }}</td>
                  <td>{{$addon_report->tax_total}}</td>
                  <td>{{$addon_report->gross_price}}</td>
                </tr>
                @php($addon_qty_total += $addon_report->quantity)
                @php($addon_price_total += $addon_report->price)
                @php($addon_sale_total += $addon_report->total_price)
                @php($addon_discount_total += $addon_report->total_discount)
                @php($addon_gross_total += $addon_report->gross_price)
                @php($addon_vat_total += $addon_report->tax_total)
                @php($addon_net_total +=  $addon_report->net_amount_total)
                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th>{{$addon_qty_total}}</th>
                  <th>{{number_format($addon_price_total,2)}}</th>
                  <th>{{number_format($addon_sale_total,2)}}</th>
                  <th>{{number_format($addon_discount_total,2)}}</th>
                  <th>{{number_format($addon_net_total,2)}}</th>
                  <th>{{number_format($addon_vat_total,2)}}</th>
                  <th>NET AMNT: {{number_format($addon_gross_total,2)}}</th>
                </tr>
              </table>
              <h4>Car Detailing</h4>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc">Name</th>
                  <th class="rc">Qty</th>
                  <th class="rc">Price</th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($car_detailing_qty_total = 0)
                @php($car_detailing_price_total = 0)
                @php($car_detailing_sale_total = 0)
                @php($car_detailing_discount_total = 0)
                @php($car_detailing_gross_total = 0)
                @php($car_detailing_net_total = 0)
                @php($vat = 5)
                @php($car_detailing_vat_total = 0)
                @php($car_detailing_vat = 0)

                @foreach($car_detailing_reports as $car_detailing_report)

                  @php($car_detailing_vat = $car_detailing_report->gross_price * ($vat/100))
                  @php($net_car_detailing_price = $car_detailing_report->gross_price - $car_detailing_vat)

                <tr>
                  <td class="lft">{{$car_detailing_report->service_name}}</td>
                  <td>{{$car_detailing_report->quantity}}</td>
                  <td>{{$car_detailing_report->price}}</td>
                  <td>{{$car_detailing_report->total_price}}</td>
                  <td>{{$car_detailing_report->total_discount}}</td>
                  <td>{{$car_detailing_report->net_amount_total }}</td>
                  <td>{{$car_detailing_report->tax_total}}</td>
                  <td>{{$car_detailing_report->gross_price}}</td>
                </tr>
                @php($car_detailing_qty_total += $car_detailing_report->quantity)
                @php($car_detailing_price_total += $car_detailing_report->price)
                @php($car_detailing_sale_total += $car_detailing_report->total_price)
                @php($car_detailing_discount_total += $car_detailing_report->total_discount)
                @php($car_detailing_gross_total += $car_detailing_report->gross_price)
                @php($car_detailing_vat_total += $car_detailing_report->tax_total)
                @php($car_detailing_net_total +=  $car_detailing_report->net_amount_total)
                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th>{{$car_detailing_qty_total}}</th>
                  <th>{{number_format($car_detailing_price_total,2)}}</th>
                  <th>{{number_format($car_detailing_sale_total,2)}}</th>
                  <th>{{number_format($car_detailing_discount_total,2)}}</th>
                  <th>{{number_format($car_detailing_net_total,2)}}</th>
                  <th>{{number_format($car_detailing_vat_total,2)}}</th>
                  <th>NET AMNT: {{number_format($car_detailing_gross_total,2)}}</th>
                </tr>
              </table>
              <h4>Card Renew</h4>
              <table class="sum table table-striped table-bordered table-hover" >
                <tr>
                  <th class="lft dc">Name</th>
                  <th class="rc">Qty</th>
                  <th class="rc">Price</th>
                  <th class="rc">Sale</th>
                  <th class="rc">Discount</th>
                  <th class="rc">Net Sale</th>
                  <th class="rc">VAT</th>
                  <th class="dc">Net Revenue</th>
                </tr>
                @php($card_renew_qty_total = 0)
                @php($card_renew_price_total = 0)
                @php($card_renew_sale_total = 0)
                @php($card_renew_discount_total = 0)
                @php($card_renew_gross_total = 0)
                @php($card_renew_net_total = 0)
                @php($vat = 5)
                @php($card_renew_vat_total = 0)
                @php($card_renew_vat = 0)

                @foreach($card_renew_reports as $card_renew_report)

                  @php($card_renew_vat = $card_renew_report->gross_price * ($vat/100))
                  @php($net_card_renew_price = $card_renew_report->gross_price - $card_renew_vat)

                <tr>
                  <td class="lft">{{$card_renew_report->service_name}}</td>
                  <td>{{$card_renew_report->quantity}}</td>
                  <td>{{$card_renew_report->price}}</td>
                  <td>{{$card_renew_report->total_price}}</td>
                  <td>{{$card_renew_report->total_discount}}</td>
                  <td>{{$card_renew_report->net_amount_total }}</td>
                  <td>{{$card_renew_report->tax_total}}</td>
                  <td>{{$card_renew_report->gross_price}}</td>
                </tr>
                @php($card_renew_qty_total += $card_renew_report->quantity)
                @php($card_renew_price_total += $card_renew_report->price)
                @php($card_renew_sale_total += $card_renew_report->total_price)
                @php($card_renew_discount_total += $card_renew_report->total_discount)
                @php($card_renew_gross_total += $card_renew_report->gross_price)
                @php($card_renew_vat_total += $card_renew_report->tax_total)
                @php($card_renew_net_total +=  $card_renew_report->net_amount_total)
                @endforeach
                <tr>
                  <th style="text-align:right;">Total</th>
                  <th>{{$card_renew_qty_total}}</th>
                  <th>{{number_format($card_renew_price_total,2)}}</th>
                  <th>{{number_format($card_renew_sale_total,2)}}</th>
                  <th>{{number_format($card_renew_discount_total,2)}}</th>
                  <th>{{number_format($card_renew_net_total,2)}}</th>
                  <th>{{number_format($card_renew_vat_total,2)}}</th>
                  <th>NET AMNT: {{number_format($card_renew_gross_total,2)}}</th>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="ibox ">
            <div class="ibox-title">
              <h5>Carwash Details</h5>
            </div>
            <div class="ibox-content">
              <div class="table-responsive eaw">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                  <thead>
                    <tr>
                      <th width="4%">Sl No</th>
                      @if($param['filter_type'] != "Daily")
                      <th>Date</th>
                      @endif
                      <th>Customer Name</th>
                      <th>Contact No</th>
                      <th>Plate Number</th>
                      <th>Service Name</th>
                      <th>Service Type</th>
                      <th>Payment Type</th>
                      <th>Amount</th>
                      <th>Discount</th>
                      <th>Net Amount</th>
                      <th>Inv Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @php($i = 1)
                  @foreach($carwash_details as $carwash_detail)
                    @if($carwash_detail->inv_status == "PENDING")
                      @php($color = "warning")
                    @elseif($carwash_detail->inv_status == "CREATED")
                      @php($color = "primary")
                    @elseif($carwash_detail->inv_status == "FAILED")
                      @php($color = "danger")
                    @else
                      @php($color = "")
                    @endif
                    <tr>
                      <td>{{$i}}</td>
                      @if($param['filter_type'] != "Daily")
                      <td>{{date('m/d/Y',strtotime($carwash_detail->created_at))}}</td>
                      @endif
                      <td>{{$carwash_detail->first_name}} {{$carwash_detail->last_name}}</td>
                      <td>{{$carwash_detail->mobile}}</td>
                      <td>{{$carwash_detail->plate_number}}</td>
                      <td>{{$carwash_detail->service_name}}</td>
                      <td>{{$carwash_detail->service_category}}</td>
                      @if($carwash_detail->payment_type == "SPOT_PAYMENT")
                        <td>CARD/CASH</td>
                      @else
                        <td>{{$carwash_detail->payment_type}}</td>
                      @endif
                      <td>{{$carwash_detail->price}}</td>
                      <td>{{$carwash_detail->discount}}</td>
                      <td>{{$carwash_detail->gross_price}}</td>
                      <td><span class="badge badge-pill badge-{{$color}}">{{$carwash_detail->inv_status}}</span></td>
                      <td>

                      @if($carwash_detail->inv_status == "PENDING")
                          <a href="/repost_invoice/{{$carwash_detail->job_card_id}}" class="btn btn-round btn-danger btn-xs" style="margin: 3px;" title="Repost The Invoice">
                          <i class="fa fa-retweet" aria-hidden="true"></i>
                          </a>
                      @endif
                      </td>
                    </tr>
                    @php($i++)
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrapper wrapper-content">
      <div class="row">
        <div class="col-lg-6">
          <div class="ibox animated fadeIn">
            <div class="ibox-title">
              <h5>Total Car Count</h5>
              <!--<div class="grh-wrp">
                <select name="" id="">
                  <option value="0">Please select</option>
                  <option value="1">Hourly</option>
                  <option value="2">Weekly</option>
                  <option value="3">Monthly</option>
                </select>
              </div>-->
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-lg-12">
                  <div id="chart"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="ibox animated fadeIn">
            <div class="ibox-title">
              <h5>Sales</h5>
              <!--<div class="grh-wrp">
                <select name="" id="">
                  <option value="0">Please select</option>
                  <option value="1">Hourly</option>
                  <option value="2">Weekly</option>
                  <option value="3">Monthly</option>
                </select>
              </div>-->
            </div>
            <div class="ibox-content">
              <div class="row">
                <div class="col-lg-12">
                  <div id="chart1"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="modal" id="repeatedCustomersModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Repeated Customers</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row" style="overflow-y: auto;height:450px">
              <div class="table-responsive">
                <table id="dataTables-repeatedCustomersModal" class="sum table table-striped table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile Number</th>
                    <th>Type</th>
                    <th>Repeat Count</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody id="history_table_body">
                    @foreach($duplicate_customers as $key=>$customer)
                    @php($key=$key+1)
                      <tr id="{{ $customer->id }}">
                      <td width="5%">{{$key}}</td>
                      <td width="15%">{{$customer->first_name}}</td>
                      <td width="10%">{{$customer->email}}</td>
                      <td width="10%">{{$customer->mobile}}</td>
                      <td width="10%">{{str_replace('_',' ',$customer->type)}}</td>
                      <td width="10%">{{$customer->CustomerCount}}</td>
                      <td width="10%" class="details-control"></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal" id="paymentDetailsModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title"><span id="pay_mode_title"></span> Payment Details</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body" style="overflow-y:auto;height:500px">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="row">
                <div class="table-responsive">
                  <table id="dataTables-jobs" class="sum table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                      <th></th>
                      <th>SN</th>
                      <th>Job Number</th>
                      <th>Wash_Date</th>
                      <th>Customer Name</th>
                      <th>Mobile</th>
                      <th>Type</th>
                      <th class="lft">Price</th>
                      <th class="lft">Discount</th>
                      <th class="lft">Net</th>
                      <th class="lft">Tax</th>
                      <th class="lft">Gross Total</th>
                      <th>Inv.Status</th>
                      <th>Inv.Ref</th>
                    </tr>
                    </thead>
                    <tbody id="jobs_table_body">
                    </tbody>
                    <tfoot align="right">
                      <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Total</th>
                        <th  id="price_total">0.00</th>
                        <th  id="discount_total">0.00</th>
                        <th  id="net_total">0.00</th>
                        <th  id="tax_total">0.00</th>
                        <th  id="gross_total">0.00</th>
                        <th></th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
              <div class="row" id="job_details_model" style="display:none;">
                  <div class="col-lg-12">
                    <div class="ibox ">
                      <div class="ibox-title">
                        <h5 style="width: 450px;">Job Details of  #<span id="job_number_id"></span> </h5>
                        <button type="button" class="close" id="close_job_details">&times;</button>
                      </div>
                      <div class="ibox-content">
                        <div class="table-responsive eaw">
                          <table id="job_details_dt" class="table table-striped table-bordered table-hover" >
                            <thead>
                              <tr>
                                <th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">#</th>
                                <th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Service Name</th>
                                <th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Gross</th>
                              </tr>
                            </thead>
                            <tbody id="job_details_body_dt"> 
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              
            </div>
          </div>
        </div>
      </div>
  </div>
    <script src="{{ asset('assets/js/apexcharts.js') }}"></script>
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

        var options = {
            series: [{
            name: 'Members Car Count',
            data: <?php echo $graph['y-values']['member_car_count'] ?>
            },{
            name: 'Non Members Car Count',
            data: <?php echo $graph['y-values']['non_member_car_count'] ?>
            }],
            chart: {
            height: '100%',
            type: 'area'
            },
            dataLabels: {
            enabled: false
            },
            stroke: {
            curve: 'smooth'
            },
            xaxis: {
            //type: 'datetime',
            categories: <?php echo $graph['x-values'] ?>
            },
            tooltip: {
            x: {
                //format: 'dd/MM/yy HH:mm'
            },
            },
            };

            var chart = new ApexCharts(document.querySelector("#chart"), options);
            chart.render();

            var options1 = {
            series: [{
            name: 'Members Total Sale',
            data: <?php echo $graph['y-values']['member_total_sale'] ?>
            }, {
            name: 'Non Members Total Sale',
            data: <?php echo $graph['y-values']['non_member_total_sale'] ?>
            }],
            chart: {
            height: '100%',
            type: 'area'
            },
            dataLabels: {
            enabled: false
            },
            stroke: {
            curve: 'smooth'
            },
            xaxis: {
            //type: 'datetime',
            categories: <?php echo $graph['x-values'] ?>
            },
            tooltip: {
            x: {
               // format: 'dd/MM/yy HH:mm'
            },
            },
            };

            var chart = new ApexCharts(document.querySelector("#chart1"), options1);
            chart.render();
    </script>
@endsection


@section('script')

<script>

$(".search-btn").on('click', function(e){
    e.preventDefault();
    $(this).prop('disabled', true);
    $(this).html('<i class="fa fa-spinner fa-spin"></i> Searching...');
    $("#search-form").submit();
});

$('a#payment_mode').on('click', function(){

  $("#job_details_model").hide();

  let payment_mode        = $(this).attr('data-payment_mode');

  $('span#pay_mode_title').html(payment_mode);

  let filter_type         = $('.tablinks:checked').val();
  let daily_date          = $("input[name='daily_date']").val();
  let weekly_date         = $("input[name='weekly_date']").val();
  let monthly_date        = $("input[name='monthly_date']").val();
  let custom_start_date   = $("input[name='custom_start_date']").val();
  let custome_end_date    = $("input[name='custome_end_date']").val();


 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
  $.ajax({
    url: '/paymentDetailsByMode',
    type:'POST',
    data: {payment_mode:payment_mode, filter_type:filter_type,daily_date:daily_date,weekly_date:weekly_date,monthly_date:monthly_date,custom_start_date:custom_start_date,custome_end_date:custome_end_date},
    success: function(response) {

          var list = '';

          var price_total = 0;
          var discount_total = 0;
          var net_total = 0;
          var tax_total = 0;
          var gross_total = 0;

          $.each(response.data.report_payment_details, function() {
            let invoice_status = this.inv_status;
            let color = '';
            if(invoice_status == "PENDING"){
              color = "warning"
            }else if(invoice_status == "CREATED") {
              color = "primary"
            }else if(invoice_status == "FAILED")  {
              color = "danger"
            }
            list += '<tr><td><a title="Job Details" class="view_job_detials" data-job_id="'+this.concern_id+'" data-job_number="'+this.number+'" data-type="'+this.type+'" data-gross="'+this.gross_amount+'" data-payment_mode="'+this.payment_mode+'"><i class="fa fa-plus-circle" style="color:green;" aria-hidden="true"></i></a></td> <td>'+this.sn+'</td><td>'+this.number+'</td><td>'+this.date+'</td><td>'+this.customer_name+'</td><td>'+this.mobile+'</td><td>'+this.type+'</td><td>'+this.amount+'</td><td>'+this.discount+'</td><td>'+this.net_amount+'</td><td>'+this.tax+'</td><td>'+this.gross_amount+'</td><td><span class="badge badge-pill badge-'+color+'">'+invoice_status+'</span></td><td>'+this.inv_ref+'</td></tr>';
            price_total += Number(this.amount);
            discount_total += Number(this.discount);
            net_total += Number(this.net_amount);
            tax_total += Number(this.tax);
            gross_total += Number(this.gross_amount);
          });

          $('#price_total').html(price_total.toFixed(2));
          $('#discount_total').html(discount_total.toFixed(2));
          $('#net_total').html(net_total.toFixed(2));
          $('#tax_total').html(tax_total.toFixed(2));
          $('#gross_total').html(gross_total.toFixed(2));

          $('#dataTables-jobs').DataTable().destroy();

          $("#jobs_table_body").html(list);

          $('#dataTables-jobs').DataTable({
            pageLength: 5,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [{
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'ExampleFile'
                },
                {
                    extend: 'pdf',
                    title: 'ExampleFile'
                },

                {
                    extend: 'print',
                    customize: function(win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                    }
                }
            ]

          });

           $('#paymentDetailsModal').modal("show");

    }
  });

});

var table = $('#dataTables-repeatedCustomersModal').DataTable({
  pageLength: 10,
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [{
                                extend: 'copy'
                            },
                            {
                                extend: 'csv'
                            },
                            {
                                extend: 'excel',
                                title: 'ExampleFile'
                            },
                            {
                                extend: 'pdf',
                                title: 'ExampleFile'
                            },

                            {
                                extend: 'print',
                                customize: function(win) {
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');

                                    $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                                }
                            }
                        ]

                      });                    

function format ( data ) {
    // `d` is the original data object for the row
    return '<table class="sub-row" style="border: .1px solid gray; background-color:#f8f9fa; ">'+
              '<thead>'+
                  '<tr>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">#</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Date</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Job Number</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Job Type</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; " class="lft">Price</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; " class="lft">Discount</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; " class="lft">Gross</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Inv Status</th>'+
                    '<th style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">Inv Ref</th>'+
                  '</tr>'+
              '</thead>'+
              '<tbody>'+
              data
              '</tbody>'+
            '</table>';
}

  $('#dataTables-repeatedCustomersModal tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var customer_id = tr.attr('id');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            let filter_type         = $('.tablinks:checked').val();
            let daily_date          = $("input[name='daily_date']").val();
            let weekly_date         = $("input[name='weekly_date']").val();
            let monthly_date        = $("input[name='monthly_date']").val();
            let custom_start_date   = $("input[name='custom_start_date']").val();
            let custome_end_date    = $("input[name='custome_end_date']").val();


            $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

            $.ajax({
              url: '/duplicateCustomerJobs',
              type:'POST',
              data: {customer_id:customer_id, filter_type:filter_type,daily_date:daily_date,weekly_date:weekly_date,monthly_date:monthly_date,custom_start_date:custom_start_date,custome_end_date:custome_end_date},
              success: function(response) {

                    var list = '';

                    $('.sub-row').DataTable().destroy();

                    $.each(response, function() {

                      let invoice_status = this.inv_status;
                      let color = '';
                      if(invoice_status == "PENDING"){
                        color = "warning"
                      }else if(invoice_status == "CREATED") {
                        color = "primary"
                      }else if(invoice_status == "FAILED")  {
                        color = "danger"
                      }

                      list += '<tr><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.sno+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px;">'+this.date+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.job_card_number+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.job_card_type+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.total_price+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.total_discount+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.gross_price+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; "><span class="badge badge-pill badge-'+color+'">'+invoice_status+'</span></td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.inv_ref+'</td></tr>';

                    });
                    row.child( format(list) ).show();
                    $('.sub-row').DataTable({ "ordering": false,"searching":false,"lengthChange": false });
                    tr.addClass('shown');
              }
            });
        }
    });
$("#close_job_details").on('click',function(){
  $("#dataTables-jobs tr").css('background-color','white');
  $('.view_job_detials').html('<i class="fa fa-plus-circle" style="color:green;" aria-hidden="true"></i>');
  $("#job_details_model").hide();
})

$("html").on('click','.view_job_detials', function(){

  $("#dataTables-jobs tr").css('background-color','white');
  $(this).closest('tr').css('background-color','#d1c7c7');

  $("#job_details_model").hide();
  $('.view_job_detials').html('<i class="fa fa-plus-circle" style="color:green;" aria-hidden="true"></i>');
  $(this).html('<i class="fa fa-minus-circle" style="color:red;" aria-hidden="true"></i>');

  var job_card_id     = $(this).attr("data-job_id");
  var job_card_number = $(this).attr("data-job_number");
  var type            = $(this).attr("data-type");
  var gross           = $(this).attr("data-gross");
  var payment_mode    = $(this).attr("data-payment_mode");

  if(type!="PACKAGE" && type!="CARD_RENEW"){

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    $.ajax({
      url: '/getJobCardDetailsFromJobId',
      type:'POST',
      data: {job_card_id:job_card_id},
                success: function(response) {

                      var list = '';

                    // $('#job_details_dt').DataTable().destroy();

                      var gross_details = 0;

                      $.each(response, function() {

                        gross_details += Number(this.gross_price);

                        list += '<tr><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.sno+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px;">'+this.service_name+'</td><td style="border: .1px solid gray; background-color:#f8f9fa; height:1px; padding: 0px 15px 0px 15px ; margin:0px; ">'+this.gross_price+'</td></tr>';

                      });

                      var pr    = "";
                      var other = gross_details - gross;

                      if(payment_mode == "CASH"){
                        pr = " CASH = "+gross+", CARD = "+ other.toFixed(2);
                      }else if(payment_mode == "CARD"){
                        pr = " CARD = "+gross+", CASH = "+ other.toFixed(2);
                      }else{
                        pr = payment_mode +" = "+gross;
                      }

                      $('#job_number_id').html("  <b>"+job_card_number+"</b> ( "+ pr +" )");

                      $("#job_details_body_dt").html(list);

                      //$('#job_details_dt').DataTable({ "ordering": false,"searching":false,"lengthChange": false });
                      $("#job_details_model").show();

                }                            
    });        

  }

  
});

</script>

@endsection
