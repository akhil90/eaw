<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisteredPlatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registered_plates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->string('plate_number')->unique();
            $table->integer('total_points_earned')->default(0);
            $table->integer('remaining_points')->default(0);
            $table->string('car_color')->nullable();
            $table->string('car_make')->nullable();
            $table->string('car_model')->nullable();
            $table->enum('car_type', ['SEDAN', 'SUV'])->nullable();
            $table->unsignedBigInteger('referer_id')->nullable();
            $table->enum('status', ['ACTIVE', 'DISABLED'])->default('ACTIVE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registered_plates');
    }
}
