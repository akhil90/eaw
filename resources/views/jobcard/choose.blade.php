@extends('layouts.outer')

@section('content')

<div class="register-panel animated fadeInDown">
  <div style="width: 100%; display: inline-block;">
  <div class="log"><a style="font-size: 12px;font-weight: 600;color:grey;border:solid 1px;border-radius:5px;padding:4px " href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Log out </a></div>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
       </form>
      <div class="cl-wrp" style="float:right; margin-top: 2px; width: 350px; padding-left: 75px;">
      
        <div class="cl-wrp-pl"></div>
        
        <span id='ct' style="font-size: 12px;font-weight: 600;color:grey"></span>
        <button type="button" id="gate_disable_buttton"  data-gate_status="DISABLED" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-danger barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier Off | Please click here for <span style="background-color:white;padding:5px;color:green;"> Activate </span></marquee></button>
        <button type="button" id="gate_active_buttton" data-gate_status="ACTIVE" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-success barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier On | Please click here for <span style="background-color:white;padding:5px;color:red"> Disable </span></marquee></button>
      </div>
  </div>
  <div class="cr4">
    <div class="tab-content">
      <p>--Continued</p>
      <h3>Select Main Services/Packages and Addons</h3>
      <hr style="color: #dee2e6;">
      <form class="m-t" role="form" method="post" action="/jobcard/redirect_jobconfirm">

        {{csrf_field()}}

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="pkg-outr bd-example bd-example-tabs">
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist"> 
                <a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="false">Main Services/Packages </a> 
                @if($addons!=null)
                  <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="true">Addons</a>
                @endif
                @if($car_detailing!=null)
                  <a class="nav-item nav-link" id="nav-detailing-tab" data-toggle="tab" href="#nav-detailing" role="tab" aria-controls="nav-detailing" aria-selected="true">Car Detailing</a> </div>
                @endif
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                @foreach($single_services as $key => $service)
                      @php
                        $features = null;
                      @endphp
                  
                    @if($service->features!= NULL)
                      @php
                        $features = explode(",",$service->features);
                      @endphp
                    @endif

                    @if($submit_type == "member_common" || $submit_type == "redeem_points")
                      @php
                        $checked = 'checked';
                      @endphp
                    @else
                      @php
                       $checked = '';
                      @endphp
                    @endif

                    @if($submit_type == "buy_package" || $submit_type == "non_member_common")
                      @php
                        $price = $service->price;
                      @endphp
                    @else
                      @php
                        $price = "0.00";
                      @endphp
                    @endif

                  <div class="pkg-sec packages">
                    <div class="selt">
                      <input id="service_id" class="service_id" {{ $checked  }} name="service_id" value="{{ $service->id }}" type="radio">
                    </div>
                    <h4>{{ $service->service_name }}</br>
                    {{ $service->car_type }} -  {{ $price }} AED </br> {{ $service->duration }} Minutes</h4>
                    <ul class="fe">
                      @if($features)
                        @foreach($features as $feature)
                          <li>{{ $feature }}</li>
                        @endforeach
                      @else
                        <li> Features Not found</li>
                      @endif
                    </ul>
                </div>
                @endforeach
                </div>

                @if($addons!=null)
                <div class="tab-pane" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  @foreach($addons as $key => $addon)
                      @if($addon->features!= NULL)
                        @php
                          $features = explode(",",$addon->features);
                        @endphp
                      @endif
                    <div class="pkg-sec">
                      <div class="selt">
                        <input id="addon_id" class="addon_id" name="addon_id[]" value="{{ $addon->id }}" type="checkbox">
                      </div>
                      <h4>{{ $addon->service_name }}</br>
                      {{ $addon->car_type }} -  {{ $addon->price }} AED </br> {{ $addon->duration }} Minutes</h4>
                      <ul class="fe">
                        @if($features)
                          @foreach($features as $feature)
                            <li>{{ $feature }}</li>
                          @endforeach
                        @else
                          <li> Features Not found</li>
                        @endif
                      </ul>
                  </div>
                  @endforeach
                </div>
              @endif

              @if($car_detailing!=null)
                <div class="tab-pane" id="nav-detailing" role="tabpanel" aria-labelledby="nav-detailing-tab">
                  @foreach($car_detailing as $key => $car_detail)
                      @if($car_detail->features!= NULL)
                        @php
                          $features = explode(",",$car_detail->features);
                        @endphp
                      @endif
                    <div class="pkg-sec">
                      <div class="selt">
                        <input id="car_detail_id" class="car_detail_id" name="car_detail_id[]" value="{{ $car_detail->id }}" type="checkbox">
                      </div>
                      <h4>{{ $car_detail->service_name }}</br>
                      {{ $car_detail->car_type }} -  {{ $car_detail->price }} AED </br> {{ $car_detail->duration }} Minutes</h4>
                      <ul class="fe">
                        @if($features)
                          @foreach($features as $feature)
                            <li>{{ $feature }}</li>
                          @endforeach
                        @else
                          <li> Features Not found</li>
                        @endif
                      </ul>
                  </div>
                  @endforeach
                </div>
              @endif

              </div>
            </div>
            <div class="bt-sec-wrp"><button type="submit"  class="btn btn78 btn-primary block m-b mt-3">Submit</button></div>
          </div>
        </div>
      </form>
    </div>
    <div class="clgo1"> <img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid">
      <p class="m-t"> <small> ©2020 All Rights Reserved. <a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </small> </p>
    </div>
  </div>
</div>
<div id="confirm" class="modal">
    <div class="modal-dialog">

      <div class="modal-content">

      <div class="modal-body">
        <h3><b>Are you sure to change the barrier status ?</b></h3>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="change">Change</button>
        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
      </div>

      </div>
    </div>
  </div>

  @endsection

  @section('script')

  <script>

  $(document).ready(function () {
    $('.service_id').on('click', function(){
      $(".packages").css("background-color", "white");
      $(this).closest(".pkg-sec").css("background-color", "#d6d6c2");
    });
    $('.addon_id').on('click', function(){
      if ($(this).is(':checked')) {
        $(this).closest(".pkg-sec").css("background-color", "#d6d6c2");
      }else{
        $(this).closest(".pkg-sec").css("background-color", "white");
      }
    });
    $('.car_detail_id').on('click', function(){
      if ($(this).is(':checked')) {
        $(this).closest(".pkg-sec").css("background-color", "#d6d6c2");
      }else{
        $(this).closest(".pkg-sec").css("background-color", "white");
      }
    });
  });

  </script>



  @endsection

  

