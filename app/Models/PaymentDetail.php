<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    use HasFactory;

    protected $table = 'payment_details';
    protected $fillable = [
        'concern_id', 'type','customer_id','customer_type','payment_mode','voucher_no','txn_number','amount','discount','gross_amount','tax','net_amount','created_at','updated_at'
    ];
}
