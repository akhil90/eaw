@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Update Package</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
          <div class="ibox">
            <div class="ibox-title">
              <h3>Update Details</h3>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content"> <br>
            <form action="/membership_packages/update/{{ $main_service->id }}" enctype="multipart/form-data" id="form" method="post" class="form-horizontal form-label-left">
              @csrf
              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Package Name </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="service_name" id="service_name" placeholder="Enter Package Name" value="{{ $main_service->service_name }}" class="form-control {{ $errors->has('service_name') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('service_name'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('service_name') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Car</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="car_type" @if($main_service->car_type == "SEDAN") checked @endif value="SEDAN"> <i></i> SEDAN </label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="car_type" @if($main_service->car_type == "SUV") checked @endif value="SUV"> <i></i> SUV </label>
                        </div>
                      </div>
                      <!-- <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="car_type" @if($main_service->car_type == "BOTH") checked @endif value="BOTH"> <i></i> BOTH </label>
                        </div>
                      </div> -->
                    </div>
                      @if($errors->has('car_type'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('car_type') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="price" id="price" placeholder="Enter Price" value="{{ $main_service->price }}" class="form-control {{ $errors->has('price') ? 'is-invalid' : ''}}">
                      @if($errors->has('price'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('price') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Tax Type</label>
                    <div class="col-md-8 col-sm-8 col-xs-12">
                    <select name="tax_id" id="tax_id" class="form-control {{ $errors->has('tax_id') ? 'is-invalid' : ''}}">
                       <option value="">-- Select Tax Type --</option>
                       @foreach($taxTypes as $taxtype)

                        <option  @if($main_service->tax_id == $taxtype->id || $default_tax_type == $taxtype->id) selected @endif   value="{{ $taxtype->id }}">{{ $taxtype->taxTitle }}</option>

                       @endforeach
                      </select>
                      @if($errors->has('vat'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('tax_id') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-1 col-sm-1 col-xs-12">
                      <button type="button" id="refresh" class="btn btn-secondary"><i class="fa fa-refresh"></i></button>
                   </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Duration</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="duration" id="duration" placeholder="Enter Duration(Minutes)" value="{{ $main_service->duration }}" class="form-control {{ $errors->has('duration') ? 'is-invalid' : ''}}">
                      @if($errors->has('duration'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('duration') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Number Of Washes</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="no_of_washes" id="no_of_washes" placeholder="Enter Number Of Washes" value="{{ $main_service->no_of_washes }}" class="form-control {{ $errors->has('no_of_washes') ? 'is-invalid' : ''}}">
                      @if($errors->has('no_of_washes'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('no_of_washes') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Discount Threshold</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="discount_threshold" id="discount_threshold" placeholder="Enter Discount Threshold" value="{{ $main_service->discount_threshold }}" class="form-control {{ $errors->has('discount_threshold') ? 'is-invalid' : ''}}">
                      @if($errors->has('discount_threshold'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('discount_threshold') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Package Validity</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="package_validity" id="package_validity" placeholder="Enter Package Validity(days)" value="{{ $main_service->package_validity }}" class="form-control {{ $errors->has('package_validity') ? 'is-invalid' : ''}}">
                      @if($errors->has('package_validity'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('package_validity') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Wash Validity</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="number" name="wash_validity" id="wash_validity" placeholder="Enter Wash Validity(days)" value="{{ $main_service->wash_validity }}" class="form-control {{ $errors->has('wash_validity') ? 'is-invalid' : ''}}">
                      @if($errors->has('wash_validity'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('wash_validity') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Service Start From</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="start_from_type" id="start_from_type" class="form-control {{ $errors->has('start_from_type') ? 'is-invalid' : ''}}">
                       <option @if($main_service->start_from_type == "FIRST_WASH") selected @endif value="FIRST_WASH">First Wash</option>
                       <option @if($main_service->start_from_type == "PURCHASE_DATE") selected @endif value="PURCHASE_DATE">Purchase Date</option>
                      </select>
                      @if($errors->has('start_from_type'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('start_from_type') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Features </label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                     <textarea class="form-control {{ $errors->has('features') ? 'is-invalid' : ''}}" id="features" name="features" rows="4" cols="50" style="height:90px;" placeholder="Enter Package Features">{{ $main_service->features }}</textarea>
                     <b>*Note : Features separate with commas</b>
                     @if($errors->has('features'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('features') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>  
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <div class="row">
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="status" @if($main_service->status == "ACTIVE") checked @endif value="ACTIVE"> <i></i> ACTIVE </label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="status" @if($main_service->status == "PENDING") checked @endif value="PENDING"> <i></i> PENDING </label>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-2">
                        <div class="i-checks">
                          <label><input type="radio" name="status" @if($main_service->status == "DISABLED") checked @endif value="DISABLED"> <i></i> DISABLED </label>
                        </div>
                      </div>
                    </div>
                      @if($errors->has('status'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>      
                <div class="form-group text-center">
                  <div class="fm-ctr col-md-12 col-sm-12 col-xs-12"> <a href="/membership_packages" class="btn btn-w-m btn-default" type="button">Cancel</a>
                    <button class="btn btn-w-m btn-info" type="reset">Reset</button>
                    <button type="submit" class="btn btn-w-m btn-primary has-spinner" id="saveUser">Update Package</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('script')

<script>

$('button#refresh').on('click', function(){
 $('#refresh').html('<i class="fa fa-refresh fa-spin"></i>');
  $.ajax({
    url: '/main_services/refreshtax',
    type:'GET',
    //data: {},
    success: function(response) {
      $('#tax_id').html(response);
      $('#refresh').html('<i class="fa fa-refresh"></i>');

    }
  });                          
           
});


</script>


@endsection