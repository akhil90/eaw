<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembershipPackageLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_package_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->unsignedBigInteger('main_service_id');
            $table->foreign('main_service_id')->references('id')->on('main_services')->onDelete('cascade');
            $table->integer('service_completed')->default(0);
            $table->integer('service_remaining')->default(0);
            $table->dateTime('package_purchase_date')->nullable();
            $table->dateTime('package_end_date')->nullable();
            $table->dateTime('wash_validity_start')->nullable();
            $table->dateTime('wash_validity_end')->nullable();
            $table->enum('status', ['ACTIVE', 'EXPIRED'])->default('ACTIVE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_package_log');
    }
}
