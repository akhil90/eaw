<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Models\LoginDetail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index(){
      
      if(Auth::user){

        if(Auth::user()->role == 'ADMIN'){
          return redirect('/home');
        }else{
          return redirect('/jobcard');
        }

      }
      return view('login');
    }

    public function username()
    {
        return 'employee_id'; //or return the field which you want to use.
    }

    public function authenticated(Request $request, $user)
    {
      $loginDetail = new LoginDetail();

      $loginDetail->user_id    = $user->id;
      $loginDetail->login_time = date('Y-m-d H:i:s');
      $loginDetail->ip_address = $request->getClientIp();
      $loginDetail->browser = $request->header('User-Agent');
      $loginDetail->save();

    }

    public function redirectTo() {
        $role = Auth::user()->role; 

        switch ($role) {

          case 'ADMIN':
            return '/home';
            break;

          case 'STAFF':
            return '/jobcard';
            break; 
      
          default:
            return '/home'; 
          break;

        }
      }
}
