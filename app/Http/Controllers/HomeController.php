<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Home;
use App\Models\MainService;
use App\Models\JobCard;
use App\Models\Settings;
use Illuminate\Support\Facades\Auth;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request){  
        
        if(Auth::user()->role != 'ADMIN'){
            return redirect('/jobcard');
        }

        if($request->input('filter_type') == ""){
            $filter_type        =   "Daily";
        }else{
            $filter_type        =   $request->input('filter_type');
        }
        if($request->input('daily_date') == ""){
            $daily_date         =   date('Y-m-d');
        }else{
            $daily_date         =   date('Y-m-d',strtotime($request->input('daily_date')));
        }
        if($request->input('weekly_date') == ""){
            $weekly_start_date  =   date('Y-m-d', strtotime('-6 days'));
            $weekly_end_date    =   date('Y-m-d');
        }else{
            $weekly_date        =   explode(" - ",$request->input('weekly_date'));
            $weekly_start_date  =   date('Y-m-d',strtotime($weekly_date[0]));
            $weekly_end_date    =   date('Y-m-d',strtotime($weekly_date[1]));
        }
        if($request->input('monthly_date') == ""){
            $month              =   date('m');
            $year               =   date('Y');
        }else{
            $monthly_date       =   explode("/",$request->input('monthly_date'));
            $month              =   $monthly_date[0];
            $year               =   $monthly_date[1];
        }
        if($request->input('custom_start_date') == ""){
            $custom_start_date  =  date('Y-m-d');
        }else{
            $custom_start_date  =  date('Y-m-d',strtotime($request->input('custom_start_date')));
        }
        if($request->input('custome_end_date') == ""){
            $custome_end_date   =  date('Y-m-d');
        }else{
            $custome_end_date   =  date('Y-m-d',strtotime($request->input('custome_end_date')));
        }

        $param['filter_type']           =   $filter_type;
        $param['daily_date']            =   date('m/d/Y',strtotime($daily_date));
        $param['weekly_date']           =   date('m/d/Y',strtotime($weekly_start_date)).' - '.date('m/d/Y',strtotime($weekly_end_date));
        $param['monthly_date']          =   $month.'/'.$year;
        $param['custom_start_date']     =   date('m/d/Y',strtotime($custom_start_date));
        $param['custome_end_date']      =   date('m/d/Y',strtotime($custome_end_date));

        $search['filter_type']          =   $filter_type;
        $search['daily_date']           =   $daily_date;
        $search['weekly_start_date']    =   $weekly_start_date;
        $search['weekly_end_date']      =   $weekly_end_date;
        $search['month']                =   $month;
        $search['year']                 =   $year;
        $search['custom_start_date']    =   $custom_start_date;
        $search['custome_end_date']     =   $custome_end_date;

        $data['carwash_details']        =   Home::carwash_details($search);
        $data['sales_reports']          =   Home::sales_report($search);
        $data['single_service_reports'] =   Home::service_report($search,"SINGLE");
        $data['package_reports']        =   Home::package_purchase_report($search);
        $data['addon_reports']          =   Home::service_report($search,"ADDON");
        $data['card_renew_reports']     =   Home::card_renew_report($search);
        $data['car_detailing_reports']  =   Home::service_report($search,"CAR_DETAILING");
        $data['free_service_reports']   =   Home::service_report($search,"FREE");
        $data['param']                  =   $param;

        $data['sedan_car_count']        =   0;
        $data['suv_car_count']          =   0;

        $data['sedan_car_count']        =   Home::total_car_count_groupby($search,"SEDAN");
        $data['suv_car_count']          =   Home::total_car_count_groupby($search,"SUV");
        $data['total_car_count']        =   $data['sedan_car_count'] + $data['suv_car_count'];


        $main_services_non_mem   = MainService::where('status', 'ACTIVE')->whereIn('service_category', ['SINGLE','ADDON','CAR_DETAILING'])->get();
            
        $carCountGroupByNON      = Home::car_count_groupby($search,'NON_MEMBER');

        $dTempCarCountNonMember  = array();

        foreach($main_services_non_mem as $service){

            $temp = array();
            $temp['id']           = $service->id;
            $temp['service_name'] = $service->service_name.",".$service->service_category;
            $temp['SEDAN']        = 0;
            $temp['SUV']          = 0;

            foreach($carCountGroupByNON as $carCount){
                if($carCount->id ==  $service->id && $carCount->car_type == "SEDAN"){
                    $temp['SEDAN'] = $carCount->count;
                }
                
                if($carCount->id ==  $service->id && $carCount->car_type =="SUV"){
                    $temp['SUV'] = $carCount->count;
                }
            }

            $temp['total'] = $temp['SEDAN'] + $temp['SUV'];

            if($temp['total'] > 0)
                $dTempCarCountNonMember[] =  $temp;

        }

        $data['non_member_service_car_count']   =   $dTempCarCountNonMember;


        $main_services_mem   = MainService::where('status', 'ACTIVE')->whereIn('service_category', ['PACKAGE','ADDON','CAR_DETAILING'])->get();

        $carCountGroupByMem  = Home::car_count_groupby($search,'MEMBER');

        $dTempCarCountMember = array();

        foreach($main_services_mem as $service){

            $temp = array();
            $temp['id']           = $service->id;
            $temp['service_name'] = $service->service_name.",".$service->service_category;
            $temp['SEDAN']        = 0;
            $temp['SUV']          = 0;

            foreach($carCountGroupByMem as $carCount){
                if($carCount->id ==  $service->id && $carCount->car_type == "SEDAN"){
                    $temp['SEDAN'] = $carCount->count;
                }
                
                if($carCount->id ==  $service->id && $carCount->car_type =="SUV"){
                    $temp['SUV'] = $carCount->count;
                }
            }

            $temp['total'] = $temp['SEDAN'] + $temp['SUV'];

            if($temp['total'] > 0)
                $dTempCarCountMember[] =  $temp;

        }

        $data['member_service_car_count']   =   $dTempCarCountMember;

        $duplicateCustomers                 =  Home::dupliacteCustomers($search);

        $data['duplicate_customers']        =   $duplicateCustomers;

        if($filter_type == "Daily"){
            $data['graph']    =   $this->day_graph($daily_date);
        }elseif ($filter_type == "Weekly"){
            $data['graph']    =   $this->week_graph($weekly_start_date, $weekly_end_date);
        }elseif ($filter_type == "Monthly"){
            $data['graph']    =   $this->month_graph($month, $year);
        }elseif ($filter_type == "Custom"){
            $data['graph']    =   $this->week_graph($custom_start_date, $custome_end_date);
        }else{
            $data['graph']    =   "";
        }

        return view('home')->with($data);
    }
    public function day_graph($date){
        $sql        = " WHERE DATE(`created_at`)='$date'";
        $sale_results     = Home::graph_values_for_sale($sql);
        $count_results    = Home::graph_values_for_count($sql);
        for($i =0 ; $i <= 23; $i++){
            $hours[]                =   date("h a", strtotime("00-00-00 $i:00:00"));
            $member_car_count[$i]   =   $non_member_car_count[$i]   =  $member_total_sale[$i]   =  $non_member_total_sale[$i]   = 0;
            foreach($sale_results as $result){
                if((int)date('H',strtotime($result->created_at)) == $i){
                    if($result->customer_type == "MEMBER"){
                        $member_total_sale[$i] = $member_total_sale[$i] + $result->gross_amount;
                    }
                    if($result->customer_type == "NON_MEMBER"){
                        $non_member_total_sale[$i] = $non_member_total_sale[$i] + $result->gross_amount;
                    }
                }
            }
            foreach($count_results as $result){
                if((int)date('H',strtotime($result->created_at)) == $i){
                    if($result->customer_type == "MEMBER"){
                        $member_car_count[$i]++;
                    }
                    if($result->customer_type == "NON_MEMBER"){
                        $non_member_car_count[$i]++;
                    }
                }
            }
        }
        return array(
                    'x-values' => json_encode($hours), 
                    'y-values' => array(
                                        'member_car_count'       => json_encode($member_car_count), 
                                        'non_member_car_count'   => json_encode($non_member_car_count),
                                        'member_total_sale'      => json_encode($member_total_sale), 
                                        'non_member_total_sale'  => json_encode($non_member_total_sale)
                                    )
                );
    }
    public function week_graph($start_date, $end_date){

        $sql              = " WHERE DATE(`created_at`)>='$start_date' AND DATE(`created_at`)<='$end_date'";
        $sale_results     = Home::graph_values_for_sale($sql);
        $count_results    = Home::graph_values_for_count($sql);
        
        for($date = $start_date, $i=0; $date <= $end_date; $date = date('Y-m-d', strtotime($date . ' +1 day')), $i++){
            $dates[]                =   date("M d-Y", strtotime($date));
            $member_car_count[$i]   =   $non_member_car_count[$i]   =  $member_total_sale[$i]   =  $non_member_total_sale[$i]   = 0;
            foreach($sale_results as $result){
                if(date('Y-m-d',strtotime($result->created_at)) == $date){
                    if($result->customer_type == "MEMBER"){
                        $member_total_sale[$i] = $member_total_sale[$i] + $result->gross_amount;
                    }
                    if($result->customer_type == "NON_MEMBER"){
                        $non_member_total_sale[$i] = $non_member_total_sale[$i] + $result->gross_amount;
                    }
                }
            }
            foreach($count_results as $result){
                if(date('Y-m-d',strtotime($result->created_at)) == $date){
                    if($result->customer_type == "MEMBER"){
                        $member_car_count[$i]++;
                    }
                    if($result->customer_type == "NON_MEMBER"){
                        $non_member_car_count[$i]++;
                    }
                }
            }
        }
        return array(
            'x-values' => json_encode($dates), 
            'y-values' => array(
                                'member_car_count'       => json_encode($member_car_count), 
                                'non_member_car_count'   => json_encode($non_member_car_count),
                                'member_total_sale'      => json_encode($member_total_sale), 
                                'non_member_total_sale'  => json_encode($non_member_total_sale)
                            )
        );
    }
    public function month_graph($month, $year){
        $start_date = $year."-".$month."-01";
        $end_date   = date("Y-m-t", strtotime($start_date));
        return $this->week_graph($start_date, $end_date);
    }

    public function salesReportByPaymentModeAjax(Request $request){

        if($request->input('filter_type') == ""){
            $filter_type        =   "Daily";
        }else{
            $filter_type        =   $request->input('filter_type');
        }
        if($request->input('daily_date') == ""){
            $daily_date         =   date('Y-m-d');
        }else{
            $daily_date         =   date('Y-m-d',strtotime($request->input('daily_date')));
        }
        if($request->input('weekly_date') == ""){
            $weekly_start_date  =   date('Y-m-d', strtotime('-6 days'));
            $weekly_end_date    =   date('Y-m-d');
        }else{
            $weekly_date        =   explode(" - ",$request->input('weekly_date'));
            $weekly_start_date  =   date('Y-m-d',strtotime($weekly_date[0]));
            $weekly_end_date    =   date('Y-m-d',strtotime($weekly_date[1]));
        }
        if($request->input('monthly_date') == ""){
            $month              =   date('m');
            $year               =   date('Y');
        }else{
            $monthly_date       =   explode("/",$request->input('monthly_date'));
            $month              =   $monthly_date[0];
            $year               =   $monthly_date[1];
        }
        if($request->input('custom_start_date') == ""){
            $custom_start_date  =  date('Y-m-d');
        }else{
            $custom_start_date  =  date('Y-m-d',strtotime($request->input('custom_start_date')));
        }
        if($request->input('custome_end_date') == ""){
            $custome_end_date   =  date('Y-m-d');
        }else{
            $custome_end_date   =  date('Y-m-d',strtotime($request->input('custome_end_date')));
        }

        $search['filter_type']          =   $filter_type;
        $search['daily_date']           =   $daily_date;
        $search['weekly_start_date']    =   $weekly_start_date;
        $search['weekly_end_date']      =   $weekly_end_date;
        $search['month']                =   $month;
        $search['year']                 =   $year;
        $search['custom_start_date']    =   $custom_start_date;
        $search['custome_end_date']     =   $custome_end_date;
        
        $sales_job_cards          = Home::payment_type_sales_report_job_cards($search, $request->input('payment_mode'));
        $sales_package_purchases  = Home::payment_type_sales_report_package_purchase($search, $request->input('payment_mode'));
        $sales_card_renews        = Home::payment_type_sales_report_card_renew($search, $request->input('payment_mode'));
        
        $report_payment_details   = array();

        foreach($sales_job_cards as $key => $sales_job_card){

            $t['sn']                 = $key +1;
            $t['number']             = $sales_job_card->job_card_number;
            $t['concern_id']         = $sales_job_card->concern_id;
            $t['customer_name']      = $sales_job_card->first_name;
            $t['mobile']             = $sales_job_card->mobile;
            $t['type']               = $sales_job_card->job_card_type;
            $t['payment_mode']       = $sales_job_card->payment_mode;
            $t['amount']             = $sales_job_card->amount;
            $t['discount']           = $sales_job_card->discount;
            $t['gross_amount']       = $sales_job_card->gross_amount;
            $t['tax']                = $sales_job_card->tax;
            $t['net_amount']         = $sales_job_card->net_amount;
            $t['date']               = date('d-m-y H:i', strtotime($sales_job_card->created_at));
            $t['inv_status']         = $sales_job_card->inv_status;
            $t['inv_ref']            = $sales_job_card->inv_ref;
            

            $report_payment_details[] = $t;

        }


        foreach($sales_package_purchases as $key => $sales_package_purchase){

            $t['sn']                 = $key +1;
            $t['number']             = $sales_package_purchase->package_purchase_number;
            $t['concern_id']         = $sales_package_purchase->concern_id;
            $t['customer_name']      = $sales_package_purchase->first_name;
            $t['mobile']             = $sales_package_purchase->mobile;
            $t['type']               = "PACKAGE";
            $t['payment_mode']       = $sales_package_purchase->payment_mode;
            $t['amount']             = $sales_package_purchase->amount;
            $t['discount']           = $sales_package_purchase->discount;
            $t['gross_amount']       = $sales_package_purchase->gross_amount;
            $t['tax']                = $sales_package_purchase->tax;
            $t['net_amount']         = $sales_package_purchase->net_amount;
            $t['date']               = date('d-m-y H:i', strtotime($sales_package_purchase->created_at));
            $t['inv_status']         = $sales_package_purchase->inv_status;
            $t['inv_ref']            = $sales_package_purchase->inv_ref;

            $report_payment_details[] = $t;

        }

        foreach($sales_card_renews as $key => $sales_card_renew){

            $t['sn']                 = $key +1;
            $t['number']             = $sales_card_renew->purchase_number;
            $t['concern_id']         = $sales_card_renew->concern_id;
            $t['customer_name']      = $sales_card_renew->first_name;
            $t['mobile']             = $sales_card_renew->mobile;
            $t['type']               = "CARD_RENEW";
            $t['payment_mode']       = $sales_card_renew->payment_mode;
            $t['amount']             = $sales_card_renew->amount;
            $t['discount']           = $sales_card_renew->discount;
            $t['gross_amount']       = $sales_card_renew->gross_amount;
            $t['tax']                = $sales_card_renew->tax;
            $t['net_amount']         = $sales_card_renew->net_amount;
            $t['date']               = date('d-m-y H:i', strtotime($sales_card_renew->created_at));
            $t['inv_status']         = $sales_card_renew->inv_status;
            $t['inv_ref']            = $sales_card_renew->inv_ref;

            $report_payment_details[] = $t;

        }

        $data['report_payment_details'] =  $report_payment_details;

        
        return response()->json(['data' => $data]);


    }

    public function duplicateCustomerJobs(Request $request){

        if($request->input('filter_type') == ""){
            $filter_type        =   "Daily";
        }else{
            $filter_type        =   $request->input('filter_type');
        }
        if($request->input('daily_date') == ""){
            $daily_date         =   date('Y-m-d');
        }else{
            $daily_date         =   date('Y-m-d',strtotime($request->input('daily_date')));
        }
        if($request->input('weekly_date') == ""){
            $weekly_start_date  =   date('Y-m-d', strtotime('-6 days'));
            $weekly_end_date    =   date('Y-m-d');
        }else{
            $weekly_date        =   explode(" - ",$request->input('weekly_date'));
            $weekly_start_date  =   date('Y-m-d',strtotime($weekly_date[0]));
            $weekly_end_date    =   date('Y-m-d',strtotime($weekly_date[1]));
        }
        if($request->input('monthly_date') == ""){
            $month              =   date('m');
            $year               =   date('Y');
        }else{
            $monthly_date       =   explode("/",$request->input('monthly_date'));
            $month              =   $monthly_date[0];
            $year               =   $monthly_date[1];
        }
        if($request->input('custom_start_date') == ""){
            $custom_start_date  =  date('Y-m-d');
        }else{
            $custom_start_date  =  date('Y-m-d',strtotime($request->input('custom_start_date')));
        }
        if($request->input('custome_end_date') == ""){
            $custome_end_date   =  date('Y-m-d');
        }else{
            $custome_end_date   =  date('Y-m-d',strtotime($request->input('custome_end_date')));
        }

        $search['filter_type']          =   $filter_type;
        $search['daily_date']           =   $daily_date;
        $search['weekly_start_date']    =   $weekly_start_date;
        $search['weekly_end_date']      =   $weekly_end_date;
        $search['month']                =   $month;
        $search['year']                 =   $year;
        $search['custom_start_date']    =   $custom_start_date;
        $search['custome_end_date']     =   $custome_end_date;
        $cutomer_id                     =   $request->input('customer_id');

        $jobs = Home::dupliacteCustomersJobs($search, $cutomer_id);

        $data = array();

        foreach($jobs as $key=>$job){

            $t=array();

            $t['sno']                = $key+1;
            $t['date']               = date('d/m/Y h:i A', strtotime($job->created_at));
            $t['job_card_number']    = $job->job_card_number;
            $t['job_card_type']      = $job->job_card_type;
            $t['customer_type']      = $job->customer_type;
            $t['total_price']        = $job->total_price;
            $t['total_discount']     = $job->total_discount;
            $t['gross_price']        = $job->gross_price;
            $t['total_tax']          = $job->total_tax;
            $t['net_amount_total']   = $job->net_amount_total;
            $t['earned_points']      = $job->earned_points;
            $t['payment_type']       = $job->payment_type;
            $t['car_type']           = $job->car_type;
            $t['inv_status']         = $job->inv_status;
            $t['inv_ref']            = $job->inv_ref;

            $data[] = $t;

        }

        return response()->json($data);

    }

    public function getJobCardDetailsFromJobId(Request $request){

        $job_card_id = $request->input('job_card_id');

        $jobDetails = Home::jobDetailsFromJobId($job_card_id);

        foreach($jobDetails as $key=>$jobDetail){

            $t=array();

            $t['sno']                = $key+1;
            $t['date']               = date('d-m-y H:i', strtotime($jobDetail->created_at));
            $t['service_name']       = $jobDetail->service_name;
            $t['price']              = $jobDetail->price;
            $t['discount']           = $jobDetail->discount;
            $t['gross_price']        = $jobDetail->gross_price;
            $t['tax']                = $jobDetail->tax;
            $t['net_amount']         = $jobDetail->net_amount;
            

            $data[] = $t;


        }

        return response()->json($data);

    }

    public function rePostInvoice($jobId){

        $cashPaymentMethodCode    = Settings::getSettingValueByType('cash_payment_code');
        $cardPaymentMethodCode    = Settings::getSettingValueByType('card_payment_code');

        $cashDepositToAccountCode = Settings::getSettingValueByType('cash_payment_account_code');
        $cardDepositToAccountCode = Settings::getSettingValueByType('card_payment_account_code');
        $discountAccountCode      = Settings::getSettingValueByType('discount_account_code');


        $jobCard       = JobCard::find($jobId);

        $qb_request    = $jobCard->qb_request;

        $qb_req_array  = json_decode($qb_request);

        $qb_req_array->DiscountAccount =  $discountAccountCode;

        $responseArray = JobCard::invoiceCreation($jobCard, (array)$qb_req_array);
        
        return redirect('/home');

    }
    public function dailyReport(){
        $search['filter_type']          =   "Daily";
        $search['daily_date']           =   "2021-04-01";
        
        $data['carwash_details']        =   Home::carwash_details($search);
        $data['sales_reports']          =   Home::sales_report($search);
        $data['single_service_reports'] =   Home::service_report($search,"SINGLE");
        $data['package_reports']        =   Home::package_purchase_report($search);
        $data['addon_reports']          =   Home::service_report($search,"ADDON");
        $data['card_renew_reports']     =   Home::card_renew_report($search);
        $data['car_detailing_reports']  =   Home::service_report($search,"CAR_DETAILING");
        $data['free_service_reports']   =   Home::service_report($search,"FREE");

        $data['sedan_car_count']        =   0;
        $data['suv_car_count']          =   0;

        $data['sedan_car_count']        =   Home::total_car_count_groupby($search,"SEDAN");
        $data['suv_car_count']          =   Home::total_car_count_groupby($search,"SUV");
        $data['total_car_count']        =   $data['sedan_car_count'] + $data['suv_car_count'];


        $main_services_non_mem   = MainService::where('status', 'ACTIVE')->whereIn('service_category', ['SINGLE','ADDON','CAR_DETAILING'])->get();
            
        $carCountGroupByNON      = Home::car_count_groupby($search,'NON_MEMBER');

        $dTempCarCountNonMember  = array();

        foreach($main_services_non_mem as $service){

            $temp = array();
            $temp['id']           = $service->id;
            $temp['service_name'] = $service->service_name.",".$service->service_category;
            $temp['SEDAN']        = 0;
            $temp['SUV']          = 0;

            foreach($carCountGroupByNON as $carCount){
                if($carCount->id ==  $service->id && $carCount->car_type == "SEDAN"){
                    $temp['SEDAN'] = $carCount->count;
                }
                
                if($carCount->id ==  $service->id && $carCount->car_type =="SUV"){
                    $temp['SUV'] = $carCount->count;
                }
            }

            $temp['total'] = $temp['SEDAN'] + $temp['SUV'];

            if($temp['total'] > 0)
                $dTempCarCountNonMember[] =  $temp;

        }

        $data['non_member_service_car_count']   =   $dTempCarCountNonMember;


        $main_services_mem   = MainService::where('status', 'ACTIVE')->whereIn('service_category', ['PACKAGE','ADDON','CAR_DETAILING'])->get();

        $carCountGroupByMem  = Home::car_count_groupby($search,'MEMBER');

        $dTempCarCountMember = array();

        foreach($main_services_mem as $service){

            $temp = array();
            $temp['id']           = $service->id;
            $temp['service_name'] = $service->service_name.",".$service->service_category;
            $temp['SEDAN']        = 0;
            $temp['SUV']          = 0;

            foreach($carCountGroupByMem as $carCount){
                if($carCount->id ==  $service->id && $carCount->car_type == "SEDAN"){
                    $temp['SEDAN'] = $carCount->count;
                }
                
                if($carCount->id ==  $service->id && $carCount->car_type =="SUV"){
                    $temp['SUV'] = $carCount->count;
                }
            }

            $temp['total'] = $temp['SEDAN'] + $temp['SUV'];

            if($temp['total'] > 0)
                $dTempCarCountMember[] =  $temp;

        }

        $data['member_service_car_count']   =   $dTempCarCountMember;

        $duplicateCustomers                 =  Home::dupliacteCustomers($search);

        $data['duplicate_customers']        =   $duplicateCustomers;

        $pdf             = PDF::loadView('daily_report', $data)->setPaper('a4', 'portrait');   
        return $pdf->stream();
    }
}
