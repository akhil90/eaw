<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;

    protected $table = 'members';

    protected $fillable = [
        'card_number', 'customer_id','membership_package_log_id','service_completed','service_remaining','card_id','start_date','end_date','status','created_at','updated_at'
    ];

    public static function getAll($filter = []){

        $query =  Member::select([
                    'members.id',
                    'members.customer_id',
                    'members.card_number',
                    'members.status',
                    'members.start_date',
                    'members.end_date',
                    'customers.first_name',
                    'customers.email',
                    'customers.type',
                    'customers.mobile',
                    'customers.address',
                    'customers.created_by',
                    'customers.modified_by',
                    'membership_package_logs.main_service_id',
                    'membership_package_logs.service_completed',
                    'membership_package_logs.package_purchase_date',
                    'membership_package_logs.package_end_date',
                    'membership_package_logs.wash_validity_start',
                    'membership_package_logs.wash_validity_end',
                    'membership_package_logs.service_remaining',
                    'main_services.service_name'
                ]);

        $query = $query->leftjoin('customers', 'customers.id', '=', 'members.customer_id');
        $query = $query->leftjoin('membership_package_logs', 'membership_package_logs.id', '=', 'members.membership_package_log_id');
        $query = $query->leftjoin('main_services', 'main_services.id', '=', 'membership_package_logs.main_service_id');
        

        if(isset($filter['card_number'])){
            $query = $query->Where('members.card_number',$filter['card_number']);
        }

        if(isset($filter['customer_id'])){
            $query = $query->Where('members.customer_id',$filter['customer_id']);
        }

        if(isset($filter['first_name'])){
            $query = $query->Where('customers.first_name',$filter['first_name']);
        }

        if(isset($filter['mobile'])){
            $query = $query->Where('customers.mobile',$filter['mobile']);
        }

        if(isset($filter['email'])){
            $query = $query->Where('customers.email',$filter['email']);
        }

        //$query = $query->Where('members.status','ACTIVE');

        $query = $query->orderBy('members.id', 'DESC');
        $plates = $query->get();

        return $plates;    
    }
}
