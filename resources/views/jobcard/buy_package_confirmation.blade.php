@extends('layouts.outer')

@section('content')

<div class="register-panel animated fadeInDown">
    <div style="width: 100%; display: inline-block;">
    <div class="log"><a style="font-size: 12px;font-weight: 600;color:grey;border:solid 1px;border-radius:5px;padding:4px " href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Log out </a></div>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
       </form>
      <div class="cl-wrp" style="float:right; margin-top: 2px; width: 350px; padding-left: 75px;">
      
        <div class="cl-wrp-pl"></div>
        
        <span id='ct' style="font-size: 12px;font-weight: 600;color:grey"></span>
        <button type="button" id="gate_disable_buttton"  data-gate_status="DISABLED" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-danger barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier Off | Please click here for <span style="background-color:white;padding:5px;color:green;"> Activate </span></marquee></button>
        <button type="button" id="gate_active_buttton" data-gate_status="ACTIVE" style="margin-left: 7px;font-weight: 600;display:none" class="btn btn-xs btn-success barrier_status"><marquee style="width: 80px;" scrollamount="2">Barrier On | Please click here for <span style="background-color:white;padding:5px;color:red"> Disable </span></marquee></button>
      </div>
    </div>
    <div class="cr4">
      <div class="jobs-content">
        <h3>Entry is Confirmed</h3>
        <div class="hcap">Check the Details Below</div>

        @if($submit_type == "buy_package")
        <div class="per-sec">
          <h4>Vehicle Details</h4>
          <div class="ps-blk">
            <div class="ps-lft">Membership ID</div>
            <div class="ps-rht">-</div>
          </div>
          <div class="ps-blk">
            <div class="ps-lft">Plate Number</div>
            <div class="ps-rht">{{  $buy_package_data['plate_number_hidden'] }}</div>
          </div>
        </div>
        <div class="per-sec">
          <h4>Personal Details</h4>
          <div class="ps-blk">
            <div class="ps-lft">Name</div>
            <div class="ps-rht">{{ $customer_data ? $customer_data->first_name : $buy_package_data['buyer_name'] }}</div>
          </div>
          <div class="ps-blk">
            <div class="ps-lft">Phone Number</div>
            <div class="ps-rht">{{  $customer_data ? $customer_data->mobile : $buy_package_data['buyer_mobile']  }}</div>
          </div>
          <div class="ps-blk">
            <div class="ps-lft">E-mail ID</div>
            <div class="ps-rht">{{  $customer_data ? $customer_data->email : $buy_package_data['buyer_email'] }}</div>
          </div>
        </div>
        @endif

        <div class="serv-sec">
          <h4>Selected Package Details</h4>
          
          @php  
          
          $total       = 0;
          $total       = $service_details->price;
          $net_total   = number_format($total / (1+ $service_details->vat / 100), 2, '.', '');
          $tax         = $total -$net_total;
          $total_tax   = number_format($tax, 2, '.', '');
          $total_value = number_format( (float)$total, 2, '.', '');
          
          @endphp

          <form class="m-t" id="confirm-form" role="form" method="post" action="/jobcard/save_package">
          @csrf

          <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
          <thead>
          <tr class="cp-hd">
                <th>Package</th>
                <th class="ctr">Price Inc.Tax</th>
                <th class="ctr">Discount</th>       
                <th class="ctr">Price Exc.Tax</th>
                <th class="ctr">Vat</th>
                <th class="ctr">Grand Total</th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td>{{ $service_details->service_name }}, {{ $service_details->car_type }} - {{ $service_details->price }} AED, {{ $service_details->duration }} Minutes</td>
                <td class="ctr"><span>AED</span> <input type="text" value="{{ $service_details->price }}" class="price form-control" name="price[{{$service_details->id}}]" readonly></td>
                <td class="ctr"><span>AED</span> <input type="text" value="0" class="discount_amount form-control" name="discount_amount[{{$service_details->id}}]"></td>               
                <td class="ctr"><span>AED</span> <input type="text" value="{{ $net_total }}" class="tax_reduced_total form-control" name="tax_reduced_total[{{$service_details->id}}]" readonly></td>
                <td class="ctr"><span>AED</span> <input type="text" value="{{ $tax }}" class="tax form-control" name="tax[{{$service_details->id}}]" readonly><input type="hidden" value="{{ $service_details->vat }}" class="vat_value form-control" name="vat_value[{{$service_details->id}}]" readonly></td>
                <td class="ctr"><span>AED</span> <input type="text" value="{{ $total }}" class="discount_reduced_amount form-control" name="discount_reduced_amount[{{$service_details->id}}]" readonly></td>
            </tr>
            <tr class="cp-hd">
                <td><b>Total</b></td>
                <td class="ctr"><b><span>AED</span> <input type="text" value="{{ $total_value }}" id="total_price" name="total_price" class="form-control" readonly> </b></td>
                <td class="ctr"><b><span>AED</span> <input type="text" value="0" id="discount_total" name="discount_total" class="form-control" readonly></b></td>
                <td class="ctr"><b><span>AED</span> <input type="text" value="{{ $net_total }}" id="net_total" name="net_total" class="form-control" readonly></b></td>
                <td class="ctr"><b><span>AED</span> <input type="text" value="{{ $total_tax }}" id="total_tax" name="total_tax" class="form-control" readonly></b></td>
                <td class="ctr"><b><span>AED</span> <input type="text" value="{{ $total_value }}" id="discount_reduced_total" name="discount_reduced_total" class="form-control" readonly></b></td>
           </tr>
          </tbody>
          </table>
          </div>  
          <div class="alert alert-warning alert-dismissible fade show" id="errorDiv" style="display:none;" role="alert">
            <strong>Warning!</strong>Discount amount not be greater than actual amount.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>
        <div class="pay-sec"> 
            <div class="ps-blk">
              <label>Payment Method? <span class="required">*</span></label>
              <ul>
                <li>Spot Payment  <input type="radio" value="SPOT_PAYMENT" id="spot_mode" name="payment_mode" checked></li>
                <li>Credit  <input type="radio" value="CREDIT" id="credit_mode" name="payment_mode"></li>
                <li>Online  <input type="radio" value="ONLINE" id="online_mode" name="payment_mode"> </li>
              </ul>
              <div id="spotpayoptions" class="spot-pay">
              <ul>
                <li>Cash Amount : <input type="text" class="spotpayCashAmount form-control" value="{{ $total }}" id="spotpayCashAmount" name="spotpayCashAmount"></li>
                <li>Card Amount : <input type="text" class="spotpayCardAmount form-control" value="0" id="spotpayCardAmount" name="spotpayCardAmount"><li>
                <li><input class="form-control" type="text" name="txn_number" id="txn_number" placeholder="Enter transaction Number if any..." > </li>
                </ul>
              </div>
              <div id="Cheque-wrp" style="width:280px"><br><input class="form-control" type="text" name="voucher_number" id="voucher_number" placeholder="Enter Voucher Number if any..." ></div><br>
            </div>
            </div>
          <div class="bt-sec-wrp"><button type="submit" id="confirmation-btn" class="btn btn78 btn-primary block m-b">Submit</button></div>
        </div>
      </form>
      <div class="clgo1"> <img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid">
      <p class="m-t"> <small> ©2020 All Rights Reserved. <a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </small> </p>
    </div>
    </div>
  </div>
  <div id="confirm" class="modal">
    <div class="modal-dialog">

      <div class="modal-content">

      <div class="modal-body">
        <h3><b>Are you sure to change the barrier status ?</b></h3>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-primary" id="change">Change</button>
        <button type="button" data-dismiss="modal" class="btn">Cancel</button>
      </div>

      </div>
    </div>
  </div>

  @endsection


  @section('script')

    <script>
      $(document).ready(function () {

      $('div#Cheque-wrp').hide();

      $("#confirmation-btn").on('click', function(e){
        e.preventDefault();
        $(this).prop('disabled', true);
        $(this).html('<i class="fa fa-spinner fa-spin"></i> Processing');
        $("#confirm-form").submit();
      });
      
      $('input#spot_mode').on('change', function(e){
          $('#spotpayoptions').show();
          $('div#Cheque-wrp').hide();
      });

      $('input#credit_mode').on('change', function(e){
        $('#spotpayoptions').hide();
        $('div#Cheque-wrp').hide();
      });

      $('input#online_mode').on('change', function(e){
        $('#spotpayoptions').hide();
        $('div#Cheque-wrp').show();
      });
      $('.discount_amount').on('keyup',function(){

        $("#errorDiv").hide();

        var total_discount  = 0;
        var total_tax       = 0;
        var discount        = Number($(this).val());
        var price           = Number($(this).closest('tr').find('.price').val());

        if(discount > price){
          $("#errorDiv").show();
          $(this).val(0);
          discount = 0;
        }

        var discount_reduced_amount = price - discount;
        var vat_value               = Number($(this).closest('tr').find('.vat_value').val());
        var net_amount              = Number(discount_reduced_amount) /(1 + (vat_value/100));
        var tax_amount              = Number(discount_reduced_amount) - net_amount;


        $(this).closest('tr').find('.discount_reduced_amount').val(discount_reduced_amount.toFixed(2));

        $(this).closest('tr').find('.tax').val(tax_amount.toFixed(2));

        $(this).closest('tr').find('.tax_reduced_total').val(net_amount.toFixed(2));

        $('.discount_amount').each(function() {
          total_discount = Number(total_discount) + Number($(this).val());
          total_tax      = Number(total_tax) + Number(tax_amount);
        });

        $("#discount_total").val(total_discount.toFixed(2));
        $("#total_tax").val(total_tax.toFixed(2));

        var discount_reduced_total = Number($("#total_price").val()) - Number($("#discount_total").val());
        $("#discount_reduced_total").val(discount_reduced_total.toFixed(2));

        var net_total = Number(discount_reduced_total) - Number(total_tax);
        $("#net_total").val(net_total.toFixed(2));

        $("#spotpayCashAmount").val(discount_reduced_total.toFixed(2));

        });

        $("#spotpayCashAmount").on('keyup',function(){
        var cash_amount = $(this).val();
        var card_amount = $("#spotpayCardAmount").val();
        var discount_reduced_total = $("#discount_reduced_total").val();
        //var net_total = $("#net_total").val();
        if(Number(cash_amount)>Number(discount_reduced_total)){
          $("#spotpayCardAmount").val(discount_reduced_total);
          $(this).val(0);
          return false;
        }
        $("#spotpayCardAmount").val(Number(discount_reduced_total)-Number(cash_amount));
        });

        $("#spotpayCardAmount").on('keyup',function(){
        var card_amount = $(this).val();
        var cash_amount = $("#spotpayCashAmount").val();
        var discount_reduced_total = $("#discount_reduced_total").val();
        //var net_total = $("#net_total").val();
        if(Number(card_amount)>Number(discount_reduced_total)){
          $("#spotpayCashAmount").val(discount_reduced_total);
          $(this).val(0);
          return false;
        }
        $("#spotpayCashAmount").val(Number(discount_reduced_total)-Number(card_amount));
        });
        
      });
    </script>

  @endsection

  

