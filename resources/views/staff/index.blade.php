@extends('layouts.outer')

@section('content')
<div class="cr-log middle-box loginscreen animated fadeInDown">
  <div>
    <p>Welcome to Express Auto Wash Dashboard</p>
    <h3> {{ Auth::user()->name }} / {{ Auth::user()->employee_id }}</h3><br><br>
    <a class="btn btn78 btn-info block full-width m-b" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form><br><br>
    <a href="{{ url('/jobcard') }}" class="btn btn78 btn-info block full-width m-b">ENTER</a>
    <div class="clgo"><img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid"></div>
    <p class="m-t"> <small> ©2020 All Rights Reserved. <a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </small> </p>
  </div>
</div>
@endsection