<!DOCTYPE html>
    <html>
        <head>
            <style>
                * { font-family: DejaVu Sans, sans-serif; }
                @page {
                    margin: 0cm 0cm;
                }
                body {
                    margin-top: 5cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    margin-bottom: 2cm;
                }
                #header {
                    position: fixed;
                    top: 0cm;
                    left: 0cm;
                    right: 0cm;
                    height: 3cm;
                    margin-top: 0cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    margin-bottom: 2cm;
                }
                #footer {
                    position: fixed;
                    bottom: 0px;
                    left: 0px;
                    right: 0px;
                    height: 1cm;
                    margin-top: 0cm;
                    margin-left: 1cm;
                    margin-right: 1cm;
                    margin-bottom: 1cm;
                    line-height: 35px;
                }
                .font12{
                    font-size: 12px;
                }
                .center{
                    text-align: center;
                }
                .bold{
                    font-weight: bold;
                }
                table th{
                    background-color:#999900;
                    color:white;
                    border-color: black;
                }
            </style>
        </head>
        <body>
            <table id="header">
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3"><img width="25%" src='{{ public_path("assets/images/eawuae_logo.png") }}' /></td>
                    <td colspan="3" align="center">
                        <h3 align = "center">
                            Express Auto Wash<br>
                            Mushrif Mall, Abu Dhabi, 02-554 4282<br>
                            Daily Report ({{ date('m/d/Y') }})
                        </h3>
                    </td>
                </tr>
            </table>
            <table id="footer">
                <tr>
                    <td colspan="6"  style="font-size: 8px;text-align:right;">Powered by Transit &trade; Security Solutions L.L.C</td>
                </tr>
            </table>
            <h3>Sales Report</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Payment Mode</th>
                        <th style="width:15%; font-size: 14px;">Sale</th>
                        <th style="width:15%; font-size: 14px;">Discount</th>
                        <th style="width:12%; font-size: 14px;">Net Sale</th>
                        <th style="width:12%; font-size: 14px;">VAT</th>
                        <th style="width:21%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($grand_total = 0)
                    @php($grand_discount = 0)
                    @php($grand_gross = 0)
                    @php($net_gross = 0)
                    @php($vat = 5)
                    @php($salesVatTax = 0)
                    @php($vatTotal = 0)
                    @php($grandGrossWithTax = 0)

                    @foreach($sales_reports as $sales_report)
                    <tr>
                        <td class="font12 center">{{$sales_report->payment_mode}}</td>
                        <td class="font12 center">{{ number_format($sales_report->total_price,2)}}</td>
                        <td class="font12 center">{{ number_format($sales_report->total_discount,2) }}</td>
                        <td class="font12 center">{{ number_format($sales_report->net_amount_total,2) }}</td>
                        <td class="font12 center">{{ number_format($sales_report->tax_total,2) }}</td>
                        <td class="font12 center">{{ number_format($sales_report->gross_price,2) }}</td>
                    </tr>
                    @php($grand_total += $sales_report->total_price)
                    @php($grand_discount += $sales_report->total_discount)
                    @php($grand_gross += $sales_report->gross_price)
                    @php($vatTotal += $sales_report->tax_total)
                    @php($net_gross += $sales_report->net_amount_total)

                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{number_format($grand_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($grand_discount,2)}}</td>
                        <td class="font12 center bold">{{number_format($net_gross,2)}}</td>
                        <td class="font12 center bold">{{number_format($vatTotal,2)}}</td>
                        <td class="font12 center bold">{{number_format($grand_gross,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="6">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Single Service</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Name</th>
                        <th style="width:5%; font-size: 14px;">Qty</th>
                        <th style="width:10%; font-size: 14px;">Price</th>
                        <th style="width:10%; font-size: 14px;">Sale</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:20%; font-size: 14px;">Net Sale</th>
                        <th style="width:10%; font-size: 14px;">VAT</th>
                        <th style="width:10%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($single_service_qty_total = 0)
                    @php($single_service_price_total = 0)
                    @php($single_service_sale_total = 0)
                    @php($single_service_discount_total = 0)
                    @php($single_service_gross_total = 0)
                    @php($single_service_vat_total = 0)
                    @php($single_service_net_total = 0)
                    @php($vat = 5)
                    @php($single_service_vat = 0)
                    @php($grossSingleService = 0)

                    @foreach($single_service_reports as $single_service_report)
                    <tr>
                        <td class="font12 center">{{$single_service_report->service_name}}</td>
                        <td class="font12 center">{{$single_service_report->quantity}}</td>
                        <td class="font12 center">{{$single_service_report->price}}</td>
                        <td class="font12 center">{{$single_service_report->total_price}}</td>
                        <td class="font12 center">{{$single_service_report->total_discount}}</td>
                        <td class="font12 center">{{ $single_service_report->net_amount_total }}</td>
                        <td class="font12 center">{{$single_service_report->tax_total}}</td>
                        <td class="font12 center">{{$single_service_report->gross_price}}</td>
                    </tr>
                    @php($single_service_qty_total += $single_service_report->quantity)
                    @php($single_service_price_total += $single_service_report->price)
                    @php($single_service_sale_total += $single_service_report->total_price)
                    @php($single_service_discount_total += $single_service_report->total_discount)
                    @php($single_service_gross_total += $single_service_report->gross_price)
                    @php($single_service_vat_total += $single_service_report->tax_total)
                    @php($single_service_net_total += $single_service_report->net_amount_total)
                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{$single_service_qty_total}}</td>
                        <td class="font12 center bold">{{number_format($single_service_price_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($single_service_sale_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($single_service_discount_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($single_service_net_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($single_service_vat_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($single_service_gross_total,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Package Purchase</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Name</th>
                        <th style="width:5%; font-size: 14px;">Qty</th>
                        <th style="width:10%; font-size: 14px;">Price</th>
                        <th style="width:10%; font-size: 14px;">Sale</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:20%; font-size: 14px;">Net Sale</th>
                        <th style="width:10%; font-size: 14px;">VAT</th>
                        <th style="width:10%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($package_qty_total = 0)
                    @php($package_price_total = 0)
                    @php($package_sale_total = 0)
                    @php($package_discount_total = 0)
                    @php($package_net_total = 0)
                    @php($package_gross_total = 0)
                    @php($vat = 5)
                    @php($package_vat_total = 0)
                    @php($package_vat = 0)

                    @foreach($package_reports as $package_report)
                    <tr>
                        <td class="font12 center">{{$package_report->service_name}}</td>
                        <td class="font12 center">{{ $package_report->quantity }}</td>
                        <td class="font12 center">{{ $package_report->price }}</td>
                        <td class="font12 center">{{ $package_report->total_price }}</td>
                        <td class="font12 center">{{ $package_report->total_discount }}</td>
                        <td class="font12 center">{{ $package_report->net_amount_total }}</td>
                        <td class="font12 center">{{ $package_report->tax_total }}</td>
                        <td class="font12 center">{{ $package_report->gross_price }}</td>
                    </tr>
                    @php($package_qty_total += $package_report->quantity)
                    @php($package_price_total += $package_report->price)
                    @php($package_sale_total += $package_report->total_price)
                    @php($package_discount_total += $package_report->total_discount)
                    @php($package_gross_total += $package_report->gross_price)
                    @php($package_vat_total += $package_report->tax_total)
                    @php($package_net_total += $package_report->net_amount_total)
                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{$package_qty_total}}</td>
                        <td class="font12 center bold">{{number_format($package_price_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($package_sale_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($package_discount_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($package_net_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($package_vat_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($package_gross_total,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Add-ons</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Name</th>
                        <th style="width:5%; font-size: 14px;">Qty</th>
                        <th style="width:10%; font-size: 14px;">Price</th>
                        <th style="width:10%; font-size: 14px;">Sale</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:20%; font-size: 14px;">Net Sale</th>
                        <th style="width:10%; font-size: 14px;">VAT</th>
                        <th style="width:10%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($addon_qty_total = 0)
                    @php($addon_price_total = 0)
                    @php($addon_sale_total = 0)
                    @php($addon_discount_total = 0)
                    @php($addon_gross_total = 0)
                    @php($addon_net_total = 0)
                    @php($vat = 5)
                    @php($addon_vat_total = 0)
                    @php($addon_vat = 0)

                    @foreach($addon_reports as $addon_report)

                    @php($addon_vat = $addon_report->gross_price * ($vat/100))
                    @php($netAddonPrice = $addon_report->gross_price - $addon_vat)
                    <tr>
                        <td class="font12 center">{{$addon_report->service_name}}</td>
                        <td class="font12 center">{{$addon_report->quantity}}</td>
                        <td class="font12 center">{{$addon_report->price}}</td>
                        <td class="font12 center">{{$addon_report->total_price}}</td>
                        <td class="font12 center">{{$addon_report->total_discount}}</td>
                        <td class="font12 center">{{ $addon_report->net_amount_total }}</td>
                        <td class="font12 center">{{$addon_report->tax_total}}</td>
                        <td class="font12 center">{{$addon_report->gross_price}}</td>
                    </tr>
                    @php($addon_qty_total += $addon_report->quantity)
                    @php($addon_price_total += $addon_report->price)
                    @php($addon_sale_total += $addon_report->total_price)
                    @php($addon_discount_total += $addon_report->total_discount)
                    @php($addon_gross_total += $addon_report->gross_price)
                    @php($addon_vat_total += $addon_report->tax_total)
                    @php($addon_net_total +=  $addon_report->net_amount_total)
                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{$addon_qty_total}}</td>
                        <td class="font12 center bold">{{number_format($addon_price_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($addon_sale_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($addon_discount_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($addon_net_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($addon_vat_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($addon_gross_total,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Car Detailing</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Name</th>
                        <th style="width:5%; font-size: 14px;">Qty</th>
                        <th style="width:10%; font-size: 14px;">Price</th>
                        <th style="width:10%; font-size: 14px;">Sale</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:20%; font-size: 14px;">Net Sale</th>
                        <th style="width:10%; font-size: 14px;">VAT</th>
                        <th style="width:10%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($car_detailing_qty_total = 0)
                    @php($car_detailing_price_total = 0)
                    @php($car_detailing_sale_total = 0)
                    @php($car_detailing_discount_total = 0)
                    @php($car_detailing_gross_total = 0)
                    @php($car_detailing_net_total = 0)
                    @php($vat = 5)
                    @php($car_detailing_vat_total = 0)
                    @php($car_detailing_vat = 0)

                    @foreach($car_detailing_reports as $car_detailing_report)

                    @php($car_detailing_vat = $car_detailing_report->gross_price * ($vat/100))
                    @php($net_car_detailing_price = $car_detailing_report->gross_price - $car_detailing_vat)
                    <tr>
                        <td class="font12 center">{{$car_detailing_report->service_name}}</td>
                        <td class="font12 center">{{$car_detailing_report->quantity}}</td>
                        <td class="font12 center">{{$car_detailing_report->price}}</td>
                        <td class="font12 center">{{$car_detailing_report->total_price}}</td>
                        <td class="font12 center">{{$car_detailing_report->total_discount}}</td>
                        <td class="font12 center">{{$car_detailing_report->net_amount_total }}</td>
                        <td class="font12 center">{{$car_detailing_report->tax_total}}</td>
                        <td class="font12 center">{{$car_detailing_report->gross_price}}</td>
                    </tr>
                    @php($car_detailing_qty_total += $car_detailing_report->quantity)
                    @php($car_detailing_price_total += $car_detailing_report->price)
                    @php($car_detailing_sale_total += $car_detailing_report->total_price)
                    @php($car_detailing_discount_total += $car_detailing_report->total_discount)
                    @php($car_detailing_gross_total += $car_detailing_report->gross_price)
                    @php($car_detailing_vat_total += $car_detailing_report->tax_total)
                    @php($car_detailing_net_total +=  $car_detailing_report->net_amount_total)
                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{$car_detailing_qty_total}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_price_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_sale_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_discount_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_net_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_vat_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($car_detailing_gross_total,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Card Renewal</h3>
            <table  border="1px;" width="100%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Name</th>
                        <th style="width:5%; font-size: 14px;">Qty</th>
                        <th style="width:10%; font-size: 14px;">Price</th>
                        <th style="width:10%; font-size: 14px;">Sale</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:20%; font-size: 14px;">Net Sale</th>
                        <th style="width:10%; font-size: 14px;">VAT</th>
                        <th style="width:10%; font-size: 14px;">Net Revenue</th>
                    </tr>
                </thead>
                <tbody>
                    @php($card_renew_qty_total = 0)
                    @php($card_renew_price_total = 0)
                    @php($card_renew_sale_total = 0)
                    @php($card_renew_discount_total = 0)
                    @php($card_renew_gross_total = 0)
                    @php($card_renew_net_total = 0)
                    @php($vat = 5)
                    @php($card_renew_vat_total = 0)
                    @php($card_renew_vat = 0)

                    @foreach($card_renew_reports as $card_renew_report)

                    @php($card_renew_vat = $card_renew_report->gross_price * ($vat/100))
                    @php($net_card_renew_price = $card_renew_report->gross_price - $card_renew_vat)
                    <tr>
                        <td class="font12 center">{{$card_renew_report->service_name}}</td>
                        <td class="font12 center">{{$card_renew_report->quantity}}</td>
                        <td class="font12 center">{{$card_renew_report->price}}</td>
                        <td class="font12 center">{{$card_renew_report->total_price}}</td>
                        <td class="font12 center">{{$card_renew_report->total_discount}}</td>
                        <td class="font12 center">{{$card_renew_report->net_amount_total }}</td>
                        <td class="font12 center">{{$card_renew_report->tax_total}}</td>
                        <td class="font12 center">{{$card_renew_report->gross_price}}</td>
                    </tr>
                    @php($card_renew_qty_total += $card_renew_report->quantity)
                    @php($card_renew_price_total += $card_renew_report->price)
                    @php($card_renew_sale_total += $card_renew_report->total_price)
                    @php($card_renew_discount_total += $card_renew_report->total_discount)
                    @php($card_renew_gross_total += $card_renew_report->gross_price)
                    @php($card_renew_vat_total += $card_renew_report->tax_total)
                    @php($card_renew_net_total +=  $card_renew_report->net_amount_total)
                    @endforeach
                    <tr>
                        <td class="font12 center bold">Total</td>
                        <td class="font12 center bold">{{$card_renew_qty_total}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_price_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_sale_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_discount_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_net_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_vat_total,2)}}</td>
                        <td class="font12 center bold">{{number_format($card_renew_gross_total,2)}}</td>
                    </tr>
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>

            <h3>Car Wash Details</h3>
            <table  border="1px;" width="86%" style ="border-style: groove; border-collapse: collapse; ">
                <thead>
                    <tr>
                        <th style="width:25%; font-size: 14px;">Customer</th>
                        <th style="width:5%; font-size: 14px;">Plate No.</th>
                        <th style="width:10%; font-size: 14px;">Service Name</th>
                        <th style="width:10%; font-size: 14px;">Service Type</th>
                        <th style="width:10%; font-size: 14px;">Payment Type</th>
                        <th style="width:20%; font-size: 14px;">Amount</th>
                        <th style="width:10%; font-size: 14px;">Discount</th>
                        <th style="width:10%; font-size: 14px;">Net Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($carwash_details as $carwash_detail)
                    <tr>
                        <td class="font12 center">{{$carwash_detail->first_name}} {{$carwash_detail->last_name}}</td>
                        <td class="font12 center">{{$carwash_detail->plate_number}}</td>
                        <td class="font12 center">{{$carwash_detail->service_name}}</td>
                        <td class="font12 center">{{$carwash_detail->service_category}}</td>
                        @if($carwash_detail->payment_type == "SPOT_PAYMENT")
                            <td class="font12 center">CARD/CASH</td>
                        @else
                            <td class="font12 center">{{$carwash_detail->payment_type}}</td>
                        @endif
                        <td class="font12 center">{{$carwash_detail->price}}</td>
                        <td class="font12 center">{{$carwash_detail->discount}}</td>
                        <td class="font12 center">{{$carwash_detail->gross_price}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td class="center" colspan="8">*****</td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>