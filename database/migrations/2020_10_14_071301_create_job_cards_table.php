<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_cards', function (Blueprint $table) {
            $table->id();
            $table->string('job_card_number')->nullable();
            $table->enum('job_card_type', ['MEMBER', 'SINGLE','FREE']);
            $table->decimal('total_price', 10, 2)->default(0);
            $table->decimal('total_discount', 10, 2)->default(0);
            $table->decimal('gross_price', 10, 2)->default(0);
            $table->decimal('total_tax', 10, 2)->default(0);
            $table->decimal('net_amount_total', 10, 2)->default(0);
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->enum('customer_type', ['MEMBER', 'NON_MEMBER'])->nullable();
            $table->enum('car_type', ['SUV', 'SEDAN'])->nullable();
            $table->string('plate_number')->nullable();
            $table->integer('earned_points')->default(0);
            $table->enum('payment_type', ['SPOT_PAYMENT', 'CREDIT', 'ONLINE'])->nullable();
            $table->string('txn_number')->nullable();
            $table->enum('inv_status', ['PENDING', 'CREATED','FAILED'])->default('PENDING');
            $table->string('inv_ref')->nullable();
            $table->text('qb_request')->nullable();
            $table->text('qb_response')->nullable();
            $table->text('inv_data')->nullable();
            $table->string('inv_pdf_name')->nullable();
            $table->unsignedBigInteger('created_user_id');
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_cards');
    }
}
