<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CustomerOrdersStaging;
use App\Models\Settings;
use DB;

class Orders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:daily';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetching new orders from EAW database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(Settings::getSettingValueByType('wp_sync') == 0){

            $max_staging_id = CustomerOrdersStaging::max('wp_order_id');
            $sql            = "";
            if($max_staging_id != NULL){
                $sql = " AND `ID`>'$max_staging_id'";
            }
            $orders = DB::connection('mysql2')->select("SELECT `ID` FROM `wp_posts` WHERE `post_type`='shop_order' AND `post_status`='wc-completed'".$sql);
            if($orders != false){
                Settings::where('type', 'wp_sync')->update(['value' => 1]);
                foreach($orders as $order){
                    $wp_order_id = $order->ID;
                    $wp_cust_data       = DB::connection('mysql2')->select("SELECT `meta_value` FROM `wp_postmeta` WHERE `post_id`='$wp_order_id' AND `meta_key`='_customer_user'");
                    $wp_cust_id         = $wp_cust_data[0]->meta_value;
                    $customer_details   = DB::connection('mysql2')->select("SELECT `ID`,`user_login`,`user_email` FROM `wp_users` WHERE `ID`='$wp_cust_id'");
                    $customer_phone     = DB::connection('mysql2')->select("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$wp_cust_id' AND `meta_key`='billing_phone'");
                    $wp_cust_phone      = $customer_phone[0]->meta_value;
                    $order_packages     = DB::connection('mysql2')->select("SELECT `order_item_id` FROM `wp_woocommerce_order_items` WHERE `order_id`='$wp_order_id'");
                        if($order_packages != false){
                            foreach($order_packages as $order_package){
                                $wp_package        = DB::connection('mysql2')->select("SELECT `meta_value` FROM `wp_woocommerce_order_itemmeta` WHERE `order_item_id`='$order_package->order_item_id' AND `meta_key`='_product_id'");
                                if($wp_package != false){
                                    CustomerOrdersStaging::create([
                                        'wp_order_id'   =>  $wp_order_id, 
                                        'wp_cust_id'    =>  $customer_details[0]->ID, 
                                        'wp_cust_name'  =>  $customer_details[0]->user_login, 
                                        'wp_cust_email' =>  $customer_details[0]->user_email,
                                        'wp_cust_phone' =>  $wp_cust_phone, 
                                        'wp_package_id' =>  $wp_package[0]->meta_value,  
                                        'created_at'    =>  date('Y-m-d H:i:s'),
                                        'updated_at'    =>  date('Y-m-d H:i:s')
                                    ]);
                                }
                            }
                        }
                }
                Settings::where('type', 'wp_sync')->update(['value' => 0]);
                $this->info('Success');
            }else{
                $this->info('No new orders');
            }

        }
    }
}
