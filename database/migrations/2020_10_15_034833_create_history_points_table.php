<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_points', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['CREDIT', 'DEBIT']);
            $table->unsignedBigInteger('registered_plate_id');
            $table->foreign('registered_plate_id')->references('id')->on('registered_plates')->onDelete('cascade');
            $table->unsignedBigInteger('job_card_id');
            $table->foreign('job_card_id')->references('id')->on('job_cards')->onDelete('cascade');
            $table->integer('recieved_points');
            $table->integer('remaining_points');
            $table->enum('recieve_type', ['SINGLE', 'REFER']);
            $table->unsignedBigInteger('refered_by')->nullable();
            $table->foreign('refered_by')->references('id')->on('registered_plates')->onDelete('cascade');
            $table->unsignedBigInteger('created_user_id')->nullable();
            $table->foreign('created_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('narration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_points');
    }
}
