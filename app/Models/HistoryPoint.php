<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryPoint extends Model
{
    use HasFactory;

    protected $table = 'history_points';

    protected $fillable = [
        'type', 'registered_plate_id','job_card_id','recieved_points','remaining_points','recieve_type','refered_by','created_user_id','narration','created_at','updated_at'
    ];

    public static function getPlatePointHistory($plate_number){

        $query = HistoryPoint::select([
        'history_points.id',
        'history_points.type',
        'history_points.recieved_points',
        'history_points.remaining_points',
        'history_points.recieve_type',
        'history_points.narration',
        'history_points.created_at',
        'history_points.updated_at',
        'registered_plates.plate_number',
        'customers.mobile',
        'customers.email'
        ]);
        $query = $query->leftjoin('registered_plates', 'registered_plates.id', '=', 'history_points.registered_plate_id');
        $query = $query->leftjoin('customers', 'customers.id', '=', 'registered_plates.customer_id');
        
        if($plate_number){
             $query = $query->Where('registered_plates.plate_number',$plate_number);
        }
        $histories = $query->get();
        return $histories;

    }
}