 
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Express Auto Wash</title>
    <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}">
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-datetimepicker.min.css') }}">
    <!-- Morris -->
    <link href="{{ asset('assets/css/plugins/morris/morris-0.4.3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/eaw-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    </head>
    <body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element"> <img alt="image" class="rounded-circle pr-pic-sm" src="{{ asset('assets/images/avatars/no-image-icon.png') }}"/> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <span class="block m-t-xs font-bold">{{ Auth::user()->name }} <b class="caret"></b></span> <span class="text-muted text-xs block">{{ Auth::user()->role }}</span> <span class="text-muted text-xs block">{{ Auth::user()->employee_id }}</span> </a>
            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                <li><a class="dropdown-item" href="/profile">Profile</a></li>
                <li><a class="dropdown-item" href="/change_password">Change Password</a></li>
                <li class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form></li>
            </ul>
            </div>
            <div class="logo-element"> EAW+ </div>
        </li>
        <li> 
            <a href="/home"><i class="fa fa-home" aria-hidden="true"></i> <span class="nav-label">Home</span></a> 
        </li>
        <li> 
            <a href="/customers"><i class="fa fa-handshake-o" aria-hidden="true"></i> <span class="nav-label">Customers</span></a> 
        </li>
        <li> 
            <a href="/users"><i class="fa fa-users" aria-hidden="true"></i> <span class="nav-label">Users</span></a> 
        </li>
        <li> 
            <a href="/main_services"><i class="fa fa-bandcamp" aria-hidden="true"></i> <span class="nav-label">Main Services</span></a> 
        </li>
        <li> 
            <a href="/membership_packages"><i class="fa fa-credit-card" aria-hidden="true"></i> <span class="nav-label">Membership Packages</span></a> 
        </li>
        <li> 
            <a href="/import_export_cards"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span class="nav-label">Export / Import Cards</span></a> 
        </li>
        <li> 
            <a href="/settings"><i class="fa fa-cog" aria-hidden="true"></i> <span class="nav-label">Settings</span></a> 
        </li>
        </ul>
    </div>
    <div class="lft-eaw animated fadeInLeft"></div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header"> <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> </div>
        <div class="in-logo"><img src="{{ asset('assets/images/eawuae_logo.png') }}" class="img-fluid"></div>
        <div class="cl-wrp">
        <!-- <div class="cl-wrp-p0"><i class="fa fa-clock-o" aria-hidden="true"></i></div> -->
        <div class="cl-wrp-pl"></div>
        <span id='ct' style="font-size: 14px;font-weight: 600;color:grey;margin-bottom:2px"></span>
        <!-- <iframe scrolling="no" frameborder="no" clocktype="html5" style="overflow:hidden;border:0;margin:0;padding:0;width:240px;height:25px;"src="https://www.clocklink.com/html5embed.php?clock=018&timezone=UnitedArabEmirates_AbuDhabi&color=gray&size=240&TimeFormat=hh:mm:ss TT&Color=gray"></iframe> -->
        </div>
        <ul class="nav navbar-top-links navbar-right">
        <li> <span class="m-r-sm text-muted welcome-message">Welcome to Express Auto Wash Dashboard.</span> </li>
        <li><a href="#"><i class="fa fa-bell"></i>&nbsp;Help</a> </li>
        <li> <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                       <i class="fa fa-power-off"></i> {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form> </li>
        </ul>
    </nav>
    </div>
    @yield('content')
    <div class="footer">
    <div> &copy;2020 Express Auto Wash, All Rights Reserved. Powered by: &nbsp;<a target="_blank" href="http://www.abacitechs.com/">Abaci Technologies</a> </div>
    </div>
    </div>
    </div>
    <!-- Mainly scripts --> 
    <script src="{{ asset('assets/js/jquery-3.1.1.min.js') }}"></script> 
    <script src="{{ asset('assets/js/popper.min.js') }}"></script> 
    <script src="{{ asset('assets/js/bootstrap.js') }}"></script> 
    <script src="{{ asset('assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script> 
    <script src="{{ asset('assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script> 


    <!-- Custom and plugin javascript --> 
    <script src="{{ asset('assets/js/inspinia.js') }}"></script> 
    <script src="{{ asset('assets/js/plugins/pace/pace.min.js') }}"></script> 
    <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}"></script>

    <!-- jQuery UI --> 
    <script src="{{ asset('assets/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script> 
    <!-- Data picker --> 
    <script src="{{ asset('assets/js/plugins/dataTables/datatables.min.js') }}"></script> 
    <script src="{{ asset('assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script> 


    <script src="{{ asset('assets/js/polyfill.min.js') }}"></script>
    <script src="{{ asset('assets/js/classList.min.js') }}"></script>
    <script src="{{ asset('assets/js/findindex_polyfill_mdn.js') }}"></script>
    

    <script src="{{ asset('assets/js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap-datetimepicker.min.js') }}"></script> 

    <script>

            function formatDate(d) {
                minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
                hours   = d.getHours();
                hours   = (hours % 12) || 12;
                hours   = hours.toString().length == 1 ? '0'+hours : hours;
                seconds = d.getSeconds().toString().length == 1 ? '0'+d.getSeconds() : d.getSeconds();
                ampm    = d.getHours() >= 12 ? 'PM' : 'AM';

                months  = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
                days    = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
                
                return '<i style="font-size:22px;" class="fa fa-clock-o" aria-hidden="true"></i>  '+ hours + ':' + minutes + ':' + seconds + ' ' + ampm;
            }


            function display_c(){
                var refresh=1000; // Refresh rate in milli seconds
                mytime=setTimeout('display_ct()',refresh)
            }

            function display_ct() {
                var x = new Date();
                var e = formatDate(x);
                document.getElementById('ct').innerHTML = e;
                display_c();
            }

            $(document).ready(function() {

                display_ct();

                $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                });
            
                $('.dataTables-example').DataTable({
                    pageLength: 10,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        { extend: 'copy'},
                        {extend: 'csv'},
                        {extend: 'excel', title: 'ExampleFile'},
                        {extend: 'pdf', title: 'ExampleFile'},

                        {extend: 'print',
                        customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                        }
                    ]
                });
            });
            
                $("#fold").click(function () {
            $("#fold_p").fadeOut(function () {
                $("#fold_p").text(($("#fold_p").text() == 'Hide Filter') ? 'Show Filter' : 'Hide Filter').fadeIn();
            })
        })
            
                            $(function () {               
                    $('#datetimepicker').datetimepicker({
                    format: 'L'			
                    });
                    $('#datetimepicker3').datetimepicker({
                    format: 'L'			
                    }); 
                    $('#datetimepicker4').datetimepicker({
                    format: 'L'			
                    }); 	 				  
                    $('#datetimepicker1').datetimepicker({
                    format: 'LT'				
                    });
                    $('#datetimepicker2').datetimepicker({
                    format: 'LT'				
                    });  
            

    });

        moment.locale('en', {
        week: { dow: 1 } // Monday is the first day of the week
        });

    //Initialize the datePicker(I have taken format as mm-dd-yyyy, you can     //have your owh)
    $("#weeklyDatePicker").click(function() {
        $(this).datetimepicker({format: 'MM/DD/YYYY'}).datetimepicker( "show" );
    });

    //Get the value of Start and End of Week
    $('#weeklyDatePicker').on('dp.change', function (e) {
        var value = $("#weeklyDatePicker").val();
        var firstDate = moment(value, "MM/DD/YYYY").day(1).format("MM/DD/YYYY");
        var lastDate =  moment(value, "MM/DD/YYYY").day(7).format("MM/DD/YYYY");
        $("#weeklyDatePicker").val(firstDate + " - " + lastDate);
    });
    
    $('#MonthDatePicker').datetimepicker({
        viewMode : 'months',
        format : 'MM/YYYY',
        toolbarPlacement: "top",
        allowInputToggle: true,
        icons: {
            time: 'fa fa-time',
            date: 'fa fa-calendar',
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    $("#MonthDatePicker").on("dp.show", function(e) {
    $(e.target).data("DateTimePicker").viewMode("months"); 
    });
    
    $('a#view_repeated_customers').click(function(){
        $('#repeatedCustomersModal').modal("show");
    });   
    </script>

      @yield('script')
      
    </body></html>