@extends('layouts.inner')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
      <div class="col-lg-3">
        <h2>Change Password</h2>
      </div>
      <div class="col-lg-9"> </div>
    </div>
    <div class="wrapper wrapper-content  animated fadeInRight">
      <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
        @if(session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>Success!</strong> {{session('success')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            @if(session('error'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Failed!</strong> {{session('error')}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
          <div class="ibox">
            <div class="ibox-title">
              <h3>Update Password</h3>
              <div class="ibox-tools"> <a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a> </div>
              <div class="clearfix"></div>
            </div>
            <div class="ibox-content"> <br>
            <form action="/change_password" enctype="multipart/form-data" id="form" method="post" class="form-horizontal form-label-left">
              @csrf
              <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Current Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" name="current_password" id="current_password" autocomplete="off" placeholder="Enter Current Password" value="" class="form-control {{ $errors->has('current_password') ? 'is-invalid' : ''}}" autofocus>
                      @if($errors->has('current_password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('current_password') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">New Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="password" name="new_password" id="new_password" placeholder="Enter New Password" value="" class="form-control {{ $errors->has('new_password') ? 'is-invalid' : ''}}">
                      @if($errors->has('new_password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('new_password') }}</strong>
                        </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="fm-ctr form-group col-md-12 col-sm-12 col-xs-12">
                  <div class="row">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Confirm New Password</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm New Password" value="" class="form-control {{ $errors->has('confirm_password') ? 'is-invalid' : ''}}">   
                    @if($errors->has('confirm_password'))
                        <span class="text-danger" role="alert">
                            <strong>{{ $errors->first('confirm_password') }}</strong>
                        </span>
                      @endif            
                    </div>
                  </div>
                </div>
                <div class="form-group text-center">
                  <div class="fm-ctr col-md-12 col-sm-12 col-xs-12">
                    <button class="btn btn-w-m btn-info" type="reset">Reset</button>
                    <button type="submit" class="btn btn-w-m btn-primary has-spinner" id="saveUser">Update Password</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
